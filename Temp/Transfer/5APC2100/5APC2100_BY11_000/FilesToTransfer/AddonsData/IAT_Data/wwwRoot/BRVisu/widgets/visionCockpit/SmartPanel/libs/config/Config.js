/*global define */
define([
], function () {

    'use strict';

    /**
     * @class widgets.visionCockpit.SmartPanel.config.Config 
     * @extends core.javascript.Object
     * @override widgets.visionCockpit.SmartPanel
     */

    /**
     * @cfg {String} visionComponentReference=''
     * @iatStudioExposed
     * @bindable
     * @iatCategory Data   
     * Reference to the visionComponent
     */

    /**
     * @cfg {NumberArray1D} loadVisionApplicationStatus=0 
     * @not_projectable
     * @bindable
     * @iatCategory Data   
     * For dynamic binding to loadVisionApplicationStatus of VRM
     */

    /**
     * @cfg {Integer} saveVisionApplicationStatus=0
     * @not_projectable
     * @bindable
     * @iatCategory Data   
     * For dynamic binding to saveVisionApplicationStatus of VRM
     */

    /**
     * @cfg {Integer} imageProcessingError=0
     * @not_projectable
     * @bindable
     * @iatCategory Data   
     * For dynamic binding to imageProcessingError of VRM
     */

    /**
     * @cfg {Integer} imageAcquisitionSettingsUpdated=0
     * @not_projectable
     * @bindable
     * @iatCategory Data   
     * For dynamic binding to imageAcquisitionSettingsUpdated of VRM
     */

    /**
     * @cfg {Integer} numResults=1   
     * @bindable
     * @iatStudioExposed 
     * @iatCategory Data   
     * For num results
     */

    /**
     * @cfg {Integer} loggerCounterOfInformations=0    
     * @bindable
     * @iatStudioExposed 
     * @iatCategory Data   
     * Logger counter of informations
     */

    /**
     * @cfg {Integer} loggerCounterOfSuccesses=0    
     * @bindable
     * @iatStudioExposed 
     * @iatCategory Data   
     * Logger counter of successes
     */

    /**
     * @cfg {Integer} loggerCounterOfWarnings=0    
     * @bindable
     * @iatStudioExposed 
     * @iatCategory Data   
     * Logger counter of warnings
     */

    /**
     * @cfg {Integer} loggerCounterOfErrors=0    
     * @bindable
     * @iatStudioExposed 
     * @iatCategory Data   
     * Logger counter of errors
     */

    /**
     * @cfg {StringArray1D} loggerArrayDescription
     * @bindable
     * @iatStudioExposed 
     * @iatCategory Data   
     * One-dimensional Array containing data of type string for the description from the logger 
     */

    /**
     * @cfg {StringArray1D} loggerArrayTime
     * @bindable
     * @iatStudioExposed 
     * @iatCategory Data   
     * One-dimensional Array containing data of type string for the time from the logger 
     */

    /**
     * @cfg {StringArray1D} loggerArrayId
     * @bindable
     * @iatStudioExposed 
     * @iatCategory Data   
     * One-dimensional Array containing data of type string for the id from the logger 
     */

    /**
     * @cfg {NumberArray1D} loggerArraySeverity 
     * @bindable
     * @iatStudioExposed 
     * @iatCategory Data   
     * One-dimensional Array containing data of type Number for the severity
     */

    /**
     * @cfg {String} visionFunctionVariablesRefId (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Widget on same Content that handles the variables of the Vision Function
     */

    /**
     * @cfg {String} visionFunctionConstantsRefId (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Widget on same Content that handles the constants of the Vision Function
     */

    /**
     * @cfg {String} refIdGroupBoxGenericVisionFuntion='' 
     * @iatStudioExposed
     * @iatCategory Data
     * Name of the groupBox for the generic Vision Funtion 
     */

    /**
     * @cfg {String} refIdGroupBoxVisionParameters='' (required)  
     * @iatStudioExposed
     * @iatCategory Data
     * Name of the groupBox for the vision parameters  
     */

    /**
     * @cfg {String} refIdGroupBoxImageAcquisition='' 
     * @iatStudioExposed
     * @iatCategory Data
     * Name of the groupBox for the image acquisition  
     */


    /**
     * @cfg {String} loggerImagePathError='Media/mappVision/loggerSeverity/gray/icon_error.svg' 
     * @bindable
     * @iatStudioExposed 
     * @iatCategory Data   
     * Path to image file of logger error icon
     */

    /**
     * @cfg {String} loggerImagePathWarning='Media/mappVision/ImageList/gray/Grau/icon_warning.svg' 
     * @bindable
     * @iatStudioExposed 
     * @iatCategory Data   
     * Path to image file of logger warning icon
     */

    /**
     * @cfg {String} loggerImagePathInfo='Media/mappVision/ImageList/gray/Grau/icon_info.svg' 
     * @bindable
     * @iatStudioExposed 
     * @iatCategory Data   
     * Path to image file of logger info icon
     */

    /**
     * @cfg {String} loggerImagePathSuccess='Media/mappVision/loggerSeverity/gray/icon_success.svg' 
     * @bindable
     * @iatStudioExposed 
     * @iatCategory Data    
     * Path to image file of logger info success
     */

    /**
     * @cfg {String} loggerNumOutWarningStyle='sloggerGrayrNumericOutput' 
     * @bindable
     * @iatStudioExposed 
     * @iatCategory Data   
     * Style of the numeric output warning
     */

    /**
     * @cfg {String} loggerNumOutErrorStyle='sloggerGrayrNumericOutput' 
     * @bindable
     * @iatStudioExposed 
     * @iatCategory Data   
     * Style of the numeric output error
     */

    /**
     * @cfg {String} loggerNumOutInfoStyle='sloggerGrayrNumericOutput' 
     * @bindable
     * @iatStudioExposed 
     * @iatCategory Data   
     * Style of the numeric output info
     */

    /**
     * @cfg {String} loggerNumOutSuccessStyle='sloggerGrayrNumericOutput' 
     * @bindable
     * @iatStudioExposed 
     * @iatCategory Data   
     * Style of the numeric output success
     */

    /**
     * @cfg {String} refIdButtonShowAllResults (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of ShowAllResults-Button 
     */

    /**
     * @cfg {String} refIdButtonHideAllResults (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of HideAllResults-Button 
     */

    /**
     * @cfg {String} refIdButtonTeach (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Teach-Button 
     */

    /**
     * @cfg {String} refIdButtonDelete (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Submit-Delete 
     */

    /**
     * @cfg {String} lightAndFocusRefId (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Widget on same Content that handles the parameters of light and focus
     */

    /**
     * @cfg {String} extendedParametersRefId (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Widget on same Content that handles the extended parameters
     */

    /**
     * @cfg {String} visionFunctionParametersRefId (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Widget on same Content that handles the parameters of the Vision Function
     */

    /**
     * @cfg {String} visionFunctionGlobalModel (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Widget on same Content that handles the parameters of the GlobalModel
     */

    /**
     * @cfg {String} visionFunctionModelParameterRefId (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Widget on same Content that handles the model parameter of the model
     */

    /**
     * @cfg {String} visionImageAcquisitionSettingsRefId (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Widget on same Content that handles the image acquisition settings
     */

    /**
     * @cfg {String} visionNormalImageParametersRefId (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Widget on same Content that handles the normal image parameters
     */

    /**
     * @cfg {String} visionLineSensorSettingsRefId (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Widget on same Content that handles the image acquisition settings
     */

    /**
     * @cfg {String} visionFunctionModelListRefId (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Widget on same Content that handles the variables of the Vision Function
     */

    /**
     * @cfg {String} visionFunctionGlobalModelListRefId (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Widget on same Content that handles the variables of the Vision Function
     */

    /**
     * @cfg {String} imageAcquisitionVariablesRefId=''
     * @iatStudioExposed
     * @iatCategory Data
     * ID for the parameter form with the PVs for image acquisition
     */

    /**
     * @cfg {String} visionFunctionsRootPath=''
     * @bindable
     * @iatStudioExposed
     * @iatCategory Data
     * Base path for all vision functions, offline mode only
     */

    /**
     * @cfg {String} visionFunctionsRootDevice=''
     * File Device name to base path for all vision functions
     */

    /**
     * @cfg {String} visionFunctionSubPath=''
     * @bindable
     * @iatStudioExposed
     * @iatCategory Data
     * sub directory in visionFunctionsRootPath for vision function (this is not necessarily equal to visionFunctionName)
     */

    /**
     * @cfg {String} visionFunctionName=''
     * @bindable
     * @readonly
     * @iatStudioExposed
     * @iatCategory Data
     * Name of currently selected vision function (as defined in config.xml)
     */

    /**
     * @cfg {String} visionAplicationNavigation=''
     * @bindable
     * @readonly
     * @iatStudioExposed
     * @iatCategory Data
     * String for the vision aplication navigation
     */

    /**
     * @cfg {String} selectedVisionFunction=''
     * @bindable
     * @readonly
     * @iatStudioExposed
     * @iatCategory Data
     * Name of currently selected vision function
     */

    /**
     * @cfg {String} selectedImage=''
     * @bindable
     * @iatStudioExposed
     * @iatCategory Data
     * Name of currently selected Image
     */

    /**
     * @cfg {Boolean} isSelectedImageAcquisition=false
     * @bindable
     * @iatStudioExposed
     * @readonly
     * @iatCategory Data
     * Flag to indicate if image acquisition is selected
     */

    /**
     * @cfg {Boolean} globalModelVisible=false
     * @bindable
     * @iatStudioExposed
     * @readonly
     * @iatCategory Data
     * Flag to indicate if global model is visible
     */

    /**
     * @cfg {Boolean} executionRoiVisible=false
     * @bindable
     * @iatStudioExposed
     * @readonly
     * @iatCategory Data
     * Flag to indicate if execution roi is visible 
     */

    /**
     * @cfg {Boolean} statusReady=false 
     * @bindable
     * @iatStudioExposed
     * @readonly
     * @iatCategory Data
     * status ready indicates that the widget is in state to accept a further command
     */

    /**
     * @cfg {Boolean} activatedLinesensor=false
     * @bindable
     * @iatStudioExposed
     * @readonly
     * @iatCategory Data
     * activatedLinesensor indicates that the the line sensor aktiv oder aktiv ist
     */

    /**
     * @cfg {String} dataProviderModelTypes=''
     * @bindable
     * @readonly
     * @iatStudioExposed
     * @iatCategory Data
     * String for the dropDownBow with the information about the moel types
     */

    /**
     * @cfg {String} selectedModelType=''
     * @bindable
     * @readonly
     * @iatStudioExposed
     * @iatCategory Data
     * Name of currently selected model type
     */

    /**
     * @cfg {Boolean} editModelsTabVisible=true
     * @bindable
     * @iatStudioExposed
     * @readonly
     * @iatCategory Data
     * Flag to indicate if edit models tab is visible 
     */


    /**
     * @cfg {Boolean} showOnlyOneTabForAVisionFuntionTab=false
     * @bindable
     * @iatStudioExposed
     * @readonly
     * @iatCategory Data
     * Flag to indicate if only one tab for a vision funtion is visible 
     */

    /**
     * @cfg {String} lineSensorNormalImageModeButtonRefId (required) 
     * @iatStudioExposed
     * @iatCategory Data
     * Name of the button for the normal image mode
     */

    /**
     * @cfg {String} refIdProcessVariablesFilterIndex (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of the numericInput for the Filter 
     */

    /**
     * @cfg {String} refIdResultFilterButtonInc (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Widget on same Content that handles the extended parameters
     */

    /**
     * @cfg {String} refIdResultFilterButtonDec (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Widget on same Content that handles the extended parameters
     */

    /**
     * @cfg {String} refIdLabelFilterInformation (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Widget on same Content that handles the extended parameters
     */

    /**
     * @cfg {String} refIdNumericInputFilter (required) 
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Widget on same Content that handles the extended parameters
     */

    /**
     * @cfg {String} textOutputConnectionIsLostRefId (required) 
     * @iatStudioExposed
     * @iatCategory Data
     * Name of Widget on same Content that handles the extended parameters
     */

    /**
     * @cfg {String} refIdButtonRepetitiveMode (required)
     * @iatStudioExposed
     * @iatCategory Data
     * Name of repetitive mode button 
     */

    return {
        imageAcquisitionName: 'Image Acquisition',
        ipAddress: '',
        statusReady: false,
        counterSocketErrors: 0,
        iconicsFilterIndex: 1,
        loggerArrayDescription: [''],
        loggerArrayTime: [''],
        loggerArrayId: [''],
        loggerArraySeverity: [1],
        visionComponentReference: '',
        loadVisionApplicationStatus: [0, 0],
        saveVisionApplicationStatus: '',
        imageProcessingError: '',
        imageAcquisitionSettingsUpdated: '',
        wsStartupSequenceStarted: false,
        visionFunctionVariablesRefId: '',
        textOutputConnectionIsLostRefId: '',
        refIdButtonEditTool: 'tglBtnROI',
        refIdDropDownBoxAddMeasurement: 'dropDownBoxAddMeasurement',  
        refIdButtonTeach: '',
        refIdButtonDelete: '',
        refIdGroupBoxGenericVisionFuntion: '',
        refIdGroupBoxImageAcquisition: '',
        refIdGroupBoxVisionParameters: '',
        refIdNumericInputFilter: '',
        refIdProcessVariablesFilterIndex: '',
        refIdResultFilterNumericInput: '',
        refIdResultFilterButtonInc: '',
        refIdResultFilterButtonDec: '',
        refIdButtonShowAllResults: '',
        refIdButtonHideAllResults: '',
        refIdResultFilterLabel: '',
        refIdLabelFilterInformation: ' ',
        refIdRepetitiveMode: '',
        repetitiveModeSkipParameterUpdate: false,
        visionFunctionConstantsRefId: '',
        visionFunctionParametersRefId: '',
        lightAndFocusRefId: '',
        extendedParametersRefId: '',
        visionFunctionInstance: 1,
        visionFunctionGlobalModel: '',
        visionFunctionModelParameterRefId: '',
        lineSensorNormalImageModeButtonRefId: '',
        visionFunctionModelTypesRefId: '',
        visionFunctionGlobalModelListRefId: '',
        visionFunctionModelListRefId: '',
        visionImageAcquisitionSettingsRefId: '',
        visionLineSensorSettingsRefId: '',
        visionNormalImageParametersRefId: '',
        imageAcquisitionVariablesRefId: '',
        visionFunctionsRootPath: '',
        loggerImagePathError: 'Media/mappVision/loggerSeverity/gray/icon_error.svg',
        loggerImagePathWarning: 'Media/mappVision/loggerSeverity/gray/icon_warning.svg',
        loggerImagePathInfo: 'Media/mappVision/loggerSeverity/gray/icon_info.svg',
        loggerImagePathSuccess: 'Media/mappVision/loggerSeverity/gray/icon_success.svg',
        loggerNumOutWarningtyle: 'sloggerGrayrNumericOutput',
        loggerNumOutErrorStyle: 'sloggerGrayrNumericOutput',
        loggerNumOutInfoStyle: 'sloggerGrayrNumericOutput',
        loggerNumOutSuccessStyle: 'sloggerGrayrNumericOutput',
        visionFunctionsRootDevice: 'VFDKRootDevice',
        visionFunctionSubPath: '',
        visionFunctionName: '',
        visionFunctionType: '',
        visionProgramName: '',
        selectedVisionFunction: 'Image Acquisition',
        visionAplicationNavigation: [{
            'value': 'Image Acquisition',
            'text': 'Image Acquisition'
        }],
        numResults: 1,
        loggerCounterOfInformations: 0,
        loggerCounterOfSuccesses: 0,
        loggerCounterOfWarnings: 0,
        loggerCounterOfErrors: 0,
        selectedImage: '',
        xoffset: 40,
        yoffset: 40,
        offlineMode: undefined,
        hmiModeActiveCounter: 0,
        selectedModelId: undefined,
        selectedGlobalModelId: undefined,
        lastCenterPosition: undefined,
        modelClouds: undefined,
        modelRois: undefined,
        vfModels: undefined,
        vfGlobalModels: undefined,
        dataProviderModelTypes: [{
            'value': 'none',
            'text': ''
        }],
        dataProviderTeachableModelTypes: '',
        dataModelTypes: [],
        defaultModelParameters: {},
        defaultGlobalModelParameters: {},
        selectedModelType: '',
        resultClouds: [],
        zoomFactor: 1.25,
        editMode: false,
        numResultsMax: '',
        isSelectedImageAcquisition: true,
        activatedLinesensor: false,
        editModelsTabVisible: true,
        showOnlyOneTabForAVisionFuntionTab: false,
        multiSelect: false,
        resultFilterTimeout: undefined,
        supportsKeyboardHandler: true, // TODO: set to false when buttons Ins/Del are available in CockpitDemo and VFDK - or discuss if remaun true
        orientationToolPosition: {
            x: 0,
            y: 0,
            width: 0,
            height: 0
        },
        orientationToolSizeMax: {
            width: 300,
            height: 80
        },
        orientationToolSizeMin: {
            width: 110,
            height: 37
        },
        accessAttributForTeach: 'r',
        accessAttributForSubmit: 'rw',
        orientationToolSearchAngleToDegRatio: 100,
        orientationToolRatioWidthToHeight: 3.5,
        orientationToolRatioViewBoxToTool: 3,
        refIdDropDownBoxToolList: 'dropDownBoxForTheRoiTools',
        dataProviderRoiTools: [{
                'value': 'tools'
            },
            {
                'value': 'rectangle+'
            },
            {
                'value': 'ellipse+'
            },
            {
                'value': 'ring+'
            },
            {
                'value': 'freehand'
            },
            {
                'value': 'rectangle-'
            },
            {
                'value': 'ellipse-'
            },
            {
                'value': 'ring-'
            },
            {
                'value': 'eraser'
            },
        ].map(function(entry){
            entry.text = (entry.value === 'tools' ? "ROI " : "") + entry.value.charAt(0).toUpperCase() + entry.value.slice(1);
            entry.image = entry.value + '.png';
            return entry;
        }),
        dataProviderRoiCommands: [{
                'value': 'commands'
            },
            {
                'value': 'size'
            },
            {
                'value': 'angle'
            },
            {
                'value': 'alignment'
            }
        ].map(function(entry){
            entry.text = (entry.value === 'commands' ? "ROI " : "Set Same ") + entry.value.charAt(0).toUpperCase() + entry.value.slice(1);
            entry.image = entry.value + '.png';
            return entry;
        }),
        refIdDropDownBoxRoiCommands: 'dropDownBoxForTheRoiCommands',
        refIAddRoiTools: 'addRoiTool',
        selectedValueOfRoiCommads: 'alignment',
        refIdSmartPanelParameterFormModelParameters: 'modelParameters',  
        refIdSmartPanelGlobalModelListMeasurementDef: 'visionFunctionGlobalModel', 
        refIdVfModelsTabControl: 'TabControlModels',
        refIdTabItemVisionFunction: 'TabItemVisionParameterProcessVaribales',
        refIdTabItemVisionModels: 'TabItemModels',
        selectedVfModelTab: 'VisionFunctionPage',
        refIdButtonExecute: 'btnPlay',
        refIdButtonImageCapture: 'btnLoadImage',
        refIdDropDownBoxVisionApplicationNavigation: 'DropDownBoxVisonFunctionName',
        refIdDropDownBoxComponentList: 'DropDownBoxComponentList',
        refIdLogoutButton: 'LogoutButton',
        headerContentId: 'mVi2Navigation',
        defaultModelType: {
            'value': 'defaultModelType',
            'text': 'Add Model'
        },
        defaultOperation: {
            'value': 'defaultOperation', 
            'text': 'Add Operation'
        },
        refIdButtonSaveGlobalModel: 'btnSaveGlobalModel',
        refIdEditMarker: 'btnEditMarker', 
        refIdRemoveMeasurement: 'btnDeleteGlobalModel', 
        refIdDropDownBoxModelType: 'dropDownBoxForModelType', 
        refIdButtonLoadVisionApplication: 'btnLoadVisionFunction',
        refIdButtonSaveVisionApplication: 'btnSaveVisionFuntion',
        refIdDropDownBoxForTheRoiManipulation: 'dropDownBoxForTheRoiManipulation',
        dataProviderRoiManipulation: [{
                'value': 'manipulation'
            },
            {
                'value': 'delete'
            },
            {
                'value': 'copy'
            },
            {
                'value': 'paste'
            }
        ].map(function(entry){
            entry.text = (entry.value === 'manipulation' ? "ROI " : "") + entry.value.charAt(0).toUpperCase() + entry.value.slice(1);
            entry.image = entry.value + '.png';
            return entry;
        }),
    };
});