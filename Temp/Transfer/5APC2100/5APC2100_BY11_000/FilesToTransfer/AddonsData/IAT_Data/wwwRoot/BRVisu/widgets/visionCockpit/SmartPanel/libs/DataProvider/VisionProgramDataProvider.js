﻿/*global define */
define([], function () {
    'use strict';

    function VisionProgramDataProvider(repository) {
        this._repository = repository;
        return this;
    }

    var p = VisionProgramDataProvider.prototype;

    p.getOfflineMode = function () {
        var offlineMode,
            vpState = this._repository.getVisionProgramState();
        if (vpState.meta && vpState.meta.VSM) {
            offlineMode = vpState.meta.VSM.offline;
        }
        return offlineMode;
    };

    p.setUserDefinedParameterMode = function (userDefinedParameterMOde) {
        var visionFunctionParameter = this._repository.getVisionFunctionParameter();
        visionFunctionParameter.userDefinedParameterMode = userDefinedParameterMOde;
    };

    p.getUserDefinedParameterMode = function () {
        var visionFunctionParameter = this._repository.getVisionFunctionParameter();
        return visionFunctionParameter.userDefinedParameterMode;
    };

    p.getVisionProgramName = function () {
        var vpState = this._repository.getVisionProgramState(),
            name = vpState.meta.program.name;
        return name;
    };

    p.getVisionFunctionName = function (visionFunctionInstance) {
        var vpState = this._repository.getVisionProgramState(),
            order,
            vfName = "";

        if (vpState.meta && vpState.meta.functions) {
            order = parseInt(visionFunctionInstance);

            vpState.meta.functions.forEach(function (vf) {
                if (vf.order === order) {
                    vfName = vf.name;
                }
            });
        }
        return vfName;
    };

    p.getVisionFunctionType = function (visionFunctionInstance) {
        var vpState = this._repository.getVisionProgramState(),
            order,
            vfType = "";

        if (vpState.meta && vpState.meta.functions) {
            order = parseInt(visionFunctionInstance);

            vpState.meta.functions.forEach(function (vf) {
                if (vf.order === order) {
                    vfType = vf.type;
                }
            });
        }
        return vfType;
    };

    p.getVisionFunctionInstanceByName = function (visionFunctionName) {
        var vpState = this._repository.getVisionProgramState(),
            vfInstance = "";

        if (vpState.meta && vpState.meta.functions) {
            vpState.meta.functions.forEach(function (vf) {
                if (vf.name === visionFunctionName) {
                    vfInstance = vf.order;
                }
            });
        }
        return vfInstance;
    };

    p.getVisionFunctionFeatures = function () {
        var vpState = this._repository.getVisionProgramState(),
            features = vpState.param[0].features;
        return features;
    };

    p.isVisionProgramLoaded = function () {
        var vpState = this._repository.getVisionProgramState(),
            isLoaded = false;

        if (vpState && vpState.meta && vpState.meta.program && vpState.meta.program.status === "loaded") {
            isLoaded = true;
        }

        return isLoaded;
    };

    p.getNumResultsMax = function () {
        var vpConfiguration = this._repository.getVisionApplicationConfiguration(),
            numResultsMax = 0,
            elementOfNumResultsMax;

        if (vpConfiguration.VisionApplication) {
            for (var index = 0; index < vpConfiguration.VisionApplication.VisionFunctions[0].Constants.length; index++) {
                if (vpConfiguration.VisionApplication.VisionFunctions[0].Constants[index].Id === "NumResultsMax") {
                    elementOfNumResultsMax = index;
                    numResultsMax = vpConfiguration.VisionApplication.VisionFunctions[0].Constants[elementOfNumResultsMax].Value;
                    break;
                }
            }
        }
        return numResultsMax;
    };

    p.getNumResults = function () {
        var executionResult = this._repository.getExecutionResult(),
            numResults = 0,
            element,
            outputs;

        if (executionResult.param && executionResult.param.outputs) {
            outputs = executionResult.param.outputs;

            for (var index = 0; index < outputs.length; index++) {
                element = outputs[index].NumResults;
                if (element) {
                    numResults = element;
                    break;
                }
            }
        }
        else if (!isNaN(executionResult))
        {
            numResults = executionResult;
        }
        return numResults;
    };

    p.dispose = function () {};

    return VisionProgramDataProvider;
});