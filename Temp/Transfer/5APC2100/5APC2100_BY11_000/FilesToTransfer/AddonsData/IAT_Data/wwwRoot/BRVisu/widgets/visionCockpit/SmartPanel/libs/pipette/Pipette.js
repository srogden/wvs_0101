/*
 * This class is the main class for the Pipette. 
 */
/*global define, $*/
define(['libs/d3/d3'], function (d3) {
    'use strict';
    function Pipette(renderer) {
        this.FunctionAtClickSmartPanel = 0;
        this.smartPanelContext = 0; 
        this.renderer = renderer;
        this.drawing = {
            rootContainer: this.renderer.select('#group')
        };
        this.renderer.select("#group").on("mousedown", this.onClick.bind(this));
    }

    Pipette.prototype.click = function () {
        if (this.FunctionAtClickSmartPanel !== 0) {
            this.FunctionAtClickSmartPanel.call(this.smartPanelContext);
        }
    };

    Pipette.prototype.registerParentFunction = function (context, fn) {
        this.FunctionAtClickSmartPanel = fn;
        this.smartPanelContext = context;
    };

    Pipette.prototype.onClick = function () {  
        var coordinates, x, y; 
        if (this.smartPanelContext.getRepetitiveMode() === false){
            coordinates = d3.mouse(this.drawing.rootContainer[0][0]);
            x = Math.floor(coordinates[0]); 
            y = Math.floor(coordinates[1]); 
            this.x = x.toString();
            this.y = y.toString(); 
            this.smartPanelContext.vsEncoder.getPixelValues(x, y);    
        } else{
            this.resetValues(); 
        }      
    };

    Pipette.prototype.resetValues = function () {
        this.x = ' --';
        this.y = ' --';    
        this.color = ' --';      
        this.click();       
    }; 

    return Pipette;
});