/*
* Controls the usability of the widgets for the logger 
*
*/
/*global define*/
define([], function () {
    'use strict';
    function LoggerHandling(context) {
        this.parent = context; 
    }

    var  p = LoggerHandling.prototype;

    p.setLoggerImagesAndStyleOfNumericInputsOfSeverities = function (loggerArraySeverity) {       
        var loggerImagePathErrorWithColor = 'Media/mappVision/loggerSeverity/color/icon_error.svg',
            loggerImagePathErrorWithoutColor = 'Media/mappVision/loggerSeverity/gray/icon_error.svg',
            loggerImagePathWarningWithColor = 'Media/mappVision/loggerSeverity/color/icon_warning.svg', 
            loggerImagePathWarningWithoutColor = 'Media/mappVision/loggerSeverity/gray/icon_warning.svg',
            loggerImagePathInfoWithColor = 'Media/mappVision/loggerSeverity/color/icon_info.svg', 
            loggerImagePathInfoWithoutColor = 'Media/mappVision/loggerSeverity/gray/icon_info.svg',
            loggerImagePathSuccessWithColor = 'Media/mappVision/loggerSeverity/color/icon_success.svg', 
            loggerImagePathSuccessWithoutColor = 'Media/mappVision/loggerSeverity/gray/icon_success.svg';   

        
        var loggerImagePathErrorToSet = loggerImagePathErrorWithoutColor,
            loggerImagePathWarningToSet = loggerImagePathWarningWithoutColor,
            loggerImagePathSuccessToSet = loggerImagePathSuccessWithoutColor,
            loggerImagePathInfoToSet = loggerImagePathInfoWithoutColor; 

        var loggerNumOuthWarningtyleToSet = 'sloggerGrayrNumericOutput',
            loggerNumOutErrorStyleToSet = 'sloggerGrayrNumericOutput',
            loggerNumOutInfoStyleToSet = 'sloggerGrayrNumericOutput',
            loggerNumOutSuccessStyleToSet = 'sloggerGrayrNumericOutput';     

        loggerArraySeverity.forEach(function (entry) {
            switch (entry) {
                case "Information":
                    loggerImagePathInfoToSet = loggerImagePathInfoWithColor;
                    loggerNumOutInfoStyleToSet = 'sloggerInfoNumericOutput';
                    break;
                case "Success":
                    loggerImagePathSuccessToSet = loggerImagePathSuccessWithColor;
                    loggerNumOutSuccessStyleToSet = 'sloggerSuccessNumericOutput';   
                    break;
                case "Warning":
                    loggerImagePathWarningToSet =   loggerImagePathWarningWithColor;
                    loggerNumOuthWarningtyleToSet = 'sloggerWarningNumericOutput';          
                    break;
                case "Error":
                    loggerImagePathErrorToSet =   loggerImagePathErrorWithColor; 
                    loggerNumOutErrorStyleToSet = 'sloggerErrorNumericOutput';         
                    break;
            }
        });
        this.parent.setLoggerImagePathWarning(loggerImagePathWarningToSet);
        this.parent.setLoggerImagePathError(loggerImagePathErrorToSet); 
        this.parent.setLoggerImagePathInfo(loggerImagePathInfoToSet);  
        this.parent.setLoggerImagePathSuccess(loggerImagePathSuccessToSet);
        
        
        this.parent.setLoggerNumOutErrorStyle(loggerNumOutErrorStyleToSet); 
        this.parent.setLoggerNumOutWarningStyle(loggerNumOuthWarningtyleToSet); 
        this.parent.setLoggerNumOutInfoStyle(loggerNumOutInfoStyleToSet);
        this.parent.setLoggerNumOutSuccessStyle(loggerNumOutSuccessStyleToSet); 
    };  
    return LoggerHandling;
});