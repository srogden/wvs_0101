/*global define*/
define([], function () {
    'use strict';

    function ResultFilter(context) {
        this.parent = context;
        this.settings = context.settings;
        this.vpDataProvider = context.vpDataProvider;
        this.drawVariablesTimer = undefined;
        this.drawVariablesTimeout = 500; //msec
        this.defaultFilterIndex = 1;
        this.initialized = false;

        if (this.parent.isUnitTestEnviroment()!== true){    
            this.filterControls = {
                numericInput: brease.callWidget(this.settings.parentContentId + '_' + this.settings.refIdNumericInputFilter, "widget"),
                buttonNext: brease.callWidget(this.settings.parentContentId + '_' + this.settings.refIdResultFilterButtonInc, "widget"),
                buttonPrevious: brease.callWidget(this.settings.parentContentId + '_' + this.settings.refIdResultFilterButtonDec, "widget"),
                processVariablesList: brease.callWidget(this.settings.parentContentId + '_' + this.settings.visionFunctionVariablesRefId, "widget"),
                buttonShowAllResults: brease.callWidget(this.settings.parentContentId + '_' + this.settings.refIdButtonShowAllResults, "widget"),
                buttonHideAllResults: brease.callWidget(this.settings.parentContentId + '_' + this.settings.refIdButtonHideAllResults, "widget"),
                processVariablesFilterIndex: brease.callWidget(this.settings.parentContentId + '_' + this.settings.refIdProcessVariablesFilterIndex, "widget"),
                labelFilterInformation: brease.callWidget(this.settings.parentContentId + '_' + this.settings.refIdLabelFilterInformation, "widget")
            };
            if ((this.filterControls.numericInput != null) &&
                (this.filterControls.numericInput != null) &&
                (this.filterControls.buttonNext != null) &&
                (this.filterControls.buttonPrevious != null) &&
                (this.filterControls.buttonShowAllResults != null) &&
                (this.filterControls.buttonHideAllResults != null) &&
                (this.filterControls.processVariablesFilterIndex != null)) {
                this.initialized = true; 
            }
        }
    }

    var p = ResultFilter.prototype;

    p.showInitialResultIndex = function () {
        if (this.initialized) {
            this.filterControls.buttonHideAllResults.setEnable(this.settings.resultClouds.length > 0);
            this.filterControls.buttonShowAllResults.setEnable(this.settings.resultClouds.length > 0);
            this.updateResultFilter(this.defaultFilterIndex);
            this.applyIconicsFilter();
        }
    };

    p.applyFilterIndexOfNumericInput = function () {
        this.settings.iconicsFilterIndex = this.filterControls.numericInput.getValue();
        this.applyFilterResultOfIndex(this.settings.iconicsFilterIndex);
    };

    p.applyFilterResultOfIndex = function (filterIndex) {
        var that = this;
        this.settings.iconicsFilterIndex = filterIndex;
        this.updateResultFilter();
        this.applyIconicsFilter();

        if (this.settings.iconicsFilterIndex === 0) {
            this.parent._showBusyIndicator();
        }

        if (this.drawVariablesTimer) {
            clearTimeout(this.drawVariablesTimer);
        }
        this.drawVariablesTimer = setTimeout(function () {
            that.filterControls.processVariablesList.filterByIndex(that.settings.iconicsFilterIndex);
            that.filterControls.processVariablesFilterIndex.setValue(that.settings.iconicsFilterIndex);
            if (that.settings.iconicsFilterIndex === 0) {
                that.parent._hideBusyIndicator();
            }
        }, this.drawVariablesTimeout);
    };

    p.showNextResult = function () {
        var value = this.filterControls.numericInput.getValue(),
            lastIndex = value,
            that = this;

        if (value < this.vpDataProvider.getNumResults()) {
            this.settings.iconicsFilterIndex = value + 1;
            this.filterControls.numericInput.setValue(this.settings.iconicsFilterIndex);
            this.updateResultFilter();
            this.applyIconicsFilter();

            if (lastIndex === 0) {
                that.parent._showBusyIndicator();
            }

            if (this.drawVariablesTimer) {
                clearTimeout(this.drawVariablesTimer);
            }
            this.drawVariablesTimer = setTimeout(function () {
                that.filterControls.processVariablesList.filterByIndex(that.settings.iconicsFilterIndex);
                that.filterControls.processVariablesFilterIndex.setValue(that.settings.iconicsFilterIndex);
                that.parent._hideBusyIndicator();
            }, this.drawVariablesTimeout);

        } else {
            this.filterControls.buttonNext.setEnable(false);
        }
    };

    p.showPreviousResult = function () {
        var value = this.filterControls.numericInput.getValue(),
            that = this;

        if (value > 0) {
            this.settings.iconicsFilterIndex = value - 1;
            this.filterControls.numericInput.setValue(this.settings.iconicsFilterIndex);
            this.updateResultFilter();
            this.applyIconicsFilter();

            if (this.settings.iconicsFilterIndex === 0) {
                this.parent._showBusyIndicator();
            }

            if (this.settings.resultFilterTimeout) {
                clearTimeout(this.settings.resultFilterTimeout);
            }
            this.settings.resultFilterTimeout = setTimeout(function () {
                that.filterControls.processVariablesList.filterByIndex(that.settings.iconicsFilterIndex);
                that.filterControls.processVariablesFilterIndex.setValue(that.settings.iconicsFilterIndex);
                if (that.settings.iconicsFilterIndex === 0) {
                    that.parent._hideBusyIndicator();
                }
            }, this.drawVariablesTimeout);

        } else {
            this.filterControls.buttonPrevious.setEnable(false);
        }
    };

    p.updateResultFilter = function (resetFilterIndex) {
        var numberOfExecutionResults = 0,
            that = this;

        if (this.initialized) {
            if (this.settings.statusReady === false) {
                this.disableAllFilterControls();
            } else if (this.settings.statusReady === true) {

                if (this.vpDataProvider) {
                    numberOfExecutionResults = this.vpDataProvider.getNumResults();

                    if (numberOfExecutionResults > 0) {
                        this.filterControls.numericInput.setEnable(true);

                        if (resetFilterIndex !== undefined) {
                            this.settings.iconicsFilterIndex = resetFilterIndex;
                            this.filterControls.numericInput.setValue(this.settings.iconicsFilterIndex);

                            if (this.settings.resultFilterTimeout) {
                                clearTimeout(this.settings.resultFilterTimeout);
                            }
                            this.settings.resultFilterTimeout = setTimeout(function () {
                                that.filterControls.processVariablesList.filterByIndex(that.settings.iconicsFilterIndex);
                                that.filterControls.processVariablesFilterIndex.setValue(that.settings.iconicsFilterIndex);
                                if (that.settings.iconicsFilterIndex === 0) {
                                    that.parent._hideBusyIndicator();
                                }
                            }, this.drawVariablesTimeout);
                        }

                        if (this.settings.iconicsFilterIndex > numberOfExecutionResults) {
                            this.settings.iconicsFilterIndex = numberOfExecutionResults;
                            this.filterControls.numericInput.setValue(this.settings.iconicsFilterIndex);
                        }

                        if (this.settings.iconicsFilterIndex === 0) {
                            this.filterControls.buttonPrevious.setEnable(false);
                            this.filterControls.labelFilterInformation.setText("Results filter state: deactivated");

                        } else {
                            this.filterControls.buttonPrevious.setEnable(true);
                            this.filterControls.labelFilterInformation.setText("Results filter state: active");
                        }

                        if (this.settings.iconicsFilterIndex >= numberOfExecutionResults) {
                            this.filterControls.buttonNext.setEnable(false);
                        } else {
                            this.filterControls.buttonNext.setEnable(true);
                        }
                    } else {
                        this.settings.iconicsFilterIndex = 0;
                        this.filterControls.labelFilterInformation.setText("Results filter state: deactivated - no results");
                        this.filterControls.numericInput.setValue(0);
                        this.filterControls.processVariablesFilterIndex.setValue(0);
                        this.filterControls.processVariablesList.filterByIndex(1);  
                        this.disableAllFilterControls();
                    }
                }
            }
        }
    };

    p.disableAllFilterControls = function () {
        if (this.initialized === true) {
            this.filterControls.numericInput.setEnable(false);
            this.filterControls.buttonNext.setEnable(false);
            this.filterControls.buttonPrevious.setEnable(false);
            this.filterControls.buttonShowAllResults.setEnable(false);
            this.filterControls.buttonHideAllResults.setEnable(false);
            this.filterControls.processVariablesFilterIndex.setEnable(false);
        }
    };

    p.applyIconicsFilter = function () {
        var iconics = this.settings.resultClouds,
            that = this,
            isHideAllResultsToggleButtonPressed = this.filterControls.buttonHideAllResults.getValue(),
            isShowAllResultsToggleButtonPressed = this.filterControls.buttonShowAllResults.getValue();
        
        if (this.settings.iconicsFilterIndex === 0 ) {
            if ( isHideAllResultsToggleButtonPressed){
                iconics.forEach(function (iconic) {iconic.hide();});
            }
            else {
                iconics.forEach(function (iconic) {iconic.redraw();});
            }
        } else {
            iconics.forEach(function (iconic) {
                if (iconic.dataResultIndex === that.settings.iconicsFilterIndex) {
                    if (isHideAllResultsToggleButtonPressed) {
                        iconic.hide();
                    }
                    else {
                        iconic.highlight();
                    }
                } else {
                    if (isShowAllResultsToggleButtonPressed) {
                        iconic.redraw();
                    }
                    else {
                        iconic.hide();
                    }
                }
            });
        }
    };

    p.showAllResults = function () {
        var iconics = this.settings.resultClouds,
        that = this,
            buttonState;

        if (this.initialized === true) {
            buttonState = this.filterControls.buttonShowAllResults.getValue();
            if (buttonState === true) {
                this.filterControls.buttonHideAllResults.setValue(false);
                iconics.forEach(function (iconic) {
                    if (iconic.dataResultIndex === that.settings.iconicsFilterIndex)
                    {
                        iconic.highlight();
                    }else{
                        iconic.redraw();
                    }
                });
            } else {
                this.applyFilterIndexOfNumericInput();
            }
        }
    };

    p.hideAllResults = function () {
        var iconics = this.settings.resultClouds,
            buttonState;

        if (this.initialized === true) {
            buttonState = this.filterControls.buttonHideAllResults.getValue();
            if (buttonState === true) {
                this.filterControls.buttonShowAllResults.setValue(false);
                iconics.forEach(function (iconic) {
                    iconic.hide();
                });
            } else {
                this.applyFilterIndexOfNumericInput();
            }
        }
    };

    return ResultFilter;
});