
/*global define*/
define([], function () {
    'use strict';

    function Utils() {
    }

    Utils.prototype.getAlignmentPostion = function (refx, refy, px, py, angleDeg) {
        var rad2deg, gammaRad, gammaDeg, angleRad, g, betaRad, a, ex, ey, qx, qy, betaDeg, aphaDeg, alignmentPostion;

        aphaDeg = angleDeg;
        rad2deg = 180 / Math.PI;
        angleRad = angleDeg / rad2deg;

        if (px > refx) {
            g = 0;
        } else {
            g = 180;
        }

        betaRad = Math.atan((py - refy) / (px - refx));
        betaDeg = (betaRad * rad2deg) + g;

        gammaDeg = aphaDeg - betaDeg;
        gammaRad = gammaDeg / rad2deg;

        a = Math.cos(gammaRad) * Math.sqrt(Math.pow((px - refx), 2) + Math.pow((py - refy), 2));
        ex = Math.cos(angleRad);
        ey = Math.sin(angleRad);
        qx = refx + ex * a;
        qy = refy + ey * a;

        alignmentPostion = {
            x: qx,
            y: qy
        };

        return alignmentPostion;
    };

    Utils.prototype.setResizeValues = function (context) {  
        context.settings.resize_x0 = d3.transform(context.drawing.anchor.attr("transform")).translate[0]; 
        context.settings.resize_y0 = d3.transform(context.drawing.anchor.attr("transform")).translate[1]; 
    };


    return Utils;
});