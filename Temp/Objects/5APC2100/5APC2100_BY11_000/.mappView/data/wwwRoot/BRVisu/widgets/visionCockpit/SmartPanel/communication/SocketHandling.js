/*
 * Socket handling
 *
 */
/*global define*/
define([], function () {
    'use strict';

    function SocketHandling(context, vsEncoder, vsDecoder) {
        this.parent = context; 
        this.settings = context.settings;
        this.server = context.server;
        this.vsDecoder = vsDecoder;
        this.vsEncoder = vsEncoder;                      
    }

    var p = SocketHandling.prototype;  


    /**
	 *  @brief URL for web socket communication depending on configuration
	 *  @return URL for web socket communication depending on configuration
	 *  
	 *  @details add the parameter &ip=xxx.xxx.xxx.xxx  to your URL to connect to SPS xxx.xxx.xxx.xxx
	 */
	p._createWebsocketCommunciationUrl = function()
	{
		var socket,
			spsTargetHost = location.hostname;

		if (this.settings.ipAddress === 'localhost') {
            socket = "ws://" + this.settings.ipAddress + ":8082?caller=browser";
        } else {
			if(location.search.includes("&ip")) {
				spsTargetHost = location.search.substr(location.search.indexOf("&ip=") + "&ip=".length); // only allowed at end of URL
				console.warn("**** Websocket will connect to remote host " + spsTargetHost + " instead of standard host " + location.hostname + " ****");
			}
			socket = "ws://" + spsTargetHost + ":15080?caller=browser&cam=" + this.settings.ipAddress + ":8082"; 
		}
		return socket;
	}

    p.openSocketCommunication = function () {
        var socket;

        if (this.settings.ipAddress === '') {
            return;
        }
        socket = this._createWebsocketCommunciationUrl();	
        if (this.parent.server.socket !== undefined) {
            this.closeSocket(); 
        }

        this.parent.server.socket = new WebSocket(socket, "json");

        if (this.vsEncoder !== undefined) {
            this.vsEncoder.setSocket(this.parent.server.socket);  
        }
        this.parent.server.socket.addEventListener('open', this.parent._bind(this._socketOpenHandler));  
        this.parent.server.socket.addEventListener('close', this.parent._bind(this._socketCloseHandler));
        this.parent.server.socket.addEventListener('message', this.parent._bind(this._socketMessageHandler));
        this.parent.server.socket.addEventListener('error', this.parent._bind(this._socketErrorHandler));       
    }; 

    p.closeSocket = function () {
        if ((this.parent.server !== undefined) && (this.parent.server.socket !== undefined)) {
            this.parent.server.socket.removeEventListener('open', this.parent._bind(this._socketOpenHandler));
            this.parent.server.socket.removeEventListener('close', this.parent._bind(this._socketCloseHandler)); 
            this.parent.server.socket.removeEventListener('message', this.parent._bind(this._socketMessageHandler)); 
            this.parent.server.socket.removeEventListener('error', this.parent._bind(this._socketErrorHandler));      
            this.parent.server.socket.close();  
        }
    };

    p.reconnectSocket = function () {
        if (this.parent.hmiStatus.visionApplicationIsLoading === false){  
            this.closeSocket(); 
            this.openSocketCommunication();  
        } 
    };
    
    p._socketOpenHandler = function () {
        this.server.numberOfReconnectRequests = 0; 
        this.hmiStatus.isImageRequestTriggeredAfterConnect = false;
        this.settings.counterSocketErrors = 0;
        this._callExternalWidget(this.settings.parentContentId + '_' + this.settings.textOutputConnectionIsLostRefId, 'setVisible', false);
        this.resetAndCleanup(); 
        this.settings.offlineMode = undefined;
        this.setVisionFunctionName("");
        this.setInitialComplete(false);
        this.paramHandler.reset(); 
        this.isWaitingForImageCommandAck = false;
        this.vsEncoder.getState(); 
    };  

    p._socketErrorHandler = function () {
        this.settings.counterSocketErrors = this.settings.counterSocketErrors + 1;
        if (this.settings.counterSocketErrors > 10) {
            this._callExternalWidget(this.settings.parentContentId + '_' + this.settings.textOutputConnectionIsLostRefId, 'setVisible', true); 
        } 
    };         

    p._socketCloseHandler = function () {
        this.setInitialComplete(false); 
        this.socketHandling.reconnectSocket();      
    };
    
    p._socketMessageHandler = function (e) {
        this.vsDecoder.handleMessage(e); 
    };   

    return SocketHandling; 
}); 