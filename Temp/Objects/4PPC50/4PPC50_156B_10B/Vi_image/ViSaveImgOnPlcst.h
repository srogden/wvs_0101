#ifndef __AS__TYPE_
#define __AS__TYPE_
typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

#ifndef __AS__TYPE_SvgTexts_Type
#define __AS__TYPE_SvgTexts_Type
typedef struct SvgTexts_Type
{	plcstring ContentStart[501];
	plcstring ContentAfterTitle[201];
	plcstring ContentBase64[201];
	plcstring ContentBeginJS[2001];
	plcstring ContentBetween1[201];
	plcstring ContentBetween2[201];
	plcstring ContentEndJS[3001];
	plcstring ContentCrossHairBegin[201];
	plcstring ContentCrossHairMiddle[201];
	plcstring ContentCrossHairEnd[201];
	plcstring ContentCrossHairRed[201];
	plcstring ContentCrossHairIndex[201];
	plcstring ContentEnd[2001];
} SvgTexts_Type;
#endif

#ifndef __AS__TYPE_DiagTime_Type
#define __AS__TYPE_DiagTime_Type
typedef struct DiagTime_Type
{	plctime Refresh;
	plctime BeginSaveBMPJPG;
	plctime EndSaveBMPJPG;
	plctime BeginSVG;
	plctime BeginSVGSave;
	plctime EndSVGSave;
} DiagTime_Type;
#endif

#ifndef __AS__TYPE_MemInfoSave_Type
#define __AS__TYPE_MemInfoSave_Type
typedef struct MemInfoSave_Type
{	unsigned long UploadAdr;
	unsigned long UploadSize;
	unsigned long SvgAdr;
	unsigned long SvgSize;
	unsigned long CrosshAdr;
	unsigned long CrosshSize;
	unsigned long SvgHeaderSize;
} MemInfoSave_Type;
#endif

#ifndef __AS__TYPE_typCrossHairInfo
#define __AS__TYPE_typCrossHairInfo
typedef struct typCrossHairInfo
{	float CrosshairTopLeftX;
	float CrosshairTopLeftY;
	float CrosshairRotateCenter;
	float CrosshairSizeScaled;
	float CrosshairScale;
	plcstring DataVis[81];
	plcstring Data[401];
	plcstring Text[1001];
} typCrossHairInfo;
#endif

#ifndef __AS__TYPE_typVisionImageConfig
#define __AS__TYPE_typVisionImageConfig
typedef struct typVisionImageConfig
{	plcstring FileDevice[81];
	plcstring DirName[81];
	plcstring PlkIPWithoutNode[81];
	plcstring EthDevice[81];
	plcstring EthIP[81];
	unsigned long ConvertCycles;
	unsigned char Format;
	unsigned char QualityJPG;
	plcbit UploadBmpJpg;
	plcbit UploadSVG;
	unsigned char MainPageQualityJPG;
	unsigned short ImageRotation_deg;
} typVisionImageConfig;
#endif

#ifndef __AS__TYPE_DTStructure
#define __AS__TYPE_DTStructure
typedef struct DTStructure
{	unsigned short year;
	unsigned char month;
	unsigned char day;
	unsigned char wday;
	unsigned char hour;
	unsigned char minute;
	unsigned char second;
	unsigned short millisec;
	unsigned short microsec;
} DTStructure;
#endif

#ifndef __AS__TYPE_httpHeaderLine_t
#define __AS__TYPE_httpHeaderLine_t
typedef struct httpHeaderLine_t
{	plcstring name[51];
	plcstring value[81];
} httpHeaderLine_t;
#endif

#ifndef __AS__TYPE_httpRawHeader_t
#define __AS__TYPE_httpRawHeader_t
typedef struct httpRawHeader_t
{	unsigned long pData;
	unsigned long dataSize;
	unsigned long dataLen;
} httpRawHeader_t;
#endif

#ifndef __AS__TYPE_httpRequestHeader_t
#define __AS__TYPE_httpRequestHeader_t
typedef struct httpRequestHeader_t
{	plcstring protocol[21];
	plcstring host[281];
	plcstring contentType[101];
	unsigned long contentLength;
	plcstring connection[81];
	plcstring keepAlive[81];
	struct httpHeaderLine_t userLine[8];
	httpRawHeader_t rawHeader;
} httpRequestHeader_t;
#endif

struct AsMemPartCreate
{	unsigned long len;
	unsigned short status;
	unsigned long ident;
	plcbit enable;
};
_BUR_PUBLIC void AsMemPartCreate(struct AsMemPartCreate* inst);
struct AsMemPartAllocClear
{	unsigned long ident;
	unsigned long len;
	unsigned short status;
	unsigned long mem;
	plcbit enable;
};
_BUR_PUBLIC void AsMemPartAllocClear(struct AsMemPartAllocClear* inst);
struct DTGetTime
{	unsigned short status;
	plcdt DT1;
	plcbit enable;
};
_BUR_PUBLIC void DTGetTime(struct DTGetTime* inst);
struct httpClient
{	unsigned long option;
	unsigned long pHost;
	unsigned short hostPort;
	unsigned long method;
	unsigned long pUri;
	unsigned long pRequestHeader;
	unsigned long pRequestData;
	unsigned long requestDataLen;
	unsigned long pResponseHeader;
	unsigned long pResponseData;
	unsigned long responseDataSize;
	unsigned long pStatistics;
	unsigned long pStruct;
	unsigned short status;
	unsigned short tcpStatus;
	unsigned short httpStatus;
	unsigned long responseDataLen;
	unsigned short phase;
	unsigned long _i_state;
	unsigned short _i_result;
	unsigned long _ident;
	unsigned long _oldEnable;
	unsigned long _oldAbort;
	unsigned long _oldSend;
	unsigned long _state;
	unsigned long _internal;
	plcbit enable;
	plcbit send;
	plcbit abort;
};
_BUR_PUBLIC void httpClient(struct httpClient* inst);
struct TON
{	plctime PT;
	plctime ET;
	plctime StartTime;
	unsigned long Restart;
	plcbit IN;
	plcbit Q;
	plcbit M;
};
_BUR_PUBLIC void TON(struct TON* inst);
struct FileCreate
{	unsigned long pDevice;
	unsigned long pFile;
	unsigned short status;
	unsigned long ident;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileCreate(struct FileCreate* inst);
struct FileWrite
{	unsigned long ident;
	unsigned long offset;
	unsigned long pSrc;
	unsigned long len;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileWrite(struct FileWrite* inst);
struct FileClose
{	unsigned long ident;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileClose(struct FileClose* inst);
struct ViSaveImgOnPlc
{	typVisionImageConfig CFG;
	unsigned char PowerlinkNode;
	struct typCrossHairInfo(* CrossHairInfo)[11];
	unsigned short Status;
	signed short Step;
	struct AsMemPartCreate AsMemPartCreate_0;
	struct AsMemPartAllocClear AsMemPartAllocClear_0;
	MemInfoSave_Type MemInfo;
	plctime DiagStartTime;
	DiagTime_Type DiagTime;
	struct DTGetTime DTGetTime_0;
	DTStructure DTStructure_0;
	plcstring FileNameImg[81];
	plcstring FileNameSvg[81];
	plcstring URI[81];
	plcstring Host[81];
	struct httpClient httpClient_0;
	unsigned short httpClientErrorStatus;
	httpRequestHeader_t RequestHeader;
	struct TON TON_ReloadTimeout;
	unsigned long UploadedImgSize;
	struct FileCreate FileCreate_0;
	struct FileWrite FileWrite_0;
	struct FileClose FileClose_0;
	unsigned long b64AdrInBuffer;
	unsigned long b64AdrOutBuffer;
	unsigned long b64neededOutput;
	unsigned char b64key[64];
	unsigned long b64actposIN;
	unsigned long b64actposOUT;
	unsigned char(* b64in)[3];
	unsigned char(* b64out)[4];
	unsigned short b64blockLen;
	SvgTexts_Type SvgTexts;
	plcstring tmpStr1[81];
	unsigned long i;
	plcbit Enable;
	plcbit FirstElement;
};
void ViSaveImgOnPlc(struct ViSaveImgOnPlc* inst);
_BUR_PUBLIC plctime clock_ms(void);
_BUR_PUBLIC unsigned long DT_TO_DTStructure(plcdt DT1, unsigned long pDTStructure);
_BUR_PUBLIC plcstring* usint2str(unsigned char IN, plcstring pStr[81], unsigned long len);
_BUR_PUBLIC plcstring* uint2str(unsigned short IN, plcstring pStr[81], unsigned long len);
_BUR_PUBLIC unsigned short brsitoa(signed long value, unsigned long pString);
_BUR_PUBLIC unsigned long brsmemset(unsigned long pDest, unsigned char value, unsigned long length);
_BUR_PUBLIC unsigned long brsmemcpy(unsigned long pDest, unsigned long pSrc, unsigned long length);
_BUR_PUBLIC unsigned long brsstrcat(unsigned long pDest, unsigned long pSrc);
_BUR_PUBLIC unsigned long brsstrlen(unsigned long pString);
_BUR_PUBLIC unsigned long brsstrcpy(unsigned long pDest, unsigned long pSrc);
_BUR_PUBLIC unsigned long CheckDivUdint(unsigned long divisor);
_BUR_PUBLIC float CheckDivReal(float divisor);
_BUR_PUBLIC signed long CheckBounds(signed long index, signed long lower, signed long upper);
_BUR_PUBLIC unsigned long CheckReadAccess(unsigned long address);
_BUR_PUBLIC unsigned long CheckWriteAccess(unsigned long address);
_BUR_LOCAL unsigned short ERR_NO_IMAGE;
_BUR_LOCAL unsigned short ERR_BUFF_TOO_SMALL;
_GLOBAL unsigned short MAX_NUM_RESULTS;
_GLOBAL unsigned short ERR_OK;
_GLOBAL unsigned short ERR_FUB_ENABLE_FALSE;
_GLOBAL unsigned short ERR_FUB_BUSY;
_GLOBAL unsigned short httpMETHOD_GET;
_GLOBAL unsigned short httpOPTION_HTTP_11;
static void __AS__Action_ViSaveImgOnPlc_ACTION_InitTexts(struct ViSaveImgOnPlc* inst);
