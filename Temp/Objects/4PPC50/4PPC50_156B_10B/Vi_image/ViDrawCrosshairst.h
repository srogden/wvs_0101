#ifndef __AS__TYPE_
#define __AS__TYPE_
typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

#ifndef __AS__TYPE_typCrossHairCfg
#define __AS__TYPE_typCrossHairCfg
typedef struct typCrossHairCfg
{	float Scale;
	float PixelRatio;
	unsigned char Size;
	unsigned char Font;
} typCrossHairCfg;
#endif

#ifndef __AS__TYPE_typCrossHair
#define __AS__TYPE_typCrossHair
typedef struct typCrossHair
{	plcstring SvgTransformation[10][201];
	plcstring SvgContent[10][1001];
	plcbit SvgVisible[10];
	typCrossHairCfg CFG;
	unsigned char DetailsNo;
	plcbit ShowCrosshair;
} typCrossHair;
#endif

#ifndef __AS__TYPE_typCrossHairInfo
#define __AS__TYPE_typCrossHairInfo
typedef struct typCrossHairInfo
{	float CrosshairTopLeftX;
	float CrosshairTopLeftY;
	float CrosshairRotateCenter;
	float CrosshairSizeScaled;
	float CrosshairScale;
	plcstring DataVis[81];
	plcstring Data[401];
	plcstring Text[1001];
} typCrossHairInfo;
#endif

#ifndef __AS__TYPE_typVisionCommand
#define __AS__TYPE_typVisionCommand
typedef struct typVisionCommand
{	plcbit ImageTrigger;
	plcbit ImageTriggerReset;
	plcbit AutoSetupStartStop;
	plcbit VaSwitchApplication;
	plcbit SaveDiagData;
	plcbit ReadCameraInfo;
} typVisionCommand;
#endif

#ifndef __AS__TYPE_enumVisionFunction
#define __AS__TYPE_enumVisionFunction
typedef enum enumVisionFunction
{	enumFunctionNone = 0,
	enumBlob = 1,
	enumCoderReader = 2,
	enumMatch = 3,
	enumMeasurement = 4,
	enumOCR = 5,
	enumPixelCounter = 6,
} enumVisionFunction;
#endif

#ifndef __AS__TYPE_ViComponentInternalMappLinkType
#define __AS__TYPE_ViComponentInternalMappLinkType
typedef struct ViComponentInternalMappLinkType
{	unsigned long Internal[2];
} ViComponentInternalMappLinkType;
#endif

#ifndef __AS__TYPE_ViComponentType
#define __AS__TYPE_ViComponentType
typedef struct ViComponentType
{	struct ViComponentInternalCameraIfType(* CameraType);
	ViComponentInternalMappLinkType MappLinkInternal;
} ViComponentType;
#endif

#ifndef __AS__TYPE_typVisionConfig
#define __AS__TYPE_typVisionConfig
typedef struct typVisionConfig
{	enumVisionFunction VisionFunction;
	unsigned char PowerlinkNode;
	unsigned long DataStructure;
	ViComponentType ComponentLink;
	unsigned short ResolutionWidth_X;
	unsigned short ResolutionHeight_Y;
} typVisionConfig;
#endif

#ifndef __AS__TYPE_typVisionData
#define __AS__TYPE_typVisionData
typedef struct typVisionData
{	plcbit Enable;
	unsigned char MaxItemCnt;
	signed long NettimeDelay;
	unsigned short Timeout;
	unsigned char Gain;
	unsigned short Focus;
	unsigned long Exposure;
	unsigned char FlashColor;
	unsigned char FlashSegment;
	unsigned char LedColor;
	plcbit IRFilter;
	signed long OffsetROIX;
	signed long OffsetROIY;
	signed long OffsetROIRotationCenterX;
	signed long OffsetROIRotationCenterY;
	signed short OffsetROIOrientation;
	plcbit ChromaticLock;
	unsigned char Alignment;
	unsigned char AutoSetupGain;
	unsigned short AutoSetupFocus;
	unsigned long AutoSetupExposure;
	unsigned char AcquisitionAcceptedCnt;
	unsigned char AcquisitionCompletedCnt;
	unsigned char AcquisitionFailedCnt;
	unsigned char LightWarningCnt;
	unsigned short ImageProcessingError;
	plcbit ImageProcessingActive;
	signed long ImageNettime;
	unsigned short ProcessingTimeCamera;
	unsigned short ProcessingTimeFunction;
	unsigned char ResultCnt;
	plcbit CockpitIsActive;
	unsigned long Status;
	plcstring StatusText[31];
	plcbit Active;
} typVisionData;
#endif

#ifndef __AS__TYPE_typVisionFunction
#define __AS__TYPE_typVisionFunction
typedef struct typVisionFunction
{	plcstring ApplicationName[41];
	signed long Status;
} typVisionFunction;
#endif

#ifndef __AS__TYPE_typVisionDiag
#define __AS__TYPE_typVisionDiag
typedef struct typVisionDiag
{	plcstring DiagName[81];
	signed long Status;
} typVisionDiag;
#endif

#ifndef __AS__TYPE_typVisionOptics
#define __AS__TYPE_typVisionOptics
typedef struct typVisionOptics
{	plcstring OrderNr[41];
	plcbit Binning;
	signed short ResolutionX;
	signed short ResolutionY;
	unsigned char Lens;
	unsigned char Sensor;
	plcbit ValidDistance;
	double Distance_mm;
	double DistanceLens_mm;
	double HyperFocalDistance_mm;
	double PixelSize_um;
	double PixelSizeBinning_um;
	double FocalLength_mm;
	double MinObjectDist_mm;
	double MaxObjectDist_mm;
	double Aperture;
	double CircleOfConfusion_mm;
	double DepthOfFieldNearPos_mm;
	double DepthOfFieldFarPos_mm;
	double DepthOfField_mm;
	double FieldOfView_X_mm;
	double FieldOfView_Y_mm;
	double Resolution_mmPerPx;
} typVisionOptics;
#endif

#ifndef __AS__TYPE_typVisionHW
#define __AS__TYPE_typVisionHW
typedef struct typVisionHW
{	plcbit Connected;
	plcbit Ready;
	plcbit DigitalInput01;
	plcbit DigitalOutput01;
	plcbit DigitalOutputStatus01;
	plcbit DigitalOutput02;
	plcbit DigitalOutputStatus02;
	unsigned long SerialNumber;
	unsigned long ID;
	unsigned long Variant;
	unsigned long Firmware;
	plcbit Undervoltage;
	signed char Temperature;
} typVisionHW;
#endif

#ifndef __AS__TYPE_typVisionMain
#define __AS__TYPE_typVisionMain
typedef struct typVisionMain
{	typVisionCommand CMD;
	typVisionConfig CFG;
	typVisionData DAT;
	typVisionFunction FCT;
	typVisionDiag DIA;
	typVisionOptics OPT;
	typVisionHW HW;
} typVisionMain;
#endif

#ifndef __AS__TYPE_typCodeReaderMain
#define __AS__TYPE_typCodeReaderMain
typedef struct typCodeReaderMain
{	unsigned char CodeType;
	plcbit GradingEnable;
	plcbit RobustnessEnable;
	unsigned char ParameterMode;
	unsigned char ParameterOptimization;
	plcstring BarcodeText[10][101];
	unsigned char BarcodeType[10];
	signed long PositionX[10];
	signed long PositionY[10];
	signed short Orientation[10];
	signed char Grading[10];
	unsigned char EnhancedGrading[23];
} typCodeReaderMain;
#endif

#ifndef __AS__TYPE_typBlobMain
#define __AS__TYPE_typBlobMain
typedef struct typBlobMain
{	plcbit RegionalFeature;
	plcbit EnhancedBlobInformation;
	unsigned char ModelNumber[10];
	plcbit Clipped[10];
	unsigned long Area[10];
	signed long PositionX[10];
	signed long PositionY[10];
	signed short Orientation[10];
	unsigned char MeanGrayValue[10];
	unsigned long Length[10];
	unsigned long Width[10];
} typBlobMain;
#endif

#ifndef __AS__TYPE_typMatchMain
#define __AS__TYPE_typMatchMain
typedef struct typMatchMain
{	unsigned char MinScore;
	unsigned char MaxOverlap;
	unsigned char ModelNumber[10];
	unsigned char Score[10];
	signed long PositionX[10];
	signed long PositionY[10];
	signed short Orientation[10];
	unsigned char Scale[10];
	signed long RotCenterX[10];
	signed long RotCenterY[10];
} typMatchMain;
#endif

#ifndef __AS__TYPE_typOCRMain
#define __AS__TYPE_typOCRMain
typedef struct typOCRMain
{	unsigned char ParameterMode;
	plcbit GradingEnable;
	plcstring Text[10][51];
	unsigned char Grading[10];
	signed long PositionX[10];
	signed long PositionY[10];
	signed short Orientation[10];
} typOCRMain;
#endif

#ifndef __AS__TYPE_typPixelMain
#define __AS__TYPE_typPixelMain
typedef struct typPixelMain
{	plcbit EnhancedPixelCounterInformation;
	unsigned char ModelNumber[10];
	unsigned long NumPixels[10];
	unsigned char MinGray[10];
	unsigned char MaxGray[10];
	unsigned short MeanGray[10];
	unsigned short DeviationGray[10];
	unsigned char MedianGray[10];
	unsigned long ModelArea[10];
	unsigned short NumConnectedComponents[10];
	signed long PositionX[10];
	signed long PositionY[10];
} typPixelMain;
#endif

#ifndef __AS__TYPE_typMTMain
#define __AS__TYPE_typMTMain
typedef struct typMTMain
{	plcbit UseResultAsXY;
	unsigned char ParameterMode;
	signed long Result[10];
} typMTMain;
#endif

#ifndef __AS__TYPE_ViComponentInternalCameraIfType
#define __AS__TYPE_ViComponentInternalCameraIfType
typedef struct ViComponentInternalCameraIfType
{	plcdword Vtable;
} ViComponentInternalCameraIfType;
#endif

struct ViDrawCrosshair
{	unsigned short SelectedSensor;
	struct typVisionMain(* VisionSensor);
	plcstring(* CodeTypes)[70][81];
	unsigned short ImageRotation_deg;
	struct typCrossHair(* visCrossHair);
	struct typCrossHairInfo CrossHairInfo[11];
	unsigned short idx;
	unsigned short SelectedSensorOld;
	struct typBlobMain(* Blob);
	struct typCodeReaderMain(* CodeReader);
	struct typMatchMain(* Match);
	struct typMTMain(* MT);
	struct typPixelMain(* Pixel);
	struct typOCRMain(* OCR);
	unsigned char DetailsNoOld;
	unsigned short ImageRotation_degOld;
	unsigned char ResultModelNumber;
	float ResultPositionX;
	float ResultPositionY;
	float ResultOrientation;
	float CrosshSize;
	float ScaleVertical;
	plcstring svgTrafo[201];
	plcstring svgContent[1001];
	plcstring tmpStr[101];
	signed short blueTextPos;
	plcbit CmdRefreshCrosshair;
	plcbit VisionDisabled;
	plcbit TextAlignment;
	plcbit ShowCrosshairOld;
	plcbit TextAlignmentOld;
	plcbit MT_UseXY;
};
void ViDrawCrosshair(struct ViDrawCrosshair* inst);
plcbit CrosshairDetailsValue(unsigned long strTarget, unsigned long strText, float fValue, plcbit TextAlignment);
plcbit CrosshairDetailsText(unsigned long strTarget, unsigned long strText, unsigned long adrText, plcbit TextAlignment);
_BUR_PUBLIC unsigned short brsftoa(float value, unsigned long pString);
_BUR_PUBLIC unsigned short brsitoa(signed long value, unsigned long pString);
_BUR_PUBLIC unsigned long brsmemset(unsigned long pDest, unsigned char value, unsigned long length);
_BUR_PUBLIC unsigned long brsmemcpy(unsigned long pDest, unsigned long pSrc, unsigned long length);
_BUR_PUBLIC unsigned long brsstrcat(unsigned long pDest, unsigned long pSrc);
_BUR_PUBLIC unsigned long brsstrcpy(unsigned long pDest, unsigned long pSrc);
_BUR_PUBLIC signed short FIND(plcstring IN1[32768], plcstring IN2[32768]);
_BUR_PUBLIC float CheckDivReal(float divisor);
_BUR_PUBLIC signed long CheckBounds(signed long index, signed long lower, signed long upper);
_BUR_PUBLIC unsigned long CheckReadAccess(unsigned long address);
_BUR_PUBLIC unsigned long CheckWriteAccess(unsigned long address);
_GLOBAL unsigned short MAX_NUM_RESULTS;
_GLOBAL unsigned char MAX_NUM_CODETYPES;
