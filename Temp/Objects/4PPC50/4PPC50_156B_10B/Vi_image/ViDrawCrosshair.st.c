#define _DEFAULT_INCLUDE
#include <bur\plctypes.h>
#include "C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_image/ViDrawCrosshairst.h"
#line 1 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViDrawCrosshair.nodebug"
#line 1 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViDrawCrosshair.st"
void ViDrawCrosshair(struct ViDrawCrosshair* inst){struct ViDrawCrosshair* __inst__=inst;{

if((((unsigned long)(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.DataStructure)!=(unsigned long)0))){
switch((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.VisionFunction)){
case 1:{
(__inst__->Blob=(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.DataStructure));
}break;case 2:{
(__inst__->CodeReader=(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.DataStructure));
}break;case 3:{
(__inst__->Match=(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.DataStructure));
}break;case 4:{
(__inst__->MT=(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.DataStructure));
}break;case 5:{
(__inst__->OCR=(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.DataStructure));
}break;case 6:{
(__inst__->Pixel=(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.DataStructure));
}break;}
}else{
return;
}


if((__inst__->CmdRefreshCrosshair|(((unsigned long)(unsigned char)(CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).ShowCrosshair)!=(unsigned long)(unsigned char)__inst__->ShowCrosshairOld))|(((unsigned long)(unsigned char)(CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).DetailsNo)!=(unsigned long)(unsigned char)__inst__->DetailsNoOld))|(((unsigned long)(unsigned short)__inst__->SelectedSensor!=(unsigned long)(unsigned short)__inst__->SelectedSensorOld))|(((unsigned long)(unsigned short)__inst__->ImageRotation_deg!=(unsigned long)(unsigned short)__inst__->ImageRotation_degOld))|(((unsigned long)(unsigned char)__inst__->TextAlignment!=(unsigned long)(unsigned char)__inst__->TextAlignmentOld)))){
(__inst__->ShowCrosshairOld=(CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).ShowCrosshair));
(__inst__->SelectedSensorOld=__inst__->SelectedSensor);
(__inst__->ImageRotation_degOld=__inst__->ImageRotation_deg);
(__inst__->TextAlignmentOld=__inst__->TextAlignment);

brsmemset(((unsigned long)(&((*(__inst__->visCrossHair)).SvgTransformation))),0,2010);
brsmemset(((unsigned long)(&((*(__inst__->visCrossHair)).SvgVisible))),0,10);
if((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).ShowCrosshair)){

if((((unsigned long)(unsigned char)(CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).DetailsNo)<(unsigned long)(unsigned char)1))){
if((((unsigned long)(unsigned char)(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).DAT.ResultCnt)>(unsigned long)(unsigned char)0))){
(CheckWriteAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).DetailsNo=(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).DAT.ResultCnt));
}else{
(CheckWriteAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).DetailsNo=1);
}
}
if((((unsigned long)(unsigned char)(CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).DetailsNo)>(unsigned long)(unsigned char)(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).DAT.ResultCnt)))){
(CheckWriteAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).DetailsNo=1);
}
(__inst__->DetailsNoOld=(CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).DetailsNo));


for((__inst__->idx=1);__inst__->idx<=MAX_NUM_RESULTS;__inst__->idx+=1){
(__inst__->ResultModelNumber=0);
(__inst__->ResultPositionX=0);
(__inst__->ResultPositionY=0);
(__inst__->ResultOrientation=0);
brsmemset(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)])),0,1504);


if(((((unsigned long)(unsigned char)(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).DAT.ResultCnt)!=(unsigned long)(unsigned char)0))&(((unsigned long)(unsigned short)__inst__->idx<=(unsigned long)(unsigned char)(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).DAT.ResultCnt))))){

(__inst__->MT_UseXY=0);
switch((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.VisionFunction)){
case 1:{
(__inst__->ResultModelNumber=(CheckReadAccess(__inst__->Blob),(*(__inst__->Blob)).ModelNumber[CheckBounds(__inst__->idx,1,10)-1]));
(__inst__->ResultPositionX=((float)(CheckReadAccess(__inst__->Blob),(*(__inst__->Blob)).PositionX[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))));
(__inst__->ResultPositionY=((float)(CheckReadAccess(__inst__->Blob),(*(__inst__->Blob)).PositionY[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))));
(__inst__->ResultOrientation=((CheckReadAccess(__inst__->Blob),(*(__inst__->Blob)).Orientation[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))));
}break;case 6:{
(__inst__->ResultModelNumber=(CheckReadAccess(__inst__->Pixel),(*(__inst__->Pixel)).ModelNumber[CheckBounds(__inst__->idx,1,10)-1]));
(__inst__->ResultPositionX=((float)(CheckReadAccess(__inst__->Pixel),(*(__inst__->Pixel)).PositionX[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))));
(__inst__->ResultPositionY=((float)(CheckReadAccess(__inst__->Pixel),(*(__inst__->Pixel)).PositionY[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))));
}break;case 2:{
(__inst__->ResultPositionX=((float)(CheckReadAccess(__inst__->CodeReader),(*(__inst__->CodeReader)).PositionX[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))));
(__inst__->ResultPositionY=((float)(CheckReadAccess(__inst__->CodeReader),(*(__inst__->CodeReader)).PositionY[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))));
(__inst__->ResultOrientation=((CheckReadAccess(__inst__->CodeReader),(*(__inst__->CodeReader)).Orientation[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))));
}break;case 3:{
(__inst__->ResultModelNumber=(CheckReadAccess(__inst__->Match),(*(__inst__->Match)).ModelNumber[CheckBounds(__inst__->idx,1,10)-1]));
(__inst__->ResultPositionX=((float)(CheckReadAccess(__inst__->Match),(*(__inst__->Match)).PositionX[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))));
(__inst__->ResultPositionY=((float)(CheckReadAccess(__inst__->Match),(*(__inst__->Match)).PositionY[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))));
(__inst__->ResultOrientation=((CheckReadAccess(__inst__->Match),(*(__inst__->Match)).Orientation[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))));
}break;case 4:{
if((CheckReadAccess(__inst__->MT),(*(__inst__->MT)).UseResultAsXY)){
(__inst__->ResultPositionX=((float)(CheckReadAccess(__inst__->MT),(*(__inst__->MT)).Result[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal(10)));
(__inst__->ResultPositionY=((float)(CheckReadAccess(__inst__->MT),(*(__inst__->MT)).Result[CheckBounds((__inst__->idx+1),1,10)-1])/CheckDivReal(10)));
(__inst__->idx=(__inst__->idx+1));
}
(__inst__->MT_UseXY=1);
}break;case 5:{
(__inst__->ResultPositionX=((float)(CheckReadAccess(__inst__->OCR),(*(__inst__->OCR)).PositionX[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))));
(__inst__->ResultPositionY=((float)(CheckReadAccess(__inst__->OCR),(*(__inst__->OCR)).PositionY[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))));
(__inst__->ResultOrientation=((CheckReadAccess(__inst__->OCR),(*(__inst__->OCR)).Orientation[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))));
}break;}


if(((((signed long)(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.VisionFunction)!=(signed long)4))|__inst__->MT_UseXY)){
if((((__inst__->ResultPositionX!=0))&((__inst__->ResultPositionY!=0)))){
(CheckWriteAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).SvgVisible[CheckBounds(__inst__->idx,1,10)-1]=1);


(__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].CrosshairScale=(CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.Scale));
(__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].CrosshairSizeScaled=((float)(CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.Size)*(CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.Scale)));
(__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].CrosshairTopLeftX=(__inst__->ResultPositionX-(__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].CrosshairSizeScaled/CheckDivReal(2.00000000000000000000E+00))));
(__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].CrosshairTopLeftY=(__inst__->ResultPositionY-(__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].CrosshairSizeScaled/CheckDivReal(2.00000000000000000000E+00))));
(__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].CrosshairRotateCenter=(__inst__->ResultOrientation*-1));


if((((unsigned long)(unsigned char)(CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).DetailsNo)==(unsigned long)(unsigned short)__inst__->idx))){
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->svgTrafo; plcstring* zzRValue=(plcstring*)"[{\"select\":\"#CrosshairBlue\",\"display\":false}, {\"select\":\"#CrosshairRed\",\"display\":true, \"translate\":["; for(zzIndex=0; zzIndex<101l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->svgTrafo; plcstring* zzRValue=(plcstring*)"[{\"select\":\"#CrosshairRed\",\"display\":false}, { \"select\":\"#CrosshairBlue\",\"display\":true, \"translate\":["; for(zzIndex=0; zzIndex<102l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}

(__inst__->CrosshSize=(__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].CrosshairSizeScaled/CheckDivReal(2.00000000000000000000E+00)));
(__inst__->ScaleVertical=((float)(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionHeight_Y)/CheckDivReal((float)(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionWidth_X))));
switch(__inst__->ImageRotation_deg){
case 0:{brsitoa((signed short)((__inst__->ResultPositionX-__inst__->CrosshSize)>=0.0?(__inst__->ResultPositionX-__inst__->CrosshSize)+0.5:(__inst__->ResultPositionX-__inst__->CrosshSize)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;case 90:{brsitoa((signed short)(((((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionHeight_Y)-__inst__->ResultPositionY)*__inst__->ScaleVertical)-__inst__->CrosshSize)>=0.0?((((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionHeight_Y)-__inst__->ResultPositionY)*__inst__->ScaleVertical)-__inst__->CrosshSize)+0.5:((((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionHeight_Y)-__inst__->ResultPositionY)*__inst__->ScaleVertical)-__inst__->CrosshSize)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;case 180:{brsitoa((signed short)((((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionWidth_X)-__inst__->ResultPositionX)-__inst__->CrosshSize)>=0.0?(((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionWidth_X)-__inst__->ResultPositionX)-__inst__->CrosshSize)+0.5:(((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionWidth_X)-__inst__->ResultPositionX)-__inst__->CrosshSize)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;case 270:{brsitoa((signed short)(((__inst__->ResultPositionY*__inst__->ScaleVertical)-__inst__->CrosshSize)>=0.0?((__inst__->ResultPositionY*__inst__->ScaleVertical)-__inst__->CrosshSize)+0.5:((__inst__->ResultPositionY*__inst__->ScaleVertical)-__inst__->CrosshSize)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;default: {
brsitoa((signed short)((__inst__->ResultPositionX-__inst__->CrosshSize)>=0.0?(__inst__->ResultPositionX-__inst__->CrosshSize)+0.5:(__inst__->ResultPositionX-__inst__->CrosshSize)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;}
brsstrcat(((unsigned long)(&__inst__->svgTrafo)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->svgTrafo)),((unsigned long)(&",")));
switch(__inst__->ImageRotation_deg){
case 0:{brsitoa((signed short)((__inst__->ResultPositionY-__inst__->CrosshSize)>=0.0?(__inst__->ResultPositionY-__inst__->CrosshSize)+0.5:(__inst__->ResultPositionY-__inst__->CrosshSize)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;case 90:{brsitoa((signed short)(((__inst__->ResultPositionX*__inst__->ScaleVertical)-__inst__->CrosshSize)>=0.0?((__inst__->ResultPositionX*__inst__->ScaleVertical)-__inst__->CrosshSize)+0.5:((__inst__->ResultPositionX*__inst__->ScaleVertical)-__inst__->CrosshSize)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;case 180:{brsitoa((signed short)((((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionHeight_Y)-__inst__->ResultPositionY)-__inst__->CrosshSize)>=0.0?(((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionHeight_Y)-__inst__->ResultPositionY)-__inst__->CrosshSize)+0.5:(((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionHeight_Y)-__inst__->ResultPositionY)-__inst__->CrosshSize)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;case 270:{brsitoa((signed short)(((((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionWidth_X)-__inst__->ResultPositionX)*__inst__->ScaleVertical)-__inst__->CrosshSize)>=0.0?((((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionWidth_X)-__inst__->ResultPositionX)*__inst__->ScaleVertical)-__inst__->CrosshSize)+0.5:((((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionWidth_X)-__inst__->ResultPositionX)*__inst__->ScaleVertical)-__inst__->CrosshSize)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;default: {
brsitoa((signed short)((__inst__->ResultPositionY-__inst__->CrosshSize)>=0.0?(__inst__->ResultPositionY-__inst__->CrosshSize)+0.5:(__inst__->ResultPositionY-__inst__->CrosshSize)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;}
brsstrcat(((unsigned long)(&__inst__->svgTrafo)),((unsigned long)(&__inst__->tmpStr)));

brsstrcat(((unsigned long)(&__inst__->svgTrafo)),((unsigned long)(&"],\"scale\":[")));
brsftoa((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.Scale),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->svgTrafo)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->svgTrafo)),((unsigned long)(&",")));
brsstrcat(((unsigned long)(&__inst__->svgTrafo)),((unsigned long)(&__inst__->tmpStr)));

brsstrcat(((unsigned long)(&__inst__->svgTrafo)),((unsigned long)(&"],\"spin\":[")));
brsftoa((__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].CrosshairRotateCenter-__inst__->ImageRotation_deg),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->svgTrafo)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->svgTrafo)),((unsigned long)(&",")));
brsftoa(((float)(CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.Size)/CheckDivReal(2)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->svgTrafo)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->svgTrafo)),((unsigned long)(&",")));
brsstrcat(((unsigned long)(&__inst__->svgTrafo)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->svgTrafo)),((unsigned long)(&"]}]")));
{int zzIndex; plcstring* zzLValue=(plcstring*)((*(__inst__->visCrossHair)).SvgTransformation[__inst__->idx-1]); plcstring* zzRValue=(plcstring*)__inst__->svgTrafo; for(zzIndex=0; zzIndex<200l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};


if((((unsigned long)(unsigned char)(CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).DetailsNo)==(unsigned long)(unsigned short)__inst__->idx))){
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->svgContent; plcstring* zzRValue=(plcstring*)"<text class=\"cText\" fill=\"red \" font-size=\"22\" font-family=\"Helvetica, Arial, sans-serif\"><tspan class=\"text\" x=\""; for(zzIndex=0; zzIndex<113l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}else{
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->svgContent; plcstring* zzRValue=(plcstring*)"<text class=\"cText\" fill=\"blue\" font-size=\"22\" font-family=\"Helvetica, Arial, sans-serif\"><tspan class=\"text\" x=\""; for(zzIndex=0; zzIndex<113l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}
switch(__inst__->ImageRotation_deg){
case 0:{brsitoa((signed short)((__inst__->ResultPositionX+__inst__->CrosshSize)>=0.0?(__inst__->ResultPositionX+__inst__->CrosshSize)+0.5:(__inst__->ResultPositionX+__inst__->CrosshSize)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;case 90:{brsitoa((signed short)(((((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionHeight_Y)-__inst__->ResultPositionY)*__inst__->ScaleVertical)+__inst__->CrosshSize)>=0.0?((((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionHeight_Y)-__inst__->ResultPositionY)*__inst__->ScaleVertical)+__inst__->CrosshSize)+0.5:((((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionHeight_Y)-__inst__->ResultPositionY)*__inst__->ScaleVertical)+__inst__->CrosshSize)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;case 180:{brsitoa((signed short)((((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionWidth_X)-__inst__->ResultPositionX)+__inst__->CrosshSize)>=0.0?(((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionWidth_X)-__inst__->ResultPositionX)+__inst__->CrosshSize)+0.5:(((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionWidth_X)-__inst__->ResultPositionX)+__inst__->CrosshSize)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;case 270:{brsitoa((signed short)(((__inst__->ResultPositionY*__inst__->ScaleVertical)+__inst__->CrosshSize)>=0.0?((__inst__->ResultPositionY*__inst__->ScaleVertical)+__inst__->CrosshSize)+0.5:((__inst__->ResultPositionY*__inst__->ScaleVertical)+__inst__->CrosshSize)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;default: {
brsitoa((signed short)((__inst__->ResultPositionX+__inst__->CrosshSize)>=0.0?(__inst__->ResultPositionX+__inst__->CrosshSize)+0.5:(__inst__->ResultPositionX+__inst__->CrosshSize)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;}
brsstrcat(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"\" y=\"")));
switch(__inst__->ImageRotation_deg){
case 0:{brsitoa((signed short)((__inst__->ResultPositionY+5)>=0.0?(__inst__->ResultPositionY+5)+0.5:(__inst__->ResultPositionY+5)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;case 90:{brsitoa((signed short)(((__inst__->ResultPositionX*__inst__->ScaleVertical)+5)>=0.0?((__inst__->ResultPositionX*__inst__->ScaleVertical)+5)+0.5:((__inst__->ResultPositionX*__inst__->ScaleVertical)+5)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;case 180:{brsitoa((signed short)((((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionHeight_Y)-__inst__->ResultPositionY)+5)>=0.0?(((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionHeight_Y)-__inst__->ResultPositionY)+5)+0.5:(((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionHeight_Y)-__inst__->ResultPositionY)+5)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;case 270:{brsitoa((signed short)(((((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionWidth_X)-__inst__->ResultPositionX)*__inst__->ScaleVertical)+5)>=0.0?((((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionWidth_X)-__inst__->ResultPositionX)*__inst__->ScaleVertical)+5)+0.5:((((CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.ResolutionWidth_X)-__inst__->ResultPositionX)*__inst__->ScaleVertical)+5)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;default: {
brsitoa((signed short)((__inst__->ResultPositionY+5)>=0.0?(__inst__->ResultPositionY+5)+0.5:(__inst__->ResultPositionY+5)-0.5),((unsigned long)(&__inst__->tmpStr)));
}break;}
brsstrcat(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"\"> ")));

brsitoa(__inst__->idx,((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"</tspan></text>")));


brsstrcpy(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Text)),((unsigned long)(&__inst__->svgContent)));
(__inst__->blueTextPos=FIND(__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Text,"fill=\"blue\""));
if((((signed long)(signed long)(short)__inst__->blueTextPos>(signed long)(signed long)(short)2))){
brsmemcpy(((((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Text))+__inst__->blueTextPos)-1),((unsigned long)(&"fill=\"red \"")),11);
}
{int zzIndex; plcstring* zzLValue=(plcstring*)((*(__inst__->visCrossHair)).SvgContent[__inst__->idx-1]); plcstring* zzRValue=(plcstring*)__inst__->svgContent; for(zzIndex=0; zzIndex<1000l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};


brsstrcat(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"<text class=\"cText\" fill=\"red\" font-size=\"35\" font-family=\"Helvetica, Arial, sans-serif\" x=\"10\" y=\"10\"> ")));
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->CrossHairInfo[1].DataVis; plcstring* zzRValue=(plcstring*)""; for(zzIndex=0; zzIndex<0l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->CrossHairInfo[__inst__->idx].Data; plcstring* zzRValue=(plcstring*)""; for(zzIndex=0; zzIndex<0l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};

if(((((signed long)(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.VisionFunction)==(signed long)1))|(((signed long)(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.VisionFunction)==(signed long)3)))){
CrosshairDetailsValue(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"Model ID: ")),__inst__->ResultModelNumber,__inst__->TextAlignment);
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(1,0,10)].DataVis)),((unsigned long)(&"1, ")));
brsitoa(__inst__->ResultModelNumber,((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&", ")));
}else{
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(1,0,10)].DataVis)),((unsigned long)(&"0, ")));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&"\"none\", ")));
}

CrosshairDetailsValue(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"Pos X: ")),__inst__->ResultPositionX,__inst__->TextAlignment);
CrosshairDetailsValue(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"Pos Y: ")),__inst__->ResultPositionY,__inst__->TextAlignment);
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(1,0,10)].DataVis)),((unsigned long)(&"1, 1, ")));
brsftoa(__inst__->ResultPositionX,((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&", ")));
brsftoa(__inst__->ResultPositionY,((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&", ")));

if((((signed long)(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.VisionFunction)!=(signed long)4))){
CrosshairDetailsValue(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"Orientation: ")),__inst__->ResultOrientation,__inst__->TextAlignment);
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(1,0,10)].DataVis)),((unsigned long)(&"1, ")));
brsftoa(__inst__->ResultOrientation,((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&", ")));
}else{
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(1,0,10)].DataVis)),((unsigned long)(&"0, ")));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&"\"none\", ")));
}

if((((signed long)(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.VisionFunction)==(signed long)1))){
if((((unsigned long)(unsigned char)(CheckReadAccess(__inst__->Blob),(*(__inst__->Blob)).EnhancedBlobInformation)==(unsigned long)(unsigned char)1))){
CrosshairDetailsValue(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"Clipped: ")),(CheckReadAccess(__inst__->Blob),(*(__inst__->Blob)).Clipped[CheckBounds(__inst__->idx,1,10)-1]),__inst__->TextAlignment);
CrosshairDetailsValue(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"Gray: ")),(CheckReadAccess(__inst__->Blob),(*(__inst__->Blob)).MeanGrayValue[CheckBounds(__inst__->idx,1,10)-1]),__inst__->TextAlignment);
CrosshairDetailsValue(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"Length: ")),((float)(CheckReadAccess(__inst__->Blob),(*(__inst__->Blob)).Length[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))),__inst__->TextAlignment);
CrosshairDetailsValue(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"Width: ")),((float)(CheckReadAccess(__inst__->Blob),(*(__inst__->Blob)).Width[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))),__inst__->TextAlignment);
CrosshairDetailsValue(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"Area: ")),((float)(CheckReadAccess(__inst__->Blob),(*(__inst__->Blob)).Area[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))),__inst__->TextAlignment);
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(1,0,10)].DataVis)),((unsigned long)(&"1, 1, 1, 1, 1, ")));
brsitoa((CheckReadAccess(__inst__->Blob),(*(__inst__->Blob)).Clipped[CheckBounds(__inst__->idx,1,10)-1]),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&", ")));
brsitoa((CheckReadAccess(__inst__->Blob),(*(__inst__->Blob)).MeanGrayValue[CheckBounds(__inst__->idx,1,10)-1]),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&", ")));
brsftoa(((float)(CheckReadAccess(__inst__->Blob),(*(__inst__->Blob)).Length[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&", ")));
brsftoa(((float)(CheckReadAccess(__inst__->Blob),(*(__inst__->Blob)).Width[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&", ")));
brsftoa(((float)(CheckReadAccess(__inst__->Blob),(*(__inst__->Blob)).Area[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal((CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).CFG.PixelRatio))),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&", ")));
}else{
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(1,0,10)].DataVis)),((unsigned long)(&"0, 0, 0, 0, 0, ")));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&"\"none\", \"none\", \"none\", \"none\", \"none\", ")));
}
}else{
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(1,0,10)].DataVis)),((unsigned long)(&"0, 0, 0, 0, 0, ")));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&"\"none\", \"none\", \"none\", \"none\", \"none\", ")));
}

if((((signed long)(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.VisionFunction)==(signed long)3))){
CrosshairDetailsValue(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"Score: ")),(CheckReadAccess(__inst__->Match),(*(__inst__->Match)).Score[CheckBounds(__inst__->idx,1,10)-1]),__inst__->TextAlignment);
CrosshairDetailsValue(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"Scale: ")),(CheckReadAccess(__inst__->Match),(*(__inst__->Match)).Scale[CheckBounds(__inst__->idx,1,10)-1]),__inst__->TextAlignment);
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(1,0,10)].DataVis)),((unsigned long)(&"1, 1, ")));
brsitoa((CheckReadAccess(__inst__->Match),(*(__inst__->Match)).Score[CheckBounds(__inst__->idx,1,10)-1]),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&", ")));
brsitoa((CheckReadAccess(__inst__->Match),(*(__inst__->Match)).Scale[CheckBounds(__inst__->idx,1,10)-1]),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&", ")));
}else{
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(1,0,10)].DataVis)),((unsigned long)(&"0, 0, ")));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&"\"none\", \"none\", ")));
}

if((((signed long)(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.VisionFunction)==(signed long)5))){
CrosshairDetailsValue(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"Grading: ")),(CheckReadAccess(__inst__->OCR),(*(__inst__->OCR)).Grading[CheckBounds(__inst__->idx,1,10)-1]),__inst__->TextAlignment);
CrosshairDetailsText(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"Text: ")),((unsigned long)(&((*(__inst__->OCR)).Text[CheckBounds(__inst__->idx,1,10)-1]))),__inst__->TextAlignment);
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(1,0,10)].DataVis)),((unsigned long)(&"1, 0, 1, ")));
brsitoa((CheckReadAccess(__inst__->OCR),(*(__inst__->OCR)).Grading[CheckBounds(__inst__->idx,1,10)-1]),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&", \"none\", \"")));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&((*(__inst__->OCR)).Text[CheckBounds(__inst__->idx,1,10)-1]))));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&"\", ")));
}else if((((signed long)(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).CFG.VisionFunction)==(signed long)2))){
CrosshairDetailsText(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"Code: ")),((unsigned long)(&((*(__inst__->CodeTypes))[CheckBounds(((unsigned long)(unsigned char)0>(unsigned long)(unsigned char)MAX_NUM_CODETYPES?MAX_NUM_CODETYPES:(unsigned long)(unsigned char)(CheckReadAccess(__inst__->CodeReader),(*(__inst__->CodeReader)).BarcodeType[CheckBounds(__inst__->idx,1,10)-1])<(unsigned long)(unsigned char)0?0:(unsigned long)(unsigned char)(CheckReadAccess(__inst__->CodeReader),(*(__inst__->CodeReader)).BarcodeType[CheckBounds(__inst__->idx,1,10)-1])>(unsigned long)(unsigned char)MAX_NUM_CODETYPES?MAX_NUM_CODETYPES:(CheckReadAccess(__inst__->CodeReader),(*(__inst__->CodeReader)).BarcodeType[CheckBounds(__inst__->idx,1,10)-1])),0,69)]))),__inst__->TextAlignment);
CrosshairDetailsText(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"Text: ")),((unsigned long)(&((*(__inst__->CodeReader)).BarcodeText[CheckBounds(__inst__->idx,1,10)-1]))),__inst__->TextAlignment);
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(1,0,10)].DataVis)),((unsigned long)(&"0, 1, 1, ")));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&"\"none\", \"")));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&((*(__inst__->CodeTypes))[CheckBounds(((unsigned long)(unsigned char)0>(unsigned long)(unsigned char)MAX_NUM_CODETYPES?MAX_NUM_CODETYPES:(unsigned long)(unsigned char)(CheckReadAccess(__inst__->CodeReader),(*(__inst__->CodeReader)).BarcodeType[CheckBounds(__inst__->idx,1,10)-1])<(unsigned long)(unsigned char)0?0:(unsigned long)(unsigned char)(CheckReadAccess(__inst__->CodeReader),(*(__inst__->CodeReader)).BarcodeType[CheckBounds(__inst__->idx,1,10)-1])>(unsigned long)(unsigned char)MAX_NUM_CODETYPES?MAX_NUM_CODETYPES:(CheckReadAccess(__inst__->CodeReader),(*(__inst__->CodeReader)).BarcodeType[CheckBounds(__inst__->idx,1,10)-1])),0,69)]))));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&"\", \"")));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&((*(__inst__->CodeReader)).BarcodeText[CheckBounds(__inst__->idx,1,10)-1]))));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&"\", ")));
}else{
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(1,0,10)].DataVis)),((unsigned long)(&"0, 0, 0, ")));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&"\"none\", \"none\", \"none\", ")));
}
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(1,0,10)].DataVis)),((unsigned long)(&"0")));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&"\"none\"")));
brsstrcat(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"</text>")));

if((((unsigned long)(unsigned char)(CheckReadAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).DetailsNo)==(unsigned long)(unsigned short)__inst__->idx))){
{int zzIndex; plcstring* zzLValue=(plcstring*)((*(__inst__->visCrossHair)).SvgContent[__inst__->idx-1]); plcstring* zzRValue=(plcstring*)__inst__->svgContent; for(zzIndex=0; zzIndex<1000l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}
}
}else{
{int zzIndex; plcstring* zzLValue=(plcstring*)((*(__inst__->visCrossHair)).SvgTransformation[__inst__->idx-1]); plcstring* zzRValue=(plcstring*)"[{\"select\":\"#CrosshairRed\",\"display\":false}, {\"select\":\"#CrosshairBlue\",\"display\":false}]"; for(zzIndex=0; zzIndex<89l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(CheckWriteAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).SvgVisible[CheckBounds(__inst__->idx,1,10)-1]=1);
brsstrcpy(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"<text class=\"cText\" fill=\"red\" font-size=\"35\" font-family=\"Helvetica, Arial, sans-serif\" x=\"10\" y=\"")));
brsitoa((__inst__->idx*40),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"\"> ")));
CrosshairDetailsValue(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"Result:")),((float)(CheckReadAccess(__inst__->MT),(*(__inst__->MT)).Result[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal(1000)),__inst__->TextAlignment);
brsstrcat(((unsigned long)(&__inst__->svgContent)),((unsigned long)(&"</text>")));
{int zzIndex; plcstring* zzLValue=(plcstring*)((*(__inst__->visCrossHair)).SvgContent[__inst__->idx-1]); plcstring* zzRValue=(plcstring*)__inst__->svgContent; for(zzIndex=0; zzIndex<1000l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
brsstrcpy(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(1,0,10)].DataVis)),((unsigned long)(&"0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1")));
brsstrcpy(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&"\"none\", \"none\", \"none\", \"none\", \"none\", \"none\", \"none\", \"none\", \"none\", \"none\", \"none\", \"none\", \"none\", \"none\", ")));
brsftoa(((float)(CheckReadAccess(__inst__->MT),(*(__inst__->MT)).Result[CheckBounds(__inst__->idx,1,10)-1])/CheckDivReal(1000)),((unsigned long)(&__inst__->tmpStr)));
brsstrcat(((unsigned long)(&__inst__->CrossHairInfo[CheckBounds(__inst__->idx,0,10)].Data)),((unsigned long)(&__inst__->tmpStr)));
}
}else if(((((unsigned long)(unsigned char)(CheckReadAccess(__inst__->VisionSensor),(*(__inst__->VisionSensor)).DAT.ResultCnt)==(unsigned long)(unsigned char)0))&__inst__->VisionDisabled)){
(CheckWriteAccess(__inst__->visCrossHair),(*(__inst__->visCrossHair)).SvgVisible[CheckBounds(1,1,10)-1]=1);
{int zzIndex; plcstring* zzLValue=(plcstring*)((*(__inst__->visCrossHair)).SvgTransformation[1-1]); plcstring* zzRValue=(plcstring*)"[{\"select\":\"#CrosshairRed\",\"display\":false}, {\"select\":\"#CrosshairBlue\",\"display\":false}]"; for(zzIndex=0; zzIndex<89l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
{int zzIndex; plcstring* zzLValue=(plcstring*)((*(__inst__->visCrossHair)).SvgContent[1-1]); plcstring* zzRValue=(plcstring*)"<text class=\"cText\" fill=\"red\" font-size=\"35\" font-family=\"Helvetica, Arial, sans-serif\" x=\"10\" y=\"40\"> Processing is disabled </text>"; for(zzIndex=0; zzIndex<134l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}
}imp1_endfor7_0:;
}
}
}imp1_else3_0:imp1_end3_0:imp1_else2_0:imp1_end2_0:;}
#line 323 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViDrawCrosshair.nodebug"

void __AS__ImplInitViDrawCrosshair_st(void){}

__asm__(".section \".plc\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/VisionTypes.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/mappRecipe/RecTypes.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBaseCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAxCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxisCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxis.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxisError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipe.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipeAlarm.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipeError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpComError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/TcpForward/Types.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/IecCheck/IecCheck.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxis.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipe.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/TcpForward/TcpForward.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/VisionVariables.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/mappRecipe/RecVariables.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/McElements.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/MpComponents.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/MpVisionComponents.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/Vi_image/Types.typ\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/Vi_image/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/Vi_image/LocalFunctions.fun\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_image/ViDrawCrosshair.st.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"plcreplace \\\"C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_image/ViDrawCrosshair.st.c\\\" \\\"C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViDrawCrosshair.st\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViDownloadImage\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViRefreshImageList\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViSaveImgOnPlc\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViDrawCrosshair\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViCreateWebDirFile\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViShowImgOnVC4\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfun \\\"CrosshairDetailsValue\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfun \\\"CrosshairDetailsText\\\"\\n\"");
__asm__(".previous");

__asm__(".section \".plciec\"");
__asm__(".ascii \"plcdata_const 'MAX_NUM_RESULTS'\\n\"");
__asm__(".ascii \"plcdata_const 'MAX_NUM_CODETYPES'\\n\"");
__asm__(".previous");
