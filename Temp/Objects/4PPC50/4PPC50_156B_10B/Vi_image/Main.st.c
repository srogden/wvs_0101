#define _DEFAULT_INCLUDE
#include <bur\plctypes.h>
#include "C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_image/Mainst.h"
#line 1 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/Main.nodebug"
#line 9 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/Main.st"
void __BUR__ENTRY_INIT_FUNCT__(void){{
if((((unsigned long)RemCfgImage.ConvertCycles==(unsigned long)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)gVisionImage.CFG.FileDevice; plcstring* zzRValue=(plcstring*)DEVICE_NAME; for(zzIndex=0; zzIndex<40l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
{int zzIndex; plcstring* zzLValue=(plcstring*)gVisionImage.CFG.DirName; plcstring* zzRValue=(plcstring*)"Vision"; for(zzIndex=0; zzIndex<6l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
{int zzIndex; plcstring* zzLValue=(plcstring*)gVisionImage.CFG.PlkIPWithoutNode; plcstring* zzRValue=(plcstring*)"192.168.200."; for(zzIndex=0; zzIndex<12l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
{int zzIndex; plcstring* zzLValue=(plcstring*)gVisionImage.CFG.EthDevice; plcstring* zzRValue=(plcstring*)"IF2"; for(zzIndex=0; zzIndex<3l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(gVisionImage.CFG.ConvertCycles=10000);
(gVisionImage.CFG.Format=0);
(gVisionImage.CFG.QualityJPG=80);
(gVisionImage.CFG.UploadBmpJpg=1);
(gVisionImage.CFG.UploadSVG=1);
(gVisionImage.CFG.ImageRotation_deg=0);
(gVisionImage.CFG.MainPageQualityJPG=30);
}else{
brsmemcpy(((unsigned long)(&gVisionImage.CFG)),((unsigned long)(&RemCfgImage)),420);
}
(gVisionImage.CMD.CreateDir=1);
{int zzIndex; plcstring* zzLValue=(plcstring*)visSelectedImage; plcstring* zzRValue=(plcstring*)""; for(zzIndex=0; zzIndex<0l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};

ViDownloadImage(&ViDownloadImage_0);
ViSaveImgOnPlc(&ViSaveImgOnPlc_0);

(CfgGetIPAddr_0.enable=1);
(CfgGetIPAddr_0.Len=81);
(CfgGetIPAddr_0.pDevice=((unsigned long)(&gVisionImage.CFG.EthDevice)));
(CfgGetIPAddr_0.pIPAddr=((unsigned long)(&gVisionImage.CFG.EthIP)));
CfgGetIPAddr(&CfgGetIPAddr_0);

(TcpForward_0.ServerPort=8888);
(TcpForward_0.ConnectToPort=8080);
}}
#line 39 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/Main.nodebug"
#line 40 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/Main.st"
void _CYCLIC __BUR__ENTRY_CYCLIC_FUNCT__(void){{
switch(Step){
case 0:{
if(gVisionImage.CMD.CreateDir){
(gVisionImage.CMD.CreateDir=0);
(gVisionImage.DAT.Status=ERR_FUB_BUSY);
(Step=1);
}else if(gVisionImage.CMD.DeleteDir){
(gVisionImage.CMD.DeleteDir=0);
(gVisionImage.DAT.Status=ERR_FUB_BUSY);
(Step=2);
}else if(gVisionImage.CMD.DeleteImage){
(gVisionImage.CMD.DeleteImage=0);
if((((unsigned long)brsstrlen(((unsigned long)(&visSelectedImage)))>(unsigned long)0))){
brsstrcpy(((unsigned long)(&FileNameComplete)),((unsigned long)(&gVisionImage.CFG.DirName)));
brsstrcat(((unsigned long)(&FileNameComplete)),((unsigned long)(&"/")));
brsstrcat(((unsigned long)(&FileNameComplete)),((unsigned long)(&visSelectedImage)));
(gVisionImage.DAT.Status=ERR_FUB_BUSY);
(Step=3);
}else{
(gVisionImage.CMD.DeleteImage=0);
}
}else if(gVisionImage.CMD.Refresh){
(gVisionImage.CMD.Refresh=0);
(gVisionImage.DAT.Status=ERR_FUB_BUSY);
(Step=4);
}else if(gVisionImage.CMD.DownloadImage){
(gVisionImage.CMD.DownloadImage=0);
if((((unsigned long)brsstrlen(((unsigned long)(&visSelectedImage)))>(unsigned long)0))){
brsstrcpy(((unsigned long)(&FileNameComplete)),((unsigned long)(&gVisionImage.CFG.DirName)));
brsstrcat(((unsigned long)(&FileNameComplete)),((unsigned long)(&"/")));
brsstrcat(((unsigned long)(&FileNameComplete)),((unsigned long)(&visSelectedImage)));
(gVisionImage.DAT.Status=ERR_FUB_BUSY);
(Step=5);
}else{
}
}else if(gVisionImage.CMD.SaveImageOnPLC){
(gVisionImage.CMD.SaveImageOnPLC=0);
if(((gVisionImage.CFG.UploadBmpJpg^1)&(gVisionImage.CFG.UploadSVG^1))){
(gVisionImage.CMD.SaveImageOnPLC=0);
}else{
(gVisionImage.DAT.Status=ERR_FUB_BUSY);
(Step=6);
}
}else if(gVisionImage.CMD.ResetError){
(gVisionImage.CMD.ResetError=0);
}


if((((__AS__STRING_CMP(visSelectedImageOld,visSelectedImage)!=0))&(((unsigned long)(unsigned short)ViRefreshImageList_0.DirEntries>(unsigned long)(unsigned short)0)))){
brsmemset(((unsigned long)(&visImagePath)),0,81);
{int zzIndex; plcstring* zzLValue=(plcstring*)visImagePath; plcstring* zzRValue=(plcstring*)"/FileDevice:"; for(zzIndex=0; zzIndex<12l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
brsstrcat(((unsigned long)(&visImagePath)),((unsigned long)(&gVisionImage.CFG.FileDevice)));
brsstrcat(((unsigned long)(&visImagePath)),((unsigned long)(&"/")));
brsstrcat(((unsigned long)(&visImagePath)),((unsigned long)(&gVisionImage.CFG.DirName)));
brsstrcat(((unsigned long)(&visImagePath)),((unsigned long)(&"/")));
brsstrcat(((unsigned long)(&visImagePath)),((unsigned long)(&visSelectedImage)));
}else if((((unsigned long)(unsigned short)ViRefreshImageList_0.DirEntries==(unsigned long)(unsigned short)0))){
brsmemset(((unsigned long)(&visImagePath)),0,81);
}
{int zzIndex; plcstring* zzLValue=(plcstring*)visSelectedImageOld; plcstring* zzRValue=(plcstring*)visSelectedImage; for(zzIndex=0; zzIndex<80l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};

(visEnableButtons_1=(((signed long)Step==(signed long)0)));
(visEnableButtons_2=(visEnableButtons_1&(((unsigned long)(unsigned short)ViRefreshImageList_0.DirEntries>(unsigned long)(unsigned short)0))&(((signed long)Step==(signed long)0))));

}break;case 1:{
(DirCreate_0.enable=1);
(DirCreate_0.pDevice=((unsigned long)(&gVisionImage.CFG.FileDevice)));
(DirCreate_0.pName=((unsigned long)(&gVisionImage.CFG.DirName)));
DirCreate(&DirCreate_0);
if(((((unsigned long)(unsigned short)DirCreate_0.status==(unsigned long)(unsigned short)ERR_OK))|(((unsigned long)(unsigned short)DirCreate_0.status==(unsigned long)(unsigned short)fiERR_DIR_ALREADY_EXIST)))){
(Step=4);
}else if((((unsigned long)(unsigned short)DirCreate_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(gVisionImage.DAT.Status=DirCreate_0.status);
(Step=7);
}

}break;case 2:{
(DirDeleteEx_0.enable=1);
(DirDeleteEx_0.pDevice=((unsigned long)(&gVisionImage.CFG.FileDevice)));
(DirDeleteEx_0.pName=((unsigned long)(&gVisionImage.CFG.DirName)));
DirDeleteEx(&DirDeleteEx_0);
if((((unsigned long)(unsigned short)DirDeleteEx_0.status==(unsigned long)(unsigned short)ERR_OK))){
(Step=1);
}else if((((unsigned long)(unsigned short)DirDeleteEx_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(gVisionImage.DAT.Status=DirDeleteEx_0.status);
(Step=7);
}

}break;case 3:{
(FileDelete_0.enable=1);
(FileDelete_0.pDevice=((unsigned long)(&gVisionImage.CFG.FileDevice)));
(FileDelete_0.pName=((unsigned long)(&FileNameComplete)));
FileDelete(&FileDelete_0);
if((((unsigned long)(unsigned short)FileDelete_0.status==(unsigned long)(unsigned short)ERR_OK))){
(Step=4);
}else if((((unsigned long)(unsigned short)FileDelete_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(gVisionImage.DAT.Status=FileDelete_0.status);
(Step=7);
}

}break;case 4:{
(ViRefreshImageList_0.Enable=1);
((*(unsigned long*)&(ViRefreshImageList_0.VisionImage))=((unsigned long)(&gVisionImage)));
((*(unsigned long*)&(ViRefreshImageList_0.ImageList))=((unsigned long)(&visImageList)));
((*(unsigned long*)&(ViRefreshImageList_0.visSelectedImage))=((unsigned long)(&visSelectedImage)));
ViRefreshImageList(&ViRefreshImageList_0);
if((((unsigned long)(unsigned short)ViRefreshImageList_0.Status==(unsigned long)(unsigned short)ERR_OK))){
(gVisionImage.DAT.Status=0);
(Step=0);
}else if((((unsigned long)(unsigned short)ViRefreshImageList_0.Status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(gVisionImage.DAT.Status=ViRefreshImageList_0.Status);
(Step=7);
}

}break;case 5:{
(ViDownloadImage_0.Enable=1);
{int zzIndex; plcstring* zzLValue=(plcstring*)ViDownloadImage_0.FileDevice; plcstring* zzRValue=(plcstring*)gVisionImage.CFG.FileDevice; for(zzIndex=0; zzIndex<80l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
{int zzIndex; plcstring* zzLValue=(plcstring*)ViDownloadImage_0.FileName; plcstring* zzRValue=(plcstring*)FileNameComplete; for(zzIndex=0; zzIndex<80l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
{int zzIndex; plcstring* zzLValue=(plcstring*)ViDownloadImage_0.ImgName; plcstring* zzRValue=(plcstring*)visSelectedImage; for(zzIndex=0; zzIndex<80l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
((*(unsigned long*)&(ViDownloadImage_0.visDownloadFileUrl))=((unsigned long)(&visDownloadFileUrl)));
ViDownloadImage(&ViDownloadImage_0);
if((((unsigned long)(unsigned short)ViDownloadImage_0.Status==(unsigned long)(unsigned short)ERR_OK))){
(gVisionImage.DAT.Status=0);
(Step=0);
}else if((((unsigned long)(unsigned short)ViDownloadImage_0.Status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(gVisionImage.DAT.Status=ViDownloadImage_0.Status);
(Step=7);
}

}break;case 6:{
(ViSaveImgOnPlc_0.Enable=1);
(ViSaveImgOnPlc_0.CFG=*(struct typVisionImageConfig*)&gVisionImage.CFG);
(ViSaveImgOnPlc_0.PowerlinkNode=gVisionSensor[CheckBounds(((unsigned long)(unsigned short)1>(unsigned long)(unsigned short)MAX_NUM_CAMS?MAX_NUM_CAMS:(unsigned long)(unsigned short)visSelectedSensor<(unsigned long)(unsigned short)1?1:(unsigned long)(unsigned short)visSelectedSensor>(unsigned long)(unsigned short)MAX_NUM_CAMS?MAX_NUM_CAMS:visSelectedSensor),0,6)].CFG.PowerlinkNode);
((*(unsigned long*)&(ViSaveImgOnPlc_0.CrossHairInfo))=((unsigned long)(&ViDrawCrosshair_0.CrossHairInfo)));
ViSaveImgOnPlc(&ViSaveImgOnPlc_0);
if((((unsigned long)(unsigned short)ViSaveImgOnPlc_0.Status==(unsigned long)(unsigned short)ERR_OK))){
(Step=4);
}else if((((unsigned long)(unsigned short)ViSaveImgOnPlc_0.Status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(gVisionImage.DAT.Status=ViSaveImgOnPlc_0.Status);
(Step=7);
}

}break;case 7:{
if(gVisionImage.CMD.ResetError){
(gVisionImage.CMD.ResetError=0);
(gVisionImage.DAT.Status=0);
(Step=0);
}
brsmemset(((unsigned long)(&gVisionImage.CMD)),0,8);
}break;}


{int zzIndex; plcstring* zzLValue=(plcstring*)ipSTR; plcstring* zzRValue=(plcstring*)gVisionImage.CFG.PlkIPWithoutNode; for(zzIndex=0; zzIndex<80l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
brsitoa(visSelectedSensor,((unsigned long)(&nodeSTR)));
brsstrcat(((unsigned long)(&ipSTR)),((unsigned long)(&nodeSTR)));
if((((signed long)brsstrcmp(((unsigned long)(&TcpForward_0.ConnectToIp)),((unsigned long)(&ipSTR)))!=(signed long)0))){
{int zzIndex; plcstring* zzLValue=(plcstring*)TcpForward_0.ConnectToIp; plcstring* zzRValue=(plcstring*)ipSTR; for(zzIndex=0; zzIndex<24l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(TcpForward_0.Enable=0);
}else{
(TcpForward_0.Enable=gVisionSensor[CheckBounds(visSelectedSensor,0,6)].HW.Connected);
}
TcpForward(&TcpForward_0);


(ViDrawCrosshair_0.SelectedSensor=visSelectedSensor);
(ViDrawCrosshair_0.CmdRefreshCrosshair=gVisionImage.CMD.RefreshCrosshair);
((*(unsigned long*)&(ViDrawCrosshair_0.VisionSensor))=((unsigned long)(&gVisionSensor[CheckBounds(visSelectedSensor,0,6)])));
((*(unsigned long*)&(ViDrawCrosshair_0.CodeTypes))=((unsigned long)(&CodeTypes)));
(ViDrawCrosshair_0.VisionDisabled=gVisionImage.DAT.VisionDisabled);
(ViDrawCrosshair_0.ImageRotation_deg=gVisionImage.CFG.ImageRotation_deg);
(ViDrawCrosshair_0.TextAlignment=visTextAlignment);
((*(unsigned long*)&(ViDrawCrosshair_0.visCrossHair))=((unsigned long)(&visCrossHair)));
ViDrawCrosshair(&ViDrawCrosshair_0);
(gVisionImage.CMD.RefreshCrosshair=0);


if((ViCreateWebDirFile_0.Done^1)){
(ViCreateWebDirFile_0.Enable=1);
{int zzIndex; plcstring* zzLValue=(plcstring*)ViCreateWebDirFile_0.FileDevUser; plcstring* zzRValue=(plcstring*)gVisionImage.CFG.FileDevice; for(zzIndex=0; zzIndex<80l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
{int zzIndex; plcstring* zzLValue=(plcstring*)ViCreateWebDirFile_0.EthDevice; plcstring* zzRValue=(plcstring*)gVisionImage.CFG.EthDevice; for(zzIndex=0; zzIndex<80l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
{int zzIndex; plcstring* zzLValue=(plcstring*)ViCreateWebDirFile_0.EthIP; plcstring* zzRValue=(plcstring*)gVisionImage.CFG.EthIP; for(zzIndex=0; zzIndex<80l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
((*(unsigned long*)&(ViCreateWebDirFile_0.visWebViewerPath))=((unsigned long)(&visWebViewerPath)));
ViCreateWebDirFile(&ViCreateWebDirFile_0);
}
{int zzIndex; plcstring* zzLValue=(plcstring*)visWebViewerQuery; plcstring* zzRValue=(plcstring*)"q="; for(zzIndex=0; zzIndex<2l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
brsitoa(gVisionImage.CFG.MainPageQualityJPG,((unsigned long)(&tmpStr)));
brsstrcat(((unsigned long)(&visWebViewerQuery)),((unsigned long)(&tmpStr)));
brsstrcat(((unsigned long)(&visWebViewerQuery)),((unsigned long)(&"&rot=")));
brsitoa(gVisionImage.CFG.ImageRotation_deg,((unsigned long)(&tmpStr)));
brsstrcat(((unsigned long)(&visWebViewerQuery)),((unsigned long)(&tmpStr)));
brsstrcat(((unsigned long)(&visWebViewerQuery)),((unsigned long)(&"&scale=")));
switch(gVisionImage.CFG.ImageRotation_deg){
case 0:{brsftoa(1.00000000000000000000E+00,((unsigned long)(&tmpStr)));
}break;case 90:{brsftoa(((float)gVisionSensor[CheckBounds(visSelectedSensor,0,6)].CFG.ResolutionHeight_Y/CheckDivReal((float)gVisionSensor[CheckBounds(visSelectedSensor,0,6)].CFG.ResolutionWidth_X)),((unsigned long)(&tmpStr)));
}break;case 180:{brsftoa(1.00000000000000000000E+00,((unsigned long)(&tmpStr)));
}break;case 270:{brsftoa(((float)gVisionSensor[CheckBounds(visSelectedSensor,0,6)].CFG.ResolutionHeight_Y/CheckDivReal((float)gVisionSensor[CheckBounds(visSelectedSensor,0,6)].CFG.ResolutionWidth_X)),((unsigned long)(&tmpStr)));
}break;default: {
brsftoa(1.00000000000000000000E+00,((unsigned long)(&tmpStr)));
}break;}
brsstrcat(((unsigned long)(&visWebViewerQuery)),((unsigned long)(&tmpStr)));


(gVisionImage.CFG.ImageRotation_deg=(visImageRotation*90));


(ViShowImgOnVC4_0.Enable=ViCreateWebDirFile_0.Done);
(ViShowImgOnVC4_0.RefreshImage=visVC4RefreshImg);
(ViShowImgOnVC4_0.CFG=*(struct typVisionImageConfig*)&gVisionImage.CFG);
(ViShowImgOnVC4_0.PowerlinkNode=gVisionSensor[CheckBounds(((unsigned long)(unsigned short)1>(unsigned long)(unsigned short)MAX_NUM_CAMS?MAX_NUM_CAMS:(unsigned long)(unsigned short)visSelectedSensor<(unsigned long)(unsigned short)1?1:(unsigned long)(unsigned short)visSelectedSensor>(unsigned long)(unsigned short)MAX_NUM_CAMS?MAX_NUM_CAMS:visSelectedSensor),0,6)].CFG.PowerlinkNode);
(ViShowImgOnVC4_0.ImgWidthInVC4_px=500);
ViShowImgOnVC4(&ViShowImgOnVC4_0);
if(ViShowImgOnVC4_0.RefreshDone){
(visVC4RefreshImg=0);
}


if((~gVisionSensor[CheckBounds(visSelectedSensor,0,6)].CMD.ImageTrigger&Edge0000400000&1?((Edge0000400000=gVisionSensor[CheckBounds(visSelectedSensor,0,6)].CMD.ImageTrigger&1),1):((Edge0000400000=gVisionSensor[CheckBounds(visSelectedSensor,0,6)].CMD.ImageTrigger&1),0))){
(gVisionImage.CMD.RefreshCrosshair=1);
}
(gVisionImage.DAT.VisionDisabled=(gVisionSensor[CheckBounds(visSelectedSensor,0,6)].DAT.Enable^1));

brsmemcpy(((unsigned long)(&RemCfgImage)),((unsigned long)(&gVisionImage.CFG)),420);
}}
#line 263 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/Main.nodebug"
#line 266 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/Main.st"
void _EXIT __BUR__ENTRY_EXIT_FUNCT__(void){{


(TcpForward_0.Enable=0);
TcpForward(&TcpForward_0);

}}
#line 272 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/Main.nodebug"

void __AS__ImplInitMain_st(void){__BUR__ENTRY_INIT_FUNCT__();}

signed long __AS__STRING_CMP(char* pstr1, char* pstr2)
{while (*pstr1 != 0 && *pstr1 == *pstr2){ pstr1++;pstr2++; } return (*pstr1 == 0 && *pstr2 != 0) ? -1 : (*pstr1 != 0 && *pstr2 == 0) ? 1 : *pstr1 - *pstr2;}

__asm__(".section \".plc\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/VisionTypes.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/mappRecipe/RecTypes.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBaseCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAxCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxisCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxis.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxisError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipe.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipeAlarm.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipeError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpComError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/TcpForward/Types.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/IecCheck/IecCheck.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxis.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipe.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/TcpForward/TcpForward.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/VisionVariables.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/mappRecipe/RecVariables.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/McElements.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/MpComponents.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/MpVisionComponents.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/Vi_image/Types.typ\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/Vi_image/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/Vi_image/LocalFunctions.fun\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_image/Main.st.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"plcreplace \\\"C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_image/Main.st.c\\\" \\\"C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/Main.st\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_image/Main.st.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViDownloadImage\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViRefreshImageList\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViSaveImgOnPlc\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViDrawCrosshair\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViCreateWebDirFile\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViShowImgOnVC4\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfun \\\"CrosshairDetailsValue\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfun \\\"CrosshairDetailsText\\\"\\n\"");
__asm__(".previous");

__asm__(".section \".plciec\"");
__asm__(".ascii \"plcdata_const 'RemCfgImage'\\n\"");
__asm__(".ascii \"plcdata_const 'visImagePath'\\n\"");
__asm__(".ascii \"plcdata_const 'gVisionImage'\\n\"");
__asm__(".ascii \"plcdata_const 'MAX_NUM_CAMS'\\n\"");
__asm__(".ascii \"plcdata_const 'MAX_NUM_RESULTS'\\n\"");
__asm__(".ascii \"plcdata_const 'DEVICE_NAME'\\n\"");
__asm__(".ascii \"plcdata_const 'MAX_NUM_CODETYPES'\\n\"");
__asm__(".ascii \"plcdata_const 'ERR_OK'\\n\"");
__asm__(".ascii \"plcdata_const 'ERR_FUB_BUSY'\\n\"");
__asm__(".ascii \"plcdata_const 'fiERR_DIR_ALREADY_EXIST'\\n\"");
__asm__(".previous");
