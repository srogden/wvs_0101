#define _DEFAULT_INCLUDE
#include <bur\plctypes.h>
#include "C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_image/ViCreateWebDirFilest.h"
#line 1 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViCreateWebDirFile.nodebug"
#line 1 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViCreateWebDirFile.st"
void ViCreateWebDirFile(struct ViCreateWebDirFile* inst){struct ViCreateWebDirFile* __inst__=inst;{
if((__inst__->Enable^1)){
(__inst__->Status=ERR_FUB_ENABLE_FALSE);
(__inst__->Step=0);
return;
}
switch(__inst__->Step){
case 0:{
(__inst__->Status=ERR_FUB_BUSY);
(__inst__->Done=0);
(__inst__->Step=1);
}break;case 1:{
__AS__Action_ViCreateWebDirFile_ACTION_CreateHtmlFile(inst);
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->FileDevSystem; plcstring* zzRValue=(plcstring*)"CF"; for(zzIndex=0; zzIndex<2l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->FileDevSystemPara; plcstring* zzRValue=(plcstring*)"/DEVICE=C:/"; for(zzIndex=0; zzIndex<11l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->DirNameWeb; plcstring* zzRValue=(plcstring*)"web"; for(zzIndex=0; zzIndex<3l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->FileNameVisImgHtml; plcstring* zzRValue=(plcstring*)"web/VisionImg.html"; for(zzIndex=0; zzIndex<18l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->FileNameEyeSystem; plcstring* zzRValue=(plcstring*)"ADDONS/DATA/IAT_Data/wwwRoot/BRVisu/Media/VisionEye.png"; for(zzIndex=0; zzIndex<55l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->FileNameEyeUser; plcstring* zzRValue=(plcstring*)"web/VisionEye.png"; for(zzIndex=0; zzIndex<17l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(__inst__->Step=2);
}break;case 2:{
(__inst__->DirInfo_0.enable=1);;(__inst__->DirInfo_0.pDevice=((unsigned long)(&__inst__->FileDevUser)));;(__inst__->DirInfo_0.pPath=((unsigned long)(&__inst__->DirNameWeb)));;DirInfo(&__inst__->DirInfo_0);
if((((unsigned long)(unsigned short)__inst__->DirInfo_0.status==(unsigned long)(unsigned short)ERR_OK))){
(__inst__->Step=9);
}else if((((unsigned long)(unsigned short)__inst__->DirInfo_0.status==(unsigned long)(unsigned short)fiERR_DIR_NOT_EXIST))){
(__inst__->Step=3);
}else if((((unsigned long)(unsigned short)__inst__->DirInfo_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->DirInfo_0.status);
(__inst__->Step=0);
}
}break;case 3:{
(__inst__->DirCreate_0.enable=1);;(__inst__->DirCreate_0.pDevice=((unsigned long)(&__inst__->FileDevUser)));;(__inst__->DirCreate_0.pName=((unsigned long)(&__inst__->DirNameWeb)));;DirCreate(&__inst__->DirCreate_0);
if((((unsigned long)(unsigned short)__inst__->DirCreate_0.status==(unsigned long)(unsigned short)ERR_OK))){
(__inst__->Step=10);
}else if((((unsigned long)(unsigned short)__inst__->DirCreate_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->DirCreate_0.status);
(__inst__->Step=0);
}
}break;case 9:{
(__inst__->FileDelete_0.enable=1);;(__inst__->FileDelete_0.pDevice=((unsigned long)(&__inst__->FileDevUser)));;(__inst__->FileDelete_0.pName=((unsigned long)(&__inst__->FileNameVisImgHtml)));;FileDelete(&__inst__->FileDelete_0);
if(((((unsigned long)(unsigned short)__inst__->FileDelete_0.status==(unsigned long)(unsigned short)ERR_OK))|(((unsigned long)(unsigned short)__inst__->FileDelete_0.status==(unsigned long)(unsigned short)fiERR_FILE_NOT_FOUND)))){
(__inst__->Step=10);
}else if((((unsigned long)(unsigned short)__inst__->FileDelete_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->FileDelete_0.status);
(__inst__->Step=0);
}
}break;case 10:{
(__inst__->FileCreate_0.enable=1);;(__inst__->FileCreate_0.pDevice=((unsigned long)(&__inst__->FileDevUser)));;(__inst__->FileCreate_0.pFile=((unsigned long)(&__inst__->FileNameVisImgHtml)));;FileCreate(&__inst__->FileCreate_0);
if((((unsigned long)(unsigned short)__inst__->FileCreate_0.status==(unsigned long)(unsigned short)ERR_OK))){
brsstrcpy(((unsigned long)(&__inst__->FileWriteData)),((unsigned long)(&__inst__->HtmlFileContent1)));
brsstrcat(((unsigned long)(&__inst__->FileWriteData)),((unsigned long)(&__inst__->EthIP)));
brsstrcat(((unsigned long)(&__inst__->FileWriteData)),((unsigned long)(&__inst__->HtmlFileContent2)));
(__inst__->Step=11);
}else if((((unsigned long)(unsigned short)__inst__->FileCreate_0.status==(unsigned long)(unsigned short)fiERR_EXIST))){
(__inst__->Step=20);
}else if((((unsigned long)(unsigned short)__inst__->FileCreate_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->FileCreate_0.status);
(__inst__->Step=0);
}
}break;case 11:{
(__inst__->FileWrite_0.enable=1);;(__inst__->FileWrite_0.ident=__inst__->FileCreate_0.ident);;(__inst__->FileWrite_0.offset=0);;(__inst__->FileWrite_0.pSrc=((unsigned long)(&__inst__->FileWriteData)));;(__inst__->FileWrite_0.len=brsstrlen(((unsigned long)(&__inst__->FileWriteData))));;FileWrite(&__inst__->FileWrite_0);
if((((unsigned long)(unsigned short)__inst__->FileWrite_0.status==(unsigned long)(unsigned short)ERR_OK))){
(__inst__->Step=12);
}else if((((unsigned long)(unsigned short)__inst__->FileWrite_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->FileWrite_0.status);
(__inst__->Step=0);
}
}break;case 12:{
(__inst__->FileClose_0.enable=1);;(__inst__->FileClose_0.ident=__inst__->FileCreate_0.ident);;FileClose(&__inst__->FileClose_0);
if((((unsigned long)(unsigned short)__inst__->FileClose_0.status==(unsigned long)(unsigned short)ERR_OK))){
(__inst__->Step=20);
}else if((((unsigned long)(unsigned short)__inst__->FileClose_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->FileClose_0.status);
(__inst__->Step=0);
}

}break;case 20:{
(__inst__->DevLink_0.enable=1);;(__inst__->DevLink_0.pDevice=((unsigned long)(&__inst__->FileDevSystem)));;(__inst__->DevLink_0.pParam=((unsigned long)(&__inst__->FileDevSystemPara)));;DevLink(&__inst__->DevLink_0);
if((((unsigned long)(unsigned short)__inst__->DevLink_0.status==(unsigned long)(unsigned short)ERR_OK))){
(__inst__->Step=21);
}else if((((unsigned long)(unsigned short)__inst__->DevLink_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->DevLink_0.status);
(__inst__->Step=0);
}
}break;case 21:{
(__inst__->FileCopy_0.enable=1);;(__inst__->FileCopy_0.pSrcDev=((unsigned long)(&__inst__->FileDevSystem)));;(__inst__->FileCopy_0.pSrc=((unsigned long)(&__inst__->FileNameEyeSystem)));;(__inst__->FileCopy_0.pDestDev=((unsigned long)(&__inst__->FileDevUser)));;(__inst__->FileCopy_0.pDest=((unsigned long)(&__inst__->FileNameEyeUser)));;FileCopy(&__inst__->FileCopy_0);
if((((unsigned long)(unsigned short)__inst__->FileCopy_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->FileCopy_0.status);
(__inst__->Step=22);
}
}break;case 22:{
(__inst__->DevUnlink_0.enable=1);;(__inst__->DevUnlink_0.handle=__inst__->DevLink_0.handle);;DevUnlink(&__inst__->DevUnlink_0);
if((((unsigned long)(unsigned short)__inst__->DevUnlink_0.status==(unsigned long)(unsigned short)ERR_OK))){
(__inst__->Step=30);
}else if((((unsigned long)(unsigned short)__inst__->DevUnlink_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->DevUnlink_0.status);
(__inst__->Step=0);
}
}break;case 30:{
(__inst__->CfgGetIPAddr_0.enable=1);;(__inst__->CfgGetIPAddr_0.pDevice=((unsigned long)(&__inst__->EthDevice)));;(__inst__->CfgGetIPAddr_0.pIPAddr=((unsigned long)(&__inst__->EthIpAddr)));;(__inst__->CfgGetIPAddr_0.Len=81);;CfgGetIPAddr(&__inst__->CfgGetIPAddr_0);
if((((unsigned long)(unsigned short)__inst__->CfgGetIPAddr_0.status==(unsigned long)(unsigned short)ERR_OK))){
{int zzIndex; plcstring* zzLValue=(plcstring*)((*(__inst__->visWebViewerPath))); plcstring* zzRValue=(plcstring*)"http://"; for(zzIndex=0; zzIndex<7l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
brsstrcat(((unsigned long)(&((*(__inst__->visWebViewerPath))))),((unsigned long)(&__inst__->EthIpAddr)));
(__inst__->Done=1);
(__inst__->Status=ERR_OK);
(__inst__->Step=0);
}else if((((unsigned long)(unsigned short)__inst__->CfgGetIPAddr_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->CfgGetIPAddr_0.status);
(__inst__->Step=0);
}
}break;}
}imp1_case1_11:imp1_endcase1_0:;}
#line 112 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViCreateWebDirFile.nodebug"
#line 169 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/Actions.st"
static void __AS__Action_ViCreateWebDirFile_ACTION_CreateHtmlFile(struct ViCreateWebDirFile* inst){struct ViCreateWebDirFile* __inst__=inst;
{







































{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->HtmlFileContent1; plcstring* zzRValue=(plcstring*)"<!DOCTYPE html>\r\n<html lang=\"de\">\r\n\t<head>\r\n\t\t<meta charset=\"utf-8\" />\r\n\t\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\r\n\t\t<title>Camera Picture</title>\r\n\t\t<style>\r\n\t\t\thtml, body{\r\n\t\t\t\theight: 100%;\r\n\t\t\t\twidth: 100%;\r\n\t\t\t\tbackground: #C0C0C0;\r\n\t\t\t\tmargin: 0;\r\n\t\t\t\tpadding: 0;\r\n\t\t\t}\r\n\t\t\timg {\r\n\t\t\t\twidth: 100%;\r\n\t\t\t\theight: auto;\r\n\t\t\t\tmargin-left: auto;\r\n\t\t\t\tmargin-right: auto;\r\n\t\t\t}\r\n\t\t\tdiv {\r\n\t\t\t\theight: 100%;\r\n\t\t\t\twidth: 100%;\r\n\t\t\t}\r\n\t\t</style>\r\n\t</head>\r\n\t<body style=\"overflow: hidden\">\r\n\t\t<div>\r\n\t\t\t<img id=\"imgError\" src=\"VisionEye.png\" />\r\n\t\t\t<img id=\"imgCamera\" src=\"\" />\r\n\t\t\t<script>\r\n\t\t\t\tfunction getParameterByName(param) {\r\n\t\t\t\t\tvar url = window.location.href;\r\n\t\t\t\t\tvar regex = new RegExp(\"[?&]\" + param + \"(=([^&#]*)|&|#|$)\"),\r\n\t\t\t\t\tresults = regex.exec(url);\r\n\t\t\t\t\tif (!results) return null;\r\n\t\t\t\t\tif (!results[2]) return \"\";\r\n\t\t\t\t\treturn decodeURIComponent(results[2].replace(/\\+/g, \" \"));\r\n\t\t\t\t}\r\n\t\t\t\tvar ImagePath \t\t\t= \"http://"; for(zzIndex=0; zzIndex<974l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};































































{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->HtmlFileContent2; plcstring* zzRValue=(plcstring*)":8888/jpg\";\r\n\t\t\t\tvar ErrorImagePath \t\t= \"VisionEye.png\";\r\n\t\t\t\tvar UpdateInterval_ms \t= 200;\r\n\t\t\t\tvar MissingImgCtr\t\t= 0;\r\n\t\t\t\tfunction updateImage() {\r\n\t\t\t\t\tdocument.getElementById(\"imgCamera\").setAttribute(\"src\", ImagePath + \"?q=\" + getParameterByName(\"q\") + \"&\" + Math.random());\r\n\t\t\t\t\tvar img = document.getElementById(\"imgCamera\");\r\n\t\t\t\t\tvar rot = getParameterByName(\"rot\");\r\n\t\t\t\t\tvar scale = getParameterByName(\"scale\");\r\n\t\t\t\t\tif (scale == 0){\r\n\t\t\t\t\t\tscale = 1;\r\n\t\t\t\t\t}\r\n\t\t\t\t\tswitch (rot) {\r\n\t\t\t\t\t\tcase \"0\":\r\n\t\t\t\t\t\t\tmove_x = 0;\r\n\t\t\t\t\t\t\tmove_y = 0;\r\n\t\t\t\t\t\t\tbreak;\r\n\t\t\t\t\t\tcase \"90\":\r\n\t\t\t\t\t\t\tmove_x = img.height;\r\n\t\t\t\t\t\t\tmove_y = 0;\r\n\t\t\t\t\t\t\tbreak;\r\n\t\t\t\t\t\tcase \"180\":\r\n\t\t\t\t\t\t\tmove_x = img.width;\r\n\t\t\t\t\t\t\tmove_y = img.height;\r\n\t\t\t\t\t\t\tbreak;\r\n\t\t\t\t\t\tcase \"270\":\r\n\t\t\t\t\t\t\tmove_x = 0;\r\n\t\t\t\t\t\t\tmove_y = img.width;\r\n\t\t\t\t\t\t\tbreak;\r\n\t\t\t\t\t\tdefault: //same as 0\r\n\t\t\t\t\t\t\tmove_x = 0;\r\n\t\t\t\t\t\t\tmove_y = 0;\r\n\t\t\t\t\t}\r\n\t\t\t\t\tdocument.getElementById(\"imgCamera\").setAttribute(\"style\", \"transform-origin: 0% 0%; transform: scale(\" + scale + \") translate(\" + move_x + \"px,\" + move_y + \"px) rotate(\" + rot + \"deg);\");\r\n\t\t\t\t\tif(img.naturalHeight !== 0 ) {\r\n\t\t\t\t\t\tMissingImgCtr = 0;\r\n\t\t\t\t\t\tshowCameraImage() ;\r\n\t\t\t\t\t} else {\r\n\t\t\t\t\t\tMissingImgCtr++;\r\n\t\t\t\t\t}\r\n\t\t\t\t\tif(MissingImgCtr > 2){\r\n\t\t\t\t\t\tshowErrorImage();\r\n\t\t\t\t\t}\r\n\t\t\t\t\tsetTimeout(updateImage, UpdateInterval_ms);\r\n\t\t\t\t}\r\n\t\t\t\tshowErrorImage();\r\n\t\t\t\tsetTimeout(updateImage, UpdateInterval_ms);\r\n\t\t\t\tfunction showErrorImage() {\r\n\t\t\t\t\tvar img = document.getElementById(\"imgError\");\r\n\t\t\t\t\timg.style.display = \"inline\";\r\n\t\t\t\t\tvar img = document.getElementById(\"imgCamera\");\r\n\t\t\t\t\timg.style.display = \"none\";\r\n\t\t\t\t}\r\n\t\t\t\tfunction showCameraImage() {\r\n\t\t\t\t\tvar img = document.getElementById(\"imgError\");\r\n\t\t\t\t\timg.style.display = \"none\";\r\n\t\t\t\t\tvar img = document.getElementById(\"imgCamera\");\r\n\t\t\t\t\timg.style.display = \"inline\";\r\n\t\t\t\t}\r\n\t\t\t</script>\r\n\t\t</div>\r\n\t</body>\r\n</html>"; for(zzIndex=0; zzIndex<1899l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}}
#line 112 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViCreateWebDirFile.nodebug"

void __AS__ImplInitViCreateWebDirFile_st(void){}

__asm__(".section \".plc\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/VisionTypes.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/mappRecipe/RecTypes.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBaseCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAxCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxisCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxis.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxisError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipe.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipeAlarm.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipeError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpComError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/TcpForward/Types.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/IecCheck/IecCheck.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxis.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipe.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/TcpForward/TcpForward.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/VisionVariables.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/mappRecipe/RecVariables.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/McElements.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/MpComponents.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/MpVisionComponents.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/Vi_image/Types.typ\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/Vi_image/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/Vi_image/LocalFunctions.fun\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_image/ViCreateWebDirFile.st.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"plcreplace \\\"C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_image/ViCreateWebDirFile.st.c\\\" \\\"C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViCreateWebDirFile.st\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViDownloadImage\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViRefreshImageList\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViSaveImgOnPlc\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViDrawCrosshair\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViCreateWebDirFile\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViShowImgOnVC4\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfun \\\"CrosshairDetailsValue\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfun \\\"CrosshairDetailsText\\\"\\n\"");
__asm__(".previous");

__asm__(".section \".plciec\"");
__asm__(".ascii \"plcdata_const 'ERR_OK'\\n\"");
__asm__(".ascii \"plcdata_const 'ERR_FUB_ENABLE_FALSE'\\n\"");
__asm__(".ascii \"plcdata_const 'ERR_FUB_BUSY'\\n\"");
__asm__(".ascii \"plcdata_const 'fiERR_EXIST'\\n\"");
__asm__(".ascii \"plcdata_const 'fiERR_FILE_NOT_FOUND'\\n\"");
__asm__(".ascii \"plcdata_const 'fiERR_DIR_NOT_EXIST'\\n\"");
__asm__(".previous");
