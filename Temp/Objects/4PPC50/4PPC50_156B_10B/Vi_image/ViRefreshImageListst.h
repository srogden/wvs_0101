#ifndef __AS__TYPE_
#define __AS__TYPE_
typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

#ifndef __AS__TYPE_typVisionImageCommand
#define __AS__TYPE_typVisionImageCommand
typedef struct typVisionImageCommand
{	plcbit CreateDir;
	plcbit DeleteDir;
	plcbit DeleteImage;
	plcbit Refresh;
	plcbit DownloadImage;
	plcbit SaveImageOnPLC;
	plcbit ResetError;
	plcbit RefreshCrosshair;
} typVisionImageCommand;
#endif

#ifndef __AS__TYPE_typVisionImageConfig
#define __AS__TYPE_typVisionImageConfig
typedef struct typVisionImageConfig
{	plcstring FileDevice[81];
	plcstring DirName[81];
	plcstring PlkIPWithoutNode[81];
	plcstring EthDevice[81];
	plcstring EthIP[81];
	unsigned long ConvertCycles;
	unsigned char Format;
	unsigned char QualityJPG;
	plcbit UploadBmpJpg;
	plcbit UploadSVG;
	unsigned char MainPageQualityJPG;
	unsigned short ImageRotation_deg;
} typVisionImageConfig;
#endif

#ifndef __AS__TYPE_typVisionImageData
#define __AS__TYPE_typVisionImageData
typedef struct typVisionImageData
{	plcstring Images[20][81];
	unsigned short Status;
	plcbit VisionDisabled;
} typVisionImageData;
#endif

#ifndef __AS__TYPE_typVisionImage
#define __AS__TYPE_typVisionImage
typedef struct typVisionImage
{	typVisionImageCommand CMD;
	typVisionImageConfig CFG;
	typVisionImageData DAT;
} typVisionImage;
#endif

#ifndef __AS__TYPE_fiDIR_READ_DATA
#define __AS__TYPE_fiDIR_READ_DATA
typedef struct fiDIR_READ_DATA
{	unsigned char Filename[260];
	plcdt Date;
	unsigned long Filelength;
} fiDIR_READ_DATA;
#endif

struct DirRead
{	unsigned long pDevice;
	unsigned long pPath;
	unsigned long entry;
	unsigned char option;
	unsigned long pData;
	unsigned long data_len;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void DirRead(struct DirRead* inst);
struct FileDelete
{	unsigned long pDevice;
	unsigned long pName;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileDelete(struct FileDelete* inst);
struct TON
{	plctime PT;
	plctime ET;
	plctime StartTime;
	unsigned long Restart;
	plcbit IN;
	plcbit Q;
	plcbit M;
};
_BUR_PUBLIC void TON(struct TON* inst);
struct ViRefreshImageList
{	struct typVisionImage(* VisionImage);
	plcstring(* ImageList)[20][81];
	plcstring(* visSelectedImage)[81];
	unsigned short Status;
	unsigned short DirEntries;
	signed short Step;
	plcstring visSelectedImageOld[81];
	unsigned short idx;
	plcstring file_newest[81];
	plcstring file_oldest[81];
	plcdt date_newest;
	plcdt date_oldest;
	struct DirRead DirRead_0;
	struct FileDelete FileDelete_0;
	fiDIR_READ_DATA dir_data;
	plcstring DeleteFileName[81];
	struct TON TON_VisPause;
	plcbit Enable;
	plcbit cmdDeleteOldest;
};
void ViRefreshImageList(struct ViRefreshImageList* inst);
_BUR_PUBLIC unsigned long brsmemset(unsigned long pDest, unsigned char value, unsigned long length);
_BUR_PUBLIC unsigned long brsstrcat(unsigned long pDest, unsigned long pSrc);
_BUR_PUBLIC unsigned long brsstrlen(unsigned long pString);
_BUR_PUBLIC unsigned long brsstrcpy(unsigned long pDest, unsigned long pSrc);
_BUR_PUBLIC signed long brsstrcmp(unsigned long pString1, unsigned long pString2);
_BUR_PUBLIC signed long CheckBounds(signed long index, signed long lower, signed long upper);
_GLOBAL unsigned short ERR_OK;
_GLOBAL unsigned short ERR_FUB_ENABLE_FALSE;
_GLOBAL unsigned short ERR_FUB_BUSY;
_GLOBAL unsigned char fiFILE;
_GLOBAL unsigned short fiERR_NO_MORE_ENTRIES;
