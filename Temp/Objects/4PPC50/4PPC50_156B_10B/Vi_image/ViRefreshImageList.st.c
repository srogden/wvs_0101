#define _DEFAULT_INCLUDE
#include <bur\plctypes.h>
#include "C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_image/ViRefreshImageListst.h"
#line 1 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViRefreshImageList.nodebug"
#line 1 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViRefreshImageList.st"
void ViRefreshImageList(struct ViRefreshImageList* inst){struct ViRefreshImageList* __inst__=inst;{
if((__inst__->Enable^1)){
(__inst__->Status=ERR_FUB_ENABLE_FALSE);
(__inst__->Step=0);
return;
}
switch(__inst__->Step){
case 0:{
(__inst__->Status=ERR_FUB_BUSY);
(__inst__->Step=1);
}break;case 1:{
brsmemset(((unsigned long)(&((*(__inst__->VisionImage)).DAT.Images))),0,1620);
brsmemset(((unsigned long)(&((*(__inst__->ImageList))))),0,1620);
brsmemset(((unsigned long)(&((*(__inst__->visSelectedImage))))),0,81);
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->visSelectedImageOld; plcstring* zzRValue=(plcstring*)""; for(zzIndex=0; zzIndex<0l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
{int zzIndex; plcstring* zzLValue=(plcstring*)((*(__inst__->visSelectedImage))); plcstring* zzRValue=(plcstring*)""; for(zzIndex=0; zzIndex<0l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(__inst__->DirEntries=0);
(__inst__->idx=0);
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->file_newest; plcstring* zzRValue=(plcstring*)""; for(zzIndex=0; zzIndex<0l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->file_oldest; plcstring* zzRValue=(plcstring*)""; for(zzIndex=0; zzIndex<0l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(__inst__->date_newest=(plcdt)0);
(__inst__->date_oldest=4115966400);
(__inst__->cmdDeleteOldest=0);
(__inst__->Step=2);
}break;case 2:{
(__inst__->DirRead_0.enable=1);
(__inst__->DirRead_0.pDevice=((unsigned long)(&((*(__inst__->VisionImage)).CFG.FileDevice))));
(__inst__->DirRead_0.pPath=((unsigned long)(&((*(__inst__->VisionImage)).CFG.DirName))));
(__inst__->DirRead_0.entry=__inst__->idx);
(__inst__->DirRead_0.option=fiFILE);
(__inst__->DirRead_0.pData=((unsigned long)(&__inst__->dir_data)));
(__inst__->DirRead_0.data_len=268);
DirRead(&__inst__->DirRead_0);
if((((unsigned long)(unsigned short)__inst__->DirRead_0.status==(unsigned long)(unsigned short)ERR_OK))){



if(((((signed long)brsstrcmp(((((unsigned long)(&__inst__->dir_data.Filename))+brsstrlen(((unsigned long)(&__inst__->dir_data.Filename))))-4),((unsigned long)(&".bmp")))==(signed long)0))|(((signed long)brsstrcmp(((((unsigned long)(&__inst__->dir_data.Filename))+brsstrlen(((unsigned long)(&__inst__->dir_data.Filename))))-4),((unsigned long)(&".jpg")))==(signed long)0))|(((signed long)brsstrcmp(((((unsigned long)(&__inst__->dir_data.Filename))+brsstrlen(((unsigned long)(&__inst__->dir_data.Filename))))-4),((unsigned long)(&".svg")))==(signed long)0)))){

if((((unsigned long)__inst__->dir_data.Date>(unsigned long)__inst__->date_newest))){
brsstrcpy(((unsigned long)(&__inst__->file_newest)),((unsigned long)(&__inst__->dir_data.Filename)));
(__inst__->date_newest=__inst__->dir_data.Date);
}
if((((unsigned long)__inst__->dir_data.Date<(unsigned long)__inst__->date_oldest))){
brsstrcpy(((unsigned long)(&__inst__->file_oldest)),((unsigned long)(&__inst__->dir_data.Filename)));
(__inst__->date_oldest=__inst__->dir_data.Date);
}
if((((unsigned long)(unsigned short)__inst__->DirEntries<=(unsigned long)19))){
brsstrcpy(((unsigned long)(&((*(__inst__->VisionImage)).DAT.Images[CheckBounds(__inst__->DirEntries,0,19)]))),((unsigned long)(&__inst__->dir_data.Filename)));

{int zzIndex; plcstring* zzLValue=(plcstring*)((*(__inst__->ImageList))[__inst__->DirEntries]); plcstring* zzRValue=(plcstring*)"{\"value\":\""; for(zzIndex=0; zzIndex<10l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
brsstrcat(((unsigned long)(&((*(__inst__->ImageList))[CheckBounds(__inst__->DirEntries,0,19)]))),((unsigned long)(&__inst__->dir_data.Filename)));
brsstrcat(((unsigned long)(&((*(__inst__->ImageList))[CheckBounds(__inst__->DirEntries,0,19)]))),((unsigned long)(&"\",\"text\":\"")));
brsstrcat(((unsigned long)(&((*(__inst__->ImageList))[CheckBounds(__inst__->DirEntries,0,19)]))),((unsigned long)(&__inst__->dir_data.Filename)));
brsstrcat(((unsigned long)(&((*(__inst__->ImageList))[CheckBounds(__inst__->DirEntries,0,19)]))),((unsigned long)(&"\"}")));
(__inst__->DirEntries=(__inst__->DirEntries+1));
}else{
(__inst__->cmdDeleteOldest=1);
}
}
(__inst__->idx=(__inst__->idx+1));
}else if((((unsigned long)(unsigned short)__inst__->DirRead_0.status==(unsigned long)(unsigned short)fiERR_NO_MORE_ENTRIES))){
if(__inst__->cmdDeleteOldest){
(__inst__->cmdDeleteOldest=0);
brsstrcpy(((unsigned long)(&__inst__->DeleteFileName)),((unsigned long)(&((*(__inst__->VisionImage)).CFG.DirName))));
brsstrcat(((unsigned long)(&__inst__->DeleteFileName)),((unsigned long)(&"/")));
brsstrcat(((unsigned long)(&__inst__->DeleteFileName)),((unsigned long)(&__inst__->file_oldest)));
(__inst__->Step=10);
}else{
(__inst__->TON_VisPause.IN=0);;TON(&__inst__->TON_VisPause);
(__inst__->Step=3);
}
}else if((((unsigned long)(unsigned short)__inst__->DirRead_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->DirRead_0.status);
(__inst__->Step=0);
}
}break;case 3:{
(__inst__->TON_VisPause.IN=1);;(__inst__->TON_VisPause.PT=250);;TON(&__inst__->TON_VisPause);
if(__inst__->TON_VisPause.Q){
brsstrcpy(((unsigned long)(&((*(__inst__->visSelectedImage))))),((unsigned long)(&__inst__->file_newest)));
(__inst__->Status=0);
(__inst__->Step=0);
}

}break;case 10:{
(__inst__->FileDelete_0.enable=1);
(__inst__->FileDelete_0.pDevice=((unsigned long)(&((*(__inst__->VisionImage)).CFG.FileDevice))));
(__inst__->FileDelete_0.pName=((unsigned long)(&__inst__->DeleteFileName)));
FileDelete(&__inst__->FileDelete_0);
if((((unsigned long)(unsigned short)__inst__->FileDelete_0.status==(unsigned long)(unsigned short)ERR_OK))){
(__inst__->Step=0);
}else if((((unsigned long)(unsigned short)__inst__->FileDelete_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->FileDelete_0.status);
(__inst__->Step=0);
}
}break;}
}imp1_case1_4:imp1_endcase1_0:;}
#line 97 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViRefreshImageList.nodebug"

void __AS__ImplInitViRefreshImageList_st(void){}

__asm__(".section \".plc\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/VisionTypes.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/mappRecipe/RecTypes.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBaseCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAxCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxisCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxis.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxisError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipe.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipeAlarm.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipeError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpComError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/TcpForward/Types.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/IecCheck/IecCheck.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxis.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipe.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/TcpForward/TcpForward.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/VisionVariables.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/mappRecipe/RecVariables.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/McElements.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/MpComponents.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/MpVisionComponents.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/Vi_image/Types.typ\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/Vi_image/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/Vi_image/LocalFunctions.fun\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_image/ViRefreshImageList.st.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"plcreplace \\\"C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_image/ViRefreshImageList.st.c\\\" \\\"C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViRefreshImageList.st\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViDownloadImage\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViRefreshImageList\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViSaveImgOnPlc\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViDrawCrosshair\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViCreateWebDirFile\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViShowImgOnVC4\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfun \\\"CrosshairDetailsValue\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfun \\\"CrosshairDetailsText\\\"\\n\"");
__asm__(".previous");

__asm__(".section \".plciec\"");
__asm__(".ascii \"plcdata_const 'ERR_OK'\\n\"");
__asm__(".ascii \"plcdata_const 'ERR_FUB_ENABLE_FALSE'\\n\"");
__asm__(".ascii \"plcdata_const 'ERR_FUB_BUSY'\\n\"");
__asm__(".ascii \"plcdata_const 'fiFILE'\\n\"");
__asm__(".ascii \"plcdata_const 'fiERR_NO_MORE_ENTRIES'\\n\"");
__asm__(".previous");
