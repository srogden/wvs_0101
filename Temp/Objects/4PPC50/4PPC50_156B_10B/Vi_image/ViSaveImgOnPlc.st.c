#define _DEFAULT_INCLUDE
#include <bur\plctypes.h>
#include "C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_image/ViSaveImgOnPlcst.h"
#line 1 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViSaveImgOnPlc.nodebug"
#line 2 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViSaveImgOnPlc.st"
void ViSaveImgOnPlc(struct ViSaveImgOnPlc* inst){struct ViSaveImgOnPlc* __inst__=inst;{
if((((unsigned long)__inst__->MemInfo.SvgAdr==(unsigned long)0))){


(__inst__->MemInfo.UploadSize=6000000);
(__inst__->MemInfo.SvgHeaderSize=2000);
(__inst__->MemInfo.CrosshSize=18000);
(__inst__->MemInfo.SvgSize=(((((unsigned long)((__inst__->MemInfo.UploadSize*4)))/((unsigned long)(CheckDivUdint(3))))+__inst__->MemInfo.SvgHeaderSize)+__inst__->MemInfo.CrosshSize));
(__inst__->AsMemPartCreate_0.enable=1);
(__inst__->AsMemPartCreate_0.len=(((__inst__->MemInfo.UploadSize+__inst__->MemInfo.SvgSize)+__inst__->MemInfo.CrosshSize)+1000));
AsMemPartCreate(&__inst__->AsMemPartCreate_0);
if((((unsigned long)(unsigned short)__inst__->AsMemPartCreate_0.status==(unsigned long)(unsigned short)ERR_OK))){

(__inst__->AsMemPartAllocClear_0.len=__inst__->MemInfo.UploadSize);
(__inst__->AsMemPartAllocClear_0.enable=1);;(__inst__->AsMemPartAllocClear_0.ident=__inst__->AsMemPartCreate_0.ident);;AsMemPartAllocClear(&__inst__->AsMemPartAllocClear_0);
if((((unsigned long)(unsigned short)__inst__->AsMemPartAllocClear_0.status==(unsigned long)(unsigned short)ERR_OK))){
(__inst__->MemInfo.UploadAdr=__inst__->AsMemPartAllocClear_0.mem);
}

(__inst__->AsMemPartAllocClear_0.len=__inst__->MemInfo.SvgSize);
(__inst__->AsMemPartAllocClear_0.enable=1);;(__inst__->AsMemPartAllocClear_0.ident=__inst__->AsMemPartCreate_0.ident);;AsMemPartAllocClear(&__inst__->AsMemPartAllocClear_0);
if((((unsigned long)(unsigned short)__inst__->AsMemPartAllocClear_0.status==(unsigned long)(unsigned short)ERR_OK))){
(__inst__->MemInfo.SvgAdr=__inst__->AsMemPartAllocClear_0.mem);
}

(__inst__->AsMemPartAllocClear_0.len=__inst__->MemInfo.CrosshSize);
(__inst__->AsMemPartAllocClear_0.enable=1);;(__inst__->AsMemPartAllocClear_0.ident=__inst__->AsMemPartCreate_0.ident);;AsMemPartAllocClear(&__inst__->AsMemPartAllocClear_0);
if((((unsigned long)(unsigned short)__inst__->AsMemPartAllocClear_0.status==(unsigned long)(unsigned short)ERR_OK))){
(__inst__->MemInfo.CrosshAdr=__inst__->AsMemPartAllocClear_0.mem);
}
}
}
if((__inst__->Enable^1)){
(__inst__->Status=ERR_FUB_ENABLE_FALSE);
(__inst__->Step=0);
return;
}
switch(__inst__->Step){
case 0:{
(__inst__->Status=ERR_FUB_BUSY);
if(((((unsigned long)__inst__->MemInfo.UploadAdr!=(unsigned long)0))&(((unsigned long)__inst__->MemInfo.SvgAdr!=(unsigned long)0))&(((unsigned long)__inst__->MemInfo.CrosshAdr!=(unsigned long)0)))){
(__inst__->Step=1);
}
}break;case 1:{
__AS__Action_ViSaveImgOnPlc_ACTION_InitTexts(inst);
brsmemcpy(((unsigned long)(&__inst__->b64key)),((unsigned long)(&"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/")),64);

(__inst__->DTGetTime_0.enable=1);;DTGetTime(&__inst__->DTGetTime_0);
DT_TO_DTStructure(__inst__->DTGetTime_0.DT1,((unsigned long)(&__inst__->DTStructure_0)));
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->FileNameImg; plcstring* zzRValue=(plcstring*)__inst__->CFG.DirName; for(zzIndex=0; zzIndex<80l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&"/")));
uint2str(__inst__->DTStructure_0.year,__inst__->tmpStr1,81);
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&"_")));
usint2str(__inst__->DTStructure_0.month,__inst__->tmpStr1,81);
if((((unsigned long)(unsigned char)__inst__->DTStructure_0.month<(unsigned long)(unsigned char)10))){
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&"0")));
}
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&"_")));
usint2str(__inst__->DTStructure_0.day,__inst__->tmpStr1,81);
if((((unsigned long)(unsigned char)__inst__->DTStructure_0.day<(unsigned long)(unsigned char)10))){
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&"0")));
}
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&"_")));
usint2str(__inst__->DTStructure_0.hour,__inst__->tmpStr1,81);
if((((unsigned long)(unsigned char)__inst__->DTStructure_0.hour<(unsigned long)(unsigned char)10))){
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&"0")));
}
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&"_")));
usint2str(__inst__->DTStructure_0.minute,__inst__->tmpStr1,81);
if((((unsigned long)(unsigned char)__inst__->DTStructure_0.minute<(unsigned long)(unsigned char)10))){
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&"0")));
}
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&"_")));
usint2str(__inst__->DTStructure_0.second,__inst__->tmpStr1,81);
if((((unsigned long)(unsigned char)__inst__->DTStructure_0.second<(unsigned long)(unsigned char)10))){
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&"0")));
}
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&".")));
brsstrcpy(((unsigned long)(&__inst__->FileNameSvg)),((unsigned long)(&__inst__->FileNameImg)));
if((((unsigned long)(unsigned char)__inst__->CFG.Format==(unsigned long)(unsigned char)0))){
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&"jpg")));
}else{
brsstrcat(((unsigned long)(&__inst__->FileNameImg)),((unsigned long)(&"bmp")));
}
brsstrcat(((unsigned long)(&__inst__->FileNameSvg)),((unsigned long)(&"svg")));

{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->URI; plcstring* zzRValue=(plcstring*)"/"; for(zzIndex=0; zzIndex<1l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
if((((unsigned long)(unsigned char)__inst__->CFG.Format==(unsigned long)(unsigned char)0))){
brsstrcat(((unsigned long)(&__inst__->URI)),((unsigned long)(&"jpg")));
brsstrcat(((unsigned long)(&__inst__->URI)),((unsigned long)(&"?q=")));
brsitoa(__inst__->CFG.QualityJPG,((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(((unsigned long)(&__inst__->URI)),((unsigned long)(&__inst__->tmpStr1)));
}else{
brsstrcat(((unsigned long)(&__inst__->URI)),((unsigned long)(&"bmp")));
}
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->Host; plcstring* zzRValue=(plcstring*)__inst__->CFG.PlkIPWithoutNode; for(zzIndex=0; zzIndex<80l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
brsitoa(__inst__->PowerlinkNode,((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(((unsigned long)(&__inst__->Host)),((unsigned long)(&__inst__->tmpStr1)));
(__inst__->DiagStartTime=clock_ms());
(__inst__->Step=10);

}break;case 10:{
(__inst__->TON_ReloadTimeout.IN=0);;TON(&__inst__->TON_ReloadTimeout);
(__inst__->httpClient_0.enable=0);;(__inst__->httpClient_0.send=0);;(__inst__->httpClient_0.abort=0);;httpClient(&__inst__->httpClient_0);
if((((unsigned long)(unsigned short)__inst__->httpClient_0.status==(unsigned long)(unsigned short)ERR_FUB_ENABLE_FALSE))){
(__inst__->Step=11);
}

}break;case 11:{
(__inst__->TON_ReloadTimeout.IN=1);;(__inst__->TON_ReloadTimeout.PT=10000);;TON(&__inst__->TON_ReloadTimeout);
{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->RequestHeader.keepAlive; plcstring* zzRValue=(plcstring*)"timeout=5, max=100"; for(zzIndex=0; zzIndex<18l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
(__inst__->httpClient_0.enable=1);
(__inst__->httpClient_0.send=1);
(__inst__->httpClient_0.method=httpMETHOD_GET);
(__inst__->httpClient_0.option=httpOPTION_HTTP_11);
(__inst__->httpClient_0.hostPort=8080);
(__inst__->httpClient_0.pHost=((unsigned long)(&__inst__->Host)));
(__inst__->httpClient_0.pUri=((unsigned long)(&__inst__->URI)));
(__inst__->httpClient_0.pResponseData=__inst__->MemInfo.UploadAdr);
(__inst__->httpClient_0.responseDataSize=__inst__->MemInfo.UploadSize);
(__inst__->httpClient_0.pRequestHeader=((unsigned long)(&__inst__->RequestHeader)));
httpClient(&__inst__->httpClient_0);
if(((((unsigned long)(unsigned short)__inst__->httpClient_0.status==(unsigned long)(unsigned short)ERR_OK))&(((unsigned long)__inst__->httpClient_0.responseDataLen!=(unsigned long)0)))){
(__inst__->UploadedImgSize=__inst__->httpClient_0.responseDataLen);
(__inst__->httpClient_0.send=0);;httpClient(&__inst__->httpClient_0);
if((((unsigned long)__inst__->UploadedImgSize<(unsigned long)100))){
(__inst__->Status=ERR_NO_IMAGE);
(__inst__->Step=0);
}else{
if(__inst__->CFG.UploadBmpJpg){
(__inst__->Step=20);
}else{
(__inst__->Step=30);
}
}
}else if(((((unsigned long)(unsigned short)__inst__->httpClient_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))&(((unsigned long)(unsigned short)__inst__->httpClient_0.status!=(unsigned long)(unsigned short)0)))){
(__inst__->httpClientErrorStatus=__inst__->httpClient_0.status);
(__inst__->Step=19);
}else if(__inst__->TON_ReloadTimeout.Q){
(__inst__->Status=ERR_NO_IMAGE);
(__inst__->Step=0);
}
}break;case 19:{
(__inst__->httpClient_0.enable=1);
(__inst__->httpClient_0.send=0);
(__inst__->httpClient_0.abort=1);
httpClient(&__inst__->httpClient_0);
if((((unsigned long)(unsigned short)__inst__->httpClient_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->httpClient_0.abort=0);
httpClient(&__inst__->httpClient_0);
(__inst__->Status=__inst__->httpClientErrorStatus);
(__inst__->Step=0);
}

}break;case 20:{
(__inst__->DiagTime.BeginSaveBMPJPG=(clock_ms()-__inst__->DiagStartTime));
(__inst__->FileCreate_0.enable=1);
(__inst__->FileCreate_0.pDevice=((unsigned long)(&__inst__->CFG.FileDevice)));
(__inst__->FileCreate_0.pFile=((unsigned long)(&__inst__->FileNameImg)));
FileCreate(&__inst__->FileCreate_0);
if((((unsigned long)(unsigned short)__inst__->FileCreate_0.status==(unsigned long)(unsigned short)ERR_OK))){
(__inst__->Step=21);
}else if((((unsigned long)(unsigned short)__inst__->FileCreate_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->FileCreate_0.status);
(__inst__->Step=0);
}
}break;case 21:{
(__inst__->FileWrite_0.enable=1);
(__inst__->FileWrite_0.ident=__inst__->FileCreate_0.ident);
(__inst__->FileWrite_0.offset=0);
(__inst__->FileWrite_0.pSrc=__inst__->MemInfo.UploadAdr);
(__inst__->FileWrite_0.len=__inst__->UploadedImgSize);
FileWrite(&__inst__->FileWrite_0);
if((((unsigned long)(unsigned short)__inst__->FileWrite_0.status==(unsigned long)(unsigned short)ERR_OK))){
(__inst__->Step=22);
}else if((((unsigned long)(unsigned short)__inst__->FileWrite_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->FileWrite_0.status);
(__inst__->Step=0);
}
}break;case 22:{
(__inst__->FileClose_0.enable=1);
(__inst__->FileClose_0.ident=__inst__->FileCreate_0.ident);
FileClose(&__inst__->FileClose_0);
if((((unsigned long)(unsigned short)__inst__->FileClose_0.status==(unsigned long)(unsigned short)ERR_OK))){
(__inst__->DiagTime.EndSaveBMPJPG=(clock_ms()-__inst__->DiagStartTime));
if(__inst__->CFG.UploadSVG){
(__inst__->Step=30);
}else{
(__inst__->Status=0);
(__inst__->Step=0);
}
}else if((((unsigned long)(unsigned short)__inst__->FileClose_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->FileClose_0.status);
(__inst__->Step=0);
}

}break;case 30:{
(__inst__->DiagTime.BeginSVG=(clock_ms()-__inst__->DiagStartTime));
(__inst__->b64neededOutput=(((((unsigned long)(__inst__->UploadedImgSize))/((unsigned long)(CheckDivUdint(3))))*4)+((unsigned long)((plcbit)((3==0?0:((unsigned long)(__inst__->UploadedImgSize))%((unsigned long)(CheckDivUdint(3))))!=0?1:0)&1)*4)));
if((((unsigned long)__inst__->b64neededOutput<(unsigned long)((__inst__->MemInfo.SvgSize-__inst__->MemInfo.SvgHeaderSize)-__inst__->MemInfo.CrosshSize)))){
brsmemset(__inst__->MemInfo.SvgAdr,0,__inst__->MemInfo.SvgSize);

brsstrcpy(__inst__->MemInfo.SvgAdr,((unsigned long)(&__inst__->SvgTexts.ContentStart)));
brsstrcat(__inst__->MemInfo.SvgAdr,((unsigned long)(&__inst__->FileNameSvg)));
brsstrcat(__inst__->MemInfo.SvgAdr,((unsigned long)(&__inst__->SvgTexts.ContentAfterTitle)));
if((((unsigned long)(unsigned char)__inst__->CFG.Format==(unsigned long)(unsigned char)0))){
brsstrcat(__inst__->MemInfo.SvgAdr,((unsigned long)(&"jpg")));
}else{
brsstrcat(__inst__->MemInfo.SvgAdr,((unsigned long)(&"bmp")));
}
brsstrcat(__inst__->MemInfo.SvgAdr,((unsigned long)(&__inst__->SvgTexts.ContentBase64)));
(__inst__->Step=31);
}else{
(__inst__->Status=ERR_BUFF_TOO_SMALL);
(__inst__->Step=0);
}

}break;case 31:{
(__inst__->b64AdrInBuffer=__inst__->MemInfo.UploadAdr);
(__inst__->b64AdrOutBuffer=(__inst__->MemInfo.SvgAdr+brsstrlen(__inst__->MemInfo.SvgAdr)));
(__inst__->b64actposIN=__inst__->b64AdrInBuffer);
(__inst__->b64actposOUT=__inst__->b64AdrOutBuffer);
(__inst__->Step=32);
}break;case 32:{
for((__inst__->i=0);__inst__->i<=__inst__->CFG.ConvertCycles;__inst__->i+=1){
if((((unsigned long)__inst__->b64actposIN<(unsigned long)(__inst__->b64AdrInBuffer+__inst__->UploadedImgSize)))){
(__inst__->b64blockLen=(unsigned char)((unsigned long)0>(unsigned long)3?3:(unsigned long)((__inst__->b64AdrInBuffer+__inst__->UploadedImgSize)-__inst__->b64actposIN)<(unsigned long)0?0:(unsigned long)((__inst__->b64AdrInBuffer+__inst__->UploadedImgSize)-__inst__->b64actposIN)>(unsigned long)3?3:((__inst__->b64AdrInBuffer+__inst__->UploadedImgSize)-__inst__->b64actposIN)));
(__inst__->b64in=__inst__->b64actposIN);
(__inst__->b64out=__inst__->b64actposOUT);
(CheckWriteAccess(__inst__->b64out),(*(__inst__->b64out))[CheckBounds(0,0,3)]=__inst__->b64key[CheckBounds(((unsigned char)((CheckReadAccess(__inst__->b64in),(*(__inst__->b64in))[CheckBounds(0,0,2)])>>2)),0,63)]);
if((((unsigned long)(unsigned short)__inst__->b64blockLen==(unsigned long)(unsigned short)1))){
(CheckWriteAccess(__inst__->b64out),(*(__inst__->b64out))[CheckBounds(1,0,3)]=__inst__->b64key[CheckBounds(((unsigned char)(((CheckReadAccess(__inst__->b64in),(*(__inst__->b64in))[CheckBounds(0,0,2)])&3)<<4)),0,63)]);
(CheckWriteAccess(__inst__->b64out),(*(__inst__->b64out))[CheckBounds(2,0,3)]=61);
(CheckWriteAccess(__inst__->b64out),(*(__inst__->b64out))[CheckBounds(3,0,3)]=61);
}else if((((unsigned long)(unsigned short)__inst__->b64blockLen==(unsigned long)(unsigned short)2))){
(CheckWriteAccess(__inst__->b64out),(*(__inst__->b64out))[CheckBounds(1,0,3)]=__inst__->b64key[CheckBounds((((unsigned char)(((CheckReadAccess(__inst__->b64in),(*(__inst__->b64in))[CheckBounds(0,0,2)])&3)<<4))|((unsigned char)((CheckReadAccess(__inst__->b64in),(*(__inst__->b64in))[CheckBounds(1,0,2)])>>4))),0,63)]);
(CheckWriteAccess(__inst__->b64out),(*(__inst__->b64out))[CheckBounds(2,0,3)]=__inst__->b64key[CheckBounds(((unsigned char)(((CheckReadAccess(__inst__->b64in),(*(__inst__->b64in))[CheckBounds(1,0,2)])&15)<<2)),0,63)]);
(CheckWriteAccess(__inst__->b64out),(*(__inst__->b64out))[CheckBounds(3,0,3)]=61);
}else if((((unsigned long)(unsigned short)__inst__->b64blockLen==(unsigned long)(unsigned short)3))){
(CheckWriteAccess(__inst__->b64out),(*(__inst__->b64out))[CheckBounds(1,0,3)]=__inst__->b64key[CheckBounds((((unsigned char)(((CheckReadAccess(__inst__->b64in),(*(__inst__->b64in))[CheckBounds(0,0,2)])&3)<<4))|((unsigned char)((CheckReadAccess(__inst__->b64in),(*(__inst__->b64in))[CheckBounds(1,0,2)])>>4))),0,63)]);
(CheckWriteAccess(__inst__->b64out),(*(__inst__->b64out))[CheckBounds(2,0,3)]=__inst__->b64key[CheckBounds((((unsigned char)(((CheckReadAccess(__inst__->b64in),(*(__inst__->b64in))[CheckBounds(1,0,2)])&15)<<2))|((unsigned char)((CheckReadAccess(__inst__->b64in),(*(__inst__->b64in))[CheckBounds(2,0,2)])>>6))),0,63)]);
(CheckWriteAccess(__inst__->b64out),(*(__inst__->b64out))[CheckBounds(3,0,3)]=__inst__->b64key[CheckBounds(((CheckReadAccess(__inst__->b64in),(*(__inst__->b64in))[CheckBounds(2,0,2)])&63),0,63)]);
}
(__inst__->b64actposIN=(__inst__->b64actposIN+3));
(__inst__->b64actposOUT=(__inst__->b64actposOUT+4));
}else{
(__inst__->Step=33);
goto imp1_endfor26_0;
}
}imp1_endfor26_0:;

}break;case 33:{
brsmemset(__inst__->MemInfo.CrosshAdr,0,__inst__->MemInfo.CrosshSize);
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->SvgTexts.ContentBeginJS)));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&((*(__inst__->CrossHairInfo))[CheckBounds(1,0,10)].DataVis))));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->SvgTexts.ContentBetween1)));
(__inst__->FirstElement=1);
for((__inst__->i=1);__inst__->i<=MAX_NUM_RESULTS;__inst__->i+=1){
if(((((CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairTopLeftX)!=0))&(((CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairTopLeftY)!=0)))){
if(__inst__->FirstElement){
(__inst__->FirstElement=0);
}else{
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->SvgTexts.ContentBetween2)));
}
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&((*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].Data))));
}
}imp1_endfor29_0:;
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->SvgTexts.ContentEndJS)));


for((__inst__->i=1);__inst__->i<=MAX_NUM_RESULTS;__inst__->i+=1){
if(((((CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairTopLeftX)!=0))&(((CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairTopLeftY)!=0)))){
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->SvgTexts.ContentCrossHairBegin)));
brsitoa(__inst__->i,((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->SvgTexts.ContentCrossHairMiddle)));
brsitoa((signed long)((CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairTopLeftX)>=0.0?(CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairTopLeftX)+0.5:(CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairTopLeftX)-0.5),((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&",")));
brsitoa((signed long)((CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairTopLeftY)>=0.0?(CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairTopLeftY)+0.5:(CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairTopLeftY)-0.5),((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&") rotate(")));
brsitoa((signed long)((CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairRotateCenter)>=0.0?(CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairRotateCenter)+0.5:(CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairRotateCenter)-0.5),((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&",")));
brsitoa((signed long)(((CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairSizeScaled)/CheckDivReal(2))>=0.0?((CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairSizeScaled)/CheckDivReal(2))+0.5:((CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairSizeScaled)/CheckDivReal(2))-0.5),((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&",")));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&") scale(")));
brsitoa((signed long)((CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairScale)>=0.0?(CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairScale)+0.5:(CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairScale)-0.5),((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&",")));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->tmpStr1)));
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->SvgTexts.ContentCrossHairEnd)));
}
}imp1_endfor32_0:;
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->SvgTexts.ContentCrossHairRed)));

for((__inst__->i=1);__inst__->i<=MAX_NUM_RESULTS;__inst__->i+=1){
if(((((CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairTopLeftX)!=0))&(((CheckReadAccess(__inst__->CrossHairInfo),(*(__inst__->CrossHairInfo))[CheckBounds(__inst__->i,0,10)].CrosshairTopLeftY)!=0)))){
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->SvgTexts.ContentCrossHairIndex)));
}
}imp1_endfor34_0:;
brsstrcat(__inst__->MemInfo.CrosshAdr,((unsigned long)(&__inst__->SvgTexts.ContentEnd)));
(__inst__->Step=41);
}break;case 41:{
brsstrcat(__inst__->MemInfo.SvgAdr,__inst__->MemInfo.CrosshAdr);
(__inst__->Step=50);
}break;case 50:{
(__inst__->DiagTime.BeginSVGSave=(clock_ms()-__inst__->DiagStartTime));
(__inst__->FileCreate_0.enable=1);
(__inst__->FileCreate_0.pDevice=((unsigned long)(&__inst__->CFG.FileDevice)));
(__inst__->FileCreate_0.pFile=((unsigned long)(&__inst__->FileNameSvg)));
FileCreate(&__inst__->FileCreate_0);
if((((unsigned long)(unsigned short)__inst__->FileCreate_0.status==(unsigned long)(unsigned short)ERR_OK))){
(__inst__->Step=51);
}else if((((unsigned long)(unsigned short)__inst__->FileCreate_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->FileCreate_0.status);
(__inst__->Step=0);
}
}break;case 51:{
(__inst__->FileWrite_0.enable=1);
(__inst__->FileWrite_0.ident=__inst__->FileCreate_0.ident);
(__inst__->FileWrite_0.offset=0);
(__inst__->FileWrite_0.pSrc=__inst__->MemInfo.SvgAdr);
(__inst__->FileWrite_0.len=brsstrlen(__inst__->MemInfo.SvgAdr));
FileWrite(&__inst__->FileWrite_0);
if((((unsigned long)(unsigned short)__inst__->FileWrite_0.status==(unsigned long)(unsigned short)ERR_OK))){
(__inst__->Step=52);
}else if((((unsigned long)(unsigned short)__inst__->FileWrite_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->FileWrite_0.status);
(__inst__->Step=0);
}
}break;case 52:{
(__inst__->FileClose_0.enable=1);
(__inst__->FileClose_0.ident=__inst__->FileCreate_0.ident);
FileClose(&__inst__->FileClose_0);
if((((unsigned long)(unsigned short)__inst__->FileClose_0.status==(unsigned long)(unsigned short)ERR_OK))){
(__inst__->DiagTime.EndSVGSave=(clock_ms()-__inst__->DiagStartTime));
(__inst__->Status=0);
(__inst__->Step=0);
}else if((((unsigned long)(unsigned short)__inst__->FileClose_0.status!=(unsigned long)(unsigned short)ERR_FUB_BUSY))){
(__inst__->Status=__inst__->FileClose_0.status);
(__inst__->Step=0);
}
}break;}
}imp1_case6_15:imp1_endcase6_0:;}
#line 355 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViSaveImgOnPlc.nodebug"
#line 1 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/Actions.st"
static void __AS__Action_ViSaveImgOnPlc_ACTION_InitTexts(struct ViSaveImgOnPlc* inst){struct ViSaveImgOnPlc* __inst__=inst;
{



{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->SvgTexts.ContentStart; plcstring* zzRValue=(plcstring*)"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">\r\n<svg xmlns=\"http://www.w3.org/2000/svg\" xml:space=\"preserve\" width=\"1280px\" height=\"1024px\" shape-rendering=\"geometricPrecision\" text-rendering=\"geometricPrecision\" image-rendering=\"optimizeQuality\" fill-rule=\"nonzero\" clip-rule=\"evenodd\" viewBox=\"0 0 1280 1024\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\r\n\t<title>"; for(zzIndex=0; zzIndex<456l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};



{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->SvgTexts.ContentAfterTitle; plcstring* zzRValue=(plcstring*)"</title>\r\n\t<image\r\n\t\txlink:href=\"data:image/"; for(zzIndex=0; zzIndex<44l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};

{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->SvgTexts.ContentBase64; plcstring* zzRValue=(plcstring*)";base64,"; for(zzIndex=0; zzIndex<8l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};







































{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->SvgTexts.ContentBeginJS; plcstring* zzRValue=(plcstring*)"\"\r\n\t\twidth=\"1280\" height=\"1024\"\r\n\t\tid=\"LoadedPicture\"\r\n\t\tx=\"0\" y=\"0\" \r\n\t/>\r\n\t<defs>\r\n\t\t<style>\r\n\t\t  text {\r\n\t\t   pointer-events: none;\r\n\t\t   user-select: none;\r\n\t\t   -webkit-user-select: none;\r\n\t\t   -moz-user-select: none;\r\n\t\t  }\r\n\t\t</style>\r\n\t\t\r\n\t\t<symbol id=\"FadenkreuzBlau\" fill=\"blue\" stroke=\"blue\" style=\"stroke-width:5\">\r\n\t\t\t<use xlink:href=\"#Crosshair\"/>\r\n\t\t</symbol>\r\n\t\t\r\n\t\t<symbol id=\"FadenkreuzRot\" fill=\"red\" stroke=\"red\" style=\"stroke-width:5\">\r\n\t\t\t<use xlink:href=\"#Crosshair\"/>\r\n\t\t</symbol>\r\n\t\t\r\n\t\t<symbol id=\"Crosshair\" >\r\n\t\t\t<path d=\"M 40,1 V 30 Z\" id=\"up\" />\r\n\t\t\t<path d=\"M 40,50 V 80 Z\" id=\"down\" />\r\n\t\t\t<path d=\"M 30,40 H 0 Z\" id=\"left\" />\r\n\t\t\t<path d=\"M 80,40 H 50 Z\" id=\"right\" />\r\n\t\t\t<circle style=\"fill:#000000; fill-opacity:0\" cx=\"40\" cy=\"40\" r=\"25\" />\r\n\t\t</symbol>\r\n\t\t\r\n\t\t<!-- Script -->\r\n\t\t<script type=\"text/javascript\">\r\n\t\t\t<![CDATA[\r\n\t\t\tvar blueCrosshairs, redCrosshair, indices, infoTxt;\r\n\t\t\tvar amountElements;\r\n\t\t\tvar actIndex = 1;\r\n\t\t\tvar textInfo = [\"Model ID: \", \"Pos X: \", \"Pos Y: \", \"Orientation: \", \"Clipped: \", \"Gray: \", \"Length: \", \"Width: \", \"Area: \", \"Score: \", \"Scale: \", \"Grading: \", \"Code Type: \", \"Text: \", \"Result: \"];\r\n\t\t\tvar visTextInfo = ["; for(zzIndex=0; zzIndex<1189l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};



{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->SvgTexts.ContentBetween1; plcstring* zzRValue=(plcstring*)"];\r\n\t\t\tvar data = [\r\n\t\t\t\t["; for(zzIndex=0; zzIndex<26l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};


{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->SvgTexts.ContentBetween2; plcstring* zzRValue=(plcstring*)"],\r\n\t\t\t\t["; for(zzIndex=0; zzIndex<9l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};






























































{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->SvgTexts.ContentEndJS; plcstring* zzRValue=(plcstring*)"]\r\n\t\t\t];\r\n\t\t\twindow.onload = function () {;\r\n\t\t\t\tblueCrosshairs \t= document.getElementsByName(\"crosshairBlue\");\r\n\t\t\t\tredCrosshair\t= document.getElementById(\"crosshairRed\");\r\n\t\t\t\tindices\t\t\t= document.getElementsByName(\"indice\");\r\n\t\t\t\tinfoTxt \t\t= document.getElementsByName(\"info\");\r\n\t\t\t\tamountElements\t= blueCrosshairs.length;\r\n\t\t\t\tlet i;\r\n\t\t\t\t//fill and position indices\r\n\t\t\t\tfor(i = 0; i < amountElements; i++) {\r\n\t\t\t\t\tindices[i].textContent = i + 1;\r\n\t\t\t\t\tlet transformValues = blueCrosshairs[i].getAttribute(\"transform\").match(/-?[\\d.]+/g);\r\n\t\t\t\t\tindices[i].setAttribute(\"x\", parseInt(transformValues[0]) + 85);\r\n\t\t\t\t\tindices[i].setAttribute(\"y\", parseInt(transformValues[1]) + 35 );\r\n\t\t\t\t}\r\n\t\t\t\t//set visibility of information text\r\n\t\t\t\tfor (i = 0; i< visTextInfo.length; i++ ){\r\n\t\t\t\t\tif (!visTextInfo[i]) {\r\n\t\t\t\t\t\tinfoTxt[i].style.display = \"none\"\r\n\t\t\t\t\t}\r\n\t\t\t\t}\r\n\t\t\t\t//fill information text with first element and position red crosshair\r\n\t\t\t\tupdateData(1);\r\n\t\t\t\tdocument.getElementById(\"crosshairRed\").style.display = \"inline\";\r\n\t\t\t\tdocument.getElementById(\"infogroup\").style.display = \"inline\";\r\n\t\t\t}\r\n\t\t\t\r\n\t\t\tfunction updateData(index) {\r\n\t\t\t\tindex--; // Internal indices start at 0\r\n\t\t\t\tlet i;\r\n\t\t\t\tfor (i = 0; i< visTextInfo.length; i++ ){\r\n\t\t\t\t\tif (visTextInfo[i]) {\r\n\t\t\t\t\t\tinfoTxt[i].textContent = textInfo[i] + data[index][i];\r\n\t\t\t\t\t}\r\n\t\t\t\t}\r\n\t\t\t\tdocument.getElementById(\"txtActElement\").textContent = index + 1;\r\n\t\t\t\t//manipulate position of red crosshair\r\n\t\t\t\tlet transform = blueCrosshairs[index].getAttribute(\"transform\");\r\n\t\t\t\tredCrosshair.setAttribute(\"transform\",transform);\r\n\t\t\t\t//set color of index number\r\n\t\t\t\tfor(i = 0; i < amountElements; i++) {\r\n\t\t\t\t\tindices[i].setAttribute(\"fill\",\"blue\");\r\n\t\t\t\t}\r\n\t\t\t\tindices[index].setAttribute(\"fill\",\"red\");\r\n\t\t\t}\r\n\t\t\tfunction decreaseIndex(){\r\n\t\t\t\tactIndex--;\r\n\t\t\t\tif(actIndex < 1) actIndex = amountElements ;\r\n\t\t\t\tupdateData(actIndex);\r\n\t\t\t}\r\n\t\t\tfunction increaseIndex(){\r\n\t\t\t\tactIndex++;\r\n\t\t\t\tif(actIndex > amountElements) actIndex = 1;\r\n\t\t\t\tupdateData(actIndex);\r\n\t\t\t}\r\n\t\t\t]]>\r\n\t\t</script>\r\n\t</defs>\r\n\t\r\n\t<!-- blue crosshairs -->\r\n"; for(zzIndex=0; zzIndex<2099l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};

{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->SvgTexts.ContentCrossHairBegin; plcstring* zzRValue=(plcstring*)"\t<use name=\"crosshairBlue\" xlink:href=\"#FadenkreuzBlau\" onclick=\"updateData("; for(zzIndex=0; zzIndex<76l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};

{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->SvgTexts.ContentCrossHairMiddle; plcstring* zzRValue=(plcstring*)")\" transform=\"translate("; for(zzIndex=0; zzIndex<24l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};


{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->SvgTexts.ContentCrossHairEnd; plcstring* zzRValue=(plcstring*)")\"/>\r\n"; for(zzIndex=0; zzIndex<6l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};






{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->SvgTexts.ContentCrossHairRed; plcstring* zzRValue=(plcstring*)"\r\n\t<!-- red crosshair -->\r\n\t<use id=\"crosshairRed\" xlink:href=\"#FadenkreuzRot\" style=\"display: none\"/>\r\n\t\r\n\t<!-- indices -->\r\n"; for(zzIndex=0; zzIndex<126l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};


{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->SvgTexts.ContentCrossHairIndex; plcstring* zzRValue=(plcstring*)"\t<text class=\"cText\" font-size=\"22\" font-family=\"Helvetica, Arial, sans-serif\" > <tspan class=\"text\" name=\"indice\" fill=\"blue\"></tspan></text>\r\n"; for(zzIndex=0; zzIndex<144l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};



























{int zzIndex; plcstring* zzLValue=(plcstring*)__inst__->SvgTexts.ContentEnd; plcstring* zzRValue=(plcstring*)"\r\n\t<g id=\"infogroup\" transform=\"translate(20,30)\" style=\"display: none\">\r\n\t\t<!-- buttons -->\r\n\t\t<path d=\"M0,30, L60,60, L60,0, Z\" stroke=\"black\" fill=\"red\" onclick=\"decreaseIndex()\" style=\"stroke-width:5\"/>\r\n\t\t<text id=\"txtActElement\" fill=\"red\" font-size=\"75\" font-family=\"Helvetica, Arial, sans-serif\" text-anchor=\"middle\" x=\"102\" y=\"55\"></text>\r\n\t\t<path d=\"M150,60, L210,30, L150,0, Z\" stroke=\"black\" fill=\"red\" onclick=\"increaseIndex()\" style=\"stroke-width:5\"/>\r\n\t\t\r\n\t\t<!-- informations -->\r\n\t\t<text class=\"cText\" fill=\"red\" font-size=\"35\" font-family=\"Helvetica, Arial, sans-serif\" x=\"0\" y=\"80\">\r\n\t\t\t<tspan class=\"text\" name=\"info\" x=\"0\" dy=\"32\"></tspan>\r\n\t\t\t<tspan class=\"text\" name=\"info\" x=\"0\" dy=\"32\"></tspan>\r\n\t\t\t<tspan class=\"text\" name=\"info\" x=\"0\" dy=\"32\"></tspan>\r\n\t\t\t<tspan class=\"text\" name=\"info\" x=\"0\" dy=\"32\"></tspan>\r\n\t\t\t<tspan class=\"text\" name=\"info\" x=\"0\" dy=\"32\"></tspan>\r\n\t\t\t<tspan class=\"text\" name=\"info\" x=\"0\" dy=\"32\"></tspan>\r\n\t\t\t<tspan class=\"text\" name=\"info\" x=\"0\" dy=\"32\"></tspan>\r\n\t\t\t<tspan class=\"text\" name=\"info\" x=\"0\" dy=\"32\"></tspan>\r\n\t\t\t<tspan class=\"text\" name=\"info\" x=\"0\" dy=\"32\"></tspan>\r\n\t\t\t<tspan class=\"text\" name=\"info\" x=\"0\" dy=\"32\"></tspan>\r\n\t\t\t<tspan class=\"text\" name=\"info\" x=\"0\" dy=\"32\"></tspan>\r\n\t\t\t<tspan class=\"text\" name=\"info\" x=\"0\" dy=\"32\"></tspan>\r\n\t\t\t<tspan class=\"text\" name=\"info\" x=\"0\" dy=\"32\"></tspan>\r\n\t\t\t<tspan class=\"text\" name=\"info\" x=\"0\" dy=\"32\"></tspan>\r\n\t\t\t<tspan class=\"text\" name=\"info\" x=\"0\" dy=\"32\"></tspan>\r\n\t\t</text>\r\n\t</g>\r\n</svg>"; for(zzIndex=0; zzIndex<1511l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}}
#line 355 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViSaveImgOnPlc.nodebug"

void __AS__ImplInitViSaveImgOnPlc_st(void){}

__asm__(".section \".plc\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/VisionTypes.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/mappRecipe/RecTypes.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBaseCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAxCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxisCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxis.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxisError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipe.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipeAlarm.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipeError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpComError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/TcpForward/Types.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/IecCheck/IecCheck.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxis.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipe.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/TcpForward/TcpForward.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/VisionVariables.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/mappRecipe/RecVariables.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/McElements.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/MpComponents.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/MpVisionComponents.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/Vi_image/Types.typ\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/Vi_image/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/Vi_image/LocalFunctions.fun\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_image/ViSaveImgOnPlc.st.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"plcreplace \\\"C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_image/ViSaveImgOnPlc.st.c\\\" \\\"C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_image/ViSaveImgOnPlc.st\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViDownloadImage\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViRefreshImageList\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViSaveImgOnPlc\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViDrawCrosshair\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViCreateWebDirFile\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfub \\\"ViShowImgOnVC4\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfun \\\"CrosshairDetailsValue\\\"\\n\"");
__asm__(".ascii \"plc_iec_localfun \\\"CrosshairDetailsText\\\"\\n\"");
__asm__(".previous");

__asm__(".section \".plciec\"");
__asm__(".ascii \"plcdata_const 'ERR_NO_IMAGE'\\n\"");
__asm__(".ascii \"plcdata_const 'ERR_BUFF_TOO_SMALL'\\n\"");
__asm__(".ascii \"plcdata_const 'MAX_NUM_RESULTS'\\n\"");
__asm__(".ascii \"plcdata_const 'ERR_OK'\\n\"");
__asm__(".ascii \"plcdata_const 'ERR_FUB_ENABLE_FALSE'\\n\"");
__asm__(".ascii \"plcdata_const 'ERR_FUB_BUSY'\\n\"");
__asm__(".ascii \"plcdata_const 'httpMETHOD_GET'\\n\"");
__asm__(".ascii \"plcdata_const 'httpOPTION_HTTP_11'\\n\"");
__asm__(".previous");
