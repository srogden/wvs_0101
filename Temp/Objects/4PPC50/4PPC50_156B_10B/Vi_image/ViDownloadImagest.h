#ifndef __AS__TYPE_
#define __AS__TYPE_
static signed long __AS__STRING_CMP(char* pstr1, char* pstr2);
typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

#ifndef __AS__TYPE_httpHeaderLine_t
#define __AS__TYPE_httpHeaderLine_t
typedef struct httpHeaderLine_t
{	plcstring name[51];
	plcstring value[81];
} httpHeaderLine_t;
#endif

#ifndef __AS__TYPE_httpRawHeader_t
#define __AS__TYPE_httpRawHeader_t
typedef struct httpRawHeader_t
{	unsigned long pData;
	unsigned long dataSize;
	unsigned long dataLen;
} httpRawHeader_t;
#endif

#ifndef __AS__TYPE_httpResponseHeader_t
#define __AS__TYPE_httpResponseHeader_t
typedef struct httpResponseHeader_t
{	plcstring protocol[21];
	plcstring status[281];
	plcstring contentType[101];
	unsigned long contentLength;
	plcstring connection[81];
	plcstring keepAlive[81];
	struct httpHeaderLine_t userLine[8];
	httpRawHeader_t rawHeader;
} httpResponseHeader_t;
#endif

struct AsMemPartCreate
{	unsigned long len;
	unsigned short status;
	unsigned long ident;
	plcbit enable;
};
_BUR_PUBLIC void AsMemPartCreate(struct AsMemPartCreate* inst);
struct AsMemPartAllocClear
{	unsigned long ident;
	unsigned long len;
	unsigned short status;
	unsigned long mem;
	plcbit enable;
};
_BUR_PUBLIC void AsMemPartAllocClear(struct AsMemPartAllocClear* inst);
struct FileOpen
{	unsigned long pDevice;
	unsigned long pFile;
	unsigned char mode;
	unsigned short status;
	unsigned long ident;
	unsigned long filelen;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileOpen(struct FileOpen* inst);
struct FileRead
{	unsigned long ident;
	unsigned long offset;
	unsigned long pDest;
	unsigned long len;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileRead(struct FileRead* inst);
struct FileClose
{	unsigned long ident;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileClose(struct FileClose* inst);
struct TON
{	plctime PT;
	plctime ET;
	plctime StartTime;
	unsigned long Restart;
	plcbit IN;
	plcbit Q;
	plcbit M;
};
_BUR_PUBLIC void TON(struct TON* inst);
struct httpService
{	unsigned long option;
	unsigned long pServiceName;
	unsigned long pUri;
	unsigned long uriSize;
	unsigned long pRequestHeader;
	unsigned long pRequestData;
	unsigned long requestDataSize;
	unsigned long pResponseHeader;
	unsigned long pResponseData;
	unsigned long responseDataLen;
	unsigned long pStatistics;
	unsigned long pStruct;
	unsigned short status;
	unsigned short method;
	unsigned long requestDataLen;
	unsigned short phase;
	unsigned long _i_state;
	unsigned short _i_result;
	unsigned long _ident;
	unsigned long _oldEnable;
	unsigned long _oldAbort;
	unsigned long _oldSend;
	unsigned long _state;
	unsigned long _internal;
	plcbit enable;
	plcbit send;
	plcbit abort;
};
_BUR_PUBLIC void httpService(struct httpService* inst);
struct ViDownloadImage
{	plcstring FileDevice[81];
	plcstring FileName[81];
	plcstring ImgName[81];
	plcstring(* visDownloadFileUrl)[201];
	unsigned short Status;
	signed short Step;
	struct AsMemPartCreate AsMemPartCreate_0;
	struct AsMemPartAllocClear AsMemPartAllocClear_0;
	unsigned long MemSvgAdr;
	unsigned long MemSvgSize;
	struct FileOpen FileOpen_0;
	struct FileRead FileRead_0;
	struct FileClose FileClose_0;
	struct TON TON_Timeout;
	struct httpService httpService_Download;
	httpResponseHeader_t response_header;
	plcstring response_header_data[301];
	plcbit Enable;
};
void ViDownloadImage(struct ViDownloadImage* inst);
_BUR_PUBLIC unsigned long brsmemset(unsigned long pDest, unsigned char value, unsigned long length);
_BUR_PUBLIC unsigned long brsstrcat(unsigned long pDest, unsigned long pSrc);
_BUR_PUBLIC unsigned long brsstrlen(unsigned long pString);
_BUR_PUBLIC unsigned long brsstrcpy(unsigned long pDest, unsigned long pSrc);
_BUR_LOCAL unsigned short ERR_TIMEOUT_DOWNLOAD;
_BUR_LOCAL unsigned short ERR_MEM_DOWNLOAD;
_GLOBAL unsigned short ERR_OK;
_GLOBAL unsigned short ERR_FUB_ENABLE_FALSE;
_GLOBAL unsigned short ERR_FUB_BUSY;
_GLOBAL unsigned short httpOPTION_HTTP_10;
_GLOBAL unsigned short httpOPTION_SERVICE_TYPE_NAME;
_GLOBAL unsigned short httpPHASE_WAITING;
