#ifndef __AS__TYPE_VIStep_Enum
#define __AS__TYPE_VIStep_Enum
typedef enum VIStep_Enum
{	VISTEP_WAIT = 0,
	VISTEP_CREATE_DIR = 1,
	VISTEP_DELETE_DIR = 2,
	VISTEP_DELETE_IMAGE = 3,
	VISTEP_REFRESH_IMG_LIST = 4,
	VISTEP_DOWNLOAD_IMAGE = 5,
	VISTEP_SAVE_IMG_ON_PLC = 6,
	VISTEP_ERR = 7,
} VIStep_Enum;
#endif

#ifndef __AS__TYPE_SvgTexts_Type
#define __AS__TYPE_SvgTexts_Type
typedef struct SvgTexts_Type
{	plcstring ContentStart[501];
	plcstring ContentAfterTitle[201];
	plcstring ContentBase64[201];
	plcstring ContentBeginJS[2001];
	plcstring ContentBetween1[201];
	plcstring ContentBetween2[201];
	plcstring ContentEndJS[3001];
	plcstring ContentCrossHairBegin[201];
	plcstring ContentCrossHairMiddle[201];
	plcstring ContentCrossHairEnd[201];
	plcstring ContentCrossHairRed[201];
	plcstring ContentCrossHairIndex[201];
	plcstring ContentEnd[2001];
} SvgTexts_Type;
#endif

#ifndef __AS__TYPE_DiagTime_Type
#define __AS__TYPE_DiagTime_Type
typedef struct DiagTime_Type
{	plctime Refresh;
	plctime BeginSaveBMPJPG;
	plctime EndSaveBMPJPG;
	plctime BeginSVG;
	plctime BeginSVGSave;
	plctime EndSVGSave;
} DiagTime_Type;
#endif

#ifndef __AS__TYPE_MemInfoSave_Type
#define __AS__TYPE_MemInfoSave_Type
typedef struct MemInfoSave_Type
{	unsigned long UploadAdr;
	unsigned long UploadSize;
	unsigned long SvgAdr;
	unsigned long SvgSize;
	unsigned long CrosshAdr;
	unsigned long CrosshSize;
	unsigned long SvgHeaderSize;
} MemInfoSave_Type;
#endif

#ifndef __AS__TYPE_MemInfoVC4_Type
#define __AS__TYPE_MemInfoVC4_Type
typedef struct MemInfoVC4_Type
{	unsigned long UploadBmp8Adr;
	unsigned long UploadBmp8Size;
	unsigned long Bmp24Adr;
	unsigned long Bmp24Size;
} MemInfoVC4_Type;
#endif

#ifndef __AS__TYPE_typCrossHairCfg
#define __AS__TYPE_typCrossHairCfg
typedef struct typCrossHairCfg
{	float Scale;
	float PixelRatio;
	unsigned char Size;
	unsigned char Font;
} typCrossHairCfg;
#endif

#ifndef __AS__TYPE_typCrossHair
#define __AS__TYPE_typCrossHair
typedef struct typCrossHair
{	plcstring SvgTransformation[10][201];
	plcstring SvgContent[10][1001];
	plcbit SvgVisible[10];
	typCrossHairCfg CFG;
	unsigned char DetailsNo;
	plcbit ShowCrosshair;
} typCrossHair;
#endif

#ifndef __AS__TYPE_typCrossHairInfo
#define __AS__TYPE_typCrossHairInfo
typedef struct typCrossHairInfo
{	float CrosshairTopLeftX;
	float CrosshairTopLeftY;
	float CrosshairRotateCenter;
	float CrosshairSizeScaled;
	float CrosshairScale;
	plcstring DataVis[81];
	plcstring Data[401];
	plcstring Text[1001];
} typCrossHairInfo;
#endif

#ifndef __AS__TYPE_BmpFileHeader_Type
#define __AS__TYPE_BmpFileHeader_Type
typedef struct BmpFileHeader_Type
{	unsigned char Fill[2];
	unsigned char bfType[2];
	unsigned long bfSize;
	unsigned long bfReserved;
	unsigned long bfOffBits;
} BmpFileHeader_Type;
#endif

#ifndef __AS__TYPE_BmpInfoHeader_Type
#define __AS__TYPE_BmpInfoHeader_Type
typedef struct BmpInfoHeader_Type
{	unsigned long biSize;
	signed long biWidth;
	signed long biHeight;
	unsigned short biPlanes;
	unsigned short biBitCount;
	unsigned long biCompression;
	unsigned long biSizeImage;
	signed long biXPelsPerMeter;
	signed long biYPelsPerMeter;
	unsigned long biClrUSer;
	unsigned long biClrImportant;
} BmpInfoHeader_Type;
#endif

#ifndef __AS__TYPE_Bmp24Bit_Type
#define __AS__TYPE_Bmp24Bit_Type
typedef struct Bmp24Bit_Type
{	BmpFileHeader_Type FileHeader;
	BmpInfoHeader_Type InfoHeader;
} Bmp24Bit_Type;
#endif

#ifndef __AS__TYPE_BmpColorTable_Type
#define __AS__TYPE_BmpColorTable_Type
typedef struct BmpColorTable_Type
{	unsigned char Blue;
	unsigned char Green;
	unsigned char Red;
	unsigned char Fill;
} BmpColorTable_Type;
#endif

#ifndef __AS__TYPE_Bmp8Bit_Type
#define __AS__TYPE_Bmp8Bit_Type
typedef struct Bmp8Bit_Type
{	BmpFileHeader_Type FileHeader;
	BmpInfoHeader_Type InfoHeader;
	struct BmpColorTable_Type ColorTable[256];
} Bmp8Bit_Type;
#endif

#ifndef __AS__TYPE_PixelDestColor_Type
#define __AS__TYPE_PixelDestColor_Type
typedef struct PixelDestColor_Type
{	unsigned char Blue;
	unsigned char Green;
	unsigned char Red;
} PixelDestColor_Type;
#endif

#ifndef __AS__TYPE_typVisionCommand
#define __AS__TYPE_typVisionCommand
typedef struct typVisionCommand
{	plcbit ImageTrigger;
	plcbit ImageTriggerReset;
	plcbit AutoSetupStartStop;
	plcbit VaSwitchApplication;
	plcbit SaveDiagData;
	plcbit ReadCameraInfo;
} typVisionCommand;
#endif

#ifndef __AS__TYPE_enumVisionFunction
#define __AS__TYPE_enumVisionFunction
typedef enum enumVisionFunction
{	enumFunctionNone = 0,
	enumBlob = 1,
	enumCoderReader = 2,
	enumMatch = 3,
	enumMeasurement = 4,
	enumOCR = 5,
	enumPixelCounter = 6,
} enumVisionFunction;
#endif

#ifndef __AS__TYPE_ViComponentInternalMappLinkType
#define __AS__TYPE_ViComponentInternalMappLinkType
typedef struct ViComponentInternalMappLinkType
{	unsigned long Internal[2];
} ViComponentInternalMappLinkType;
#endif

#ifndef __AS__TYPE_ViComponentType
#define __AS__TYPE_ViComponentType
typedef struct ViComponentType
{	struct ViComponentInternalCameraIfType(* CameraType);
	ViComponentInternalMappLinkType MappLinkInternal;
} ViComponentType;
#endif

#ifndef __AS__TYPE_typVisionConfig
#define __AS__TYPE_typVisionConfig
typedef struct typVisionConfig
{	enumVisionFunction VisionFunction;
	unsigned char PowerlinkNode;
	unsigned long DataStructure;
	ViComponentType ComponentLink;
	unsigned short ResolutionWidth_X;
	unsigned short ResolutionHeight_Y;
} typVisionConfig;
#endif

#ifndef __AS__TYPE_typVisionData
#define __AS__TYPE_typVisionData
typedef struct typVisionData
{	plcbit Enable;
	unsigned char MaxItemCnt;
	signed long NettimeDelay;
	unsigned short Timeout;
	unsigned char Gain;
	unsigned short Focus;
	unsigned long Exposure;
	unsigned char FlashColor;
	unsigned char FlashSegment;
	unsigned char LedColor;
	plcbit IRFilter;
	signed long OffsetROIX;
	signed long OffsetROIY;
	signed long OffsetROIRotationCenterX;
	signed long OffsetROIRotationCenterY;
	signed short OffsetROIOrientation;
	plcbit ChromaticLock;
	unsigned char Alignment;
	unsigned char AutoSetupGain;
	unsigned short AutoSetupFocus;
	unsigned long AutoSetupExposure;
	unsigned char AcquisitionAcceptedCnt;
	unsigned char AcquisitionCompletedCnt;
	unsigned char AcquisitionFailedCnt;
	unsigned char LightWarningCnt;
	unsigned short ImageProcessingError;
	plcbit ImageProcessingActive;
	signed long ImageNettime;
	unsigned short ProcessingTimeCamera;
	unsigned short ProcessingTimeFunction;
	unsigned char ResultCnt;
	plcbit CockpitIsActive;
	unsigned long Status;
	plcstring StatusText[31];
	plcbit Active;
} typVisionData;
#endif

#ifndef __AS__TYPE_typVisionFunction
#define __AS__TYPE_typVisionFunction
typedef struct typVisionFunction
{	plcstring ApplicationName[41];
	signed long Status;
} typVisionFunction;
#endif

#ifndef __AS__TYPE_typVisionDiag
#define __AS__TYPE_typVisionDiag
typedef struct typVisionDiag
{	plcstring DiagName[81];
	signed long Status;
} typVisionDiag;
#endif

#ifndef __AS__TYPE_typVisionOptics
#define __AS__TYPE_typVisionOptics
typedef struct typVisionOptics
{	plcstring OrderNr[41];
	plcbit Binning;
	signed short ResolutionX;
	signed short ResolutionY;
	unsigned char Lens;
	unsigned char Sensor;
	plcbit ValidDistance;
	double Distance_mm;
	double DistanceLens_mm;
	double HyperFocalDistance_mm;
	double PixelSize_um;
	double PixelSizeBinning_um;
	double FocalLength_mm;
	double MinObjectDist_mm;
	double MaxObjectDist_mm;
	double Aperture;
	double CircleOfConfusion_mm;
	double DepthOfFieldNearPos_mm;
	double DepthOfFieldFarPos_mm;
	double DepthOfField_mm;
	double FieldOfView_X_mm;
	double FieldOfView_Y_mm;
	double Resolution_mmPerPx;
} typVisionOptics;
#endif

#ifndef __AS__TYPE_typVisionHW
#define __AS__TYPE_typVisionHW
typedef struct typVisionHW
{	plcbit Connected;
	plcbit Ready;
	plcbit DigitalInput01;
	plcbit DigitalOutput01;
	plcbit DigitalOutputStatus01;
	plcbit DigitalOutput02;
	plcbit DigitalOutputStatus02;
	unsigned long SerialNumber;
	unsigned long ID;
	unsigned long Variant;
	unsigned long Firmware;
	plcbit Undervoltage;
	signed char Temperature;
} typVisionHW;
#endif

#ifndef __AS__TYPE_typVisionMain
#define __AS__TYPE_typVisionMain
typedef struct typVisionMain
{	typVisionCommand CMD;
	typVisionConfig CFG;
	typVisionData DAT;
	typVisionFunction FCT;
	typVisionDiag DIA;
	typVisionOptics OPT;
	typVisionHW HW;
} typVisionMain;
#endif

#ifndef __AS__TYPE_typVisionImageCommand
#define __AS__TYPE_typVisionImageCommand
typedef struct typVisionImageCommand
{	plcbit CreateDir;
	plcbit DeleteDir;
	plcbit DeleteImage;
	plcbit Refresh;
	plcbit DownloadImage;
	plcbit SaveImageOnPLC;
	plcbit ResetError;
	plcbit RefreshCrosshair;
} typVisionImageCommand;
#endif

#ifndef __AS__TYPE_typVisionImageConfig
#define __AS__TYPE_typVisionImageConfig
typedef struct typVisionImageConfig
{	plcstring FileDevice[81];
	plcstring DirName[81];
	plcstring PlkIPWithoutNode[81];
	plcstring EthDevice[81];
	plcstring EthIP[81];
	unsigned long ConvertCycles;
	unsigned char Format;
	unsigned char QualityJPG;
	plcbit UploadBmpJpg;
	plcbit UploadSVG;
	unsigned char MainPageQualityJPG;
	unsigned short ImageRotation_deg;
} typVisionImageConfig;
#endif

#ifndef __AS__TYPE_typVisionImageData
#define __AS__TYPE_typVisionImageData
typedef struct typVisionImageData
{	plcstring Images[20][81];
	unsigned short Status;
	plcbit VisionDisabled;
} typVisionImageData;
#endif

#ifndef __AS__TYPE_typVisionImage
#define __AS__TYPE_typVisionImage
typedef struct typVisionImage
{	typVisionImageCommand CMD;
	typVisionImageConfig CFG;
	typVisionImageData DAT;
} typVisionImage;
#endif

#ifndef __AS__TYPE_typCodeReaderMain
#define __AS__TYPE_typCodeReaderMain
typedef struct typCodeReaderMain
{	unsigned char CodeType;
	plcbit GradingEnable;
	plcbit RobustnessEnable;
	unsigned char ParameterMode;
	unsigned char ParameterOptimization;
	plcstring BarcodeText[10][101];
	unsigned char BarcodeType[10];
	signed long PositionX[10];
	signed long PositionY[10];
	signed short Orientation[10];
	signed char Grading[10];
	unsigned char EnhancedGrading[23];
} typCodeReaderMain;
#endif

#ifndef __AS__TYPE_typBlobMain
#define __AS__TYPE_typBlobMain
typedef struct typBlobMain
{	plcbit RegionalFeature;
	plcbit EnhancedBlobInformation;
	unsigned char ModelNumber[10];
	plcbit Clipped[10];
	unsigned long Area[10];
	signed long PositionX[10];
	signed long PositionY[10];
	signed short Orientation[10];
	unsigned char MeanGrayValue[10];
	unsigned long Length[10];
	unsigned long Width[10];
} typBlobMain;
#endif

#ifndef __AS__TYPE_typMatchMain
#define __AS__TYPE_typMatchMain
typedef struct typMatchMain
{	unsigned char MinScore;
	unsigned char MaxOverlap;
	unsigned char ModelNumber[10];
	unsigned char Score[10];
	signed long PositionX[10];
	signed long PositionY[10];
	signed short Orientation[10];
	unsigned char Scale[10];
	signed long RotCenterX[10];
	signed long RotCenterY[10];
} typMatchMain;
#endif

#ifndef __AS__TYPE_typOCRMain
#define __AS__TYPE_typOCRMain
typedef struct typOCRMain
{	unsigned char ParameterMode;
	plcbit GradingEnable;
	plcstring Text[10][51];
	unsigned char Grading[10];
	signed long PositionX[10];
	signed long PositionY[10];
	signed short Orientation[10];
} typOCRMain;
#endif

#ifndef __AS__TYPE_typPixelMain
#define __AS__TYPE_typPixelMain
typedef struct typPixelMain
{	plcbit EnhancedPixelCounterInformation;
	unsigned char ModelNumber[10];
	unsigned long NumPixels[10];
	unsigned char MinGray[10];
	unsigned char MaxGray[10];
	unsigned short MeanGray[10];
	unsigned short DeviationGray[10];
	unsigned char MedianGray[10];
	unsigned long ModelArea[10];
	unsigned short NumConnectedComponents[10];
	signed long PositionX[10];
	signed long PositionY[10];
} typPixelMain;
#endif

#ifndef __AS__TYPE_typMTMain
#define __AS__TYPE_typMTMain
typedef struct typMTMain
{	plcbit UseResultAsXY;
	unsigned char ParameterMode;
	signed long Result[10];
} typMTMain;
#endif

#ifndef __AS__TYPE_DTStructure
#define __AS__TYPE_DTStructure
typedef struct DTStructure
{	unsigned short year;
	unsigned char month;
	unsigned char day;
	unsigned char wday;
	unsigned char hour;
	unsigned char minute;
	unsigned char second;
	unsigned short millisec;
	unsigned short microsec;
} DTStructure;
#endif

#ifndef __AS__TYPE_fiDIR_READ_DATA
#define __AS__TYPE_fiDIR_READ_DATA
typedef struct fiDIR_READ_DATA
{	unsigned char Filename[260];
	plcdt Date;
	unsigned long Filelength;
} fiDIR_READ_DATA;
#endif

#ifndef __AS__TYPE_httpHeaderLine_t
#define __AS__TYPE_httpHeaderLine_t
typedef struct httpHeaderLine_t
{	plcstring name[51];
	plcstring value[81];
} httpHeaderLine_t;
#endif

#ifndef __AS__TYPE_httpRawHeader_t
#define __AS__TYPE_httpRawHeader_t
typedef struct httpRawHeader_t
{	unsigned long pData;
	unsigned long dataSize;
	unsigned long dataLen;
} httpRawHeader_t;
#endif

#ifndef __AS__TYPE_httpResponseHeader_t
#define __AS__TYPE_httpResponseHeader_t
typedef struct httpResponseHeader_t
{	plcstring protocol[21];
	plcstring status[281];
	plcstring contentType[101];
	unsigned long contentLength;
	plcstring connection[81];
	plcstring keepAlive[81];
	struct httpHeaderLine_t userLine[8];
	httpRawHeader_t rawHeader;
} httpResponseHeader_t;
#endif

#ifndef __AS__TYPE_httpRequestHeader_t
#define __AS__TYPE_httpRequestHeader_t
typedef struct httpRequestHeader_t
{	plcstring protocol[21];
	plcstring host[281];
	plcstring contentType[101];
	unsigned long contentLength;
	plcstring connection[81];
	plcstring keepAlive[81];
	struct httpHeaderLine_t userLine[8];
	httpRawHeader_t rawHeader;
} httpRequestHeader_t;
#endif

#ifndef __AS__TYPE_ViComponentInternalCameraIfType
#define __AS__TYPE_ViComponentInternalCameraIfType
typedef struct ViComponentInternalCameraIfType
{	plcdword Vtable;
} ViComponentInternalCameraIfType;
#endif

#ifndef __AS__TYPE_TcpForwardInfoStateEnum
#define __AS__TYPE_TcpForwardInfoStateEnum
typedef enum TcpForwardInfoStateEnum
{	TcpForwardInfoState_NotRunning = 0,
	TcpForwardInfoState_Listening = 1,
	TcpForwardInfoState_BuildingCon = 2,
	TcpForwardInfoState_ClientActive = 3,
} TcpForwardInfoStateEnum;
#endif

#ifndef __AS__TYPE_TcpForwardInfoTyp
#define __AS__TYPE_TcpForwardInfoTyp
typedef struct TcpForwardInfoTyp
{	signed long DataSend;
	signed long DataRecieved;
	TcpForwardInfoStateEnum State;
	plcstring LastConnectedClientIp[16];
	unsigned char ConnectionsActive;
} TcpForwardInfoTyp;
#endif

struct AsMemPartCreate
{	unsigned long len;
	unsigned short status;
	unsigned long ident;
	plcbit enable;
};
_BUR_PUBLIC void AsMemPartCreate(struct AsMemPartCreate* inst);
struct AsMemPartAllocClear
{	unsigned long ident;
	unsigned long len;
	unsigned short status;
	unsigned long mem;
	plcbit enable;
};
_BUR_PUBLIC void AsMemPartAllocClear(struct AsMemPartAllocClear* inst);
struct FileOpen
{	unsigned long pDevice;
	unsigned long pFile;
	unsigned char mode;
	unsigned short status;
	unsigned long ident;
	unsigned long filelen;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileOpen(struct FileOpen* inst);
struct FileRead
{	unsigned long ident;
	unsigned long offset;
	unsigned long pDest;
	unsigned long len;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileRead(struct FileRead* inst);
struct FileClose
{	unsigned long ident;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileClose(struct FileClose* inst);
struct TON
{	plctime PT;
	plctime ET;
	plctime StartTime;
	unsigned long Restart;
	plcbit IN;
	plcbit Q;
	plcbit M;
};
_BUR_PUBLIC void TON(struct TON* inst);
struct httpService
{	unsigned long option;
	unsigned long pServiceName;
	unsigned long pUri;
	unsigned long uriSize;
	unsigned long pRequestHeader;
	unsigned long pRequestData;
	unsigned long requestDataSize;
	unsigned long pResponseHeader;
	unsigned long pResponseData;
	unsigned long responseDataLen;
	unsigned long pStatistics;
	unsigned long pStruct;
	unsigned short status;
	unsigned short method;
	unsigned long requestDataLen;
	unsigned short phase;
	unsigned long _i_state;
	unsigned short _i_result;
	unsigned long _ident;
	unsigned long _oldEnable;
	unsigned long _oldAbort;
	unsigned long _oldSend;
	unsigned long _state;
	unsigned long _internal;
	plcbit enable;
	plcbit send;
	plcbit abort;
};
_BUR_PUBLIC void httpService(struct httpService* inst);
struct ViDownloadImage
{	plcstring FileDevice[81];
	plcstring FileName[81];
	plcstring ImgName[81];
	plcstring(* visDownloadFileUrl)[201];
	unsigned short Status;
	signed short Step;
	struct AsMemPartCreate AsMemPartCreate_0;
	struct AsMemPartAllocClear AsMemPartAllocClear_0;
	unsigned long MemSvgAdr;
	unsigned long MemSvgSize;
	struct FileOpen FileOpen_0;
	struct FileRead FileRead_0;
	struct FileClose FileClose_0;
	struct TON TON_Timeout;
	struct httpService httpService_Download;
	httpResponseHeader_t response_header;
	plcstring response_header_data[301];
	plcbit Enable;
};
void ViDownloadImage(struct ViDownloadImage* inst);
struct DirRead
{	unsigned long pDevice;
	unsigned long pPath;
	unsigned long entry;
	unsigned char option;
	unsigned long pData;
	unsigned long data_len;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void DirRead(struct DirRead* inst);
struct FileDelete
{	unsigned long pDevice;
	unsigned long pName;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileDelete(struct FileDelete* inst);
struct ViRefreshImageList
{	struct typVisionImage(* VisionImage);
	plcstring(* ImageList)[20][81];
	plcstring(* visSelectedImage)[81];
	unsigned short Status;
	unsigned short DirEntries;
	signed short Step;
	plcstring visSelectedImageOld[81];
	unsigned short idx;
	plcstring file_newest[81];
	plcstring file_oldest[81];
	plcdt date_newest;
	plcdt date_oldest;
	struct DirRead DirRead_0;
	struct FileDelete FileDelete_0;
	fiDIR_READ_DATA dir_data;
	plcstring DeleteFileName[81];
	struct TON TON_VisPause;
	plcbit Enable;
	plcbit cmdDeleteOldest;
};
void ViRefreshImageList(struct ViRefreshImageList* inst);
struct DTGetTime
{	unsigned short status;
	plcdt DT1;
	plcbit enable;
};
_BUR_PUBLIC void DTGetTime(struct DTGetTime* inst);
struct httpClient
{	unsigned long option;
	unsigned long pHost;
	unsigned short hostPort;
	unsigned long method;
	unsigned long pUri;
	unsigned long pRequestHeader;
	unsigned long pRequestData;
	unsigned long requestDataLen;
	unsigned long pResponseHeader;
	unsigned long pResponseData;
	unsigned long responseDataSize;
	unsigned long pStatistics;
	unsigned long pStruct;
	unsigned short status;
	unsigned short tcpStatus;
	unsigned short httpStatus;
	unsigned long responseDataLen;
	unsigned short phase;
	unsigned long _i_state;
	unsigned short _i_result;
	unsigned long _ident;
	unsigned long _oldEnable;
	unsigned long _oldAbort;
	unsigned long _oldSend;
	unsigned long _state;
	unsigned long _internal;
	plcbit enable;
	plcbit send;
	plcbit abort;
};
_BUR_PUBLIC void httpClient(struct httpClient* inst);
struct FileCreate
{	unsigned long pDevice;
	unsigned long pFile;
	unsigned short status;
	unsigned long ident;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileCreate(struct FileCreate* inst);
struct FileWrite
{	unsigned long ident;
	unsigned long offset;
	unsigned long pSrc;
	unsigned long len;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileWrite(struct FileWrite* inst);
struct ViSaveImgOnPlc
{	typVisionImageConfig CFG;
	unsigned char PowerlinkNode;
	struct typCrossHairInfo(* CrossHairInfo)[11];
	unsigned short Status;
	signed short Step;
	struct AsMemPartCreate AsMemPartCreate_0;
	struct AsMemPartAllocClear AsMemPartAllocClear_0;
	MemInfoSave_Type MemInfo;
	plctime DiagStartTime;
	DiagTime_Type DiagTime;
	struct DTGetTime DTGetTime_0;
	DTStructure DTStructure_0;
	plcstring FileNameImg[81];
	plcstring FileNameSvg[81];
	plcstring URI[81];
	plcstring Host[81];
	struct httpClient httpClient_0;
	unsigned short httpClientErrorStatus;
	httpRequestHeader_t RequestHeader;
	struct TON TON_ReloadTimeout;
	unsigned long UploadedImgSize;
	struct FileCreate FileCreate_0;
	struct FileWrite FileWrite_0;
	struct FileClose FileClose_0;
	unsigned long b64AdrInBuffer;
	unsigned long b64AdrOutBuffer;
	unsigned long b64neededOutput;
	unsigned char b64key[64];
	unsigned long b64actposIN;
	unsigned long b64actposOUT;
	unsigned char(* b64in)[3];
	unsigned char(* b64out)[4];
	unsigned short b64blockLen;
	SvgTexts_Type SvgTexts;
	plcstring tmpStr1[81];
	unsigned long i;
	plcbit Enable;
	plcbit FirstElement;
};
void ViSaveImgOnPlc(struct ViSaveImgOnPlc* inst);
struct ViDrawCrosshair
{	unsigned short SelectedSensor;
	struct typVisionMain(* VisionSensor);
	plcstring(* CodeTypes)[70][81];
	unsigned short ImageRotation_deg;
	struct typCrossHair(* visCrossHair);
	struct typCrossHairInfo CrossHairInfo[11];
	unsigned short idx;
	unsigned short SelectedSensorOld;
	struct typBlobMain(* Blob);
	struct typCodeReaderMain(* CodeReader);
	struct typMatchMain(* Match);
	struct typMTMain(* MT);
	struct typPixelMain(* Pixel);
	struct typOCRMain(* OCR);
	unsigned char DetailsNoOld;
	unsigned short ImageRotation_degOld;
	unsigned char ResultModelNumber;
	float ResultPositionX;
	float ResultPositionY;
	float ResultOrientation;
	float CrosshSize;
	float ScaleVertical;
	plcstring svgTrafo[201];
	plcstring svgContent[1001];
	plcstring tmpStr[101];
	signed short blueTextPos;
	plcbit CmdRefreshCrosshair;
	plcbit VisionDisabled;
	plcbit TextAlignment;
	plcbit ShowCrosshairOld;
	plcbit TextAlignmentOld;
	plcbit MT_UseXY;
};
void ViDrawCrosshair(struct ViDrawCrosshair* inst);
struct DirInfo
{	unsigned long pDevice;
	unsigned long pPath;
	unsigned short status;
	unsigned long dirnum;
	unsigned long filenum;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void DirInfo(struct DirInfo* inst);
struct DirCreate
{	unsigned long pDevice;
	unsigned long pName;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void DirCreate(struct DirCreate* inst);
struct DevLink
{	unsigned long pDevice;
	unsigned long pParam;
	unsigned short status;
	unsigned long handle;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void DevLink(struct DevLink* inst);
struct FileCopy
{	unsigned long pSrcDev;
	unsigned long pSrc;
	unsigned long pDestDev;
	unsigned long pDest;
	unsigned char option;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileCopy(struct FileCopy* inst);
struct DevUnlink
{	unsigned long handle;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void DevUnlink(struct DevUnlink* inst);
struct CfgGetIPAddr
{	unsigned long pDevice;
	unsigned long pIPAddr;
	unsigned char Len;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void CfgGetIPAddr(struct CfgGetIPAddr* inst);
struct ViCreateWebDirFile
{	plcstring FileDevUser[81];
	plcstring EthDevice[81];
	plcstring EthIP[81];
	plcstring(* visWebViewerPath)[81];
	unsigned short Status;
	signed short Step;
	struct DirInfo DirInfo_0;
	struct DirCreate DirCreate_0;
	struct FileDelete FileDelete_0;
	struct FileCreate FileCreate_0;
	struct FileWrite FileWrite_0;
	struct FileClose FileClose_0;
	struct DevLink DevLink_0;
	struct FileCopy FileCopy_0;
	struct DevUnlink DevUnlink_0;
	struct CfgGetIPAddr CfgGetIPAddr_0;
	plcstring FileDevSystem[81];
	plcstring FileDevSystemPara[81];
	plcstring DirNameWeb[81];
	plcstring FileNameVisImgHtml[81];
	plcstring FileNameEyeSystem[81];
	plcstring FileNameEyeUser[81];
	plcstring EthIpAddr[81];
	plcstring HtmlFileContent1[1501];
	plcstring HtmlFileContent2[2001];
	plcstring FileWriteData[3601];
	plcbit Enable;
	plcbit Done;
};
void ViCreateWebDirFile(struct ViCreateWebDirFile* inst);
struct ViShowImgOnVC4
{	typVisionImageConfig CFG;
	unsigned char PowerlinkNode;
	unsigned short ImgWidthInVC4_px;
	unsigned short Status;
	plcstring HTMLStreamContent[601];
	signed short Step;
	struct AsMemPartCreate AsMemPartCreate_0;
	struct AsMemPartAllocClear AsMemPartAllocClear_0;
	MemInfoVC4_Type MemInfo;
	plcstring FileNameBmp24Bit[81];
	plcstring FileNameBmp24BitDir[81];
	plcstring URI[81];
	plcstring Host[81];
	unsigned short ImgHeight_px;
	struct httpClient httpClient_0;
	unsigned short httpClientErrorStatus;
	httpRequestHeader_t RequestHeader;
	struct TON TON_Timeout;
	unsigned long FileSizeIn8Bit;
	struct FileDelete FileDelete_0;
	struct FileCreate FileCreate_0;
	struct FileWrite FileWrite_0;
	struct FileClose FileClose_0;
	Bmp8Bit_Type Bmp8Bit;
	Bmp24Bit_Type Bmp24Bit;
	signed long BytesPerLine;
	unsigned long PixelSourceAdr;
	unsigned long PixelDestAdr;
	unsigned char(* PixelSourceColor);
	struct PixelDestColor_Type(* PixelDestColor);
	signed long BytesRemaining;
	plcstring VC4HTML[4][301];
	plcstring tmpStr1[31];
	unsigned long i;
	plcbit Enable;
	plcbit RefreshImage;
	plcbit RefreshDone;
};
void ViShowImgOnVC4(struct ViShowImgOnVC4* inst);
struct DirDeleteEx
{	unsigned long pDevice;
	unsigned long pName;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void DirDeleteEx(struct DirDeleteEx* inst);
struct TcpForward
{	unsigned short ServerPort;
	plcstring ConnectToIp[25];
	unsigned short ConnectToPort;
	unsigned char TaskPriority;
	TcpForwardInfoTyp Info;
	unsigned char InternalState;
	unsigned long InternalTaskhandle;
	unsigned long InternalMem;
	plcbit Enable;
	plcbit Running;
	plcbit Error;
};
_BUR_PUBLIC void TcpForward(struct TcpForward* inst);
_BUR_LOCAL VIStep_Enum Step;
_BUR_LOCAL plcstring FileNameComplete[81];
_BUR_LOCAL_RETAIN typVisionImageConfig RemCfgImage;
_BUR_LOCAL plcstring tmpStr[81];
_BUR_LOCAL plcstring nodeSTR[81];
_BUR_LOCAL plcstring ipSTR[81];
_BUR_LOCAL struct TcpForward TcpForward_0;
_BUR_LOCAL struct DirCreate DirCreate_0;
_BUR_LOCAL struct DirDeleteEx DirDeleteEx_0;
_BUR_LOCAL struct FileDelete FileDelete_0;
_BUR_LOCAL struct CfgGetIPAddr CfgGetIPAddr_0;
_BUR_LOCAL struct ViDownloadImage ViDownloadImage_0;
_BUR_LOCAL struct ViSaveImgOnPlc ViSaveImgOnPlc_0;
_BUR_LOCAL struct ViRefreshImageList ViRefreshImageList_0;
_BUR_LOCAL struct ViDrawCrosshair ViDrawCrosshair_0;
_BUR_LOCAL struct ViCreateWebDirFile ViCreateWebDirFile_0;
_BUR_LOCAL struct ViShowImgOnVC4 ViShowImgOnVC4_0;
_BUR_LOCAL plcstring visSelectedImageOld[81];
_BUR_LOCAL plcstring visSelectedImage[81];
_BUR_LOCAL plcstring visImagePath[81];
_BUR_LOCAL plcbit visEnableButtons_1;
_BUR_LOCAL plcbit visEnableButtons_2;
_BUR_LOCAL plcstring visDownloadFileUrl[201];
_BUR_LOCAL typCrossHair visCrossHair;
_BUR_LOCAL plcstring visImageList[20][81];
_BUR_LOCAL plcstring visWebViewerPath[81];
_BUR_LOCAL plcstring visWebViewerQuery[81];
_BUR_LOCAL plcbit visVC4RefreshImg;
_BUR_LOCAL unsigned char visImageRotation;
_BUR_LOCAL plcbit visTextAlignment;
_BUR_LOCAL unsigned short ERR_NO_IMAGE;
_BUR_LOCAL unsigned short ERR_BUFF_TOO_SMALL;
_BUR_LOCAL unsigned short ERR_TIMEOUT_DOWNLOAD;
_BUR_LOCAL unsigned short ERR_MEM_DOWNLOAD;
_BUR_LOCAL unsigned short ERR_MEM_BMP_TOO_SMALL;
_BUR_LOCAL unsigned short ERR_WRONG_DATA;
_GLOBAL unsigned short MAX_NUM_RESULTS;
_GLOBAL unsigned char MAX_NUM_CODETYPES;
