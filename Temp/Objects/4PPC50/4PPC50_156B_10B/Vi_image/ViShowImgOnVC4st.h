#ifndef __AS__TYPE_
#define __AS__TYPE_
typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

#ifndef __AS__TYPE_MemInfoVC4_Type
#define __AS__TYPE_MemInfoVC4_Type
typedef struct MemInfoVC4_Type
{	unsigned long UploadBmp8Adr;
	unsigned long UploadBmp8Size;
	unsigned long Bmp24Adr;
	unsigned long Bmp24Size;
} MemInfoVC4_Type;
#endif

#ifndef __AS__TYPE_BmpFileHeader_Type
#define __AS__TYPE_BmpFileHeader_Type
typedef struct BmpFileHeader_Type
{	unsigned char Fill[2];
	unsigned char bfType[2];
	unsigned long bfSize;
	unsigned long bfReserved;
	unsigned long bfOffBits;
} BmpFileHeader_Type;
#endif

#ifndef __AS__TYPE_BmpInfoHeader_Type
#define __AS__TYPE_BmpInfoHeader_Type
typedef struct BmpInfoHeader_Type
{	unsigned long biSize;
	signed long biWidth;
	signed long biHeight;
	unsigned short biPlanes;
	unsigned short biBitCount;
	unsigned long biCompression;
	unsigned long biSizeImage;
	signed long biXPelsPerMeter;
	signed long biYPelsPerMeter;
	unsigned long biClrUSer;
	unsigned long biClrImportant;
} BmpInfoHeader_Type;
#endif

#ifndef __AS__TYPE_Bmp24Bit_Type
#define __AS__TYPE_Bmp24Bit_Type
typedef struct Bmp24Bit_Type
{	BmpFileHeader_Type FileHeader;
	BmpInfoHeader_Type InfoHeader;
} Bmp24Bit_Type;
#endif

#ifndef __AS__TYPE_BmpColorTable_Type
#define __AS__TYPE_BmpColorTable_Type
typedef struct BmpColorTable_Type
{	unsigned char Blue;
	unsigned char Green;
	unsigned char Red;
	unsigned char Fill;
} BmpColorTable_Type;
#endif

#ifndef __AS__TYPE_Bmp8Bit_Type
#define __AS__TYPE_Bmp8Bit_Type
typedef struct Bmp8Bit_Type
{	BmpFileHeader_Type FileHeader;
	BmpInfoHeader_Type InfoHeader;
	struct BmpColorTable_Type ColorTable[256];
} Bmp8Bit_Type;
#endif

#ifndef __AS__TYPE_PixelDestColor_Type
#define __AS__TYPE_PixelDestColor_Type
typedef struct PixelDestColor_Type
{	unsigned char Blue;
	unsigned char Green;
	unsigned char Red;
} PixelDestColor_Type;
#endif

#ifndef __AS__TYPE_typVisionImageConfig
#define __AS__TYPE_typVisionImageConfig
typedef struct typVisionImageConfig
{	plcstring FileDevice[81];
	plcstring DirName[81];
	plcstring PlkIPWithoutNode[81];
	plcstring EthDevice[81];
	plcstring EthIP[81];
	unsigned long ConvertCycles;
	unsigned char Format;
	unsigned char QualityJPG;
	plcbit UploadBmpJpg;
	plcbit UploadSVG;
	unsigned char MainPageQualityJPG;
	unsigned short ImageRotation_deg;
} typVisionImageConfig;
#endif

#ifndef __AS__TYPE_httpHeaderLine_t
#define __AS__TYPE_httpHeaderLine_t
typedef struct httpHeaderLine_t
{	plcstring name[51];
	plcstring value[81];
} httpHeaderLine_t;
#endif

#ifndef __AS__TYPE_httpRawHeader_t
#define __AS__TYPE_httpRawHeader_t
typedef struct httpRawHeader_t
{	unsigned long pData;
	unsigned long dataSize;
	unsigned long dataLen;
} httpRawHeader_t;
#endif

#ifndef __AS__TYPE_httpRequestHeader_t
#define __AS__TYPE_httpRequestHeader_t
typedef struct httpRequestHeader_t
{	plcstring protocol[21];
	plcstring host[281];
	plcstring contentType[101];
	unsigned long contentLength;
	plcstring connection[81];
	plcstring keepAlive[81];
	struct httpHeaderLine_t userLine[8];
	httpRawHeader_t rawHeader;
} httpRequestHeader_t;
#endif

struct AsMemPartCreate
{	unsigned long len;
	unsigned short status;
	unsigned long ident;
	plcbit enable;
};
_BUR_PUBLIC void AsMemPartCreate(struct AsMemPartCreate* inst);
struct AsMemPartAllocClear
{	unsigned long ident;
	unsigned long len;
	unsigned short status;
	unsigned long mem;
	plcbit enable;
};
_BUR_PUBLIC void AsMemPartAllocClear(struct AsMemPartAllocClear* inst);
struct httpClient
{	unsigned long option;
	unsigned long pHost;
	unsigned short hostPort;
	unsigned long method;
	unsigned long pUri;
	unsigned long pRequestHeader;
	unsigned long pRequestData;
	unsigned long requestDataLen;
	unsigned long pResponseHeader;
	unsigned long pResponseData;
	unsigned long responseDataSize;
	unsigned long pStatistics;
	unsigned long pStruct;
	unsigned short status;
	unsigned short tcpStatus;
	unsigned short httpStatus;
	unsigned long responseDataLen;
	unsigned short phase;
	unsigned long _i_state;
	unsigned short _i_result;
	unsigned long _ident;
	unsigned long _oldEnable;
	unsigned long _oldAbort;
	unsigned long _oldSend;
	unsigned long _state;
	unsigned long _internal;
	plcbit enable;
	plcbit send;
	plcbit abort;
};
_BUR_PUBLIC void httpClient(struct httpClient* inst);
struct TON
{	plctime PT;
	plctime ET;
	plctime StartTime;
	unsigned long Restart;
	plcbit IN;
	plcbit Q;
	plcbit M;
};
_BUR_PUBLIC void TON(struct TON* inst);
struct FileDelete
{	unsigned long pDevice;
	unsigned long pName;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileDelete(struct FileDelete* inst);
struct FileCreate
{	unsigned long pDevice;
	unsigned long pFile;
	unsigned short status;
	unsigned long ident;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileCreate(struct FileCreate* inst);
struct FileWrite
{	unsigned long ident;
	unsigned long offset;
	unsigned long pSrc;
	unsigned long len;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileWrite(struct FileWrite* inst);
struct FileClose
{	unsigned long ident;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileClose(struct FileClose* inst);
struct ViShowImgOnVC4
{	typVisionImageConfig CFG;
	unsigned char PowerlinkNode;
	unsigned short ImgWidthInVC4_px;
	unsigned short Status;
	plcstring HTMLStreamContent[601];
	signed short Step;
	struct AsMemPartCreate AsMemPartCreate_0;
	struct AsMemPartAllocClear AsMemPartAllocClear_0;
	MemInfoVC4_Type MemInfo;
	plcstring FileNameBmp24Bit[81];
	plcstring FileNameBmp24BitDir[81];
	plcstring URI[81];
	plcstring Host[81];
	unsigned short ImgHeight_px;
	struct httpClient httpClient_0;
	unsigned short httpClientErrorStatus;
	httpRequestHeader_t RequestHeader;
	struct TON TON_Timeout;
	unsigned long FileSizeIn8Bit;
	struct FileDelete FileDelete_0;
	struct FileCreate FileCreate_0;
	struct FileWrite FileWrite_0;
	struct FileClose FileClose_0;
	Bmp8Bit_Type Bmp8Bit;
	Bmp24Bit_Type Bmp24Bit;
	signed long BytesPerLine;
	unsigned long PixelSourceAdr;
	unsigned long PixelDestAdr;
	unsigned char(* PixelSourceColor);
	struct PixelDestColor_Type(* PixelDestColor);
	signed long BytesRemaining;
	plcstring VC4HTML[4][301];
	plcstring tmpStr1[31];
	unsigned long i;
	plcbit Enable;
	plcbit RefreshImage;
	plcbit RefreshDone;
};
void ViShowImgOnVC4(struct ViShowImgOnVC4* inst);
_BUR_PUBLIC unsigned short brsitoa(signed long value, unsigned long pString);
_BUR_PUBLIC unsigned long brsmemcpy(unsigned long pDest, unsigned long pSrc, unsigned long length);
_BUR_PUBLIC unsigned long brsstrcat(unsigned long pDest, unsigned long pSrc);
_BUR_PUBLIC unsigned long brsstrcpy(unsigned long pDest, unsigned long pSrc);
_BUR_PUBLIC signed long CheckDivDint(signed long divisor);
_BUR_PUBLIC unsigned long CheckDivUdint(unsigned long divisor);
_BUR_PUBLIC signed long CheckBounds(signed long index, signed long lower, signed long upper);
_BUR_PUBLIC unsigned long CheckReadAccess(unsigned long address);
_BUR_PUBLIC unsigned long CheckWriteAccess(unsigned long address);
_BUR_LOCAL unsigned short ERR_NO_IMAGE;
_BUR_LOCAL unsigned short ERR_MEM_BMP_TOO_SMALL;
_BUR_LOCAL unsigned short ERR_WRONG_DATA;
_GLOBAL unsigned short ERR_OK;
_GLOBAL unsigned short ERR_FUB_ENABLE_FALSE;
_GLOBAL unsigned short ERR_FUB_BUSY;
_GLOBAL unsigned short fiERR_FILE_NOT_FOUND;
_GLOBAL unsigned short httpMETHOD_GET;
_GLOBAL unsigned short httpOPTION_HTTP_11;
static void __AS__Action_ViShowImgOnVC4_CreateVC4HTML(struct ViShowImgOnVC4* inst);
