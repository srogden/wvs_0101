#ifndef __AS__TYPE_
#define __AS__TYPE_
typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

#ifndef __AS__TYPE_typVisionCommand
#define __AS__TYPE_typVisionCommand
typedef struct typVisionCommand
{	plcbit ImageTrigger;
	plcbit ImageTriggerReset;
	plcbit AutoSetupStartStop;
	plcbit VaSwitchApplication;
	plcbit SaveDiagData;
	plcbit ReadCameraInfo;
} typVisionCommand;
#endif

#ifndef __AS__TYPE_enumVisionFunction
#define __AS__TYPE_enumVisionFunction
typedef enum enumVisionFunction
{	enumFunctionNone = 0,
	enumBlob = 1,
	enumCoderReader = 2,
	enumMatch = 3,
	enumMeasurement = 4,
	enumOCR = 5,
	enumPixelCounter = 6,
} enumVisionFunction;
#endif

#ifndef __AS__TYPE_ViComponentInternalMappLinkType
#define __AS__TYPE_ViComponentInternalMappLinkType
typedef struct ViComponentInternalMappLinkType
{	unsigned long Internal[2];
} ViComponentInternalMappLinkType;
#endif

#ifndef __AS__TYPE_ViComponentType
#define __AS__TYPE_ViComponentType
typedef struct ViComponentType
{	struct ViComponentInternalCameraIfType(* CameraType);
	ViComponentInternalMappLinkType MappLinkInternal;
} ViComponentType;
#endif

#ifndef __AS__TYPE_typVisionConfig
#define __AS__TYPE_typVisionConfig
typedef struct typVisionConfig
{	enumVisionFunction VisionFunction;
	unsigned char PowerlinkNode;
	unsigned long DataStructure;
	ViComponentType ComponentLink;
	unsigned short ResolutionWidth_X;
	unsigned short ResolutionHeight_Y;
} typVisionConfig;
#endif

#ifndef __AS__TYPE_typVisionData
#define __AS__TYPE_typVisionData
typedef struct typVisionData
{	plcbit Enable;
	unsigned char MaxItemCnt;
	signed long NettimeDelay;
	unsigned short Timeout;
	unsigned char Gain;
	unsigned short Focus;
	unsigned long Exposure;
	unsigned char FlashColor;
	unsigned char FlashSegment;
	unsigned char LedColor;
	plcbit IRFilter;
	signed long OffsetROIX;
	signed long OffsetROIY;
	signed long OffsetROIRotationCenterX;
	signed long OffsetROIRotationCenterY;
	signed short OffsetROIOrientation;
	plcbit ChromaticLock;
	unsigned char Alignment;
	unsigned char AutoSetupGain;
	unsigned short AutoSetupFocus;
	unsigned long AutoSetupExposure;
	unsigned char AcquisitionAcceptedCnt;
	unsigned char AcquisitionCompletedCnt;
	unsigned char AcquisitionFailedCnt;
	unsigned char LightWarningCnt;
	unsigned short ImageProcessingError;
	plcbit ImageProcessingActive;
	signed long ImageNettime;
	unsigned short ProcessingTimeCamera;
	unsigned short ProcessingTimeFunction;
	unsigned char ResultCnt;
	plcbit CockpitIsActive;
	unsigned long Status;
	plcstring StatusText[31];
	plcbit Active;
} typVisionData;
#endif

#ifndef __AS__TYPE_typVisionFunction
#define __AS__TYPE_typVisionFunction
typedef struct typVisionFunction
{	plcstring ApplicationName[41];
	signed long Status;
} typVisionFunction;
#endif

#ifndef __AS__TYPE_typVisionDiag
#define __AS__TYPE_typVisionDiag
typedef struct typVisionDiag
{	plcstring DiagName[81];
	signed long Status;
} typVisionDiag;
#endif

#ifndef __AS__TYPE_typVisionOptics
#define __AS__TYPE_typVisionOptics
typedef struct typVisionOptics
{	plcstring OrderNr[41];
	plcbit Binning;
	signed short ResolutionX;
	signed short ResolutionY;
	unsigned char Lens;
	unsigned char Sensor;
	plcbit ValidDistance;
	double Distance_mm;
	double DistanceLens_mm;
	double HyperFocalDistance_mm;
	double PixelSize_um;
	double PixelSizeBinning_um;
	double FocalLength_mm;
	double MinObjectDist_mm;
	double MaxObjectDist_mm;
	double Aperture;
	double CircleOfConfusion_mm;
	double DepthOfFieldNearPos_mm;
	double DepthOfFieldFarPos_mm;
	double DepthOfField_mm;
	double FieldOfView_X_mm;
	double FieldOfView_Y_mm;
	double Resolution_mmPerPx;
} typVisionOptics;
#endif

#ifndef __AS__TYPE_typVisionHW
#define __AS__TYPE_typVisionHW
typedef struct typVisionHW
{	plcbit Connected;
	plcbit Ready;
	plcbit DigitalInput01;
	plcbit DigitalOutput01;
	plcbit DigitalOutputStatus01;
	plcbit DigitalOutput02;
	plcbit DigitalOutputStatus02;
	unsigned long SerialNumber;
	unsigned long ID;
	unsigned long Variant;
	unsigned long Firmware;
	plcbit Undervoltage;
	signed char Temperature;
} typVisionHW;
#endif

#ifndef __AS__TYPE_typVisionMain
#define __AS__TYPE_typVisionMain
typedef struct typVisionMain
{	typVisionCommand CMD;
	typVisionConfig CFG;
	typVisionData DAT;
	typVisionFunction FCT;
	typVisionDiag DIA;
	typVisionOptics OPT;
	typVisionHW HW;
} typVisionMain;
#endif

#ifndef __AS__TYPE_typLightCommand
#define __AS__TYPE_typLightCommand
typedef struct typLightCommand
{	plcbit FlashTrigger;
	plcbit FlashTriggerReset;
} typLightCommand;
#endif

#ifndef __AS__TYPE_enumLightType
#define __AS__TYPE_enumLightType
typedef enum enumLightType
{	enumLightNone = 0,
	enumBacklight = 1,
	enumLightbar = 2,
} enumLightType;
#endif

#ifndef __AS__TYPE_typLightConfig
#define __AS__TYPE_typLightConfig
typedef struct typLightConfig
{	enumLightType LightType;
	unsigned char PowerlinkNode;
} typLightConfig;
#endif

#ifndef __AS__TYPE_typLightData
#define __AS__TYPE_typLightData
typedef struct typLightData
{	unsigned char Enable;
	unsigned char FlashColor;
	unsigned short FlashSegment;
	unsigned long Exposure;
	unsigned short SetAngle;
	unsigned short Timeout;
	signed long NettimeDelay;
	unsigned char FlashAcceptedCnt;
	unsigned char FlashCompletedCnt;
	unsigned char FlashFailedCnt;
	unsigned long Status;
} typLightData;
#endif

#ifndef __AS__TYPE_typLightHW
#define __AS__TYPE_typLightHW
typedef struct typLightHW
{	plcbit Connected;
	plcbit Ready;
	unsigned long SerialNumber;
	unsigned long ID;
	unsigned long Variant;
	unsigned long Firmware;
	signed char TemperatureLED;
	signed char TemperatureControllerBoard;
	unsigned char WarningCnt;
} typLightHW;
#endif

#ifndef __AS__TYPE_typLightMain
#define __AS__TYPE_typLightMain
typedef struct typLightMain
{	typLightCommand CMD;
	typLightConfig CFG;
	typLightData DAT;
	typLightHW HW;
} typLightMain;
#endif

#ifndef __AS__TYPE_typVisionImageCommand
#define __AS__TYPE_typVisionImageCommand
typedef struct typVisionImageCommand
{	plcbit CreateDir;
	plcbit DeleteDir;
	plcbit DeleteImage;
	plcbit Refresh;
	plcbit DownloadImage;
	plcbit SaveImageOnPLC;
	plcbit ResetError;
	plcbit RefreshCrosshair;
} typVisionImageCommand;
#endif

#ifndef __AS__TYPE_typVisionImageConfig
#define __AS__TYPE_typVisionImageConfig
typedef struct typVisionImageConfig
{	plcstring FileDevice[81];
	plcstring DirName[81];
	plcstring PlkIPWithoutNode[81];
	plcstring EthDevice[81];
	plcstring EthIP[81];
	unsigned long ConvertCycles;
	unsigned char Format;
	unsigned char QualityJPG;
	plcbit UploadBmpJpg;
	plcbit UploadSVG;
	unsigned char MainPageQualityJPG;
	unsigned short ImageRotation_deg;
} typVisionImageConfig;
#endif

#ifndef __AS__TYPE_typVisionImageData
#define __AS__TYPE_typVisionImageData
typedef struct typVisionImageData
{	plcstring Images[20][81];
	unsigned short Status;
	plcbit VisionDisabled;
} typVisionImageData;
#endif

#ifndef __AS__TYPE_typVisionImage
#define __AS__TYPE_typVisionImage
typedef struct typVisionImage
{	typVisionImageCommand CMD;
	typVisionImageConfig CFG;
	typVisionImageData DAT;
} typVisionImage;
#endif

#ifndef __AS__TYPE_typCodeReaderMain
#define __AS__TYPE_typCodeReaderMain
typedef struct typCodeReaderMain
{	unsigned char CodeType;
	plcbit GradingEnable;
	plcbit RobustnessEnable;
	unsigned char ParameterMode;
	unsigned char ParameterOptimization;
	plcstring BarcodeText[10][101];
	unsigned char BarcodeType[10];
	signed long PositionX[10];
	signed long PositionY[10];
	signed short Orientation[10];
	signed char Grading[10];
	unsigned char EnhancedGrading[23];
} typCodeReaderMain;
#endif

#ifndef __AS__TYPE_typBlobMain
#define __AS__TYPE_typBlobMain
typedef struct typBlobMain
{	plcbit RegionalFeature;
	plcbit EnhancedBlobInformation;
	unsigned char ModelNumber[10];
	plcbit Clipped[10];
	unsigned long Area[10];
	signed long PositionX[10];
	signed long PositionY[10];
	signed short Orientation[10];
	unsigned char MeanGrayValue[10];
	unsigned long Length[10];
	unsigned long Width[10];
} typBlobMain;
#endif

#ifndef __AS__TYPE_typMatchMain
#define __AS__TYPE_typMatchMain
typedef struct typMatchMain
{	unsigned char MinScore;
	unsigned char MaxOverlap;
	unsigned char ModelNumber[10];
	unsigned char Score[10];
	signed long PositionX[10];
	signed long PositionY[10];
	signed short Orientation[10];
	unsigned char Scale[10];
	signed long RotCenterX[10];
	signed long RotCenterY[10];
} typMatchMain;
#endif

#ifndef __AS__TYPE_typOCRMain
#define __AS__TYPE_typOCRMain
typedef struct typOCRMain
{	unsigned char ParameterMode;
	plcbit GradingEnable;
	plcstring Text[10][51];
	unsigned char Grading[10];
	signed long PositionX[10];
	signed long PositionY[10];
	signed short Orientation[10];
} typOCRMain;
#endif

#ifndef __AS__TYPE_typPixelMain
#define __AS__TYPE_typPixelMain
typedef struct typPixelMain
{	plcbit EnhancedPixelCounterInformation;
	unsigned char ModelNumber[10];
	unsigned long NumPixels[10];
	unsigned char MinGray[10];
	unsigned char MaxGray[10];
	unsigned short MeanGray[10];
	unsigned short DeviationGray[10];
	unsigned char MedianGray[10];
	unsigned long ModelArea[10];
	unsigned short NumConnectedComponents[10];
	signed long PositionX[10];
	signed long PositionY[10];
} typPixelMain;
#endif

#ifndef __AS__TYPE_typMTMain
#define __AS__TYPE_typMTMain
typedef struct typMTMain
{	plcbit UseResultAsXY;
	unsigned char ParameterMode;
	signed long Result[10];
} typMTMain;
#endif

#ifndef __AS__TYPE_recVariableCam
#define __AS__TYPE_recVariableCam
typedef struct recVariableCam
{	plcstring ApplicationName[41];
	unsigned char MaxItemCnt;
	unsigned short Timeout;
	unsigned char Gain;
	unsigned short Focus;
	unsigned long Exposure;
	unsigned char FlashColor;
	unsigned char FlashSegment;
} recVariableCam;
#endif

#ifndef __AS__TYPE_recVariableLight
#define __AS__TYPE_recVariableLight
typedef struct recVariableLight
{	unsigned short Timeout;
	unsigned long Exposure;
	unsigned char FlashColor;
	unsigned short FlashSegment;
	unsigned short SetAngle;
} recVariableLight;
#endif

#ifndef __AS__TYPE_recVariable
#define __AS__TYPE_recVariable
typedef struct recVariable
{	struct recVariableCam Cam[6];
	struct recVariableLight Light[5];
} recVariable;
#endif

#ifndef __AS__TYPE_recSTATE
#define __AS__TYPE_recSTATE
typedef enum recSTATE
{	REC_WAIT = 0,
	REC_REG_NAME = 1,
	REC_REG_VAR = 2,
	REC_CREATE_DIR = 3,
	REC_READ_DIR = 4,
	REC_READ_DIR_1 = 5,
	REC_READ_DIR_2 = 6,
	REC_READ_DIR_3 = 7,
	REC_GEN_FILE_NAME = 8,
	REC_LOAD_SAVE = 9,
	REC_DELETE = 10,
	REC_RENAME = 11,
	REC_VIEW = 12,
	REC_DOWNLOAD = 13,
	REC_DOWNLOAD_1 = 14,
	REC_DOWNLOAD_2 = 15,
	REC_DOWNLOAD_3 = 16,
	REC_DOWNLOAD_4 = 17,
	REC_UPLOAD = 18,
	REC_UPLOAD_1 = 19,
	REC_UPLOAD_2 = 20,
	REC_UPLOAD_3 = 21,
	REC_UPLOAD_4 = 22,
	REC_UPLOAD_5 = 23,
	REC_UPLOAD_6 = 24,
	REC_ERROR = 25,
} recSTATE;
#endif

#ifndef __AS__TYPE_recERR
#define __AS__TYPE_recERR
typedef struct recERR
{	plcwstring Text[201];
	recSTATE State;
} recERR;
#endif

#ifndef __AS__TYPE_recCMD
#define __AS__TYPE_recCMD
typedef struct recCMD
{	plcbit Init;
	plcbit New;
	plcbit Load;
	plcbit Save;
	plcbit View;
	plcbit Rename;
	plcbit Delete;
	plcbit Download;
	plcbit Upload;
	plcbit ErrorReset;
} recCMD;
#endif

#ifndef __AS__TYPE_recPAR
#define __AS__TYPE_recPAR
typedef struct recPAR
{	plcwstring RecipeName[41];
	plcwstring RecipeNameNew[41];
	plcstring RecipeID[41];
	plcbit Initialized;
	unsigned char VisuSlotID;
	plcbit VisuEnableCommand;
} recPAR;
#endif

#ifndef __AS__TYPE_recDAT
#define __AS__TYPE_recDAT
typedef struct recDAT
{	plcwstring RecipeNames[101][41];
	plcstring RecipeIDs[101][41];
	unsigned short RecipeNum;
} recDAT;
#endif

#ifndef __AS__TYPE_recVIS
#define __AS__TYPE_recVIS
typedef struct recVIS
{	plcwstring RecipeNames[101][121];
	unsigned short RecipeNum;
	plcwstring RecipeFilter[41];
	plcwstring RecipeSelect[41];
	plcstring DownloadFileUrl[41];
	plcbit UploadOverwriteRequest;
	unsigned char UploadOverwriteResponse;
	plcbit ReloadUpload;
	unsigned char RecipeDoubleClick;
	plcstring ViewFilePath[121];
	plcbit ViewFile;
	plcbit ShowMessageBoxError;
} recVIS;
#endif

#ifndef __AS__TYPE_recMAIN
#define __AS__TYPE_recMAIN
typedef struct recMAIN
{	recCMD CMD;
	recPAR PAR;
	recDAT DAT;
	struct recVIS VIS[3];
	recERR ERR;
	plcwstring StatusText[101];
	signed long StatusNo;
} recMAIN;
#endif

#ifndef __AS__TYPE_ViComponentInternalCameraIfType
#define __AS__TYPE_ViComponentInternalCameraIfType
typedef struct ViComponentInternalCameraIfType
{	plcdword Vtable;
} ViComponentInternalCameraIfType;
#endif

_BUR_PUBLIC signed long CheckBounds(signed long index, signed long lower, signed long upper);
_BUR_LOCAL plcbit visNewRecipe;
_BUR_LOCAL plcbit visSaveRecipe;
_BUR_LOCAL plcbit cmdLoadRecipeAfterBoot;
_BUR_LOCAL signed short i;
_GLOBAL typBlobMain gBlob;
_GLOBAL typCodeReaderMain gCodeReader;
_GLOBAL typMatchMain gMatch;
_GLOBAL typMTMain gMT;
_GLOBAL typOCRMain gOCR;
_GLOBAL typPixelMain gPixel;
_GLOBAL typVisionImage gVisionImage;
_GLOBAL struct typLightMain gVisionLight[6];
_GLOBAL struct typVisionMain gVisionSensor[7];
_GLOBAL unsigned short visSelectedSensor;
_GLOBAL unsigned short visSelectedLight;
_GLOBAL_RETAIN recVariable RecipeData;
_GLOBAL unsigned short MAX_NUM_CAMS;
_GLOBAL unsigned short MAX_NUM_LIGHTS;
_GLOBAL unsigned short MAX_NUM_RESULTS;
_GLOBAL unsigned char REC_REC_NUM;
_GLOBAL unsigned char REC_NAME_LENGTH;
_GLOBAL unsigned char REC_VIS_LENGTH;
_GLOBAL unsigned char REC_MAX_CLIENTS_ID;
_GLOBAL recMAIN Recipe_01;
_GLOBAL ViComponentType gCameraBlob;
_GLOBAL ViComponentType gCameraCodeReading;
_GLOBAL ViComponentType gCameraMatch;
_GLOBAL ViComponentType gCameraMeasurement;
_GLOBAL ViComponentType gCameraOCR;
_GLOBAL ViComponentType gCameraPxl;
_LOCAL plcbit Edge0000100000;
_LOCAL plcbit Edge0000100001;
