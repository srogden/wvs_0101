#define _DEFAULT_INCLUDE
#include <bur\plctypes.h>
#include "C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_color/Mainst.h"
#line 1 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_color/Main.nodebug"
#line 2 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_color/Main.st"
void __BUR__ENTRY_INIT_FUNCT__(void){{


(gVisionSensor[CheckBounds(VI_COLOR_SENSOR,0,6)].DAT.Gain=1);
(gVisionSensor[CheckBounds(VI_COLOR_SENSOR,0,6)].DAT.Focus=14000);
(gVisionSensor[CheckBounds(VI_COLOR_SENSOR,0,6)].DAT.Exposure=150);
(gVisionSensor[CheckBounds(VI_COLOR_SENSOR,0,6)].DAT.ChromaticLock=1);
(gBlob.EnhancedBlobInformation=1);
*((char volatile*)&(visTableNo)) = *((char*)&(visTableNo));

}}
#line 12 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_color/Main.nodebug"
#line 14 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_color/Main.st"
void _CYCLIC __BUR__ENTRY_CYCLIC_FUNCT__(void){{
switch(Step){


case 0:{
(gVisionColor.DAT.Status=ERR_OK);
if((gVisionColor.CMD.Evaluate|gVisionColor.CMD.Teach)){
(idx=1);
brsmemset(((unsigned long)(&gVisionColor.DAT)),0,114);
(gVisionColor.CFG.TeachingIndex=CheckUnsignedSubrange((visSelectedProduct+1),1,1000));
(gVisionColor.DAT.Status=ERR_FUB_BUSY);
(gVisionSensor[CheckBounds(VI_COLOR_SENSOR,0,6)].DAT.Enable=1);
(Step=1);
}


}break;case 1:{

if(((((unsigned long)(unsigned short)idx==(unsigned long)(unsigned short)1))&(((unsigned long)(unsigned char)gVisionColor.CFG.FlashColor1!=(unsigned long)(unsigned char)0)))){
(gVisionSensor[CheckBounds(VI_COLOR_SENSOR,0,6)].DAT.FlashColor=gVisionColor.CFG.FlashColor1);
}else if(((((unsigned long)(unsigned short)idx==(unsigned long)(unsigned short)2))&(((unsigned long)(unsigned char)gVisionColor.CFG.FlashColor2!=(unsigned long)(unsigned char)0)))){
(gVisionSensor[CheckBounds(VI_COLOR_SENSOR,0,6)].DAT.FlashColor=gVisionColor.CFG.FlashColor2);
}else if(((((unsigned long)(unsigned short)idx==(unsigned long)(unsigned short)3))&(((unsigned long)(unsigned char)gVisionColor.CFG.FlashColor3!=(unsigned long)(unsigned char)0)))){
(gVisionSensor[CheckBounds(VI_COLOR_SENSOR,0,6)].DAT.FlashColor=gVisionColor.CFG.FlashColor3);
}else if(((((unsigned long)(unsigned short)idx==(unsigned long)(unsigned short)4))&(((unsigned long)(unsigned char)gVisionColor.CFG.FlashColor4!=(unsigned long)(unsigned char)0)))){
(gVisionSensor[CheckBounds(VI_COLOR_SENSOR,0,6)].DAT.FlashColor=gVisionColor.CFG.FlashColor4);

}else{
if(gVisionColor.CMD.Teach){
(gVisionColor.CMD.Teach=0);
(Step=0);
}else{
(Step=3);
}
return;
}

if(NETTIME_ENABLE){
(gVisionSensor[CheckBounds(VI_COLOR_SENSOR,0,6)].DAT.NettimeDelay=(NettimeCurrent_us+NETTIME_DEFAULT_DELAY));
}
(gVisionSensor[CheckBounds(VI_COLOR_SENSOR,0,6)].CMD.ImageTrigger=1);
if(gVisionColor.CMD.Teach){
(Step=2);
}else{
(Step=3);
}


}break;case 2:{

if((((unsigned long)(unsigned char)gVisionSensor[CheckBounds(VI_COLOR_SENSOR,0,6)].CMD.ImageTrigger==(unsigned long)(unsigned char)0))){
if((((unsigned long)(unsigned char)gVisionSensor[CheckBounds(VI_COLOR_SENSOR,0,6)].DAT.ResultCnt>(unsigned long)(unsigned char)0))){
if((((unsigned long)(unsigned short)idx==(unsigned long)(unsigned short)1))){
(gVisionColor.CFG.GrayValue1[CheckBounds(gVisionColor.CFG.TeachingIndex,1,8)-1]=gBlob.MeanGrayValue[CheckBounds(1,1,10)-1]);
}else if((((unsigned long)(unsigned short)idx==(unsigned long)(unsigned short)2))){
(gVisionColor.CFG.GrayValue2[CheckBounds(gVisionColor.CFG.TeachingIndex,1,8)-1]=gBlob.MeanGrayValue[CheckBounds(1,1,10)-1]);
}else if((((unsigned long)(unsigned short)idx==(unsigned long)(unsigned short)3))){
(gVisionColor.CFG.GrayValue3[CheckBounds(gVisionColor.CFG.TeachingIndex,1,8)-1]=gBlob.MeanGrayValue[CheckBounds(1,1,10)-1]);
}else if((((unsigned long)(unsigned short)idx==(unsigned long)(unsigned short)4))){
(gVisionColor.CFG.GrayValue4[CheckBounds(gVisionColor.CFG.TeachingIndex,1,8)-1]=gBlob.MeanGrayValue[CheckBounds(1,1,10)-1]);
}

(idx=(idx+1));
if((((unsigned long)(unsigned short)idx>(unsigned long)(unsigned short)4))){
(gVisionColor.CMD.Teach=0);
(Step=0);
}else{
(Step=1);
}
}else{
(gVisionColor.DAT.Status=ERR_VI_COLOR_IDENTIFY);
(Step=5);
}
}


}break;case 3:{

if((((unsigned long)(unsigned char)gVisionSensor[CheckBounds(VI_COLOR_SENSOR,0,6)].CMD.ImageTrigger==(unsigned long)(unsigned char)0))){
if((((unsigned long)(unsigned char)gVisionSensor[CheckBounds(VI_COLOR_SENSOR,0,6)].DAT.ResultCnt>(unsigned long)(unsigned char)0))){
if((((unsigned long)(unsigned short)idx==(unsigned long)(unsigned short)1))){
(gVisionColor.DAT.GrayValue1=gBlob.MeanGrayValue[CheckBounds(1,1,10)-1]);
}else if((((unsigned long)(unsigned short)idx==(unsigned long)(unsigned short)2))){
(gVisionColor.DAT.GrayValue2=gBlob.MeanGrayValue[CheckBounds(1,1,10)-1]);
}else if((((unsigned long)(unsigned short)idx==(unsigned long)(unsigned short)3))){
(gVisionColor.DAT.GrayValue3=gBlob.MeanGrayValue[CheckBounds(1,1,10)-1]);
}else if((((unsigned long)(unsigned short)idx==(unsigned long)(unsigned short)4))){
(gVisionColor.DAT.GrayValue4=gBlob.MeanGrayValue[CheckBounds(1,1,10)-1]);
}

(idx=(idx+1));
if((((unsigned long)(unsigned short)idx>(unsigned long)(unsigned short)4))){
(gVisionColor.DAT.LowError=65535);
(gVisionColor.DAT.LowIndex=65535);
(gVisionColor.DAT.LowDistance=65535);
(Step=4);
}else{
(Step=1);
}
}else{
(gVisionColor.DAT.Status=ERR_VI_COLOR_IDENTIFY);
(Step=5);
}
}


}break;case 4:{
for((idx=1);idx<=MAX_NUM_PRODUCTS;idx+=1){

if(((((unsigned long)(unsigned short)gVisionColor.CFG.GrayValue1[CheckBounds(idx,1,8)-1]==(unsigned long)(unsigned short)0))&(((unsigned long)(unsigned short)gVisionColor.CFG.GrayValue2[CheckBounds(idx,1,8)-1]==(unsigned long)(unsigned short)0))&(((unsigned long)(unsigned short)gVisionColor.CFG.GrayValue3[CheckBounds(idx,1,8)-1]==(unsigned long)(unsigned short)0))&(((unsigned long)(unsigned short)gVisionColor.CFG.GrayValue4[CheckBounds(idx,1,8)-1]==(unsigned long)(unsigned short)0)))){
goto imp1_endfor14_0;
}

for((idy=1);idy<=4;idy+=1){
if(((((unsigned long)(unsigned short)idy==(unsigned long)(unsigned short)1))&(((unsigned long)(unsigned char)gVisionColor.CFG.FlashColor1!=(unsigned long)(unsigned char)0)))){
(gVisionColor.DAT.TotalError[CheckBounds(idx,1,8)-1]=(gVisionColor.DAT.TotalError[CheckBounds(idx,1,8)-1]+(((signed short)gVisionColor.DAT.GrayValue1-gVisionColor.CFG.GrayValue1[CheckBounds(idx,1,8)-1])<0?-(((signed short)gVisionColor.DAT.GrayValue1-gVisionColor.CFG.GrayValue1[CheckBounds(idx,1,8)-1])):((signed short)gVisionColor.DAT.GrayValue1-gVisionColor.CFG.GrayValue1[CheckBounds(idx,1,8)-1]))));
}else if(((((unsigned long)(unsigned short)idy==(unsigned long)(unsigned short)2))&(((unsigned long)(unsigned char)gVisionColor.CFG.FlashColor2!=(unsigned long)(unsigned char)0)))){
(gVisionColor.DAT.TotalError[CheckBounds(idx,1,8)-1]=(gVisionColor.DAT.TotalError[CheckBounds(idx,1,8)-1]+(((signed short)gVisionColor.DAT.GrayValue2-gVisionColor.CFG.GrayValue2[CheckBounds(idx,1,8)-1])<0?-(((signed short)gVisionColor.DAT.GrayValue2-gVisionColor.CFG.GrayValue2[CheckBounds(idx,1,8)-1])):((signed short)gVisionColor.DAT.GrayValue2-gVisionColor.CFG.GrayValue2[CheckBounds(idx,1,8)-1]))));
}else if(((((unsigned long)(unsigned short)idy==(unsigned long)(unsigned short)3))&(((unsigned long)(unsigned char)gVisionColor.CFG.FlashColor3!=(unsigned long)(unsigned char)0)))){
(gVisionColor.DAT.TotalError[CheckBounds(idx,1,8)-1]=(gVisionColor.DAT.TotalError[CheckBounds(idx,1,8)-1]+(((signed short)gVisionColor.DAT.GrayValue3-gVisionColor.CFG.GrayValue3[CheckBounds(idx,1,8)-1])<0?-(((signed short)gVisionColor.DAT.GrayValue3-gVisionColor.CFG.GrayValue3[CheckBounds(idx,1,8)-1])):((signed short)gVisionColor.DAT.GrayValue3-gVisionColor.CFG.GrayValue3[CheckBounds(idx,1,8)-1]))));
}else if(((((unsigned long)(unsigned short)idy==(unsigned long)(unsigned short)4))&(((unsigned long)(unsigned char)gVisionColor.CFG.FlashColor4!=(unsigned long)(unsigned char)0)))){
(gVisionColor.DAT.TotalError[CheckBounds(idx,1,8)-1]=(gVisionColor.DAT.TotalError[CheckBounds(idx,1,8)-1]+(((signed short)gVisionColor.DAT.GrayValue4-gVisionColor.CFG.GrayValue4[CheckBounds(idx,1,8)-1])<0?-(((signed short)gVisionColor.DAT.GrayValue4-gVisionColor.CFG.GrayValue4[CheckBounds(idx,1,8)-1])):((signed short)gVisionColor.DAT.GrayValue4-gVisionColor.CFG.GrayValue4[CheckBounds(idx,1,8)-1]))));
}
}imp1_endfor16_0:;

if((((unsigned long)(unsigned short)gVisionColor.DAT.TotalError[CheckBounds(idx,1,8)-1]<(unsigned long)(unsigned short)gVisionColor.DAT.LowError))){
(gVisionColor.DAT.LowError=gVisionColor.DAT.TotalError[CheckBounds(idx,1,8)-1]);
(gVisionColor.DAT.LowIndex=idx);
{int zzIndex; plcstring* zzLValue=(plcstring*)gVisionColor.DAT.LowName; plcstring* zzRValue=(plcstring*)gVisionColor.CFG.ProductName[idx-1]; for(zzIndex=0; zzIndex<80l && zzRValue[zzIndex]!=0; zzIndex++) zzLValue[zzIndex] = zzRValue[zzIndex]; zzLValue[zzIndex] = 0;};
}
}imp1_endfor14_0:;

if((((unsigned long)(unsigned short)gVisionColor.DAT.LowIndex!=(unsigned long)(unsigned short)65535))){
for((idx=1);idx<=MAX_NUM_PRODUCTS;idx+=1){

if(((((unsigned long)(unsigned short)gVisionColor.CFG.GrayValue1[CheckBounds(idx,1,8)-1]==(unsigned long)(unsigned short)0))&(((unsigned long)(unsigned short)gVisionColor.CFG.GrayValue2[CheckBounds(idx,1,8)-1]==(unsigned long)(unsigned short)0))&(((unsigned long)(unsigned short)gVisionColor.CFG.GrayValue3[CheckBounds(idx,1,8)-1]==(unsigned long)(unsigned short)0))&(((unsigned long)(unsigned short)gVisionColor.CFG.GrayValue4[CheckBounds(idx,1,8)-1]==(unsigned long)(unsigned short)0)))){
goto imp1_endfor20_0;
}

if(((((unsigned long)(unsigned short)gVisionColor.DAT.LowIndex!=(unsigned long)(unsigned short)idx))&(((unsigned long)(unsigned short)(((signed short)gVisionColor.DAT.TotalError[CheckBounds(gVisionColor.DAT.LowIndex,1,8)-1]<0?-((signed short)gVisionColor.DAT.TotalError[CheckBounds(gVisionColor.DAT.LowIndex,1,8)-1]):(signed short)gVisionColor.DAT.TotalError[CheckBounds(gVisionColor.DAT.LowIndex,1,8)-1])-gVisionColor.DAT.TotalError[CheckBounds(idx,1,8)-1])<(unsigned long)(unsigned short)gVisionColor.DAT.LowDistance)))){
(gVisionColor.DAT.LowDistance=(((signed short)gVisionColor.DAT.TotalError[CheckBounds(gVisionColor.DAT.LowIndex,1,8)-1]-gVisionColor.DAT.TotalError[CheckBounds(idx,1,8)-1])<0?-(((signed short)gVisionColor.DAT.TotalError[CheckBounds(gVisionColor.DAT.LowIndex,1,8)-1]-gVisionColor.DAT.TotalError[CheckBounds(idx,1,8)-1])):((signed short)gVisionColor.DAT.TotalError[CheckBounds(gVisionColor.DAT.LowIndex,1,8)-1]-gVisionColor.DAT.TotalError[CheckBounds(idx,1,8)-1])));
}
}imp1_endfor20_0:;
(visSelectedProduct=(gVisionColor.DAT.LowIndex-1));
}

if((((unsigned long)(unsigned short)gVisionColor.DAT.LowError>(unsigned long)(unsigned short)gVisionColor.CFG.MaxError))){
(gVisionColor.DAT.Status=ERR_VI_COLOR_MAX_ERROR);
(Step=5);

}else if((((unsigned long)(unsigned short)gVisionColor.DAT.LowDistance<(unsigned long)(unsigned short)gVisionColor.CFG.MinDifference))){
(gVisionColor.DAT.Status=ERR_VI_COLOR_MIN_DISTANCE);
(Step=5);

}else{
(gVisionColor.CMD.Evaluate=0);
(Step=0);
}


}break;case 5:{
if(gVisionColor.CMD.ResetError){
(gVisionColor.CMD.ResetError=0);
(gVisionColor.DAT.Status=0);
(Step=0);
}else{
brsmemset(((unsigned long)(&gVisionColor.CMD)),0,3);
}
}break;}
}imp1_case0_5:imp1_endcase0_0:;}
#line 183 "C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_color/Main.nodebug"

void __AS__ImplInitMain_st(void){__BUR__ENTRY_INIT_FUNCT__();}

__asm__(".section \".plc\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/VisionTypes.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/mappRecipe/RecTypes.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBaseCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAxCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxisCfg.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxis.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxisError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipe.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipeAlarm.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipeError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpComError.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/TcpForward/Types.typ\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/IecCheck/IecCheck.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpAxis/MpAxis.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpRecipe/MpRecipe.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/TcpForward/TcpForward.fun\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Global.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/VisionVariables.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/mappRecipe/RecVariables.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/McElements.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/MpComponents.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Temp/Includes/AS_TempDecl/4PPC50/GlobalComponents/MpVisionComponents.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/operator/operator.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/runtime/runtime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/astime/astime.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIecCon/AsIecCon.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIO/AsIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsEPL/AsEPL.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsZip/AsZip.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/FileIO/FileIO.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/powerlnk/powerlnk.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrStr/AsBrStr.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/standard/standard.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/sys_lib/sys_lib.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ArEventLog/ArEventLog.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsTCP/AsTCP.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsHttp/AsHttp.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsMem/AsMem.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsARCfg/AsARCfg.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsBrWStr/AsBrWStr.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsIODiag/AsIODiag.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/AsETH/AsETH.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McBase/McBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAcpAx/McAcpAx.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/McAxis/McAxis.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpBase/MpBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/MpCom/MpCom.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/ViBase/ViBase.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Libraries/CoTrace/CoTrace.var\\\" scope \\\"global\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/Vi_color/Types.typ\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"Logical/Vision/Vi_color/Variables.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"iecfile \\\"C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_color/Main.st.var\\\" scope \\\"local\\\"\\n\"");
__asm__(".ascii \"plcreplace \\\"C:/Projects/BR_MappViewVisionDemo/Temp/Objects/4PPC50/4PPC50_156B_10B/Vi_color/Main.st.c\\\" \\\"C:/Projects/BR_MappViewVisionDemo/Logical/Vision/Vi_color/Main.st\\\"\\n\"");
__asm__(".previous");

__asm__(".section \".plciec\"");
__asm__(".ascii \"plcdata_const 'visTableNo'\\n\"");
__asm__(".ascii \"plcdata_const 'VI_COLOR_SENSOR'\\n\"");
__asm__(".ascii \"plcdata_const 'ERR_VI_COLOR_IDENTIFY'\\n\"");
__asm__(".ascii \"plcdata_const 'ERR_VI_COLOR_MAX_ERROR'\\n\"");
__asm__(".ascii \"plcdata_const 'ERR_VI_COLOR_MIN_DISTANCE'\\n\"");
__asm__(".ascii \"plcdata_const 'gVisionColor'\\n\"");
__asm__(".ascii \"plcdata_const 'MAX_NUM_CAMS'\\n\"");
__asm__(".ascii \"plcdata_const 'MAX_NUM_RESULTS'\\n\"");
__asm__(".ascii \"plcdata_const 'MAX_NUM_PRODUCTS'\\n\"");
__asm__(".ascii \"plcdata_const 'NETTIME_DEFAULT_DELAY'\\n\"");
__asm__(".ascii \"plcdata_const 'NETTIME_ENABLE'\\n\"");
__asm__(".ascii \"plcdata_const 'ERR_OK'\\n\"");
__asm__(".ascii \"plcdata_const 'ERR_FUB_BUSY'\\n\"");
__asm__(".previous");
