#ifndef __AS__TYPE_VIStep_Enum
#define __AS__TYPE_VIStep_Enum
typedef enum VIStep_Enum
{	VISTEP_WAIT = 0,
	VISTEP_FLASH = 1,
	VISTEP_TEACH = 2,
	VISTEP_CHECK = 3,
	VISTEP_EVAL = 4,
	VISTEP_ERR = 5,
} VIStep_Enum;
#endif

_BUR_LOCAL VIStep_Enum Step;
_BUR_LOCAL unsigned short idy;
_BUR_LOCAL unsigned short idx;
_BUR_LOCAL unsigned short visSelectedProduct;
_BUR_LOCAL unsigned char visTableNo[8];
_BUR_LOCAL unsigned char VI_COLOR_SENSOR;
_BUR_LOCAL unsigned short ERR_VI_COLOR_IDENTIFY;
_BUR_LOCAL unsigned short ERR_VI_COLOR_MAX_ERROR;
_BUR_LOCAL unsigned short ERR_VI_COLOR_MIN_DISTANCE;
_GLOBAL unsigned short MAX_NUM_PRODUCTS;
