#ifndef __AS__TYPE_
#define __AS__TYPE_
typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

#ifndef __AS__TYPE_VIStep_Enum
#define __AS__TYPE_VIStep_Enum
typedef enum VIStep_Enum
{	VISTEP_WAIT = 0,
	VISTEP_FLASH = 1,
	VISTEP_TEACH = 2,
	VISTEP_CHECK = 3,
	VISTEP_EVAL = 4,
	VISTEP_ERR = 5,
} VIStep_Enum;
#endif

#ifndef __AS__TYPE_typVisionCommand
#define __AS__TYPE_typVisionCommand
typedef struct typVisionCommand
{	plcbit ImageTrigger;
	plcbit ImageTriggerReset;
	plcbit AutoSetupStartStop;
	plcbit VaSwitchApplication;
	plcbit SaveDiagData;
	plcbit ReadCameraInfo;
} typVisionCommand;
#endif

#ifndef __AS__TYPE_enumVisionFunction
#define __AS__TYPE_enumVisionFunction
typedef enum enumVisionFunction
{	enumFunctionNone = 0,
	enumBlob = 1,
	enumCoderReader = 2,
	enumMatch = 3,
	enumMeasurement = 4,
	enumOCR = 5,
	enumPixelCounter = 6,
} enumVisionFunction;
#endif

#ifndef __AS__TYPE_ViComponentInternalMappLinkType
#define __AS__TYPE_ViComponentInternalMappLinkType
typedef struct ViComponentInternalMappLinkType
{	unsigned long Internal[2];
} ViComponentInternalMappLinkType;
#endif

#ifndef __AS__TYPE_ViComponentType
#define __AS__TYPE_ViComponentType
typedef struct ViComponentType
{	struct ViComponentInternalCameraIfType(* CameraType);
	ViComponentInternalMappLinkType MappLinkInternal;
} ViComponentType;
#endif

#ifndef __AS__TYPE_typVisionConfig
#define __AS__TYPE_typVisionConfig
typedef struct typVisionConfig
{	enumVisionFunction VisionFunction;
	unsigned char PowerlinkNode;
	unsigned long DataStructure;
	ViComponentType ComponentLink;
	unsigned short ResolutionWidth_X;
	unsigned short ResolutionHeight_Y;
} typVisionConfig;
#endif

#ifndef __AS__TYPE_typVisionData
#define __AS__TYPE_typVisionData
typedef struct typVisionData
{	plcbit Enable;
	unsigned char MaxItemCnt;
	signed long NettimeDelay;
	unsigned short Timeout;
	unsigned char Gain;
	unsigned short Focus;
	unsigned long Exposure;
	unsigned char FlashColor;
	unsigned char FlashSegment;
	unsigned char LedColor;
	plcbit IRFilter;
	signed long OffsetROIX;
	signed long OffsetROIY;
	signed long OffsetROIRotationCenterX;
	signed long OffsetROIRotationCenterY;
	signed short OffsetROIOrientation;
	plcbit ChromaticLock;
	unsigned char Alignment;
	unsigned char AutoSetupGain;
	unsigned short AutoSetupFocus;
	unsigned long AutoSetupExposure;
	unsigned char AcquisitionAcceptedCnt;
	unsigned char AcquisitionCompletedCnt;
	unsigned char AcquisitionFailedCnt;
	unsigned char LightWarningCnt;
	unsigned short ImageProcessingError;
	plcbit ImageProcessingActive;
	signed long ImageNettime;
	unsigned short ProcessingTimeCamera;
	unsigned short ProcessingTimeFunction;
	unsigned char ResultCnt;
	plcbit CockpitIsActive;
	unsigned long Status;
	plcstring StatusText[31];
	plcbit Active;
} typVisionData;
#endif

#ifndef __AS__TYPE_typVisionFunction
#define __AS__TYPE_typVisionFunction
typedef struct typVisionFunction
{	plcstring ApplicationName[41];
	signed long Status;
} typVisionFunction;
#endif

#ifndef __AS__TYPE_typVisionDiag
#define __AS__TYPE_typVisionDiag
typedef struct typVisionDiag
{	plcstring DiagName[81];
	signed long Status;
} typVisionDiag;
#endif

#ifndef __AS__TYPE_typVisionOptics
#define __AS__TYPE_typVisionOptics
typedef struct typVisionOptics
{	plcstring OrderNr[41];
	plcbit Binning;
	signed short ResolutionX;
	signed short ResolutionY;
	unsigned char Lens;
	unsigned char Sensor;
	plcbit ValidDistance;
	double Distance_mm;
	double DistanceLens_mm;
	double HyperFocalDistance_mm;
	double PixelSize_um;
	double PixelSizeBinning_um;
	double FocalLength_mm;
	double MinObjectDist_mm;
	double MaxObjectDist_mm;
	double Aperture;
	double CircleOfConfusion_mm;
	double DepthOfFieldNearPos_mm;
	double DepthOfFieldFarPos_mm;
	double DepthOfField_mm;
	double FieldOfView_X_mm;
	double FieldOfView_Y_mm;
	double Resolution_mmPerPx;
} typVisionOptics;
#endif

#ifndef __AS__TYPE_typVisionHW
#define __AS__TYPE_typVisionHW
typedef struct typVisionHW
{	plcbit Connected;
	plcbit Ready;
	plcbit DigitalInput01;
	plcbit DigitalOutput01;
	plcbit DigitalOutputStatus01;
	plcbit DigitalOutput02;
	plcbit DigitalOutputStatus02;
	unsigned long SerialNumber;
	unsigned long ID;
	unsigned long Variant;
	unsigned long Firmware;
	plcbit Undervoltage;
	signed char Temperature;
} typVisionHW;
#endif

#ifndef __AS__TYPE_typVisionMain
#define __AS__TYPE_typVisionMain
typedef struct typVisionMain
{	typVisionCommand CMD;
	typVisionConfig CFG;
	typVisionData DAT;
	typVisionFunction FCT;
	typVisionDiag DIA;
	typVisionOptics OPT;
	typVisionHW HW;
} typVisionMain;
#endif

#ifndef __AS__TYPE_typVisionColorCommand
#define __AS__TYPE_typVisionColorCommand
typedef struct typVisionColorCommand
{	plcbit Evaluate;
	plcbit Teach;
	plcbit ResetError;
} typVisionColorCommand;
#endif

#ifndef __AS__TYPE_typVisionColorConfig
#define __AS__TYPE_typVisionColorConfig
typedef struct typVisionColorConfig
{	unsigned char FlashColor1;
	unsigned char FlashColor2;
	unsigned char FlashColor3;
	unsigned char FlashColor4;
	plcstring ProductName[8][81];
	unsigned short GrayValue1[8];
	unsigned short GrayValue2[8];
	unsigned short GrayValue3[8];
	unsigned short GrayValue4[8];
	unsigned short TeachingIndex;
	unsigned short MaxError;
	unsigned short MinDifference;
} typVisionColorConfig;
#endif

#ifndef __AS__TYPE_typVisionColorData
#define __AS__TYPE_typVisionColorData
typedef struct typVisionColorData
{	unsigned short GrayValue1;
	unsigned short GrayValue2;
	unsigned short GrayValue3;
	unsigned short GrayValue4;
	unsigned short TotalError[8];
	unsigned short LowError;
	unsigned short LowDistance;
	unsigned short LowIndex;
	plcstring LowName[81];
	unsigned short Status;
} typVisionColorData;
#endif

#ifndef __AS__TYPE_typVisionColor
#define __AS__TYPE_typVisionColor
typedef struct typVisionColor
{	typVisionColorCommand CMD;
	typVisionColorConfig CFG;
	typVisionColorData DAT;
} typVisionColor;
#endif

#ifndef __AS__TYPE_typBlobMain
#define __AS__TYPE_typBlobMain
typedef struct typBlobMain
{	plcbit RegionalFeature;
	plcbit EnhancedBlobInformation;
	unsigned char ModelNumber[10];
	plcbit Clipped[10];
	unsigned long Area[10];
	signed long PositionX[10];
	signed long PositionY[10];
	signed short Orientation[10];
	unsigned char MeanGrayValue[10];
	unsigned long Length[10];
	unsigned long Width[10];
} typBlobMain;
#endif

#ifndef __AS__TYPE_ViComponentInternalCameraIfType
#define __AS__TYPE_ViComponentInternalCameraIfType
typedef struct ViComponentInternalCameraIfType
{	plcdword Vtable;
} ViComponentInternalCameraIfType;
#endif

_BUR_PUBLIC unsigned long brsmemset(unsigned long pDest, unsigned char value, unsigned long length);
_BUR_PUBLIC signed long CheckBounds(signed long index, signed long lower, signed long upper);
_BUR_PUBLIC unsigned long CheckUnsignedSubrange(unsigned long value, unsigned long lower, unsigned long upper);
_BUR_LOCAL VIStep_Enum Step;
_BUR_LOCAL unsigned short idy;
_BUR_LOCAL unsigned short idx;
_BUR_LOCAL unsigned short visSelectedProduct;
_BUR_LOCAL unsigned char visTableNo[8];
_BUR_LOCAL unsigned char VI_COLOR_SENSOR;
_BUR_LOCAL unsigned short ERR_VI_COLOR_IDENTIFY;
_BUR_LOCAL unsigned short ERR_VI_COLOR_MAX_ERROR;
_BUR_LOCAL unsigned short ERR_VI_COLOR_MIN_DISTANCE;
_GLOBAL typBlobMain gBlob;
_GLOBAL typVisionColor gVisionColor;
_GLOBAL struct typVisionMain gVisionSensor[7];
_GLOBAL signed long NettimeCurrent_us;
_GLOBAL unsigned short MAX_NUM_CAMS;
_GLOBAL unsigned short MAX_NUM_RESULTS;
_GLOBAL unsigned short MAX_NUM_PRODUCTS;
_GLOBAL unsigned long NETTIME_DEFAULT_DELAY;
_GLOBAL plcbit NETTIME_ENABLE;
_GLOBAL unsigned short ERR_OK;
_GLOBAL unsigned short ERR_FUB_BUSY;
