#ifndef __AS__TYPE_
#define __AS__TYPE_
typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

#ifndef __AS__TYPE_typVisionCommand
#define __AS__TYPE_typVisionCommand
typedef struct typVisionCommand
{	plcbit ImageTrigger;
	plcbit ImageTriggerReset;
	plcbit AutoSetupStartStop;
	plcbit VaSwitchApplication;
	plcbit SaveDiagData;
	plcbit ReadCameraInfo;
} typVisionCommand;
#endif

#ifndef __AS__TYPE_enumVisionFunction
#define __AS__TYPE_enumVisionFunction
typedef enum enumVisionFunction
{	enumFunctionNone = 0,
	enumBlob = 1,
	enumCoderReader = 2,
	enumMatch = 3,
	enumMeasurement = 4,
	enumOCR = 5,
	enumPixelCounter = 6,
} enumVisionFunction;
#endif

#ifndef __AS__TYPE_ViComponentInternalMappLinkType
#define __AS__TYPE_ViComponentInternalMappLinkType
typedef struct ViComponentInternalMappLinkType
{	unsigned long Internal[2];
} ViComponentInternalMappLinkType;
#endif

#ifndef __AS__TYPE_ViComponentType
#define __AS__TYPE_ViComponentType
typedef struct ViComponentType
{	struct ViComponentInternalCameraIfType(* CameraType);
	ViComponentInternalMappLinkType MappLinkInternal;
} ViComponentType;
#endif

#ifndef __AS__TYPE_typVisionConfig
#define __AS__TYPE_typVisionConfig
typedef struct typVisionConfig
{	enumVisionFunction VisionFunction;
	unsigned char PowerlinkNode;
	unsigned long DataStructure;
	ViComponentType ComponentLink;
	unsigned short ResolutionWidth_X;
	unsigned short ResolutionHeight_Y;
} typVisionConfig;
#endif

#ifndef __AS__TYPE_typVisionData
#define __AS__TYPE_typVisionData
typedef struct typVisionData
{	plcbit Enable;
	unsigned char MaxItemCnt;
	signed long NettimeDelay;
	unsigned short Timeout;
	unsigned char Gain;
	unsigned short Focus;
	unsigned long Exposure;
	unsigned char FlashColor;
	unsigned char FlashSegment;
	unsigned char LedColor;
	plcbit IRFilter;
	signed long OffsetROIX;
	signed long OffsetROIY;
	signed long OffsetROIRotationCenterX;
	signed long OffsetROIRotationCenterY;
	signed short OffsetROIOrientation;
	plcbit ChromaticLock;
	unsigned char Alignment;
	unsigned char AutoSetupGain;
	unsigned short AutoSetupFocus;
	unsigned long AutoSetupExposure;
	unsigned char AcquisitionAcceptedCnt;
	unsigned char AcquisitionCompletedCnt;
	unsigned char AcquisitionFailedCnt;
	unsigned char LightWarningCnt;
	unsigned short ImageProcessingError;
	plcbit ImageProcessingActive;
	signed long ImageNettime;
	unsigned short ProcessingTimeCamera;
	unsigned short ProcessingTimeFunction;
	unsigned char ResultCnt;
	plcbit CockpitIsActive;
	unsigned long Status;
	plcstring StatusText[31];
	plcbit Active;
} typVisionData;
#endif

#ifndef __AS__TYPE_typVisionFunction
#define __AS__TYPE_typVisionFunction
typedef struct typVisionFunction
{	plcstring ApplicationName[41];
	signed long Status;
} typVisionFunction;
#endif

#ifndef __AS__TYPE_typVisionDiag
#define __AS__TYPE_typVisionDiag
typedef struct typVisionDiag
{	plcstring DiagName[81];
	signed long Status;
} typVisionDiag;
#endif

#ifndef __AS__TYPE_typVisionOptics
#define __AS__TYPE_typVisionOptics
typedef struct typVisionOptics
{	plcstring OrderNr[41];
	plcbit Binning;
	signed short ResolutionX;
	signed short ResolutionY;
	unsigned char Lens;
	unsigned char Sensor;
	plcbit ValidDistance;
	double Distance_mm;
	double DistanceLens_mm;
	double HyperFocalDistance_mm;
	double PixelSize_um;
	double PixelSizeBinning_um;
	double FocalLength_mm;
	double MinObjectDist_mm;
	double MaxObjectDist_mm;
	double Aperture;
	double CircleOfConfusion_mm;
	double DepthOfFieldNearPos_mm;
	double DepthOfFieldFarPos_mm;
	double DepthOfField_mm;
	double FieldOfView_X_mm;
	double FieldOfView_Y_mm;
	double Resolution_mmPerPx;
} typVisionOptics;
#endif

#ifndef __AS__TYPE_typVisionHW
#define __AS__TYPE_typVisionHW
typedef struct typVisionHW
{	plcbit Connected;
	plcbit Ready;
	plcbit DigitalInput01;
	plcbit DigitalOutput01;
	plcbit DigitalOutputStatus01;
	plcbit DigitalOutput02;
	plcbit DigitalOutputStatus02;
	unsigned long SerialNumber;
	unsigned long ID;
	unsigned long Variant;
	unsigned long Firmware;
	plcbit Undervoltage;
	signed char Temperature;
} typVisionHW;
#endif

#ifndef __AS__TYPE_typVisionMain
#define __AS__TYPE_typVisionMain
typedef struct typVisionMain
{	typVisionCommand CMD;
	typVisionConfig CFG;
	typVisionData DAT;
	typVisionFunction FCT;
	typVisionDiag DIA;
	typVisionOptics OPT;
	typVisionHW HW;
} typVisionMain;
#endif

#ifndef __AS__TYPE_typLightCommand
#define __AS__TYPE_typLightCommand
typedef struct typLightCommand
{	plcbit FlashTrigger;
	plcbit FlashTriggerReset;
} typLightCommand;
#endif

#ifndef __AS__TYPE_enumLightType
#define __AS__TYPE_enumLightType
typedef enum enumLightType
{	enumLightNone = 0,
	enumBacklight = 1,
	enumLightbar = 2,
} enumLightType;
#endif

#ifndef __AS__TYPE_typLightConfig
#define __AS__TYPE_typLightConfig
typedef struct typLightConfig
{	enumLightType LightType;
	unsigned char PowerlinkNode;
} typLightConfig;
#endif

#ifndef __AS__TYPE_typLightData
#define __AS__TYPE_typLightData
typedef struct typLightData
{	unsigned char Enable;
	unsigned char FlashColor;
	unsigned short FlashSegment;
	unsigned long Exposure;
	unsigned short SetAngle;
	unsigned short Timeout;
	signed long NettimeDelay;
	unsigned char FlashAcceptedCnt;
	unsigned char FlashCompletedCnt;
	unsigned char FlashFailedCnt;
	unsigned long Status;
} typLightData;
#endif

#ifndef __AS__TYPE_typLightHW
#define __AS__TYPE_typLightHW
typedef struct typLightHW
{	plcbit Connected;
	plcbit Ready;
	unsigned long SerialNumber;
	unsigned long ID;
	unsigned long Variant;
	unsigned long Firmware;
	signed char TemperatureLED;
	signed char TemperatureControllerBoard;
	unsigned char WarningCnt;
} typLightHW;
#endif

#ifndef __AS__TYPE_typLightMain
#define __AS__TYPE_typLightMain
typedef struct typLightMain
{	typLightCommand CMD;
	typLightConfig CFG;
	typLightData DAT;
	typLightHW HW;
} typLightMain;
#endif

#ifndef __AS__TYPE_ViBaseFubProcessingType
#define __AS__TYPE_ViBaseFubProcessingType
typedef struct ViBaseFubProcessingType
{	signed long Mediator[2];
} ViBaseFubProcessingType;
#endif

#ifndef __AS__TYPE_ViBaseControlIfType
#define __AS__TYPE_ViBaseControlIfType
typedef struct ViBaseControlIfType
{	plcdword VTable;
} ViBaseControlIfType;
#endif

#ifndef __AS__TYPE_ViBaseInternalType
#define __AS__TYPE_ViBaseInternalType
typedef struct ViBaseInternalType
{	unsigned long ID;
	unsigned long Check;
	unsigned long ParamHash;
	plcword State;
	unsigned short Error;
	struct ViBaseFubProcessingType(* Treating);
	unsigned long Reserve[2];
	unsigned char Flags;
	struct ViBaseControlIfType(* ControlIf);
	signed long SeqNo;
} ViBaseInternalType;
#endif

#ifndef __AS__TYPE_ViComponentInternalCameraIfType
#define __AS__TYPE_ViComponentInternalCameraIfType
typedef struct ViComponentInternalCameraIfType
{	plcdword Vtable;
} ViComponentInternalCameraIfType;
#endif

_BUR_PUBLIC double RealSqrt(double x);
struct FileDelete
{	unsigned long pDevice;
	unsigned long pName;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileDelete(struct FileDelete* inst);
_BUR_PUBLIC unsigned long brsstrlen(unsigned long pString);
struct TON
{	plctime PT;
	plctime ET;
	plctime StartTime;
	unsigned long Restart;
	plcbit IN;
	plcbit Q;
	plcbit M;
};
_BUR_PUBLIC void TON(struct TON* inst);
_BUR_PUBLIC plcstring* MID(plcstring IN[32768], signed short L, signed short P);
_BUR_PUBLIC plcstring* CONCAT(plcstring IN1[32768], plcstring IN2[32768]);
_BUR_PUBLIC signed short CheckDivInt(signed short divisor);
_BUR_PUBLIC unsigned short CheckDivUint(unsigned short divisor);
_BUR_PUBLIC float CheckDivReal(float divisor);
_BUR_PUBLIC double CheckDivLReal(double divisor);
_BUR_PUBLIC signed long CheckBounds(signed long index, signed long lower, signed long upper);
_BUR_PUBLIC unsigned long CheckReadAccess(unsigned long address);
struct DiagCreateInfo
{	unsigned long infoKind;
	unsigned short status;
	unsigned long ident;
	unsigned long nrEntries;
	unsigned short StateMan;
	unsigned short ErrMan;
	unsigned long Init;
	plcbit enable;
};
_BUR_PUBLIC void DiagCreateInfo(struct DiagCreateInfo* inst);
struct DiagDisposeInfo
{	unsigned long ident;
	unsigned short status;
	unsigned short StateMan;
	unsigned short ErrMan;
	unsigned long Init;
	plcbit enable;
};
_BUR_PUBLIC void DiagDisposeInfo(struct DiagDisposeInfo* inst);
struct DiagGetNumInfo
{	unsigned long ident;
	unsigned long index;
	unsigned long infoCode;
	unsigned short status;
	unsigned long value;
	plcbit enable;
};
_BUR_PUBLIC void DiagGetNumInfo(struct DiagGetNumInfo* inst);
struct DiagGetStrInfo
{	unsigned long ident;
	unsigned long index;
	unsigned long infoCode;
	unsigned long pBuffer;
	unsigned long bufferLen;
	unsigned short status;
	unsigned long stringLen;
	plcbit enable;
};
_BUR_PUBLIC void DiagGetStrInfo(struct DiagGetStrInfo* inst);
struct ViBaseSaveDiagData
{	struct ViComponentType(* MpLink);
	plcstring(* DeviceName)[51];
	plcstring(* FileName)[256];
	signed long StatusID;
	ViBaseInternalType Internal;
	plcbit Execute;
	plcbit Done;
	plcbit Busy;
	plcbit Error;
};
_BUR_PUBLIC void ViBaseSaveDiagData(struct ViBaseSaveDiagData* inst);
struct ViBaseLoadApplication
{	struct ViComponentType(* MpLink);
	plcstring(* Name)[51];
	signed long StatusID;
	ViBaseInternalType Internal;
	plcbit Execute;
	plcbit Done;
	plcbit Busy;
	plcbit Error;
};
_BUR_PUBLIC void ViBaseLoadApplication(struct ViBaseLoadApplication* inst);
_BUR_LOCAL struct ViBaseLoadApplication ViBaseLoadApplication_01;
_BUR_LOCAL struct ViBaseSaveDiagData ViBaseSaveDiagData_01;
_BUR_LOCAL struct FileDelete FDelete;
_BUR_LOCAL unsigned short idSensor;
_BUR_LOCAL unsigned short idLight;
_BUR_LOCAL struct TON TriggerTimeoutSensor[6];
_BUR_LOCAL struct TON TriggerTimeoutLight[5];
_BUR_LOCAL unsigned long AcquisitionFailedCntOld[6];
_BUR_LOCAL unsigned long AcquisitionCompletedCntOld[6];
_BUR_LOCAL plcbit AutoSetupRunning[6];
_BUR_LOCAL unsigned char FlashFailedCntOld[5];
_BUR_LOCAL unsigned char FlashCompletedCntOld[5];
_BUR_LOCAL struct DiagCreateInfo DiagCreateInfo_0;
_BUR_LOCAL struct DiagGetNumInfo DiagGetNumInfo_0;
_BUR_LOCAL struct DiagGetStrInfo DiagGetStrInfo_0;
_BUR_LOCAL struct DiagDisposeInfo DiagDisposeInfo_0;
_BUR_LOCAL unsigned char OpticsReadStep[7];
_BUR_LOCAL unsigned long Index;
_BUR_LOCAL unsigned char(* pChar);
_BUR_LOCAL double DISTANCE_FRONTGLASS_LENS_MM;
_BUR_LOCAL unsigned char SENSOR_1_3_MP;
_BUR_LOCAL unsigned char SENSOR_3_5_MP;
_BUR_LOCAL unsigned char SENSOR_5_3_MP;
_BUR_LOCAL unsigned char LENS_4_6;
_BUR_LOCAL unsigned char LENS_6_0;
_BUR_LOCAL unsigned char LENS_8_0;
_BUR_LOCAL unsigned char LENS_12_0;
_BUR_LOCAL unsigned char LENS_16_0;
_BUR_LOCAL unsigned char LENS_25_0;
_BUR_LOCAL unsigned char LENS_12_0_MACRO;
_BUR_LOCAL unsigned short i;
_BUR_LOCAL plcstring HexTab[17];
_BUR_LOCAL struct TON TON_DelayAfterAutoSetup[6];
_GLOBAL struct typLightMain gVisionLight[6];
_GLOBAL struct typVisionMain gVisionSensor[7];
_GLOBAL signed long NettimeCurrent_us;
_GLOBAL unsigned short visSelectedSensor;
_GLOBAL unsigned short MAX_NUM_CAMS;
_GLOBAL unsigned short MAX_NUM_LIGHTS;
_GLOBAL plcstring DEVICE_NAME[41];
_GLOBAL unsigned long NETTIME_DEFAULT_DELAY;
_GLOBAL plcbit NETTIME_ENABLE;
_GLOBAL unsigned short ERR_OK;
_GLOBAL unsigned short ERR_FUB_BUSY;
_GLOBAL unsigned long asdiagPLUGGED_MODNO;
_GLOBAL unsigned long asdiagPLUGGED_MODULE;
_GLOBAL unsigned long asdiagPLUGGED;
