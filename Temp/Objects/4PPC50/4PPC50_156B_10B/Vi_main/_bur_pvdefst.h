#ifndef __AS__TYPE_ViBaseFubProcessingType
#define __AS__TYPE_ViBaseFubProcessingType
typedef struct ViBaseFubProcessingType
{	signed long Mediator[2];
} ViBaseFubProcessingType;
#endif

#ifndef __AS__TYPE_ViBaseControlIfType
#define __AS__TYPE_ViBaseControlIfType
typedef struct ViBaseControlIfType
{	plcdword VTable;
} ViBaseControlIfType;
#endif

#ifndef __AS__TYPE_ViBaseInternalType
#define __AS__TYPE_ViBaseInternalType
typedef struct ViBaseInternalType
{	unsigned long ID;
	unsigned long Check;
	unsigned long ParamHash;
	plcword State;
	unsigned short Error;
	struct ViBaseFubProcessingType(* Treating);
	unsigned long Reserve[2];
	unsigned char Flags;
	struct ViBaseControlIfType(* ControlIf);
	signed long SeqNo;
} ViBaseInternalType;
#endif

#ifndef __AS__TYPE_ViComponentInternalMappLinkType
#define __AS__TYPE_ViComponentInternalMappLinkType
typedef struct ViComponentInternalMappLinkType
{	unsigned long Internal[2];
} ViComponentInternalMappLinkType;
#endif

#ifndef __AS__TYPE_ViComponentInternalCameraIfType
#define __AS__TYPE_ViComponentInternalCameraIfType
typedef struct ViComponentInternalCameraIfType
{	plcdword Vtable;
} ViComponentInternalCameraIfType;
#endif

#ifndef __AS__TYPE_ViComponentType
#define __AS__TYPE_ViComponentType
typedef struct ViComponentType
{	struct ViComponentInternalCameraIfType(* CameraType);
	ViComponentInternalMappLinkType MappLinkInternal;
} ViComponentType;
#endif

struct FileDelete
{	unsigned long pDevice;
	unsigned long pName;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileDelete(struct FileDelete* inst);
struct TON
{	plctime PT;
	plctime ET;
	plctime StartTime;
	unsigned long Restart;
	plcbit IN;
	plcbit Q;
	plcbit M;
};
_BUR_PUBLIC void TON(struct TON* inst);
struct DiagCreateInfo
{	unsigned long infoKind;
	unsigned short status;
	unsigned long ident;
	unsigned long nrEntries;
	unsigned short StateMan;
	unsigned short ErrMan;
	unsigned long Init;
	plcbit enable;
};
_BUR_PUBLIC void DiagCreateInfo(struct DiagCreateInfo* inst);
struct DiagDisposeInfo
{	unsigned long ident;
	unsigned short status;
	unsigned short StateMan;
	unsigned short ErrMan;
	unsigned long Init;
	plcbit enable;
};
_BUR_PUBLIC void DiagDisposeInfo(struct DiagDisposeInfo* inst);
struct DiagGetNumInfo
{	unsigned long ident;
	unsigned long index;
	unsigned long infoCode;
	unsigned short status;
	unsigned long value;
	plcbit enable;
};
_BUR_PUBLIC void DiagGetNumInfo(struct DiagGetNumInfo* inst);
struct DiagGetStrInfo
{	unsigned long ident;
	unsigned long index;
	unsigned long infoCode;
	unsigned long pBuffer;
	unsigned long bufferLen;
	unsigned short status;
	unsigned long stringLen;
	plcbit enable;
};
_BUR_PUBLIC void DiagGetStrInfo(struct DiagGetStrInfo* inst);
struct ViBaseSaveDiagData
{	struct ViComponentType(* MpLink);
	plcstring(* DeviceName)[51];
	plcstring(* FileName)[256];
	signed long StatusID;
	ViBaseInternalType Internal;
	plcbit Execute;
	plcbit Done;
	plcbit Busy;
	plcbit Error;
};
_BUR_PUBLIC void ViBaseSaveDiagData(struct ViBaseSaveDiagData* inst);
struct ViBaseLoadApplication
{	struct ViComponentType(* MpLink);
	plcstring(* Name)[51];
	signed long StatusID;
	ViBaseInternalType Internal;
	plcbit Execute;
	plcbit Done;
	plcbit Busy;
	plcbit Error;
};
_BUR_PUBLIC void ViBaseLoadApplication(struct ViBaseLoadApplication* inst);
_BUR_LOCAL struct ViBaseLoadApplication ViBaseLoadApplication_01;
_BUR_LOCAL struct ViBaseSaveDiagData ViBaseSaveDiagData_01;
_BUR_LOCAL struct FileDelete FDelete;
_BUR_LOCAL unsigned short idSensor;
_BUR_LOCAL unsigned short idLight;
_BUR_LOCAL struct TON TriggerTimeoutSensor[6];
_BUR_LOCAL struct TON TriggerTimeoutLight[5];
_BUR_LOCAL unsigned long AcquisitionFailedCntOld[6];
_BUR_LOCAL unsigned long AcquisitionCompletedCntOld[6];
_BUR_LOCAL plcbit AutoSetupRunning[6];
_BUR_LOCAL unsigned char FlashFailedCntOld[5];
_BUR_LOCAL unsigned char FlashCompletedCntOld[5];
_BUR_LOCAL struct DiagCreateInfo DiagCreateInfo_0;
_BUR_LOCAL struct DiagGetNumInfo DiagGetNumInfo_0;
_BUR_LOCAL struct DiagGetStrInfo DiagGetStrInfo_0;
_BUR_LOCAL struct DiagDisposeInfo DiagDisposeInfo_0;
_BUR_LOCAL unsigned char OpticsReadStep[7];
_BUR_LOCAL unsigned long Index;
_BUR_LOCAL unsigned char(* pChar);
_BUR_LOCAL double DISTANCE_FRONTGLASS_LENS_MM;
_BUR_LOCAL unsigned char SENSOR_1_3_MP;
_BUR_LOCAL unsigned char SENSOR_3_5_MP;
_BUR_LOCAL unsigned char SENSOR_5_3_MP;
_BUR_LOCAL unsigned char LENS_4_6;
_BUR_LOCAL unsigned char LENS_6_0;
_BUR_LOCAL unsigned char LENS_8_0;
_BUR_LOCAL unsigned char LENS_12_0;
_BUR_LOCAL unsigned char LENS_16_0;
_BUR_LOCAL unsigned char LENS_25_0;
_BUR_LOCAL unsigned char LENS_12_0_MACRO;
_BUR_LOCAL unsigned short i;
_BUR_LOCAL plcstring HexTab[17];
_BUR_LOCAL unsigned short ERR_VA_LIST_NUM;
_BUR_LOCAL struct TON TON_DelayAfterAutoSetup[6];
_GLOBAL unsigned short MAX_NUM_CAMS;
_GLOBAL unsigned short MAX_NUM_LIGHTS;
