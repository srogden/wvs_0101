#ifndef __AS__TYPE_
#define __AS__TYPE_
static signed long __AS__WSTRING_CMP(unsigned short* pstr1, unsigned short* pstr2);
static signed long __AS__STRING_CMP(char* pstr1, char* pstr2);
typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

#ifndef __AS__TYPE_recSTATE
#define __AS__TYPE_recSTATE
typedef enum recSTATE
{	REC_WAIT = 0,
	REC_REG_NAME = 1,
	REC_REG_VAR = 2,
	REC_CREATE_DIR = 3,
	REC_READ_DIR = 4,
	REC_READ_DIR_1 = 5,
	REC_READ_DIR_2 = 6,
	REC_READ_DIR_3 = 7,
	REC_GEN_FILE_NAME = 8,
	REC_LOAD_SAVE = 9,
	REC_DELETE = 10,
	REC_RENAME = 11,
	REC_VIEW = 12,
	REC_DOWNLOAD = 13,
	REC_DOWNLOAD_1 = 14,
	REC_DOWNLOAD_2 = 15,
	REC_DOWNLOAD_3 = 16,
	REC_DOWNLOAD_4 = 17,
	REC_UPLOAD = 18,
	REC_UPLOAD_1 = 19,
	REC_UPLOAD_2 = 20,
	REC_UPLOAD_3 = 21,
	REC_UPLOAD_4 = 22,
	REC_UPLOAD_5 = 23,
	REC_UPLOAD_6 = 24,
	REC_ERROR = 25,
} recSTATE;
#endif

#ifndef __AS__TYPE_recTYPE
#define __AS__TYPE_recTYPE
typedef enum recTYPE
{	typeCSV = 0,
	typeXML = 1,
} recTYPE;
#endif

#ifndef __AS__TYPE_recERR
#define __AS__TYPE_recERR
typedef struct recERR
{	plcwstring Text[201];
	recSTATE State;
} recERR;
#endif

#ifndef __AS__TYPE_recCMD
#define __AS__TYPE_recCMD
typedef struct recCMD
{	plcbit Init;
	plcbit New;
	plcbit Load;
	plcbit Save;
	plcbit View;
	plcbit Rename;
	plcbit Delete;
	plcbit Download;
	plcbit Upload;
	plcbit ErrorReset;
} recCMD;
#endif

#ifndef __AS__TYPE_recPAR
#define __AS__TYPE_recPAR
typedef struct recPAR
{	plcwstring RecipeName[41];
	plcwstring RecipeNameNew[41];
	plcstring RecipeID[41];
	plcbit Initialized;
	unsigned char VisuSlotID;
	plcbit VisuEnableCommand;
} recPAR;
#endif

#ifndef __AS__TYPE_recDAT
#define __AS__TYPE_recDAT
typedef struct recDAT
{	plcwstring RecipeNames[101][41];
	plcstring RecipeIDs[101][41];
	unsigned short RecipeNum;
} recDAT;
#endif

#ifndef __AS__TYPE_recVIS
#define __AS__TYPE_recVIS
typedef struct recVIS
{	plcwstring RecipeNames[101][121];
	unsigned short RecipeNum;
	plcwstring RecipeFilter[41];
	plcwstring RecipeSelect[41];
	plcstring DownloadFileUrl[41];
	plcbit UploadOverwriteRequest;
	unsigned char UploadOverwriteResponse;
	plcbit ReloadUpload;
	unsigned char RecipeDoubleClick;
	plcstring ViewFilePath[121];
	plcbit ViewFile;
	plcbit ShowMessageBoxError;
} recVIS;
#endif

#ifndef __AS__TYPE_recMAIN
#define __AS__TYPE_recMAIN
typedef struct recMAIN
{	recCMD CMD;
	recPAR PAR;
	recDAT DAT;
	struct recVIS VIS[3];
	recERR ERR;
	plcwstring StatusText[101];
	signed long StatusNo;
} recMAIN;
#endif

#ifndef __AS__TYPE_fiDIR_READ_EX_DATA
#define __AS__TYPE_fiDIR_READ_EX_DATA
typedef struct fiDIR_READ_EX_DATA
{	unsigned char Filename[260];
	plcdt Date;
	unsigned long Filelength;
	unsigned short Mode;
} fiDIR_READ_EX_DATA;
#endif

#ifndef __AS__TYPE_httpHeaderLine_t
#define __AS__TYPE_httpHeaderLine_t
typedef struct httpHeaderLine_t
{	plcstring name[51];
	plcstring value[81];
} httpHeaderLine_t;
#endif

#ifndef __AS__TYPE_httpRawHeader_t
#define __AS__TYPE_httpRawHeader_t
typedef struct httpRawHeader_t
{	unsigned long pData;
	unsigned long dataSize;
	unsigned long dataLen;
} httpRawHeader_t;
#endif

#ifndef __AS__TYPE_httpResponseHeader_t
#define __AS__TYPE_httpResponseHeader_t
typedef struct httpResponseHeader_t
{	plcstring protocol[21];
	plcstring status[281];
	plcstring contentType[101];
	unsigned long contentLength;
	plcstring connection[81];
	plcstring keepAlive[81];
	struct httpHeaderLine_t userLine[8];
	httpRawHeader_t rawHeader;
} httpResponseHeader_t;
#endif

#ifndef __AS__TYPE_MpComSeveritiesEnum
#define __AS__TYPE_MpComSeveritiesEnum
typedef enum MpComSeveritiesEnum
{	mpCOM_SEV_SUCCESS = 0,
	mpCOM_SEV_INFORMATIONAL = 1,
	mpCOM_SEV_WARNING = 2,
	mpCOM_SEV_ERROR = 3,
} MpComSeveritiesEnum;
#endif

#ifndef __AS__TYPE_MpComIdentType
#define __AS__TYPE_MpComIdentType
typedef struct MpComIdentType
{	unsigned long Internal[2];
} MpComIdentType;
#endif

#ifndef __AS__TYPE_MpComInternalDataType
#define __AS__TYPE_MpComInternalDataType
typedef struct MpComInternalDataType
{	unsigned long pObject;
	unsigned long State;
} MpComInternalDataType;
#endif

#ifndef __AS__TYPE_MpRecipeErrorEnum
#define __AS__TYPE_MpRecipeErrorEnum
typedef enum MpRecipeErrorEnum
{	mpRECIPE_NO_ERROR = 0,
	mpRECIPE_ERR_ACTIVATION = -1064239103,
	mpRECIPE_ERR_MPLINK_NULL = -1064239102,
	mpRECIPE_ERR_MPLINK_INVALID = -1064239101,
	mpRECIPE_ERR_MPLINK_CHANGED = -1064239100,
	mpRECIPE_ERR_MPLINK_CORRUPT = -1064239099,
	mpRECIPE_ERR_MPLINK_IN_USE = -1064239098,
	mpRECIPE_ERR_CONFIG_INVALID = -1064239091,
	mpRECIPE_ERR_SAVE_DATA = -1064140799,
	mpRECIPE_ERR_LOAD_DATA = -1064140798,
	mpRECIPE_ERR_INVALID_FILE_DEV = -1064140797,
	mpRECIPE_ERR_INVALID_FILE_NAME = -1064140796,
	mpRECIPE_ERR_CMD_IN_PROGRESS = -1064140795,
	mpRECIPE_WRN_SAVE_WITH_WARN = -2137882618,
	mpRECIPE_WRN_LOAD_WITH_WARN = -2137882617,
	mpRECIPE_ERR_SAVE_WITH_ERRORS = -1064140792,
	mpRECIPE_ERR_LOAD_WITH_ERRORS = -1064140791,
	mpRECIPE_ERR_MISSING_RECIPE = -1064140790,
	mpRECIPE_ERR_MISSING_MPFILE = -1064140789,
	mpRECIPE_ERR_INVALID_SORT_ORDER = -1064140788,
	mpRECIPE_WRN_MISSING_UICONNECT = -2137882611,
	mpRECIPE_ERR_INVALID_PV_NAME = -1064140786,
	mpRECIPE_ERR_INVALID_LOAD_TYPE = -1064140785,
	mpRECIPE_ERR_LISTING_FILES = -1064140784,
	mpRECIPE_ERR_PV_NAME_NULL = -1064140783,
	mpRECIPE_WRN_NO_PV_REGISTERED = -2137882606,
	mpRECIPE_ERR_SYNC_SAVE_ACTIVE = -1064140781,
	mpRECIPE_ERR_DELETING_FILE = -1064140780,
	mpRECIPE_WRN_EMPTY_RECIPE = -2137882603,
	mpRECIPE_INF_WAIT_RECIPE_FB = 1083342870,
	mpRECIPE_ERR_RENAMING_FILE = -1064140777,
	mpRECIPE_WRN_NO_PV_FOUND = -2137882600,
	mpRECIPE_WRN_LIST_SIZE = -2137882599,
} MpRecipeErrorEnum;
#endif

#ifndef __AS__TYPE_MpRecipeStatusIDType
#define __AS__TYPE_MpRecipeStatusIDType
typedef struct MpRecipeStatusIDType
{	MpRecipeErrorEnum ID;
	MpComSeveritiesEnum Severity;
	unsigned short Code;
} MpRecipeStatusIDType;
#endif

#ifndef __AS__TYPE_MpRecipeDiagType
#define __AS__TYPE_MpRecipeDiagType
typedef struct MpRecipeDiagType
{	MpRecipeStatusIDType StatusID;
} MpRecipeDiagType;
#endif

#ifndef __AS__TYPE_MpRecipeInfoType
#define __AS__TYPE_MpRecipeInfoType
typedef struct MpRecipeInfoType
{	MpRecipeDiagType Diag;
} MpRecipeInfoType;
#endif

#ifndef __AS__TYPE_MpRecipeXmlInfoType
#define __AS__TYPE_MpRecipeXmlInfoType
typedef struct MpRecipeXmlInfoType
{	unsigned long FileSize;
	unsigned long PendingSync;
	MpRecipeDiagType Diag;
	plcstring LastLoadedRecipe[256];
} MpRecipeXmlInfoType;
#endif

#ifndef __AS__TYPE_MpRecipeXmlHeaderType
#define __AS__TYPE_MpRecipeXmlHeaderType
typedef struct MpRecipeXmlHeaderType
{	plcstring Name[101];
	plcstring Description[256];
	plcstring Version[21];
	plcdt DateTime;
} MpRecipeXmlHeaderType;
#endif

#ifndef __AS__TYPE_MpRecipeXmlLoadEnum
#define __AS__TYPE_MpRecipeXmlLoadEnum
typedef enum MpRecipeXmlLoadEnum
{	mpRECIPE_XML_LOAD_ALL = 0,
	mpRECIPE_XML_LOAD_HEADER = 1,
} MpRecipeXmlLoadEnum;
#endif

#ifndef __AS__TYPE_MpRecipeCsvHeaderType
#define __AS__TYPE_MpRecipeCsvHeaderType
typedef struct MpRecipeCsvHeaderType
{	plcstring Name[101];
	plcstring Description[256];
	plcstring Version[21];
	plcdt DateTime;
} MpRecipeCsvHeaderType;
#endif

#ifndef __AS__TYPE_MpRecipeCsvInfoType
#define __AS__TYPE_MpRecipeCsvInfoType
typedef struct MpRecipeCsvInfoType
{	unsigned long FileSize;
	unsigned long PendingSync;
	plcstring LastLoadedRecipe[256];
	MpRecipeDiagType Diag;
} MpRecipeCsvInfoType;
#endif

#ifndef __AS__TYPE_MpRecipeCsvLoadEnum
#define __AS__TYPE_MpRecipeCsvLoadEnum
typedef enum MpRecipeCsvLoadEnum
{	mpRECIPE_CSV_LOAD_ALL = 0,
	mpRECIPE_CSV_LOAD_HEADER = 1,
} MpRecipeCsvLoadEnum;
#endif

recSTATE CreateError(struct recMAIN(* RECIPE), signed long No, plcwstring Text[201], recSTATE State);
plcbit CreateMessage(unsigned long TargetString, unsigned long Text, unsigned long RecipeName, unsigned char VisuSlotID);
unsigned short FindRecName(struct recMAIN(* RECIPE), plcwstring RecipeName[41]);
unsigned short InsertRecName(struct recMAIN(* RECIPE), plcwstring new_name[41], plcstring new_id[41]);
plcbit RemoveRecName(struct recMAIN(* RECIPE), unsigned short RecipeIndex);
plcbit IsInstrW(unsigned long string1, unsigned long string2);
unsigned char WorkingStatus(struct recMAIN(* RECIPE), unsigned long str, unsigned char animation);
plcbit WString2DataProvider(unsigned long SourceString, unsigned long TargetString);
plcbit ReplaceString(unsigned long dataSTR, unsigned long searchSTR, unsigned long replaceSTR, plcbit first);
unsigned long FindInMem(unsigned long data1, unsigned long len1, unsigned long data2, unsigned long len2);
_BUR_PUBLIC plcwstring* usint2wstr(unsigned char IN, plcstring pStr[81], unsigned long len);
struct FileCreate
{	unsigned long pDevice;
	unsigned long pFile;
	unsigned short status;
	unsigned long ident;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileCreate(struct FileCreate* inst);
struct FileOpen
{	unsigned long pDevice;
	unsigned long pFile;
	unsigned char mode;
	unsigned short status;
	unsigned long ident;
	unsigned long filelen;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileOpen(struct FileOpen* inst);
struct FileClose
{	unsigned long ident;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileClose(struct FileClose* inst);
struct FileReadEx
{	unsigned long ident;
	unsigned long offset;
	unsigned long pDest;
	unsigned long len;
	unsigned short status;
	unsigned long bytesread;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileReadEx(struct FileReadEx* inst);
struct FileWrite
{	unsigned long ident;
	unsigned long offset;
	unsigned long pSrc;
	unsigned long len;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileWrite(struct FileWrite* inst);
struct FileRename
{	unsigned long pDevice;
	unsigned long pName;
	unsigned long pNewName;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileRename(struct FileRename* inst);
struct FileDelete
{	unsigned long pDevice;
	unsigned long pName;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void FileDelete(struct FileDelete* inst);
struct DirCreate
{	unsigned long pDevice;
	unsigned long pName;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void DirCreate(struct DirCreate* inst);
struct DirOpen
{	unsigned long pDevice;
	unsigned long pName;
	unsigned short status;
	unsigned long ident;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void DirOpen(struct DirOpen* inst);
struct DirClose
{	unsigned long ident;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void DirClose(struct DirClose* inst);
struct DirReadEx
{	unsigned long ident;
	unsigned long pData;
	unsigned long data_len;
	unsigned short status;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void DirReadEx(struct DirReadEx* inst);
struct DirInfo
{	unsigned long pDevice;
	unsigned long pPath;
	unsigned short status;
	unsigned long dirnum;
	unsigned long filenum;
	unsigned short i_state;
	unsigned short i_result;
	unsigned long i_tmp;
	plcbit enable;
};
_BUR_PUBLIC void DirInfo(struct DirInfo* inst);
_BUR_PUBLIC unsigned short brsitoa(signed long value, unsigned long pString);
_BUR_PUBLIC signed long brsatoi(unsigned long pString);
_BUR_PUBLIC unsigned long brsmemset(unsigned long pDest, unsigned char value, unsigned long length);
_BUR_PUBLIC unsigned long brsmemcpy(unsigned long pDest, unsigned long pSrc, unsigned long length);
_BUR_PUBLIC unsigned long brsmemmove(unsigned long pDest, unsigned long pSrc, unsigned long length);
_BUR_PUBLIC signed long brsmemcmp(unsigned long pMem1, unsigned long pMem2, unsigned long length);
_BUR_PUBLIC unsigned long brsstrcat(unsigned long pDest, unsigned long pSrc);
_BUR_PUBLIC unsigned long brsstrlen(unsigned long pString);
_BUR_PUBLIC unsigned long brsstrcpy(unsigned long pDest, unsigned long pSrc);
struct TON_10ms
{	unsigned long PT;
	unsigned long ET;
	unsigned long StartTime;
	plcbit IN;
	plcbit Q;
	plcbit M;
};
_BUR_PUBLIC void TON_10ms(struct TON_10ms* inst);
struct httpService
{	unsigned long option;
	unsigned long pServiceName;
	unsigned long pUri;
	unsigned long uriSize;
	unsigned long pRequestHeader;
	unsigned long pRequestData;
	unsigned long requestDataSize;
	unsigned long pResponseHeader;
	unsigned long pResponseData;
	unsigned long responseDataLen;
	unsigned long pStatistics;
	unsigned long pStruct;
	unsigned short status;
	unsigned short method;
	unsigned long requestDataLen;
	unsigned short phase;
	unsigned long _i_state;
	unsigned short _i_result;
	unsigned long _ident;
	unsigned long _oldEnable;
	unsigned long _oldAbort;
	unsigned long _oldSend;
	unsigned long _state;
	unsigned long _internal;
	plcbit enable;
	plcbit send;
	plcbit abort;
};
_BUR_PUBLIC void httpService(struct httpService* inst);
struct AsMemPartAllocClear
{	unsigned long ident;
	unsigned long len;
	unsigned short status;
	unsigned long mem;
	plcbit enable;
};
_BUR_PUBLIC void AsMemPartAllocClear(struct AsMemPartAllocClear* inst);
_BUR_PUBLIC unsigned short CheckDivUint(unsigned short divisor);
_BUR_PUBLIC unsigned long CheckDivUdint(unsigned long divisor);
_BUR_PUBLIC signed long CheckBounds(signed long index, signed long lower, signed long upper);
_BUR_PUBLIC signed long CheckRange(signed long value, signed long lower, signed long upper);
_BUR_PUBLIC unsigned long CheckReadAccess(unsigned long address);
_BUR_PUBLIC unsigned long CheckWriteAccess(unsigned long address);
_BUR_PUBLIC unsigned long brwcsconv(unsigned char(* pDestination), unsigned char(* pSource), unsigned char level);
_BUR_PUBLIC unsigned long brwcscat(unsigned short(* pDestination), unsigned short(* pSource));
_BUR_PUBLIC signed long brwcscmp(unsigned short(* pUcstr1), unsigned short(* pUcstr2));
_BUR_PUBLIC unsigned long brwcslen(unsigned short(* pwcString));
_BUR_PUBLIC unsigned long brwcscpy(unsigned short(* pDestination), unsigned short(* pSource));
struct MpRecipeRegPar
{	struct MpComIdentType(* MpLink);
	plcstring(* PVName)[101];
	plcstring(* Category)[51];
	signed long StatusID;
	MpRecipeInfoType Info;
	MpComInternalDataType Internal;
	plcbit Enable;
	plcbit ErrorReset;
	plcbit Active;
	plcbit Error;
	plcbit UpdateNotification;
};
_BUR_PUBLIC void MpRecipeRegPar(struct MpRecipeRegPar* inst);
struct MpRecipeXml
{	struct MpComIdentType(* MpLink);
	plcstring(* DeviceName)[51];
	plcstring(* FileName)[256];
	struct MpRecipeXmlHeaderType(* Header);
	plcstring(* Category)[51];
	MpRecipeXmlLoadEnum LoadType;
	signed long StatusID;
	MpRecipeXmlInfoType Info;
	MpComInternalDataType Internal;
	plcbit Enable;
	plcbit ErrorReset;
	plcbit Load;
	plcbit Save;
	plcbit UpdateNotification;
	plcbit Active;
	plcbit Error;
	plcbit CommandBusy;
	plcbit CommandDone;
};
_BUR_PUBLIC void MpRecipeXml(struct MpRecipeXml* inst);
struct MpRecipeCsv
{	struct MpComIdentType(* MpLink);
	plcstring(* DeviceName)[51];
	plcstring(* FileName)[256];
	struct MpRecipeCsvHeaderType(* Header);
	plcstring(* Category)[51];
	MpRecipeCsvLoadEnum LoadType;
	signed long StatusID;
	MpRecipeCsvInfoType Info;
	MpComInternalDataType Internal;
	plcbit Enable;
	plcbit ErrorReset;
	plcbit Load;
	plcbit Save;
	plcbit UpdateNotification;
	plcbit Active;
	plcbit Error;
	plcbit CommandBusy;
	plcbit CommandDone;
};
_BUR_PUBLIC void MpRecipeCsv(struct MpRecipeCsv* inst);
struct MpComGetLink
{	plcstring(* ComponentName)[101];
	MpComIdentType MpLink;
	signed long StatusID;
	plcbit Enable;
	plcbit Error;
	plcbit Active;
	plcbit Internal;
};
_BUR_PUBLIC void MpComGetLink(struct MpComGetLink* inst);
_BUR_LOCAL unsigned char OK;
_BUR_LOCAL unsigned short REC_DOES_NOT_EXIST;
_BUR_LOCAL unsigned char YES;
_BUR_LOCAL unsigned char NO;
_BUR_LOCAL struct recMAIN(* RECIPE);
_BUR_LOCAL plcstring DeviceName[81];
_BUR_LOCAL plcbit RecipeIDs[101];
_BUR_LOCAL unsigned short RecipeIndex;
_BUR_LOCAL plcwstring RecipeName[41];
_BUR_LOCAL plcwstring RecipeNameNew[41];
_BUR_LOCAL plcstring RecipeFullFileName[201];
_BUR_LOCAL plcwstring RecipeFilterOld[3][41];
_BUR_LOCAL unsigned short RecipeNumOld[3];
_BUR_LOCAL recSTATE rec_state;
_BUR_LOCAL unsigned char animation;
_BUR_LOCAL unsigned char VisuSlotID;
_BUR_LOCAL unsigned short idx;
_BUR_LOCAL unsigned short idy;
_BUR_LOCAL plcbit has_oversized_items;
_BUR_LOCAL plcstring tmpStr1[201];
_BUR_LOCAL plcstring tmpStr2[201];
_BUR_LOCAL plcwstring tmpWStr1[121];
_BUR_LOCAL plcwstring tmpWStr2[121];
_BUR_LOCAL fiDIR_READ_EX_DATA lDirReadData;
_BUR_LOCAL unsigned short taskStatus;
_BUR_LOCAL unsigned short pvStatus;
_BUR_LOCAL unsigned short pvCnt;
_BUR_LOCAL plcbit MpRecipeDone;
_BUR_LOCAL plcbit MpRecipeError;
_BUR_LOCAL signed long MpRecipeStatusID;
_BUR_LOCAL struct TON_10ms DoubleClickRecipeName[3];
_BUR_LOCAL httpResponseHeader_t response_header;
_BUR_LOCAL plcstring response_header_data[301];
_BUR_LOCAL struct TON_10ms upload_delay;
_BUR_LOCAL struct TON_10ms download_timeout;
_BUR_LOCAL plcstring upload_response[601];
_BUR_LOCAL unsigned long file_name_start;
_BUR_LOCAL unsigned long file_name_len;
_BUR_LOCAL unsigned long file_data_start;
_BUR_LOCAL unsigned long file_data_len;
_BUR_LOCAL plcstring item_name[41];
_BUR_LOCAL struct DirInfo DInfo;
_BUR_LOCAL struct DirOpen DOpen;
_BUR_LOCAL struct DirReadEx DRead;
_BUR_LOCAL struct DirClose DClose;
_BUR_LOCAL struct DirCreate DCreate;
_BUR_LOCAL struct FileOpen FOpen;
_BUR_LOCAL struct FileCreate FCreate;
_BUR_LOCAL struct FileReadEx FRead;
_BUR_LOCAL struct FileWrite FWrite;
_BUR_LOCAL struct FileClose FClose;
_BUR_LOCAL struct FileDelete FDelete;
_BUR_LOCAL struct FileRename FRename;
_BUR_LOCAL struct httpService WebserviceDownload;
_BUR_LOCAL struct httpService WebServiceUpload;
_BUR_LOCAL struct AsMemPartAllocClear AsMemPartAllocClear_0;
_BUR_LOCAL struct MpRecipeCsv MpRecipeCsv_0;
_BUR_LOCAL struct MpRecipeXml MpRecipeXml_0;
_BUR_LOCAL struct MpRecipeRegPar MpRecipeRegPar_0;
_BUR_LOCAL struct MpRecipeRegPar MpRecipeRegPar_1[10];
_BUR_LOCAL struct MpComGetLink MpComGetLink_0;
_BUR_LOCAL signed long ERR_REC_TOO_MANY_CLIENTS;
_BUR_LOCAL signed long ERR_REC_NAME_EMPTY;
_BUR_LOCAL signed long ERR_REC_NAME_EXISTS;
_BUR_LOCAL signed long ERR_REC_NAME_LOST;
_BUR_LOCAL signed long ERR_REC_NAME_LENGTH;
_BUR_LOCAL signed long ERR_REC_DEV_NAME_EMPTY;
_BUR_LOCAL signed long ERR_REC_VAR_NAME_EMPTY;
_BUR_LOCAL signed long ERR_REC_NOT_INITIALIZED;
_BUR_LOCAL signed long ERR_REC_REC_NUM;
_BUR_LOCAL signed long ERR_REC_TASK_NAME;
_BUR_LOCAL unsigned short ERR_MEM_DOWNLOAD;
_BUR_LOCAL unsigned short ERR_TIMEOUT_DOWNLOAD;
_BUR_LOCAL unsigned short ERR_POS_NAME_UPLOAD;
_BUR_LOCAL unsigned short ERR_SIZE_NAME_UPLOAD;
_BUR_LOCAL unsigned short ERR_LEN_NAME_UPLOAD;
_BUR_LOCAL unsigned short ERR_POS_DATA_UPLOAD;
_BUR_LOCAL unsigned short ERR_SIZE_DATA_UPLOAD;
_BUR_LOCAL unsigned short ERR_MEM_UPLOAD;
_BUR_LOCAL unsigned short ERR_UNICODE_UPLOAD;
_GLOBAL plcbit REC_USE_UNICODE;
_GLOBAL recTYPE REC_RECIPE_TYPE;
_GLOBAL plcstring REC_DIRECTORY[81];
_GLOBAL plcstring REC_THIS_TASK[41];
_GLOBAL unsigned char REC_REC_NUM;
_GLOBAL unsigned char REC_VAR_NUM;
_GLOBAL unsigned char REC_NAME_LENGTH;
_GLOBAL unsigned long REC_UPLOAD_DOWNLOAD_SIZE;
_GLOBAL unsigned char REC_VIS_LENGTH;
_GLOBAL unsigned char REC_MAX_CLIENTS_ID;
_GLOBAL plcstring REC_VIEW_FILTER[10][6];
_GLOBAL plcstring REC_VAR_LIST[10][101];
_GLOBAL unsigned short ERR_OK;
_GLOBAL unsigned short ERR_FUB_BUSY;
_GLOBAL unsigned char fiFILE;
_GLOBAL unsigned short fiERR_NO_MORE_ENTRIES;
_GLOBAL unsigned short fiERR_DIR_NOT_EXIST;
_GLOBAL unsigned short fiERR_DIR_ALREADY_EXIST;
_GLOBAL unsigned short fiERR_DEVICE_MANAGER;
_GLOBAL unsigned short httpOPTION_HTTP_10;
_GLOBAL unsigned short httpOPTION_HTTP_11;
_GLOBAL unsigned short httpOPTION_SERVICE_TYPE_NAME;
_GLOBAL unsigned short httpPHASE_WAITING;
_GLOBAL unsigned char brwUCtoU8;
_GLOBAL unsigned char brwU8toUC;
