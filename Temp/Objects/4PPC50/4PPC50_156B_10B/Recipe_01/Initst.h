#ifndef __AS__TYPE_
#define __AS__TYPE_
typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

#ifndef __AS__TYPE_recSTATE
#define __AS__TYPE_recSTATE
typedef enum recSTATE
{	REC_WAIT = 0,
	REC_REG_NAME = 1,
	REC_REG_VAR = 2,
	REC_CREATE_DIR = 3,
	REC_READ_DIR = 4,
	REC_READ_DIR_1 = 5,
	REC_READ_DIR_2 = 6,
	REC_READ_DIR_3 = 7,
	REC_GEN_FILE_NAME = 8,
	REC_LOAD_SAVE = 9,
	REC_DELETE = 10,
	REC_RENAME = 11,
	REC_VIEW = 12,
	REC_DOWNLOAD = 13,
	REC_DOWNLOAD_1 = 14,
	REC_DOWNLOAD_2 = 15,
	REC_DOWNLOAD_3 = 16,
	REC_DOWNLOAD_4 = 17,
	REC_UPLOAD = 18,
	REC_UPLOAD_1 = 19,
	REC_UPLOAD_2 = 20,
	REC_UPLOAD_3 = 21,
	REC_UPLOAD_4 = 22,
	REC_UPLOAD_5 = 23,
	REC_UPLOAD_6 = 24,
	REC_ERROR = 25,
} recSTATE;
#endif

#ifndef __AS__TYPE_recERR
#define __AS__TYPE_recERR
typedef struct recERR
{	plcwstring Text[201];
	recSTATE State;
} recERR;
#endif

#ifndef __AS__TYPE_recCMD
#define __AS__TYPE_recCMD
typedef struct recCMD
{	plcbit Init;
	plcbit New;
	plcbit Load;
	plcbit Save;
	plcbit View;
	plcbit Rename;
	plcbit Delete;
	plcbit Download;
	plcbit Upload;
	plcbit ErrorReset;
} recCMD;
#endif

#ifndef __AS__TYPE_recPAR
#define __AS__TYPE_recPAR
typedef struct recPAR
{	plcwstring RecipeName[41];
	plcwstring RecipeNameNew[41];
	plcstring RecipeID[41];
	plcbit Initialized;
	unsigned char VisuSlotID;
	plcbit VisuEnableCommand;
} recPAR;
#endif

#ifndef __AS__TYPE_recDAT
#define __AS__TYPE_recDAT
typedef struct recDAT
{	plcwstring RecipeNames[101][41];
	plcstring RecipeIDs[101][41];
	unsigned short RecipeNum;
} recDAT;
#endif

#ifndef __AS__TYPE_recVIS
#define __AS__TYPE_recVIS
typedef struct recVIS
{	plcwstring RecipeNames[101][121];
	unsigned short RecipeNum;
	plcwstring RecipeFilter[41];
	plcwstring RecipeSelect[41];
	plcstring DownloadFileUrl[41];
	plcbit UploadOverwriteRequest;
	unsigned char UploadOverwriteResponse;
	plcbit ReloadUpload;
	unsigned char RecipeDoubleClick;
	plcstring ViewFilePath[121];
	plcbit ViewFile;
	plcbit ShowMessageBoxError;
} recVIS;
#endif

#ifndef __AS__TYPE_recMAIN
#define __AS__TYPE_recMAIN
typedef struct recMAIN
{	recCMD CMD;
	recPAR PAR;
	recDAT DAT;
	struct recVIS VIS[3];
	recERR ERR;
	plcwstring StatusText[101];
	signed long StatusNo;
} recMAIN;
#endif

#ifndef __AS__TYPE_MpComIdentType
#define __AS__TYPE_MpComIdentType
typedef struct MpComIdentType
{	unsigned long Internal[2];
} MpComIdentType;
#endif

recSTATE CreateError(struct recMAIN(* RECIPE), signed long No, plcwstring Text[201], recSTATE State);
_BUR_PUBLIC unsigned long brsmemset(unsigned long pDest, unsigned char value, unsigned long length);
_BUR_PUBLIC unsigned short ST_ident(unsigned long st_name, unsigned char st_grp, unsigned long st_ident);
_BUR_PUBLIC unsigned short PV_xgetadr(unsigned long pv_name_p, unsigned long pv_adresse, unsigned long data_len);
struct httpService
{	unsigned long option;
	unsigned long pServiceName;
	unsigned long pUri;
	unsigned long uriSize;
	unsigned long pRequestHeader;
	unsigned long pRequestData;
	unsigned long requestDataSize;
	unsigned long pResponseHeader;
	unsigned long pResponseData;
	unsigned long responseDataLen;
	unsigned long pStatistics;
	unsigned long pStruct;
	unsigned short status;
	unsigned short method;
	unsigned long requestDataLen;
	unsigned short phase;
	unsigned long _i_state;
	unsigned short _i_result;
	unsigned long _ident;
	unsigned long _oldEnable;
	unsigned long _oldAbort;
	unsigned long _oldSend;
	unsigned long _state;
	unsigned long _internal;
	plcbit enable;
	plcbit send;
	plcbit abort;
};
_BUR_PUBLIC void httpService(struct httpService* inst);
struct AsMemPartCreate
{	unsigned long len;
	unsigned short status;
	unsigned long ident;
	plcbit enable;
};
_BUR_PUBLIC void AsMemPartCreate(struct AsMemPartCreate* inst);
struct AsMemPartAllocClear
{	unsigned long ident;
	unsigned long len;
	unsigned short status;
	unsigned long mem;
	plcbit enable;
};
_BUR_PUBLIC void AsMemPartAllocClear(struct AsMemPartAllocClear* inst);
_BUR_PUBLIC signed long CheckRange(signed long value, signed long lower, signed long upper);
_BUR_PUBLIC unsigned long CheckWriteAccess(unsigned long address);
_BUR_PUBLIC plcbit DiagCpuIsSimulated(void);
struct MpComGetLink
{	plcstring(* ComponentName)[101];
	MpComIdentType MpLink;
	signed long StatusID;
	plcbit Enable;
	plcbit Error;
	plcbit Active;
	plcbit Internal;
};
_BUR_PUBLIC void MpComGetLink(struct MpComGetLink* inst);
_BUR_LOCAL struct recMAIN(* RECIPE);
_BUR_LOCAL plcstring DeviceName[81];
_BUR_LOCAL recSTATE rec_state;
_BUR_LOCAL unsigned long taskID;
_BUR_LOCAL unsigned char taskGroup;
_BUR_LOCAL unsigned short taskStatus;
_BUR_LOCAL unsigned long pvADR;
_BUR_LOCAL unsigned long pvLEN;
_BUR_LOCAL unsigned short pvStatus;
_BUR_LOCAL plcstring upload_response[601];
_BUR_LOCAL struct httpService WebServiceUpload;
_BUR_LOCAL struct AsMemPartCreate AsMemPartCreate_0;
_BUR_LOCAL struct AsMemPartAllocClear AsMemPartAllocClear_0;
_BUR_LOCAL struct MpComGetLink MpComGetLink_0;
_BUR_LOCAL signed long ERR_REC_MPLINK;
_BUR_LOCAL unsigned short ERR_MEM_ALLOC;
_GLOBAL plcstring REC_MPLINK[81];
_GLOBAL plcstring REC_DEVICE[81];
_GLOBAL plcstring REC_THIS_TASK[41];
_GLOBAL unsigned char REC_REC_NUM;
_GLOBAL unsigned char REC_NAME_LENGTH;
_GLOBAL unsigned long REC_UPLOAD_DOWNLOAD_SIZE;
_GLOBAL unsigned char REC_VIS_LENGTH;
_GLOBAL unsigned char REC_MAX_CLIENTS_ID;
