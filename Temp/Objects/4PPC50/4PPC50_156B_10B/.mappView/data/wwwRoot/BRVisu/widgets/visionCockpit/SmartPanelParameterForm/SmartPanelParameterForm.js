﻿/*global define, brease, $*/
define(['brease/core/BaseWidget',
    'brease/events/BreaseEvent',
    'brease/decorators/VisibilityDependency',
    'brease/config/NumberFormat',
    'brease/core/Utils',
    'brease/helper/Scroller',
    'brease/enum/Enum'
], function (SuperClass, BreaseEvent, VisibiliyDependency, NumberFormat, Utils, Scroller, Enum) {

    'use strict';

    /**
     * @class widgets.visionCockpit.SmartPanelParameterForm      
     * #Description
     * shows the parameter of a selected Vision Function
     * 
     * @breaseNote 
     * @extends brease.core.BaseWidget
     * @requires widgets.brease.Label
     * @requires widgets.brease.NumericInput
     * @requires widgets.brease.TextInput
     * @requires widgets.brease.DropDownBox
     * @requires widgets.brease.Table 
     * @requires widgets.brease.TableItem 
     * @requires widgets.brease.TabControl  
     * @requires widgets.brease.TableItemWidget   
     *  
     *
     * @iatMeta category:Category
     * Process
     * @iatMeta description:short
     * Parameter Form
     * @iatMeta description:de
     * (INTERNAL USE ONLY) Zeigt die Parameter der selektierten Vision Funktion an
     * @iatMeta description:en
     * (INTERNAL USE ONLY) shows the parameter of a selected Vision Function
     */

    /**
     * @cfg {StyleReference} labelStyle='default' 
     * @iatStudioExposed
     * @iatCategory Appearance
     * @typeRefId widgets.brease.Label
     * Styling of the paramter labels. 
     */

    /**
     * @cfg {StyleReference} numericInputStyle='default'
     * @iatStudioExposed
     * @iatCategory Appearance
     * @typeRefId widgets.brease.NumericInput
     * Styling of numeric input fields. 
     */

    /**
     * @cfg {StyleReference} textInputStyle='default'
     * @iatStudioExposed
     * @iatCategory Appearance
     * @typeRefId widgets.brease.Textinput
     * Styling of text input fields. 
     */

    /**
     * @cfg {StyleReference} textOutputStyle='default'
     * @iatStudioExposed
     * @iatCategory Appearance
     * @typeRefId widgets.brease.TextOutput
     * Styling of text output fields. 
     */

    /**
     * @cfg {StyleReference} dropDownBoxStyle='default'
     * @iatStudioExposed
     * @iatCategory Appearance
     * @typeRefId widgets.brease.DropDownBox
     * Styling of drop down boxes. 
     */

    /**
     * @cfg {Integer} dropDownBoxListWidth= 150
     * @iatStudioExposed
     * @iatCategory Layout DropDownBox
     */

    /**
     * @cfg {Boolean} sortByResult=false  
     * @iatStudioExposed    
     * @bindable
     * @iatCategory Data
     * Order the widget by by name '0' or number '1' 
     */

    /**
     * @cfg {Boolean} useTableWidgetForResultsList =false  
     * @iatStudioExposed    
     * @iatCategory Data
     * Define the table widget for results list 
     */

    var defaultSettings = {
        labelStyle: 'default',
        useTableWidgetForResultsList: false,
        numericInputStyle: 'default',
        textInputStyle: 'default',
        textOutputStyle: 'default',
        dropDownBoxStyle: 'default',
        padding: 20,
        parameters: [],
        dropDownBoxListWidth: 150,
        sortByResult: false,
        lastFilterIndex: 1,
        numResults: 0
    },


        WidgetClass = SuperClass.extend(function SmartPanelParameterForm() {
            SuperClass.apply(this, arguments);
        }, defaultSettings),

        p = WidgetClass.prototype;

    p.init = function () {
        if (this.settings.omitClass !== true) {
            this.addInitialClass('visionCockpitSmartPanelParameterForm');
        }

        this.scrollContainer = $('<div class="scrollContainer">');
        this.drawContainer = $('<div class="drawContainer">');
        this.labelContainer = $('<div class="ParamRowContainer">');
        this.inputContainer = $('<div class="ParamRowContainer">');
        this.tableContainer = $('<div class="ParamRowContainer">');
        this.inputContainer.on("ValueChanged", this._bind('_valueChangedHandler'));
        this.inputContainer.on("SelectedIndexChanged", this._bind('_indexChangedHandler'));

        this.resultTable = {
            'resultValues': new Map(),
            'viewModel': new Map(),
        };

        if (brease.config.editMode) {
            // we only got events for appended elements in iat-designer... 
            this.drawContainer.append(this.labelContainer);
            this.drawContainer.append(this.inputContainer);
            this.scrollContainer.append(this.drawContainer);
            this.scrollContainer.append(this.tableContainer);

            this.el.append(this.scrollContainer);
            _initEditor(this);
            _addChildWidgets(this);
        } else {
            _update(this);
        }


        SuperClass.prototype.init.call(this, true);
    };

    p.setHeight = function (height) {
        SuperClass.prototype._setHeight.apply(this, arguments);
        this.elem.style.height = parseInt(height) + 'px';
    };

    /**
     * @method setSortByResult
     * Sets the sortByResult
     * @param {Boolean} sortByResult
     */
    p.setSortByResult = function (sortByResult) {
        this.settings.sortByResult = sortByResult;
        if (this.state === Enum.WidgetState.READY) {
            this.drawResultTable();
        } else {
            this.postponeDrawResultTable = true;
        }
    };

    /**
     * @method getSortByResult
     * Gets the sortByResult
     * @return {Boolean} 
     */
    p.getSortByResult = function () {
        return this.settings.sortByResult;
    };

    /**
     * @method filterByIndex        
     * @param {UInteger} filterIndex
     * @iatStudioExposed
     */
    p.filterByIndex = function (filterIndex) {
        this.settings.lastFilterIndex = filterIndex;

        if (this.state === 2) {
            this.drawResultTable();
        } else {
            this.postponeDrawResultTable = true;
        }
    };

    p.drawResultTable = function () {
        var widget = this,
            table = brease.callWidget(widget.tableIds[0], "widget"),
            colLabel = brease.callWidget(widget.tableIds[1], "widget"),
            colValue = brease.callWidget(widget.tableIds[2], "widget"),
            filterIndex = this.settings.lastFilterIndex,
            TableWidth = "458px",
            visibleLabels = [],
            visibleValues = [],
            valueObjects = [];

        // we only want to do this once, move it somewhere else
        if (typeof table.placedInDatabase === 'function') {
            table.placedInDatabase();
        }
        if (colLabel && colValue) {
            valueObjects = Array.from(widget.resultTable.resultValues.values());

            if (widget.settings.sortByResult === "true") {
                valueObjects.sort(widget.sortResultsByIndex);
            } else {
                valueObjects.sort(widget.sortResultsByName);
            }

            valueObjects.forEach(function (valueObject) {
                if ((valueObject.index === undefined) ||
                    (valueObject.index === filterIndex) ||
                    //(filterIndex === 0) && ((valueObject.index === 1) || (valueObject.index <= widget.settings.numResults))) { // KaP: TODO: use this line to limit number of outs to numResults
                    (filterIndex === 0)) {                                                                                       // KaP: TODO: use this line to show all elements - used for workaround "enhancedgradinginfo"                   
                    visibleLabels.push(valueObject.label);
                    visibleValues.push(valueObject.value);
                }
            });

            // since this is not a standard usecase we have to purge the data from the table since
            // the sice of the arrays magically change - non OPCUA arrays
            if (typeof table.purgeDatabaseData === 'function') {
                table.purgeDatabaseData();
            }
            colLabel.setStringValue(visibleLabels);
            colValue.setStringValue(visibleValues);
        }

        table.elem.style.height = "auto";

        if (table.renderer.tableWrapper) {
            table.renderer.tableWrapper.find('.dataTables_scrollBody')[0].style.height = 'auto';
        }
        table.elem.style.width = TableWidth;
        this._refreshScroller();
    };

    p.sortResultsByIndex = function (a, b) {
        if (a.index && b.index) {
            if (a.index < b.index) {
                return -1;
            }
            if (a.index > b.index) {
                return 1;
            }
        }
        return 0;
    };

    p.sortResultsByName = function (a, b) {
        var sa, sb;
        if (a.index && b.index && a.label && b.label) {
            sa = a.label.replace(/\d+/g, '') + a.index.toString().padStart(4, "0");
            sb = b.label.replace(/\d+/g, '') + b.index.toString().padStart(4, "0");

            if (sa < sb) {
                return -1;
            }
            if (sa > sb) {
                return 1;
            }
        }
        return 0;
    };

    /**
     * @method setDropDownBoxListWidth
     * @param {Integer} dropDownBoxListWidth
     */
    p.setDropDownBoxListWidth = function (dropDownBoxListWidth) {
        this.settings.dropDownBoxListWidth = dropDownBoxListWidth;
    };

    /**
     * @method getDropDownBoxListWidth
     * @return {Integer} dropDownBoxListWidth
     */
    p.getDropDownBoxListWidth = function () {
        return this.settings.dropDownBoxListWidth;
    };

    p.disable = function () {
        SuperClass.prototype.disable.apply(this, arguments);
        if (this.initialized !== true) {
            _selectChildren(this.inputContainer).each(function () {
                if (brease.uiController.callWidget(this.id, "setParentEnableState", false) === null) {
                    brease.uiController.addWidgetOption(this.id, 'parentEnableState', false);
                }
            });
            _selectChildren(this.labelContainer).each(function () {
                if (brease.uiController.callWidget(this.id, "setParentEnableState", false) === null) {
                    brease.uiController.addWidgetOption(this.id, 'parentEnableState', false);
                }
            });
        }
    };

    p._enableHandler = function () {
        var disabled;
        SuperClass.prototype._enableHandler.apply(this, arguments);
        disabled = this.isDisabled;

        _selectChildren(this.inputContainer).each(function () {
            if (this.id.endsWith("_Input")) {
                brease.callWidget(this.id, "setEnable", !disabled);
            }
        });
        _selectChildren(this.labelContainer).each(function () {
            if (this.id.endsWith("_parameterLabelInput")) {
                brease.callWidget(this.id, "setEnable", !disabled);
            }
        });
    };

    p.wake = function () {
        this.inputContainer.on("ValueChanged", this._bind('_valueChangedHandler'));
        this.inputContainer.on("SelectedIndexChanged", this._bind('_indexChangedHandler'));
        SuperClass.prototype.wake.apply(this, arguments);
    };

    p.suspend = function () {
        this.inputContainer.off();
        this.settings.parameters = undefined;
        _removeChildWidgets(this);
        SuperClass.prototype.suspend.apply(this, arguments);
    };

    p.dispose = function () {
        this.inputContainer.off();
        _removeChildWidgets(this);
        SuperClass.prototype.dispose.apply(this, arguments);
    };

    /**
     * @method setLabelStyle
     * Sets labelStyle
     * @param {StyleReference} labelStyle
     */
    p.setLabelStyle = function (labelStyle) {
        this.settings.labelStyle = labelStyle;
        if (brease.config.editMode) {
            _selectChildrenInEditor(this.labelContainer).each(function () {
                if (brease.uiController.callWidget(this.id, 'setStyle', labelStyle) === null) {
                    brease.uiController.addWidgetOption(this.id, 'setStyle', labelStyle);
                }
            });
        }
    };

    /**
     * @method getLabelStyle 
     * Returns labelStyle.
     * @return {StyleReference}
     */
    p.getLabelStyle = function () {
        return this.settings.labelStyle;
    };

    /**
     * @method setNumericInputStyle
     * Sets numericInputStyle
     * @param {StyleReference} numericInputStyle
     */
    p.setNumericInputStyle = function (numericInputStyle) {
        this.settings.numericInputStyle = numericInputStyle;
        if (brease.config.editMode) {
            _selectChildrenInEditor(this.inputContainer).each(function () {
                if (brease.uiController.callWidget(this.id, 'setStyle', numericInputStyle) === null) {
                    brease.uiController.addWidgetOption(this.id, 'setStyle', numericInputStyle);
                }
            });
        }
    };

    /**
     * @method getNumericInputStyle 
     * Returns numericInputStyle.
     * @return {StyleReference}
     */
    p.getNumericInputStyle = function () {
        return this.settings.numericInputStyle;
    };

    /**
     * @method setTextInputStyle
     * Sets textInputStyle
     * @param {StyleReference} textInputStyle
     */
    p.setTextinputStyle = function (textInputStyle) {
        this.settings.textInputStyle = textInputStyle;
        if (brease.config.editMode) {
            _selectChildrenInEditor(this.inputContainer).each(function () {
                if (brease.uiController.callWidget(this.id, 'setStyle', textInputStyle) === null) {
                    brease.uiController.addWidgetOption(this.id, 'setStyle', textInputStyle);
                }
            });
        }
    };

    /**
     * @method getTextinputStyle 
     * Returns textInputStyle.
     * @return {StyleReference}
     */
    p.getTextinputStyle = function () {
        return this.settings.textInputStyle;
    };

    /**
     * @method setTextOutputStyle
     * Sets textOutputStyle
     * @param {StyleReference} textOutputStyle
     */
    p.setTextOutputStyle = function (textOutputStyle) {
        this.settings.textOutputStyle = textOutputStyle;
        if (brease.config.editMode) {
            _selectChildrenInEditor(this.inputContainer).each(function () {
                if (brease.uiController.callWidget(this.id, 'setStyle', textOutputStyle) === null) {
                    brease.uiController.addWidgetOption(this.id, 'setStyle', textOutputStyle);
                }
            });
        }
    };

    /**
     * @method getTextOutputStyle 
     * Returns textOutputStyle.
     * @return {StyleReference}
     */
    p.getTextOutputStyle = function () {
        return this.settings.textOutputStyle;
    };

    /**
     * @method setDropDownBoxStyle
     * Sets dropDownBoxStyle
     * @param {StyleReference} dropDownBoxStyle
     */
    p.setDropDownBoxStyle = function (dropDownBoxStyle) {
        this.settings.dropDownBoxStyle = dropDownBoxStyle;
        if (brease.config.editMode) {
            _selectChildrenInEditor(this.inputContainer).each(function () {
                if (brease.uiController.callWidget(this.id, 'setStyle', dropDownBoxStyle) === null) {
                    brease.uiController.addWidgetOption(this.id, 'setStyle', dropDownBoxStyle);
                }
            });
        }
    };

    /**
     * @method getDropDownBoxStyle 
     * Returns textinputStyle.
     * @return {StyleReference}
     */
    p.getDropDownBoxStyle = function () {
        return this.settings.dropDownBoxStyle;
    };


    /**
     * @method setData 
     * @param {Array} data array containing all parameters as objects  name, type and value
     */
    p.setData = function (data) {
        this.settings.lastData = undefined;
        this.settings.allChildWidgetsReady = false;
        if (data) {
            this.settings.parameters = data;
            this.settings.parameters = $.extend(true, [], data);
            _update(this);
        } else { // if the data are not as excepted
            _removeChildWidgets(this);
        }
    };

    p.setUpdateSource = function (source) {
        this.settings.updateSource = source;
    };

    p.getUpdateSource = function () {
        return this.settings.updateSource;
    };

    /**
     * @method update
     * @return {Array} array containing all parameters as objects  name, type and value
     **/
    p.update = function (data, direction) {
        if (this.settings.allChildWidgetsReady) {
            this.setUpdateSource('internal');
            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                    this._changeValue(key, data[key], direction);
                }
            }
            this.setUpdateSource(undefined);

            if (this.settings.useTableWidgetForResultsList === true) {
                this.updateResultTableData(data);

                if (this.state === 2) {
                    this.drawResultTable();
                } else {
                    this.postponeDrawResultTable = true;
                }
            }


        } else {
            this.settings.lastData = data;
        }
    };

    p.updateResultTableData = function (data) {
        var widget = this,
            colLabel,
            colValue,
            outFormat,
            outOptions,
            unit,
            type,
            textOut,
            i, key,
            outVal,
            mms,
            numberFormat,
            resultIndex,
            valueWithLinearTransform,
            resultValue;

        if ((widget.tableIds === undefined) || (widget.tableIds.length < 3)) {
            return;
        }

        colLabel = brease.callWidget(widget.tableIds[1], "widget");
        colValue = brease.callWidget(widget.tableIds[2], "widget");

        if ((colLabel === null) || (colValue === null)) {
            return;
        }

        for (i = 0; i < widget.settings.parameters.length; i++) {
            if (widget.settings.parameters[i].isNoInput) {
                key = widget.settings.parameters[i].name;

                if (data && data.hasOwnProperty(key)) {
                    if (key === "NumResults") {
                        widget.settings.numResults = data[key]; // used by filtering for calculation of max index to list if filter index is 0 
                    }
                    unit = widget.settings.parameters[i].unit;
                    type = widget.settings.parameters[i].type;
                    resultIndex = widget.settings.parameters[i].ResultIndex;

                    outFormat = widget.resultTable.viewModel.get(key);
                    if (outFormat) {
                        outOptions = outFormat.options;
                        outVal = data[key];

                        if (outOptions.divisor) {
                            valueWithLinearTransform = (outOptions.multiplicand * ((outVal + outOptions.initialAddend) / outOptions.divisor)) + outOptions.finalAddend;
                            Number(valueWithLinearTransform.toString().replace(',', '.'));
                            outVal = valueWithLinearTransform;
                        }

                        switch (type) {
                            case "integer":
                            case "number":
                            case "double":
                                mms = brease.measurementSystem.getCurrentMeasurementSystem();
                                numberFormat = NumberFormat.getFormat(outOptions.format, mms);
                                textOut = brease.formatter.formatNumber(outVal, numberFormat, outOptions.useDigitGrouping);
                                break;

                            case "string":
                                textOut = data[key];
                                break;

                            case "selection":
                                textOut = getSelectedTextOfTheDataProvider(outOptions.dataProvider, outVal);
                                break;
                        }

                        if (unit) {
                            textOut = textOut + ' ' + widget.settings.parameters[i].unitSymbol;
                        }

                        resultValue = {
                            'label': key,
                            'value': textOut,
                            'index': resultIndex
                        };

                        widget.resultTable.resultValues.set(key, resultValue);
                    }
                }
            }
        }
    };

    function getSelectedTextOfTheDataProvider(dataProvider, value) {
        var selectedTextOfDataProvider, selectedElemOfDataProvider;


        selectedElemOfDataProvider = dataProvider.find(function (dataProvider) {
            return dataProvider.value === value.toString();
        });

        if (selectedElemOfDataProvider !== undefined) {
            selectedTextOfDataProvider = selectedElemOfDataProvider.text;
        } else {
            selectedTextOfDataProvider = "";
        }

        return selectedTextOfDataProvider;
    }

    /**
     * @method getData
     * @return {Array} array containing all parameters as objects  name, type and value
     */
    p.getData = function () {
        if (this.settings.allChildWidgetsReady)
        {
            return this.settings.parameters;
        }
        else
        {
            return this.generateShadowParameters();
        } 
        
    };

    //extracts properties of object this.settings.lastData  e.g
    //and updates the value properties inside the copy "shadowParameters" of the list this.settings.parameters 
    //e.g.   {"property1": value1, "property2": value2, ...}  -> [{"name": "property1", "value": value1, "type": "integer", ...}, {"name": "property2", "value": value2, "type": "double", ...}, .... ]
    p.generateShadowParameters = function () {
        var shadowParameters = [];
        for (var index in this.settings.parameters)
        {
            shadowParameters[index] = this.settings.parameters[index];
            if (this.settings.lastData.hasOwnProperty(shadowParameters[index].name)) 
            {
                shadowParameters[index].value = this.settings.lastData[shadowParameters[index].name];
            }
        }
        return shadowParameters;
        
    };



    //private function
    p._updateCurrentParameter = function (refName, newValue, direction) {
        var key;
        if (direction == "Output") {
            for (key in this.settings.parameters) {
                if (this.settings.parameters.hasOwnProperty(key) && (this.settings.parameters[key].isNoInput === true)) {
                    if (this.settings.parameters[key].name === refName) {
                        this.settings.parameters[key].value = newValue;
                    }
                }
            }
        } else {
            for (key in this.settings.parameters) {
                if (this.settings.parameters.hasOwnProperty(key) && (this.settings.parameters[key].isNoInput !== true)) {
                    if (this.settings.parameters[key].name === refName) {
                        this.settings.parameters[key].value = newValue;
                    }
                }
            }
        }
    };

    p._getValueWithLinearTransform = function (outputWidget, newValue) {
        var valueWithLinearTransform;
        valueWithLinearTransform = (outputWidget.settings.multiplicand * ((newValue + outputWidget.settings.initialAddend) / outputWidget.settings.divisor)) + outputWidget.settings.finalAddend;
        Number(valueWithLinearTransform.toString().replace(',', '.'));
        return valueWithLinearTransform;
    };

    p._changeValue = function (refName, newValue, direction) {
        var context = this;
        var outputWidget, valueWithLinearTransform;
        this.labelElements.forEach(function (value, index) {
            if (value.options.text === refName) {
                if (!direction || value.id.endsWith(direction)) {
                    if ((context.inputElements[index].className === 'widgets.brease.NumericInput') || (context.inputElements[index].className === 'widgets.brease.TextInput') || (context.inputElements[index].className === 'widgets.brease.TextOutput')) {
                        outputWidget = brease.callWidget(context.inputElements[index].id, "widget");
                        if (outputWidget.settings.divisor !== undefined) {
                            valueWithLinearTransform = context._getValueWithLinearTransform(outputWidget, newValue);
                            brease.uiController.callWidget(context.inputElements[index].id, 'setValue', valueWithLinearTransform);
                        } else {
                            brease.uiController.callWidget(context.inputElements[index].id, 'setValue', newValue);
                        }
                        if ((typeof newValue === 'string') && (newValue !== '')) {
                            context._adjustWidgetHeightToContent(context, index, outputWidget);
                        } else if ((typeof newValue === 'string') && (newValue === '')) {
                            context._defaultWidgetHeightToContent(context, index, outputWidget);
                        }
                    }
                    if (context.inputElements[index].className === 'widgets.brease.DropDownBox') {
                        brease.uiController.callWidget(context.inputElements[index].id, 'setSelectedValue', '' + newValue);
                    }

                    if (valueWithLinearTransform === undefined) {
                        context._updateCurrentParameter(refName, newValue, direction);
                    } else {
                        context._updateCurrentParameter(refName, valueWithLinearTransform, direction);
                    }
                    context.settings.parameters[index].value = newValue;
                }
            }
        });
    };

    p._defaultWidgetHeightToContent = function (context, index, outputWidget) {
        var heightToUpdate, labelWidget;
        labelWidget = brease.callWidget(context.labelElements[index].id, "widget");
        heightToUpdate = context.settings.itemHeight;
        outputWidget.el.height(heightToUpdate);
        labelWidget.el.height(heightToUpdate);
    };

    p._adjustWidgetHeightToContent = function (context, index, outputWidget) {
        var heightToUpdate, labelWidget, heightSpan;
        labelWidget = brease.callWidget(context.labelElements[index].id, "widget");
        if (outputWidget.settings.visible === false) {
            outputWidget.setVisible(true);
            heightSpan = outputWidget.el.find("span")[0].clientHeight;
            outputWidget.setVisible(false);
        } else {
            heightSpan = outputWidget.el.find("span")[0].clientHeight;
        }
        heightToUpdate = heightSpan + context.defaultSettings.padding;
        outputWidget.el.height(heightToUpdate);
        labelWidget.el.height(heightToUpdate);
    };

    p._indexChangedHandler = function (e) {
        var i = this.inputIds.findIndex(function (elem) {
            if (this === elem) {
                return true;
            }
            return false;
        }, e.target.id);

        if ((i !== -1) && (this.settings.parameters[i])) {
            if (isNaN(e.detail.selectedValue)) {
                this.settings.parameters[i].value = brease.callWidget(e.target.id, 'getSelectedValue');
            } else {
                this.settings.parameters[i].value = parseInt(brease.callWidget(e.target.id, 'getSelectedValue'));
            }
        } else {
            //console.log("ValueChanged for unkown Parameter");
        }
        if (this.getUpdateSource() !== 'internal') {
            $('#' + this.elem.id).trigger("parameterValueChanged", [this.settings.parameters[i].name, this.settings.parameters[i].access]);
        }
    };

    p._valueChangedHandler = function (e) {
        var i = this.inputIds.findIndex(function (elem) {
            if (this === elem) {
                return true;
            }
            return false;
        }, e.target.id);

        if ((i !== -1) && (this.settings.parameters[i])) {
            if (this.settings.parameters[i].linearTransform !== undefined) {
                this.settings.parameters[i].value = (this.settings.parameters[i].divisor * ((e.detail.value - this.settings.parameters[i].finalAddend) / this.settings.parameters[i].multiplicand)) - this.settings.parameters[i].initialAddend;
            } else {
                this.settings.parameters[i].value = e.detail.value;
            }
        } else {
            //console.log("ValueChanged for unkown Parameter");
        }

        // update the angle of the ocr orientation tool
        $('#' + this.elem.id).trigger("parameterValueChanged", [this.settings.parameters[i].name, this.settings.parameters[i].access]);
    };

    p._onAllChildWidgetsReady = function (widget) {

        widget.drawContainer.append(widget.labelContainer);
        widget.drawContainer.append(widget.inputContainer);

        widget.scrollContainer.append(widget.drawContainer);
        widget.scrollContainer.append(widget.tableContainer);
        widget.el.append(widget.scrollContainer);


        this.settings.allChildWidgetsReady = true;

        if (this.inputElements.length > 0) {
            brease.uiController.callWidget(widget.inputElements[0].id, 'widget');
            brease.uiController.callWidget(widget.labelElements[0].id, 'widget');
        }
        if (!widget.scroller) { // only first time
            _addScroller(widget);
            this.setVisibilityDependency(true);
        } else {
            widget._refreshScroller();
        }

        // should be set to false in addChildWidgets but there is a bug in the scroller
        for (var i = 0; i < widget.settings.parameters.length; i++) {
            if (widget.settings.parameters[i].isNoInput && (widget.tableIds != undefined)) { // Output and table exists
                continue;
            }

            var hasPermission = (widget.settings.editable && widget.settings.permissions.operate);
            if (widget.settings.parameters[i].isNoInput || !widget.settings.enable || !hasPermission) {

                brease.uiController.callWidget(widget.inputElements[i].id, "setEnable", false);
            }
        }
        widget.inputContainer.off(BreaseEvent.WIDGET_READY);
        widget.labelContainer.off(BreaseEvent.WIDGET_READY);
        widget.tableContainer.off(BreaseEvent.WIDGET_READY);

        if (this.settings.lastData) {
            widget.update(this.settings.lastData);
        }

        this._dispatchReady();

        if (this.postponeDrawResultTable) {
            this.postponeDrawResultTable = false;
            this.drawResultTable();
        }
        this._refreshScroller();
    };

    function _selectChildrenInEditor(container) {
        var children = container.find('.breaseWidget');
        if (children.length > 0) {
            children = children.first().parent().children('.breaseWidget');
        }
        return children;
    }

    function _selectChildren(container) {
        if (brease.config.editMode) {
            return _selectChildrenInEditor(container);
        }
        var children = $(container).find('[data-brease-widget]');
        if (children.length > 0) {
            children = children.first().parent().children('[data-brease-widget]');
        }
        return children;
    }

    function _update(widget) {
        _removeChildWidgets(widget);
        _addChildWidgets(widget);
    }

    function _removeChildWidgets(widget) {
        widget.scrollContainer.detach();
        brease.uiController.dispose(widget.labelContainer, false, function () { });
        brease.uiController.dispose(widget.inputContainer, false, function () { });
        brease.uiController.dispose(widget.tableContainer, false, function () { });
        brease.uiController.dispose(widget.scrollContainer, false, function () { });
    }

    p._getUnitSymbolAsync = function (unit, deferredUnitSymbol, widget, index) {
        brease.language.pipeAsyncUnitSymbol(unit, function (unitSymbol) {
            if (unitSymbol === undefined) {
                widget.settings.parameters[index].unitSymbol = '';
            } else {
                widget.settings.parameters[index].unitSymbol = unitSymbol;
            }
            deferredUnitSymbol[index].resolve();
        });
    };

    function _addChildWidgets(widget) {
        var deferredInitStates = [],
            deferredUnitSymbol = [],
            deferredInitStatesFlatList = [],
            deferredUnitSymbolFlatList = [],
            numericOptions,
            textOptions,
            dropDownOptions,
            factor,
            digits,
            i;

        widget.labelElements = [];
        widget.inputElements = [];
        widget.inputIds = [];
        widget.labelIds = [];
        if (widget.settings.useTableWidgetForResultsList === true) {
            widget.tableElements = [];
            widget.tableIds = [];
            widget.tableIds[0] = Utils.uniqueID(widget.elem.id) + '_ResultTable';
            widget.tableIds[1] = Utils.uniqueID(widget.elem.id) + '_ResultTable_ColLabel';
            widget.tableIds[2] = Utils.uniqueID(widget.elem.id) + '_ResultTable_ColValue';

            deferredInitStates[widget.tableIds[0]] = $.Deferred();
            deferredInitStates[widget.tableIds[0]].promise();
            deferredInitStatesFlatList.push(deferredInitStates[widget.tableIds[0]]);
            widget.resultTable.viewModel.clear();

            // Result Table Outputs
            for (i = 0; i < widget.settings.parameters.length; i++) {
                if (widget.settings.parameters[i].isNoInput) {
                    switch (widget.settings.parameters[i].type) {
                        case 'integer':
                        case 'number':
                        case 'double':
                            numericOptions = {
                                useDigitGrouping: false,
                                enable: true,
                                style: widget.settings.numericInputStyle,
                                value: widget.settings.parameters[i].value,
                                modelOrder: i,
                                multiplicand: 1,
                                unitAlign: 'right',
                                divisor: 1,
                                initialAddend: 0,
                                finalAddend: 0,
                            };
                            if (widget.settings.parameters[i].min) {
                                numericOptions.minValue = widget.settings.parameters[i].min;
                            }
                            if (widget.settings.parameters[i].max !== undefined) {
                                numericOptions.maxValue = widget.settings.parameters[i].max;
                            }
                            if (widget.settings.parameters[i].linearTransform !== undefined) {
                                numericOptions.multiplicand = widget.settings.parameters[i].multiplicand;
                                numericOptions.divisor = widget.settings.parameters[i].divisor;
                                numericOptions.initialAddend = widget.settings.parameters[i].initialAddend;
                                numericOptions.finalAddend = widget.settings.parameters[i].finalAddend;
                            }
                            if (widget.settings.parameters[i].unit) {
                                numericOptions.unit = {
                                    'metric': widget.settings.parameters[i].unit,
                                    'imperial': widget.settings.parameters[i].unit,
                                    'imperial-us': widget.settings.parameters[i].unit
                                };

                                widget.settings.parameters[i].unitSymbol = $.Deferred();
                                widget.settings.parameters[i].unitSymbol.promise();
                                deferredUnitSymbol[i] = widget.settings.parameters[i].unitSymbol;
                                deferredUnitSymbolFlatList.push(deferredUnitSymbol[i]);


                                widget._getUnitSymbolAsync(widget.settings.parameters[i].unit, deferredUnitSymbol, widget, i);
                            }
                            if (widget.settings.parameters[i].type === 'integer') {
                                factor = 0;
                                digits = 0;
                                if (widget.settings.parameters[i].linearTransform !== undefined) {
                                    factor = widget.settings.parameters[i].divisor / widget.settings.parameters[i].multiplicand;
                                    digits = Math.trunc(factor.toString().length) - 1;
                                }
                                numericOptions.format = {
                                    'metric': {
                                        decimalPlaces: digits,
                                        maximumIntegerDigits: 10
                                    },
                                    'imperial': {
                                        decimalPlaces: digits,
                                        maximumIntegerDigits: 10
                                    },
                                    'imperial-us': {
                                        decimalPlaces: digits,
                                        maximumIntegerDigits: 10
                                    }
                                };

                            }
                            if (widget.settings.parameters[i].type === 'double') {
                                numericOptions.format = {
                                    'metric': {
                                        decimalPlaces: 3,
                                        maximumIntegerDigits: 10
                                    },
                                    'imperial': {
                                        decimalPlaces: 3,
                                        maximumIntegerDigits: 10
                                    },
                                    'imperial-us': {
                                        decimalPlaces: 3,
                                        maximumIntegerDigits: 10
                                    }
                                };
                            }
                            widget.resultTable.viewModel.set(widget.settings.parameters[i].name, {
                                type: widget.settings.parameters[i].type,
                                options: numericOptions
                            });
                            break;
                        case 'selection':
                            dropDownOptions = {
                                enable: true,
                                style: widget.settings.dropDownBoxStyle,
                                dataProvider: widget.settings.parameters[i].dataProvider,
                                selectedValue: '' + widget.settings.parameters[i].value,
                                width: widget.settings.inputWidth,
                                listWidth: widget.settings.dropDownBoxListWidth,
                                listPosition: 'bottom',
                                multiLine: true,
                                fitHeight2Items: true,
                                breakWord: true,
                                wordWrap: true,
                                height: 0,
                                modelOrder: i
                            };
                            widget.resultTable.viewModel.set(widget.settings.parameters[i].name, {
                                type: widget.settings.parameters[i].type,
                                options: dropDownOptions
                            });
                            break;
                        case 'string':
                            textOptions = {
                                enable: widget.settings.enable,
                                value: widget.settings.parameters[i].value,
                                multiLine: true,
                                wordWrap: true,
                                breakWord: true,
                                modelOrder: i
                            };
                            textOptions.style = widget.settings.textOutputStyle;
                            if (textOptions.value === undefined) {
                                textOptions.value = '';
                            }
                            textOptions.enable = true;
                            widget.resultTable.viewModel.set(widget.settings.parameters[i].name, {
                                type: widget.settings.parameters[i].type,
                                options: textOptions
                            });
                            break;
                    }
                }
            }
        }

        for (i = 0; i < widget.settings.parameters.length; i++) {
            if (widget.settings.parameters[i].isNoInput) {
                if (widget.settings.useTableWidgetForResultsList === true) {
                    continue;
                } else {
                    widget.inputIds[i] = Utils.uniqueID(widget.elem.id) + '_Output';
                    widget.labelIds[i] = Utils.uniqueID(widget.elem.id) + '_parameterLabelOutput';
                }
            } else {
                widget.inputIds[i] = Utils.uniqueID(widget.elem.id) + '_Input';
                widget.labelIds[i] = Utils.uniqueID(widget.elem.id) + '_parameterLabelInput';
            }
            deferredInitStates[widget.inputIds[i]] = $.Deferred();
            deferredInitStates[widget.inputIds[i]].promise();
            deferredInitStatesFlatList.push(deferredInitStates[widget.inputIds[i]]);

            deferredInitStates[widget.labelIds[i]] = $.Deferred();
            deferredInitStates[widget.labelIds[i]].promise();
            deferredInitStatesFlatList.push(deferredInitStates[widget.labelIds[i]]);

            widget.labelElements[i] = {
                className: 'Label',
                id: widget.labelIds[i],
                options: {
                    text: widget.settings.parameters[i].name,
                    enable: widget.settings.enable,
                    permissions: widget.settings.permissions,
                    style: widget.settings.labelStyle,
                    modelOrder: i,
                    resultIndex: widget.settings.parameters[i].ResultIndex
                },
                HTMLAttributes: {
                    style: 'order:' + i + ';',
                    class: 'smartPanelParameterLabel'
                }
            };

            // enable should be set to this.settings.enable but there is a bug in the scroller
            // the widget will be disabled after initialitaion if necessary

            switch (widget.settings.parameters[i].type) {

                case 'integer':
                case 'number':
                case 'double':
                    numericOptions = {
                        useDigitGrouping: false,
                        enable: true,
                        style: widget.settings.numericInputStyle,
                        value: widget.settings.parameters[i].value,
                        modelOrder: i,
                        unitAlign: 'right',
                        multiplicand: 1,
                        divisor: 1,
                        initialAddend: 0,
                        finalAddend: 0,
                    };
                    if (widget.settings.parameters[i].min) {
                        numericOptions.minValue = widget.settings.parameters[i].min;
                    }
                    if (widget.settings.parameters[i].max !== undefined) {
                        numericOptions.maxValue = widget.settings.parameters[i].max;
                    }
                    if (widget.settings.parameters[i].linearTransform !== undefined) {
                        numericOptions.multiplicand = widget.settings.parameters[i].multiplicand;
                        numericOptions.divisor = widget.settings.parameters[i].divisor;
                        numericOptions.initialAddend = widget.settings.parameters[i].initialAddend;
                        numericOptions.finalAddend = widget.settings.parameters[i].finalAddend;
                    }
                    if (widget.settings.parameters[i].unit) {
                        numericOptions.unit = {
                            'metric': widget.settings.parameters[i].unit,
                            'imperial': widget.settings.parameters[i].unit,
                            'imperial-us': widget.settings.parameters[i].unit
                        };
                    }
                    if (widget.settings.parameters[i].type === 'integer') {
                        factor = 0;
                        digits = 0;
                        if (widget.settings.parameters[i].linearTransform !== undefined) {
                            factor = widget.settings.parameters[i].divisor / widget.settings.parameters[i].multiplicand;
                            digits = Math.trunc(factor.toString().length) - 1;
                        }
                        numericOptions.format = {
                            'metric': {
                                decimalPlaces: digits,
                                maximumIntegerDigits: 10
                            },
                            'imperial': {
                                decimalPlaces: digits,
                                maximumIntegerDigits: 10
                            },
                            'imperial-us': {
                                decimalPlaces: digits,
                                maximumIntegerDigits: 10
                            }
                        };

                    }
                    if (widget.settings.parameters[i].type === 'double') {
                        numericOptions.format = {
                            'metric': {
                                decimalPlaces: 3,
                                maximumIntegerDigits: 10
                            },
                            'imperial': {
                                decimalPlaces: 3,
                                maximumIntegerDigits: 10
                            },
                            'imperial-us': {
                                decimalPlaces: 3,
                                maximumIntegerDigits: 10
                            }
                        };

                    }
                    widget.inputElements[i] = {
                        className: 'NumericInput',
                        id: widget.inputIds[i],
                        options: numericOptions,
                        HTMLAttributes: {
                            style: 'order:' + i + ';',
                            class: 'smartPanelParameterNumericInput'
                        }
                    };
                    break;
                case 'selection':
                    dropDownOptions = {
                        enable: true,
                        style: widget.settings.dropDownBoxStyle,
                        dataProvider: widget.settings.parameters[i].dataProvider,
                        selectedValue: '' + widget.settings.parameters[i].value,
                        width: widget.settings.inputWidth,
                        listWidth: widget.settings.dropDownBoxListWidth,
                        listPosition: 'bottom',
                        multiLine: true,
                        fitHeight2Items: true,
                        breakWord: true,
                        wordWrap: true,
                        height: 0,
                        modelOrder: i
                    };

                    widget.inputElements[i] = {
                        className: 'DropDownBox',
                        id: widget.inputIds[i],
                        options: dropDownOptions,
                        HTMLAttributes: {
                            style: 'order:' + i + ';',
                            class: 'smartPanelParameterDropDownBox'
                        }
                    };
                    break;
                case 'string':
                    textOptions = {
                        enable: widget.settings.enable,
                        value: widget.settings.parameters[i].value,
                        multiLine: true,
                        wordWrap: true,
                        breakWord: true,
                        modelOrder: i
                    };
                    if (widget.settings.parameters[i].isNoInput) {
                        textOptions.style = widget.settings.textOutputStyle;
                        if (textOptions.value === undefined) {
                            textOptions.value = '';
                        }
                        widget.inputElements[i] = {
                            className: 'TextOutput',
                            id: widget.inputIds[i],
                            options: textOptions,
                            HTMLAttributes: {
                                style: 'order:' + i + ';',
                                class: 'smartPanelParameterFormTextOutput'
                            }
                        };
                        textOptions.enable = true;
                    } else {
                        textOptions.style = widget.settings.textInputStyle;
                        widget.inputElements[i] = {
                            className: 'TextInput',
                            id: widget.inputIds[i],
                            options: textOptions,
                            HTMLAttributes: {
                                style: 'order:' + i + ';',
                                class: 'smartPanelParameterTextInput'
                            }
                        };
                        textOptions.enable = true;
                    }
                    break;
            }
        }

        widget.labelContainer.on(BreaseEvent.WIDGET_READY, function (e) {
            if (deferredInitStates[e.target.id]) {
                deferredInitStates[e.target.id].resolve();
            }
        });
        widget.inputContainer.on(BreaseEvent.WIDGET_READY, function (e) {
            if (deferredInitStates[e.target.id]) {
                deferredInitStates[e.target.id].resolve();
            }
        });

        if (widget.settings.useTableWidgetForResultsList === true) {
            widget.tableContainer.on(BreaseEvent.WIDGET_READY, function (e) {
                if (deferredInitStates[e.target.id]) {
                    deferredInitStates[e.target.id].resolve();
                }
            });
        }

        $.when.apply($, deferredInitStatesFlatList, deferredUnitSymbolFlatList).then(function () {
            widget._onAllChildWidgetsReady(widget);
        });

        brease.uiController.createWidgets(widget.labelContainer, widget.labelElements, true);
        brease.uiController.createWidgets(widget.inputContainer, widget.inputElements, true);

        ////////////////////////
        // RESULT TABLE - TODO:
        if (widget.settings.useTableWidgetForResultsList === true) {
            _addTableElements(widget);
        }
    }

    function _addTableElements(widget) {
        var libPath = 'widgets.brease.',
            libPathItems = 'widgets/brease/',
            className = 'Table';

        widget.tableElements[0] = {
            className: libPath + className,
            id: widget.tableIds[0],
            options: {
                rowHeight: 35,
                columnWidth: 229,
                selection: false,
                showHeader: false,
                multiLine: true,
                wordWrap: true,
                showScrollbars: false,
                height: 800,
                // maxHeight: 800,  
                width: 600,
                offsetRow: 0,
                offsetColumn: 0,
                fontName: 'arial',
                style: 'TableSmartPanel',
                useTableStyling: false,
                refreshRate: 0,
                initRefreshRate: 0,
            },
            content: {

                //HTMLAttributes:
                html: generateHtmlContentString([{
                    id: widget.tableIds[1],
                    className: libPathItems + 'TableItem',
                    options: {
                        style: 'TableItemLeftStyle',
                        columnWidth: 226
                    }
                },
                {
                    id: widget.tableIds[2],
                    className: libPathItems + 'TableItem',
                    options: {
                        style: 'TableItemRightStyle',
                        columnWidth: 202
                    }
                }
                ])
            }
        };
        widget.tableContainer.on('TableReady', widget._bind(widget._addMockDataToTableItems));
        brease.uiController.createWidgets(widget.tableContainer, widget.tableElements, true);

    }

    p._addMockDataToTableItems = function () {
        brease.callWidget(this.tableIds[1], 'setValue', [0]);
        brease.callWidget(this.tableIds[2], 'setValue', [0]);
    };

    function _addScroller(widget) {
        widget.scroller = Scroller.addScrollbars('#' + widget.elem.id, {
            mouseWheel: true
        });
    }

    function _initEditor(widget) {
        widget.settings.parameters = [{
            name: 'Parameter 1',
            value: 2000
        },
        {
            name: 'Parameter 2',
            value: 4
        },
        {
            name: 'Parameter 3',
            value: 6
        }
        ];
    }


    function createHtmlForWidget(classname, options, id, htmlTagName) {
        if (htmlTagName === undefined) {
            htmlTagName = 'div';
        }
        var item = $('<' + htmlTagName + '/>').attr("id", id);
        item.attr("data-brease-options", JSON.stringify(options).replace(/"/g, "'"));
        item.attr("data-brease-widget", classname);

        //TBD wether content.html should do a recursive call and append to this div
        return item;
    }

    function generateHtmlContentString(contentConfiguration) {
        //TODO each widget call createHtmlForWidget
        //collect and generate html string out of them...
        var collection = "",
            domEl;

        for (var index in contentConfiguration) {
            var item = contentConfiguration[index];
            domEl = createHtmlForWidget(item.className, item.options, item.id, item.htmlTagName);
            collection = collection + domEl.prop('outerHTML');
        }

        return collection;
    }


    return VisibiliyDependency.decorate(WidgetClass, false);
});