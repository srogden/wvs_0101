/*
 * Controls to load vision application
 *
 */
/*global define*/
define([], function () {
    'use strict';

    function LoadVisionApplication(context) {
        this.parent = context;
    }

    var p = LoadVisionApplication.prototype,
        MAX_NUMBER_OF_RECONNECT_REQUESTS = 10;

    p._closeSocketAndSetIntervalToOpenWsAndUpdateHMI = function () {
        var context = this;
        if (this.parent.server.socket.readyState !== this.parent.server.socket.CONNECTING) {
            this.parent.socketHandling.closeSocket();
        }
        this.parent.server.intervalReconnect = setInterval(this._openSocketComunicationAndUpdateHMI, 1000, context);
    };

    p._openSocketComunicationAndUpdateHMI = function (context) {
        context.parent.server.numberOfReconnectRequests = context.parent.server.numberOfReconnectRequests + 1;

        switch (context.parent.server.socket.readyState) {
            case context.parent.server.socket.CLOSED:
                context.parent.socketHandling.openSocketCommunication();
                break;
            case context.parent.server.socket.OPEN:
                clearInterval(context.parent.server.intervalReconnect);
                context.setVisionApplicationIsLoading(false);
                break;
            default:
                if (context.parent.server.numberOfReconnectRequests >= MAX_NUMBER_OF_RECONNECT_REQUESTS) {
                    clearInterval(context.parent.server.intervalReconnect);
                    context.setVisionApplicationIsLoading(false);
                }
        }
    };

    p.setVisionApplicationIsLoading = function (visionApplicationIsLoading) {
        this.parent.hmiStatus.visionApplicationIsLoading = visionApplicationIsLoading;
        this.parent.setStatusReady();
        this.parent.setStatusErrorModel(false);
        this.parent.smartPanelModelList.setSelectedModelLock(false); 
        if (visionApplicationIsLoading === false){
            this.parent.widgetsHandling.setSelectVisionFunctionTab(); 
        }      
        this.parent.updateButtonStates();
    };

    p.getVisionApplicationIsLoading = function () {
        return this.parent.hmiStatus.visionApplicationIsLoading;
    };

    return LoadVisionApplication;

});