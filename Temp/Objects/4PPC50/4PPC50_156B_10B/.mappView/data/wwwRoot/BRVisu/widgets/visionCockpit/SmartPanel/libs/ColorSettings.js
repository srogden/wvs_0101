﻿
define(function() {
    'use strict';

    var ColorSettings = {
        // Tool settings
        modelRoiToolColors: {
            color_default: "#aa7700",
            color_selected: "#ffcc00",
            color_transparent: '#00000000',
            fillColor_selected: '#ffcc0099',
            fillColor_roi: "darkgreen",
            fillColor_roni: "darkred",
        },
        executeRoiToolColors: {
            color_default: "#279822",
            color_selected: "#00FF30",
            color_transparent: '#00000000',
            fillColor_selected: '#ffcc0099',
            fillColor_roi: "darkgreen",
            fillColor_roni: "darkred",
        },

        // Iconics settings
        modelRoiIconicsSettings: {
            color: {
                fillOpacity: 0.4,
                strokeOpacity: 0,
                strokeOpacitySelected: 1.0,
                strokeColor: "#F6A80D",
                fillColor: "#F6A80D",
                infoText: "#F6A80D",
                rgba: getRGBAValue("#F6A80D", 0.4)
            },
        },
    
        executeRoiIconicsSettings: {
            color: {
                fillOpacity: 0.2,
                strokeOpacity: 0, 
                strokeOpacitySelected: 1.0,
                strokeColor: "#896389",
                fillColor: "#785078",
                infoText: "#896389",
                rgba: getRGBAValue("#785078", 0.2)
            },
        },
    
        executeResultIconicsSettingsEdge: {
            color: {
                fillOpacity: 0.0,
                strokeOpacity: 0.6,
                strokeColor: "#0088ff",
                highlightColor: "#7fc3ff",
                fillColor: "#0088ff",
                infoText: "#00ffff",
                highlightRgba: getRGBAValue("#7fc3ff", 0.8),
                rgba: getRGBAValue("#0088ff", 0.6)
            },
        },
    
        executeResultIconicsSettingsRegion: {
            color: {
                fillOpacity: 0.6,
                strokeOpacity: 0.0,
                strokeColor: "#0088ff",
                highlightColor: "#7fc3ff",
                fillColor: "#0088ff",
                infoText: "#00ffff",
                highlightRgba: getRGBAValue("#7fc3ff", 0.8),
                rgba: getRGBAValue("#0088ff", 0.6)
            },
        },
    
        teachResultIconicsSettingsEdge: {
            color: {
                fillOpacity: 0.0,
                strokeOpacity: 0.6,
                strokeColor: "#2BC750",
                fillColor: "#2BC750",
                infoText: "#2BC750",
                rgba: getRGBAValue("#2BC750", 0.6)
            },
        },
    
        teachResultIconicsSettingsRegion: {
            color: {
                fillOpacity: 0.6,
                strokeOpacity: 0.0,
                strokeColor: "#2BC750",
                fillColor: "#2BC750",
                infoText: "#2BC750",
                rgba: getRGBAValue("#2BC750", 0.6)
            },
        },
        
    
        orientationToolColors: {
            color_default: "#00aaaa",
            color_selected: "#00ffff" 
        },
    };  

    
    function getRGBAValue(color, alpha) {
        var r = parseInt(color.substring(1,3),16),
         g = parseInt(color.substring(3,5),16),
         b = parseInt(color.substring(5,7),16),
         rgba = {
            r: r,
            g:g,
            b:b,
            a:Math.floor(alpha * 255)
        };
        return rgba;
    }
    

    return ColorSettings;
});
