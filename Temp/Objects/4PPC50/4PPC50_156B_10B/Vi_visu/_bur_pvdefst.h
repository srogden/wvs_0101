#ifndef __AS__TYPE_typVisionCommand
#define __AS__TYPE_typVisionCommand
typedef struct typVisionCommand
{	plcbit ImageTrigger;
	plcbit ImageTriggerReset;
	plcbit AutoSetupStartStop;
	plcbit VaSwitchApplication;
	plcbit SaveDiagData;
	plcbit ReadCameraInfo;
} typVisionCommand;
#endif

#ifndef __AS__TYPE_enumVisionFunction
#define __AS__TYPE_enumVisionFunction
typedef enum enumVisionFunction
{	enumFunctionNone = 0,
	enumBlob = 1,
	enumCoderReader = 2,
	enumMatch = 3,
	enumMeasurement = 4,
	enumOCR = 5,
	enumPixelCounter = 6,
} enumVisionFunction;
#endif

#ifndef __AS__TYPE_ViComponentInternalMappLinkType
#define __AS__TYPE_ViComponentInternalMappLinkType
typedef struct ViComponentInternalMappLinkType
{	unsigned long Internal[2];
} ViComponentInternalMappLinkType;
#endif

#ifndef __AS__TYPE_ViComponentType
#define __AS__TYPE_ViComponentType
typedef struct ViComponentType
{	struct ViComponentInternalCameraIfType(* CameraType);
	ViComponentInternalMappLinkType MappLinkInternal;
} ViComponentType;
#endif

#ifndef __AS__TYPE_typVisionConfig
#define __AS__TYPE_typVisionConfig
typedef struct typVisionConfig
{	enumVisionFunction VisionFunction;
	unsigned char PowerlinkNode;
	unsigned long DataStructure;
	ViComponentType ComponentLink;
	unsigned short ResolutionWidth_X;
	unsigned short ResolutionHeight_Y;
} typVisionConfig;
#endif

#ifndef __AS__TYPE_typVisionData
#define __AS__TYPE_typVisionData
typedef struct typVisionData
{	plcbit Enable;
	unsigned char MaxItemCnt;
	signed long NettimeDelay;
	unsigned short Timeout;
	unsigned char Gain;
	unsigned short Focus;
	unsigned long Exposure;
	unsigned char FlashColor;
	unsigned char FlashSegment;
	unsigned char LedColor;
	plcbit IRFilter;
	signed long OffsetROIX;
	signed long OffsetROIY;
	signed long OffsetROIRotationCenterX;
	signed long OffsetROIRotationCenterY;
	signed short OffsetROIOrientation;
	plcbit ChromaticLock;
	unsigned char Alignment;
	unsigned char AutoSetupGain;
	unsigned short AutoSetupFocus;
	unsigned long AutoSetupExposure;
	unsigned char AcquisitionAcceptedCnt;
	unsigned char AcquisitionCompletedCnt;
	unsigned char AcquisitionFailedCnt;
	unsigned char LightWarningCnt;
	unsigned short ImageProcessingError;
	plcbit ImageProcessingActive;
	signed long ImageNettime;
	unsigned short ProcessingTimeCamera;
	unsigned short ProcessingTimeFunction;
	unsigned char ResultCnt;
	plcbit CockpitIsActive;
	unsigned long Status;
	plcstring StatusText[31];
	plcbit Active;
} typVisionData;
#endif

#ifndef __AS__TYPE_typVisionFunction
#define __AS__TYPE_typVisionFunction
typedef struct typVisionFunction
{	plcstring ApplicationName[41];
	signed long Status;
} typVisionFunction;
#endif

#ifndef __AS__TYPE_typVisionDiag
#define __AS__TYPE_typVisionDiag
typedef struct typVisionDiag
{	plcstring DiagName[81];
	signed long Status;
} typVisionDiag;
#endif

#ifndef __AS__TYPE_typVisionOptics
#define __AS__TYPE_typVisionOptics
typedef struct typVisionOptics
{	plcstring OrderNr[41];
	plcbit Binning;
	signed short ResolutionX;
	signed short ResolutionY;
	unsigned char Lens;
	unsigned char Sensor;
	plcbit ValidDistance;
	double Distance_mm;
	double DistanceLens_mm;
	double HyperFocalDistance_mm;
	double PixelSize_um;
	double PixelSizeBinning_um;
	double FocalLength_mm;
	double MinObjectDist_mm;
	double MaxObjectDist_mm;
	double Aperture;
	double CircleOfConfusion_mm;
	double DepthOfFieldNearPos_mm;
	double DepthOfFieldFarPos_mm;
	double DepthOfField_mm;
	double FieldOfView_X_mm;
	double FieldOfView_Y_mm;
	double Resolution_mmPerPx;
} typVisionOptics;
#endif

#ifndef __AS__TYPE_typVisionHW
#define __AS__TYPE_typVisionHW
typedef struct typVisionHW
{	plcbit Connected;
	plcbit Ready;
	plcbit DigitalInput01;
	plcbit DigitalOutput01;
	plcbit DigitalOutputStatus01;
	plcbit DigitalOutput02;
	plcbit DigitalOutputStatus02;
	unsigned long SerialNumber;
	unsigned long ID;
	unsigned long Variant;
	unsigned long Firmware;
	plcbit Undervoltage;
	signed char Temperature;
} typVisionHW;
#endif

#ifndef __AS__TYPE_typVisionMain
#define __AS__TYPE_typVisionMain
typedef struct typVisionMain
{	typVisionCommand CMD;
	typVisionConfig CFG;
	typVisionData DAT;
	typVisionFunction FCT;
	typVisionDiag DIA;
	typVisionOptics OPT;
	typVisionHW HW;
} typVisionMain;
#endif

#ifndef __AS__TYPE_typLightCommand
#define __AS__TYPE_typLightCommand
typedef struct typLightCommand
{	plcbit FlashTrigger;
	plcbit FlashTriggerReset;
} typLightCommand;
#endif

#ifndef __AS__TYPE_enumLightType
#define __AS__TYPE_enumLightType
typedef enum enumLightType
{	enumLightNone = 0,
	enumBacklight = 1,
	enumLightbar = 2,
} enumLightType;
#endif

#ifndef __AS__TYPE_typLightConfig
#define __AS__TYPE_typLightConfig
typedef struct typLightConfig
{	enumLightType LightType;
	unsigned char PowerlinkNode;
} typLightConfig;
#endif

#ifndef __AS__TYPE_typLightData
#define __AS__TYPE_typLightData
typedef struct typLightData
{	unsigned char Enable;
	unsigned char FlashColor;
	unsigned short FlashSegment;
	unsigned long Exposure;
	unsigned short SetAngle;
	unsigned short Timeout;
	signed long NettimeDelay;
	unsigned char FlashAcceptedCnt;
	unsigned char FlashCompletedCnt;
	unsigned char FlashFailedCnt;
	unsigned long Status;
} typLightData;
#endif

#ifndef __AS__TYPE_typLightHW
#define __AS__TYPE_typLightHW
typedef struct typLightHW
{	plcbit Connected;
	plcbit Ready;
	unsigned long SerialNumber;
	unsigned long ID;
	unsigned long Variant;
	unsigned long Firmware;
	signed char TemperatureLED;
	signed char TemperatureControllerBoard;
	unsigned char WarningCnt;
} typLightHW;
#endif

#ifndef __AS__TYPE_typLightMain
#define __AS__TYPE_typLightMain
typedef struct typLightMain
{	typLightCommand CMD;
	typLightConfig CFG;
	typLightData DAT;
	typLightHW HW;
} typLightMain;
#endif

#ifndef __AS__TYPE_typCodeReaderMain
#define __AS__TYPE_typCodeReaderMain
typedef struct typCodeReaderMain
{	unsigned char CodeType;
	plcbit GradingEnable;
	plcbit RobustnessEnable;
	unsigned char ParameterMode;
	unsigned char ParameterOptimization;
	plcstring BarcodeText[10][101];
	unsigned char BarcodeType[10];
	signed long PositionX[10];
	signed long PositionY[10];
	signed short Orientation[10];
	signed char Grading[10];
	unsigned char EnhancedGrading[23];
} typCodeReaderMain;
#endif

#ifndef __AS__TYPE_typBlobMain
#define __AS__TYPE_typBlobMain
typedef struct typBlobMain
{	plcbit RegionalFeature;
	plcbit EnhancedBlobInformation;
	unsigned char ModelNumber[10];
	plcbit Clipped[10];
	unsigned long Area[10];
	signed long PositionX[10];
	signed long PositionY[10];
	signed short Orientation[10];
	unsigned char MeanGrayValue[10];
	unsigned long Length[10];
	unsigned long Width[10];
} typBlobMain;
#endif

#ifndef __AS__TYPE_typMatchMain
#define __AS__TYPE_typMatchMain
typedef struct typMatchMain
{	unsigned char MinScore;
	unsigned char MaxOverlap;
	unsigned char ModelNumber[10];
	unsigned char Score[10];
	signed long PositionX[10];
	signed long PositionY[10];
	signed short Orientation[10];
	unsigned char Scale[10];
	signed long RotCenterX[10];
	signed long RotCenterY[10];
} typMatchMain;
#endif

#ifndef __AS__TYPE_typOCRMain
#define __AS__TYPE_typOCRMain
typedef struct typOCRMain
{	unsigned char ParameterMode;
	plcbit GradingEnable;
	plcstring Text[10][51];
	unsigned char Grading[10];
	signed long PositionX[10];
	signed long PositionY[10];
	signed short Orientation[10];
} typOCRMain;
#endif

#ifndef __AS__TYPE_typPixelMain
#define __AS__TYPE_typPixelMain
typedef struct typPixelMain
{	plcbit EnhancedPixelCounterInformation;
	unsigned char ModelNumber[10];
	unsigned long NumPixels[10];
	unsigned char MinGray[10];
	unsigned char MaxGray[10];
	unsigned short MeanGray[10];
	unsigned short DeviationGray[10];
	unsigned char MedianGray[10];
	unsigned long ModelArea[10];
	unsigned short NumConnectedComponents[10];
	signed long PositionX[10];
	signed long PositionY[10];
} typPixelMain;
#endif

#ifndef __AS__TYPE_typMTMain
#define __AS__TYPE_typMTMain
typedef struct typMTMain
{	plcbit UseResultAsXY;
	unsigned char ParameterMode;
	signed long Result[10];
} typMTMain;
#endif

#ifndef __AS__TYPE_MO_List_typ
#define __AS__TYPE_MO_List_typ
typedef struct MO_List_typ
{	signed char name[14];
	unsigned char grp;
	unsigned char type;
	unsigned char state;
	unsigned char reserve;
	unsigned long adress;
	unsigned long memtype;
} MO_List_typ;
#endif

#ifndef __AS__TYPE_ViComponentInternalCameraIfType
#define __AS__TYPE_ViComponentInternalCameraIfType
typedef struct ViComponentInternalCameraIfType
{	plcdword Vtable;
} ViComponentInternalCameraIfType;
#endif

struct VaGetList
{	unsigned char VaApplicationCnt;
	plcstring VaApplicationNames[20][41];
	unsigned short Status;
	unsigned char State;
	unsigned char STATE_WAIT;
	unsigned char STATE_NEXT_OBJ;
	unsigned char STATE_VA_NAME;
	unsigned short i;
	unsigned short j;
	unsigned short PrevIndex;
	MO_List_typ ModuleList;
	unsigned short ModuleStatus;
	plcbit Enable;
	plcbit zzEdge00000;
};
void VaGetList(struct VaGetList* inst);
struct TON
{	plctime PT;
	plctime ET;
	plctime StartTime;
	unsigned long Restart;
	plcbit IN;
	plcbit Q;
	plcbit M;
};
_BUR_PUBLIC void TON(struct TON* inst);
_BUR_LOCAL struct typVisionMain(* VisionSensor);
_BUR_LOCAL struct typLightMain(* VisionLight);
_BUR_LOCAL struct typMatchMain(* Match);
_BUR_LOCAL struct typCodeReaderMain(* CodeReader);
_BUR_LOCAL struct typMTMain(* MT);
_BUR_LOCAL struct typOCRMain(* OCR);
_BUR_LOCAL struct typBlobMain(* Blob);
_BUR_LOCAL struct typPixelMain(* Pixel);
_BUR_LOCAL typMatchMain tmpMatch;
_BUR_LOCAL typCodeReaderMain tmpCodeReader;
_BUR_LOCAL typMTMain tmpMT;
_BUR_LOCAL typOCRMain tmpOCR;
_BUR_LOCAL typBlobMain tmpBlob;
_BUR_LOCAL typPixelMain tmpPixel;
_BUR_LOCAL struct VaGetList VaGetList_01;
_BUR_LOCAL plcbit visAutoArchiv;
_BUR_LOCAL plcbit visSensorEnableCommand;
_BUR_LOCAL plcbit visEnableSetup;
_BUR_LOCAL plcbit visEnableRepetitive;
_BUR_LOCAL plcbit visImageTrigger;
_BUR_LOCAL plcstring visVisionApplicationList[20][81];
_BUR_LOCAL plcstring visSensorFlashColor[11];
_BUR_LOCAL plcstring visSensorFlashColorOld[11];
_BUR_LOCAL unsigned char visSelectedFlashSegment[4];
_BUR_LOCAL unsigned char visSelectedFlashSegmentOld[4];
_BUR_LOCAL plcbit visRepetitiveMode;
_BUR_LOCAL struct TON RepetitiveModeTimer;
_BUR_LOCAL plcbit DisableAutoArchiv;
_BUR_LOCAL plcbit InitialSearchSensor;
_BUR_LOCAL plcbit AutoSetupRunning[6];
_BUR_LOCAL plcbit InitialSearchLight;
_BUR_LOCAL plcbit visLightsTrigger;
_BUR_LOCAL plcbit visLightsReset;
_BUR_LOCAL plcbit visLightsEnableCommand;
_BUR_LOCAL plcstring visLightsFlashColor[11];
_BUR_LOCAL plcstring visLightsFlashColorOld[11];
_BUR_LOCAL plcbit visEnableLightBarValues;
_BUR_LOCAL plcstring visSelectedCodeTypeDetected[10][81];
_BUR_LOCAL plcstring visSelectedCodeType[81];
_BUR_LOCAL plcstring visSelectedCodeTypeOld[81];
_BUR_LOCAL plcstring visSelectedParameterMode[81];
_BUR_LOCAL plcstring visSelectedParameterModeOld[81];
_BUR_LOCAL plcstring visCodeTypePresentDropDownValues[70][81];
_BUR_LOCAL unsigned char visTableNo[10];
_BUR_LOCAL plcstring tmpStr1[1001];
_BUR_LOCAL unsigned short i;
_BUR_LOCAL plcstring visCameraStatus[31];
_BUR_LOCAL unsigned short ERR_VA_LIST_NUM;
_BUR_LOCAL plcbit visVaListRefresh;
_GLOBAL unsigned short MAX_NUM_CAMS;
_GLOBAL unsigned short MAX_NUM_RESULTS;
_GLOBAL unsigned short MAX_IDX_VA_LIST;
_GLOBAL unsigned char MAX_NUM_CODETYPES;
