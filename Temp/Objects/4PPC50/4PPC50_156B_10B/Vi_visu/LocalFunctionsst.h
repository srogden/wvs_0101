#ifndef __AS__TYPE_
#define __AS__TYPE_
typedef struct {
	unsigned char bit0  : 1;
	unsigned char bit1  : 1;
	unsigned char bit2  : 1;
	unsigned char bit3  : 1;
	unsigned char bit4  : 1;
	unsigned char bit5  : 1;
	unsigned char bit6  : 1;
	unsigned char bit7  : 1;
} _1byte_bit_field_;

typedef struct {
	unsigned short bit0  : 1;
	unsigned short bit1  : 1;
	unsigned short bit2  : 1;
	unsigned short bit3  : 1;
	unsigned short bit4  : 1;
	unsigned short bit5  : 1;
	unsigned short bit6  : 1;
	unsigned short bit7  : 1;
	unsigned short bit8  : 1;
	unsigned short bit9  : 1;
	unsigned short bit10 : 1;
	unsigned short bit11 : 1;
	unsigned short bit12 : 1;
	unsigned short bit13 : 1;
	unsigned short bit14 : 1;
	unsigned short bit15 : 1;
} _2byte_bit_field_;

typedef struct {
	unsigned long bit0  : 1;
	unsigned long bit1  : 1;
	unsigned long bit2  : 1;
	unsigned long bit3  : 1;
	unsigned long bit4  : 1;
	unsigned long bit5  : 1;
	unsigned long bit6  : 1;
	unsigned long bit7  : 1;
	unsigned long bit8  : 1;
	unsigned long bit9  : 1;
	unsigned long bit10 : 1;
	unsigned long bit11 : 1;
	unsigned long bit12 : 1;
	unsigned long bit13 : 1;
	unsigned long bit14 : 1;
	unsigned long bit15 : 1;
	unsigned long bit16 : 1;
	unsigned long bit17 : 1;
	unsigned long bit18 : 1;
	unsigned long bit19 : 1;
	unsigned long bit20 : 1;
	unsigned long bit21 : 1;
	unsigned long bit22 : 1;
	unsigned long bit23 : 1;
	unsigned long bit24 : 1;
	unsigned long bit25 : 1;
	unsigned long bit26 : 1;
	unsigned long bit27 : 1;
	unsigned long bit28 : 1;
	unsigned long bit29 : 1;
	unsigned long bit30 : 1;
	unsigned long bit31 : 1;
} _4byte_bit_field_;
#endif

#ifndef __AS__TYPE_MO_List_typ
#define __AS__TYPE_MO_List_typ
typedef struct MO_List_typ
{	signed char name[14];
	unsigned char grp;
	unsigned char type;
	unsigned char state;
	unsigned char reserve;
	unsigned long adress;
	unsigned long memtype;
} MO_List_typ;
#endif

struct VaGetList
{	unsigned char VaApplicationCnt;
	plcstring VaApplicationNames[20][41];
	unsigned short Status;
	unsigned char State;
	unsigned char STATE_WAIT;
	unsigned char STATE_NEXT_OBJ;
	unsigned char STATE_VA_NAME;
	unsigned short i;
	unsigned short j;
	unsigned short PrevIndex;
	MO_List_typ ModuleList;
	unsigned short ModuleStatus;
	plcbit Enable;
	plcbit zzEdge00000;
};
void VaGetList(struct VaGetList* inst);
plcbit CrosshairDetailsText(unsigned long strTarget, unsigned long strText, float fValue);
plcbit String2DataProvider(unsigned long SourceString, unsigned long TargetString);
_BUR_PUBLIC unsigned short brsftoa(float value, unsigned long pString);
_BUR_PUBLIC unsigned long brsmemset(unsigned long pDest, unsigned char value, unsigned long length);
_BUR_PUBLIC unsigned long brsmemcpy(unsigned long pDest, unsigned long pSrc, unsigned long length);
_BUR_PUBLIC signed long brsmemcmp(unsigned long pMem1, unsigned long pMem2, unsigned long length);
_BUR_PUBLIC unsigned long brsstrcat(unsigned long pDest, unsigned long pSrc);
_BUR_PUBLIC unsigned long brsstrcpy(unsigned long pDest, unsigned long pSrc);
_BUR_PUBLIC unsigned short MO_list(unsigned short prev_index, unsigned long index, unsigned long MO_List_struct);
_BUR_PUBLIC signed long CheckBounds(signed long index, signed long lower, signed long upper);
_BUR_LOCAL unsigned short ERR_VA_LIST_NUM;
_GLOBAL unsigned short MAX_IDX_VA_LIST;
_GLOBAL unsigned short ERR_OK;
_GLOBAL unsigned short ERR_FUB_ENABLE_FALSE;
_GLOBAL unsigned short ERR_FUB_BUSY;
_GLOBAL unsigned short ERR_BUR_NOENTRY;
