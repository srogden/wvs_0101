/*
 * Controls the usability of the widgets from the visualization 
 *
 */
/*global define*/
define([], function () {
    'use strict';

    function WidgetsHandling(context) {
        var statusToSet,
            updateInterval = 100,
            that = this;

        this.parent = context;
        this.triggerUpdate = false;
        this.widgetRefIds = this.defineWidgetReferences();
        this.widgetStatus = this.defineWidgetEnableStatus();

        this.updateTimer = setInterval(function () {
            if (that.triggerUpdate === true) {
                statusToSet = that.determineWidgetsStatus(that.parent.settings.statusReady);
                that.updateWidgets(statusToSet);
                that.triggerUpdate = false;
            }
        }, updateInterval);
    }

    var p = WidgetsHandling.prototype;

    p.dispose = function () {
        clearInterval(this.updateTimer);
    };

    p.defineWidgetReferences = function () {
        var widgetRefIds = {
            menuPanel: {
                imageCapture: this.callExternalWidget(this.parent.settings.refIdButtonImageCapture),
                repetitiveCapture: this.callExternalWidget(this.parent.settings.refIdButtonRepetitiveMode),
                execute: this.callExternalWidget(this.parent.settings.refIdButtonExecute),
                edit: this.callExternalWidget(this.parent.settings.refIdButtonEditTool),
            },
            menuTool: {
                toolList: this.callExternalWidget(this.parent.settings.refIdDropDownBoxToolList),
                commandList: this.callExternalWidget(this.parent.settings.refIdDropDownBoxRoiCommands),
                manipulation: this.callExternalWidget(this.parent.settings.refIdDropDownBoxForTheRoiManipulation)
            },
            menuModels: {
                modelType: this.callExternalWidget(this.parent.settings.refIdDropDownBoxModelType),
                teach: this.callExternalWidget(this.parent.settings.refIdButtonTeach),
                marker: this.callExternalWidget(this.parent.settings.refIdEditMarker),
                delete: this.callExternalWidget(this.parent.settings.refIdButtonDelete),
                modelParametersList: this.callExternalWidget(this.parent.settings.refIdSmartPanelParameterFormModelParameters),
            },
            tabVisionFunctionModels: {
                visionFunctionTab: this.callExternalWidget(this.parent.settings.refIdTabItemVisionFunction),
                modelsTab: this.callExternalWidget(this.parent.settings.refIdTabItemVisionModels),
                visionFuntionAndModelsTabControl: this.callExternalWidget(this.parent.settings.refIdVfModelsTabControl),
            },
            menuApplication: {
                visionComponentSelector: this.parent.callWidgetOnContent(this.parent.settings.refIdDropDownBoxComponentList, this.parent.settings.headerContentId),
                visionFunctionSelector: this.parent.callWidgetOnContent(this.parent.settings.refIdDropDownBoxVisionApplicationNavigation, this.parent.settings.headerContentId),
                logoutButton: this.parent.callWidgetOnContent(this.parent.settings.refIdLogoutButton, this.parent.settings.headerContentId),
            },
            menuGlobalModels: {
                addMeasurement: this.callExternalWidget(this.parent.settings.refIdDropDownBoxAddMeasurement),
                removeMeasurement: this.callExternalWidget(this.parent.settings.refIdRemoveMeasurement),
                saveGlobalModel: this.callExternalWidget(this.parent.settings.refIdButtonSaveGlobalModel),
                measurementDefinition: this.callExternalWidget(this.parent.settings.refIdSmartPanelGlobalModelListMeasurementDef),
            },
            visionApplication: {
                load: this.callExternalWidget(this.parent.settings.refIdButtonLoadVisionApplication),
                save: this.callExternalWidget(this.parent.settings.refIdButtonSaveVisionApplication),
            },
        };
        return widgetRefIds;
    };

    p.defineWidgetEnableStatus = function () {
        var enableStatus = {
            menuPanel: {
                imageCapture: false,
                repetitiveCapture: false,
                execute: false,
                edit: false,
            },
            menuTool: {
                toolList: false,
                commandList: false,
                manipulation: false,
            },
            menuModels: {
                modelType: false,
                teach: false,
                marker: {
                    enableStatus: true,
                    visibleStatus: true
                },
                delete: false,
                modelParametersList: true,
            },
            tabVisionFunctionModels: {
                visionFunctionTab: {
                    enableStatus: true,
                    visibleStatus: true
                },
                modelsTab: {
                    enableStatus: true,
                    visibleStatus: true
                }
            },
            menuApplication: {
                visionComponentSelector: true,
                visionFunctionSelector: true,
                logoutButton: true,
            },
            menuGlobalModels: {
                addMeasurement: false,
                removeMeasurement: false,
                saveGlobalModel: false,
                measurementDefinition: true,
            },
            visionApplication: {
                load: false,
                save: false,
            },
        };
        return enableStatus;
    };

    p.updateWidgetsStatus = function () {
        this.triggerUpdate = true;
    };

    p.determineWidgetsStatus = function (statusReady) {
        var status = this.widgetStatus;

        if (statusReady === false) {
            status.menuPanel.imageCapture = false;
            status.menuPanel.repetitiveCapture = this.determineEnableStatusOfButtonRepetitiveCapture();
            status.menuPanel.execute = false;
            status.menuPanel.edit = false;

            status.menuTool.toolList = false;
            status.menuTool.commandList = false;
            status.menuTool.manipulation = false;

            status.menuModels.modelType = false;
            status.menuModels.teach = false;
            status.menuModels.marker.enableStatus = false;
            status.menuModels.marker.visibleStatus = this.determineVisibleStatusOfButtonMarker();
            status.menuModels.delete = false;
            status.menuModels.modelParametersList = false;

            status.menuGlobalModels.addMeasurement = false;
            status.menuGlobalModels.removeMeasurement = false;
            status.menuGlobalModels.saveGlobalModel = false;
            status.menuGlobalModels.measurementDefinition = false;

            status.menuApplication.visionComponentSelector = this.determineEnableStatusOfVisionComponentSelector();
            status.menuApplication.visionFunctionSelector = this.determineEnableStatusOfVisionFunctionSelector();
            status.menuApplication.logoutButton = this.determineEnableStatusOfLogoutButton();

            status.tabVisionFunctionModels.visionFunctionTab.visibleStatus = this.determineVisibleStatusOfTabPageVisionFunction();
            status.tabVisionFunctionModels.visionFunctionTab.enableStatus = false;

            status.tabVisionFunctionModels.modelsTab.visibleStatus = this.determineVisibleStatusOfTabPageModels();
            status.tabVisionFunctionModels.modelsTab.enableStatus = false;

            status.visionApplication.load = false;
            status.visionApplication.save = false;
        } else {
            status.menuPanel.imageCapture = this.determineEnableStatusOfButtonImageCapture();
            status.menuPanel.repetitiveCapture = this.determineEnableStatusOfButtonRepetitiveCapture();
            status.menuPanel.execute = this.determineEnableStatusOfButtonExecuteVisionFunction();
            status.menuPanel.edit = this.determineEnableStatusOfButtonEdit();

            status.menuTool.toolList = this.determineEnableStatusOfDropdownTools();
            status.menuTool.commandList = this.determineEnableStatusOfToolCommands();
            status.menuTool.manipulation = this.determineEnableStatusOfToolManipulation();

            status.menuModels.modelType = this.determineEnableStatusOfDropdownModelType();
            status.menuModels.teach = this.determineEnableStatusOfButtonTeach();
            status.menuModels.modelParametersList = this.determineEnableStatusOfSmartPanelParameterFormModelParameters();
            status.menuModels.marker.enableStatus = this.determineEnableStatusOfButtonMarker();
            status.menuModels.marker.visibleStatus = this.determineVisibleStatusOfButtonMarker();

            status.menuModels.delete = this.determineEnableStatusOfButtonDelete();

            status.menuGlobalModels.addMeasurement = this.determineEnableStatusOfButtonAddMeasurement();
            status.menuGlobalModels.removeMeasurement = this.determineEnableStatusOfButtonRemoveMeasurement();
            status.menuGlobalModels.saveGlobalModel = this.determineEnableStatusOfButtonSaveGlobalModel();
            status.menuGlobalModels.measurementDefinition = this.determineEnableStatusOfSmartPanelGlobalModelMeasurementDef();

            status.menuApplication.visionComponentSelector = this.determineEnableStatusOfVisionComponentSelector();
            status.menuApplication.visionFunctionSelector = this.determineEnableStatusOfVisionFunctionSelector();
            status.menuApplication.logoutButton = this.determineEnableStatusOfLogoutButton();

            status.tabVisionFunctionModels.visionFunctionTab.visibleStatus = this.determineVisibleStatusOfTabPageVisionFunction();
            status.tabVisionFunctionModels.visionFunctionTab.enableStatus = this.determineEnableStatusOfTabPageVisionFunction();
            status.tabVisionFunctionModels.modelsTab.visibleStatus = this.determineVisibleStatusOfTabPageModels();

            status.visionApplication.load = this.determineEnableStatusOfButtonApplicationLoad();
            status.visionApplication.save = this.determineEnableStatusOfButtonApplicationSave();
        }
        return status;
    };

    p.determineEnableStatusOfSmartPanelParameterFormModelParameters = function () {
        var status = true;
        if (this.isEditingMeasurement() === true) {
            status = false;
        }
        if (this.parent.orientationArrow) {
            this.parent.orientationArrow.changeEditMode(status);
        }
        return status;
    };

    p.determineEnableStatusOfSmartPanelGlobalModelMeasurementDef = function () {
        var status = true;
        if (this.isEditingModels() === true) {
            status = false;
        }
        return status;
    };

    p.isEditingMeasurement = function () {
        var status = false;
        if (this.parent.vfCapabilities.has("GlobalModel")) {
            if (this.parent.smartPanelGlobalModelList.getPersistedStatus() === false) {
                status = true;
            }
        }
        return status;
    };

    p.isEditingModels = function () {
        var model, status = false, selectedModelId;
        if (this.parent.vfCapabilities.has("Models")) {
            model = this.parent.getSelectedModel();
            selectedModelId = this.parent.getSelectedModelId();
            if (model) {
                if ((model.modelRoi && (model.modelRoi.getDirtyFlag() === true)) || (this.parent.getDirtyFlagOfChangedSubmitParameter() === true) || (this.parent.getDirtyFlagOfChangedTeachParameter() === true) || (this.widgetRefIds.menuPanel.edit.getValue() === true) ) { // the dirty fla has to be set which indicates the user changed something
                    status = true;
                }
            } else if (selectedModelId === 0) {
                status = true;
            }
        }
        return status;
    };

    p.updateWidgets = function (status) {
        var refIds = this.widgetRefIds;

        // panel menu
        refIds.menuPanel.imageCapture.setEnable(status.menuPanel.imageCapture);
        refIds.menuPanel.repetitiveCapture.setEnable(status.menuPanel.repetitiveCapture);
        refIds.menuPanel.execute.setEnable(status.menuPanel.execute);
        refIds.menuPanel.edit.setEnable(status.menuPanel.edit);

        // tools menu
        refIds.menuTool.toolList.setEnable(status.menuTool.toolList);
        refIds.menuTool.commandList.setEnable(status.menuTool.commandList);
        refIds.menuTool.manipulation.setEnable(status.menuTool.manipulation);

        // models menu
        refIds.menuModels.modelType.setEnable(status.menuModels.modelType);
        refIds.menuModels.delete.setEnable(status.menuModels.delete);
        refIds.menuModels.teach.setEnable(status.menuModels.teach);
        refIds.menuModels.modelParametersList.setEnable(status.menuModels.modelParametersList);
        refIds.menuModels.marker.setEnable(status.menuModels.marker.enableStatus);
        refIds.menuModels.marker.setVisible(status.menuModels.marker.visibleStatus);

        // global models menu
        refIds.menuGlobalModels.addMeasurement.setEnable(status.menuGlobalModels.addMeasurement);
        refIds.menuGlobalModels.removeMeasurement.setEnable(status.menuGlobalModels.removeMeasurement);
        refIds.menuGlobalModels.saveGlobalModel.setEnable(status.menuGlobalModels.saveGlobalModel);
        refIds.menuGlobalModels.measurementDefinition.setEnable(status.menuGlobalModels.measurementDefinition);

        // Application menu
        refIds.menuApplication.visionComponentSelector.setEnable(status.menuApplication.visionComponentSelector);
        refIds.menuApplication.visionFunctionSelector.setEnable(status.menuApplication.visionFunctionSelector);
        refIds.menuApplication.logoutButton.setEnable(status.menuApplication.logoutButton);

        // Application Load/Save
        refIds.visionApplication.load.setEnable(status.visionApplication.load);
        refIds.visionApplication.save.setEnable(status.visionApplication.save);

        // Vision Function tab
        refIds.tabVisionFunctionModels.visionFunctionTab.setVisible(status.tabVisionFunctionModels.visionFunctionTab.visibleStatus);
        refIds.tabVisionFunctionModels.visionFunctionTab.setEnable(status.tabVisionFunctionModels.visionFunctionTab.enableStatus);

        // Edit Models tab
        refIds.tabVisionFunctionModels.modelsTab.setVisible(status.tabVisionFunctionModels.modelsTab.visibleStatus);
    };

    p.setEnableDropDownBoxRoiTools = function () {
        var enable;
        if (this.parent.isSelectedVfModelTabVisionFunctionPage()) {
            enable = true;
        } else {
            enable = false;
        }
        this.tools.list.setEnable(enable);
    };

    p.setSelectVisionFunctionTab = function () {
        this.parent.setSelectedVfModelTabWithVisionFunctionPage();
        this.widgetRefIds.tabVisionFunctionModels.visionFuntionAndModelsTabControl.setSelectedIndex(0);
    };

    p.setDefaultValueOfRoiCommands = function () {
        this.widgetRefIds.menuTool.commandList.setSelectedValue('commands');
    };

    p.setRoiToolsDataProvider = function () {
        this.widgetRefIds.menuTool.toolList.setDataProvider(this.parent.settings.dataProviderRoiTools);
    };

    p.setRoiCommandsDataProvider = function () {
        this.widgetRefIds.menuTool.commandList.setDataProvider(this.parent.settings.dataProviderRoiCommands);
    };

    p.setRoiManipulationDataProvider = function () {
        this.widgetRefIds.menuTool.manipulation.setDataProvider(this.parent.settings.dataProviderRoiManipulation);
    };

    p.setDefaultValueOfRoiManipulation = function () {
        this.widgetRefIds.menuTool.manipulation.setSelectedValue('manipulation');
    };

    p.setDefaultValueOfRoiTools = function () {
        this.widgetRefIds.menuTool.toolList.setSelectedValue('tools');
    };

    p.setDefaultValueOfModelType = function () {
        this.widgetRefIds.menuModels.modelType.setSelectedValue(this.parent.settings.defaultModelType.value);
    };

    p.setDataProviderOfModelType = function () {
        this.widgetRefIds.menuModels.modelType.setDataProvider(this.parent.settings.dataProviderModelTypes);
    };

    p.setDataProviderOfOperationForGlobalModel = function (dataProvider) {
        this.widgetRefIds.menuGlobalModels.addMeasurement.setDataProvider(dataProvider);
    };

    p.setDefaultValueOfOperationForGlobalModel = function () {
        this.widgetRefIds.menuGlobalModels.addMeasurement.setSelectedValue(this.parent.settings.defaultOperation.value);
    };

    p.getSelectedValueOfOperation = function () {
        return this.widgetRefIds.menuGlobalModels.addMeasurement.getSelectedValue();
    };

    p.callExternalWidget = function (widgetId) {
        if (this.parent.isUnitTestEnviroment() === false) {
            return brease.callWidget(this.parent.settings.parentContentId + '_' + widgetId, "widget");
        } else {
            return window.fakeWidgetCaller();
        }
    };

    p.setEditButtonValueToOn = function () {
        this.setEditButtonValue(1);
    };

    p.setEditButtonValueToOff = function () {
        this.setEditButtonValue(0);
    };

    p.setEditButtonValue = function (value) {
        var modelType;

        switch (this.parent.settings.selectedVfModelTab) {
            case "VisionFunctionPage":
                this.widgetRefIds.menuPanel.edit.setValue(value);
                break;
            case "EditModelsPage":
                modelType = this.parent.getModelTypeOfSelectedModel();
                if (modelType) {
                    if (this.parent.supportsModelRoi(modelType) === true) {
                        this.widgetRefIds.menuPanel.edit.setValue(value);
                    }

                    if (this.parent.supportsMarker(modelType) === true) {
                        this.widgetRefIds.menuModels.marker.setValue(value);
                    }
                } else if (this.parent.getSelectedModelId() === undefined) {
                    this.widgetRefIds.menuPanel.edit.setValue(0);
                    this.widgetRefIds.menuModels.marker.setValue(0);
                }
                break;
        }
    };

    //////////////////////////////////////////////////////////////////////////////////////////////
    // Functions to determine enable status of widgets - return true if widget may set to enabled
    //////////////////////////////////////////////////////////////////////////////////////////////

    p.determineEnableStatusOfButtonEdit = function () {
        var status = false,
            modelType;

        switch (this.parent.settings.selectedVfModelTab) {
            case "VisionFunctionPage":
                if (this.parent.vfCapabilities.has("ExecutionRoi") === true) {
                    status = true;
                }
                break;
            case "EditModelsPage":
                modelType = this.parent.getModelTypeOfSelectedModel();
                if ((this.parent.supportsModelRoi(modelType) === true) && (this.isEditingMeasurement() === false)) {
                    status = true;
                }
                break;
        }
        return status;
    };

    p.determineEnableStatusOfButtonImageCapture = function () {
        var status = true;

        if (this.parent.getInitialComplete() === false){
            status = false;
        }
        if ((this.parent.getVisionApplicationIsSaving() === true) || (this.parent.loadVisionApplicationHandling.getVisionApplicationIsLoading() === true)){
            status = false;
        }

        if (this.parent.getStatusResponseReciv() === false||(this.parent.getRepetitiveMode() === true)){ 
            status = false;
        }

        if (this.parent.applicationContext.includes("edit_")){
            status = false;
        }
        return status;
    };

    p.determineEnableStatusOfButtonRepetitiveCapture = function () {
        var status = true;

        if (this.parent.applicationContext.includes("edit_")) {
            status = false;
        } else if (!this.parent.getRepetitiveMode()) {
            status = this.parent.getStatusResponseReciv() &&
                this.parent.getInitialComplete() &&
                !this.parent.loadVisionApplicationHandling.getVisionApplicationIsLoading() &&
                !this.parent.getVisionApplicationIsSaving() &&
                !this.parent.getImageIsLoading();
        }
        return status;
    };

    p.determineEnableStatusOfButtonExecuteVisionFunction = function () {
        var status = true;
        if (this.parent.applicationContext.includes("edit_")) {
            status = false;
        }

        if (this.parent.vfCapabilities.has("Models") === true) {
            if (this.parent.settings.vfModels.size === 0)
                status = false;
            if (this.parent.vfCapabilities.has("GlobalModel")) {
                if (this.parent.smartPanelGlobalModelList.getPersistedStatus() === false) {
                    status = false;
                }
            }
        }
        return status;
    };

    p.determineEnableStatusOfDropdownTools = function () {
        var setEnable = false,
            toolInstances,
            modelType;

        switch (this.parent.applicationContext) {
            case "edit_execution_roi":
                setEnable = true;
                break;

            case "edit_model_roi":
                modelType = this.parent.getModelTypeOfSelectedModel();
                if (modelType) {
                    toolInstances = this.parent.getToolInstancesOfModelType(modelType);
                    switch (toolInstances) {
                        case "single":
                            if (this.parent.teachTools.length === 0) {
                                setEnable = true;
                            }
                            break;

                        case "multi":
                            setEnable = true;
                            break;
                    }
                }
                break;
        }
        return setEnable;
    };

    p.determineEnableStatusOfToolCommands = function () {
        var setEnable = false,
            selectedCount = 0,
            model, modelid;

        switch (this.parent.applicationContext) {
            case "edit_execution_roi":
                this.parent.executionTools.forEach(function (tool) {
                    if (tool.isSelected() === true) {
                        selectedCount++;
                    }
                });

                if (selectedCount > 1) {
                    setEnable = true;
                    return setEnable;
                }
                break;

            case "edit_model_roi":
                model = this.parent.getSelectedModel();
                if (model) {
                    this.parent.teachTools.forEach(function (tool) {
                        if (tool.isSelected() === true) {
                            selectedCount++;
                        }
                    });
                    if (selectedCount > 1) {
                        setEnable = true;
                        return setEnable;
                    }
                } else {
                    modelid = this.parent.getSelectedModelId();
                    if (modelid === 0) {
                        this.parent.teachTools.forEach(function (tool) {
                            if (tool.isSelected() === true) {
                                selectedCount++;
                            }
                        });
                        if (selectedCount > 1) {
                            setEnable = true;
                            return setEnable;
                        }
                    }
                }
                break;
        }
        return setEnable;
    };

    p.determineEnableStatusOfToolManipulation = function () {
        // Copy/Paste/Delete
        var enableStatus = false,
            modelType,
            toolInstances;

        switch (this.parent.applicationContext) {
            case "edit_execution_roi":
                if (this.parent.executionRoi && (this.parent.executionRoi.isSelected() === true)) {
                    enableStatus = true;
                    return enableStatus;
                }

                if ((this.parent.executionTools.length > 0) || (this.parent.toolsClipboard.length > 0)) {
                    enableStatus = true;
                    return enableStatus;
                }
                break;

            case "edit_model_roi":

                modelType = this.parent.getModelTypeOfSelectedModel();
                if (modelType) {
                    toolInstances = this.parent.getToolInstancesOfModelType(modelType);
                    if (toolInstances === "single") {
                        if (this.parent.teachTools.length > 0) {
                            enableStatus = true;
                        }
                    } else {
                        this.parent.settings.vfModels.forEach(function (model) {
                            if (model && model.modelRoi && model.modelRoi.isSelected() === true) {
                                enableStatus = true;
                                return enableStatus;
                            }
                        });
                        if ((this.parent.teachTools.length > 0) || (this.parent.toolsClipboard.length > 0)) {
                            enableStatus = true;
                            return enableStatus;
                        }
                    }
                    break;
                }
        }
        return enableStatus;
    };

    p.determineEnableStatusOfButtonToolsCopy = function () {
        var enableStatus = false;

        if (!this.parent.applicationContext.includes("edit_")) {
            return enableStatus;
        }
        switch (this.parent.applicationContext) {
            case "edit_execution_roi":
                this.parent.executionTools.forEach(function (tool) {
                    if (tool.isSelected() === true) {
                        enableStatus = true;
                        return enableStatus;
                    }
                });
                break;

            case "edit_model_roi": {
                this.parent.teachTools.forEach(function (tool) {
                    if (tool.isSelected() === true) {
                        enableStatus = true;
                        return enableStatus;
                    }
                });
            }
                break;
        }
        return enableStatus;
    };

    p.determineVisibleStatusOfTabPageModels = function () {
        var status = true;
        if (this.parent.applicationContext === "edit_execution_roi") {
            status = false;
            return status;
        }
        return status;
    };

    p.determineVisibleStatusOfTabPageVisionFunction = function () {
        var status = true;
        if (this.parent.applicationContext === "edit_model_roi") {
            status = false;
            return status;
        }
        return status;
    };

    p.determineEnableStatusOfTabPageVisionFunction = function () {
        var status = true;
        if (this.parent.applicationContext === "edit_execution_roi") {
            status = false;
            return status;
        }
        return status;
    };

    p.determineEnableStatusOfDropdownModelType = function () {
        var enableStatus = false,
            selectedModelId,
            features;

        if (this.parent.applicationContext.includes("edit_")) {
            return enableStatus;
        }

        if (this.parent.vpDataProvider.isVisionProgramLoaded() === false) {
            // Vision Program not initialized yet - deactivate all model buttons
            return enableStatus;
        }
        features = this.parent.getVisionFunctionFeatures();
        if (features === undefined) {
            // the initialize phase is still ongoing - deactivate all model buttons
            return enableStatus;
        }

        if (features.includes("teachable")) {
            // vf does support models
            selectedModelId = this.parent.getSelectedModelId();
            if (selectedModelId === undefined) {
                // no Item in model list is selected - activate AddModel button
                if (this.parent.vfCapabilities.has("GlobalModel")) {
                    if (this.parent.smartPanelGlobalModelList.getPersistedStatus() === true) {
                        enableStatus = true;
                    }
                } else {
                    enableStatus = true;
                }
                return enableStatus;
            } else if ((selectedModelId > 0) && (this.parent.getStatusErrorModel() === false)) {
                // an  already persisted model is selected - activate AddModel button
                if (this.parent.vfCapabilities.has("GlobalModel")) {
                    if (this.parent.smartPanelGlobalModelList.getPersistedStatus() === true) {
                        enableStatus = true;
                    }
                } else {
                    enableStatus = true;
                }
                return enableStatus;
            }
        }
        return enableStatus;
    };

    p.determineEnableStatusOfButtonTeach = function () {
        var enableStatus = false,
            selectedModelId,
            features,
            modelType,
            isTeachable,
            isRequired = false,
            isOptional = false,
            model;

        if (this.parent.vpDataProvider.isVisionProgramLoaded() === false) {
            // Vision Program not initialized yet - deactivate all model buttons
            return enableStatus;
        }
        features = this.parent.getVisionFunctionFeatures();
        if (features === undefined) {
            // the initialize phase is still ongoing - deactivate all model buttons
            return enableStatus;
        }

        if (features.includes("teachable")) {
            // vf does support models
            selectedModelId = this.parent.getSelectedModelId();
            if (selectedModelId === undefined) {
                // no Item in model list is selected - deactivate teach
                return enableStatus;
            }

            if (selectedModelId === 0) { // its a new model
                modelType = this.parent.settings.selectedModelType;

                isRequired = (this.parent.requiresMarker(modelType) === true) || (this.parent.requiresModelRoi(modelType) === true);
                if (isRequired) {
                    if (this.parent.teachTools.length > 0) {
                        enableStatus = true;
                    }
                } else {
                    isOptional = !isRequired && (this.parent.supportsMarker(modelType) === true) || (this.parent.supportsModelRoi(modelType) === true);

                    if (isOptional) {
                        enableStatus = true;
                    }
                }
                return enableStatus;
            }

            model = this.parent.getSelectedModel();
            if (model) {
                isTeachable = this.parent.isModelTypeTeachable(model.modelType);
                if (isTeachable === false) {
                    // the selected modelType is not teachable
                    return enableStatus;
                }

                if ((model.modelRoi && (model.modelRoi.getDirtyFlag() === true)) || (this.parent.getDirtyFlagOfChangedSubmitParameter() === true) || (this.parent.getDirtyFlagOfChangedTeachParameter() === true)) { // the dirty fla has to be set which indicates the user changed something
                    if (this.isEditingMeasurement() === false) {
                        enableStatus = true;
                    } else {
                        enableStatus = false;
                    }
                    return enableStatus;
                }

                isRequired = (this.parent.requiresMarker(model.modelType) === true) || (this.parent.requiresModelRoi(model.modelType) === true);
                if (isRequired) {
                    if (this.parent.teachTools.length > 0) {
                        enableStatus = true;
                    }
                } else {
                    isOptional = (!isRequired && (this.parent.supportsMarker(model.modelType) === true) ||
                        (this.parent.supportsModelRoi(model.modelType) === true));
                    if (isOptional) {
                        if ((this.parent.getDirtyFlagOfChangedSubmitParameter() === true) ||
                            (this.parent.getDirtyFlagOfChangedTeachParameter() === true)) {
                            enableStatus = true;
                        }
                    }
                }
                return enableStatus;
            }
        }
        return enableStatus;
    };

    p.determineEnableStatusOfButtonMarker = function () {
        var status = false,
            modelType = this.parent.getModelTypeOfSelectedModel();

        if (this.parent.supportsMarker(modelType) === true) {
            status = true;
        }
        return status;
    };

    p.determineVisibleStatusOfButtonMarker = function () {
        var status = this.parent.doesAnyModelTypeSupportMarker();
        return status;
    };

    p.determineEnableStatusOfButtonDelete = function () {
        var enableStatus = true,
            selectedModelId;

        if (this.parent.vpDataProvider.isVisionProgramLoaded() === false) {
            // Vision Program not initialized yet - deactivate all model buttons
            return enableStatus;
        }
        // vf does support models
        selectedModelId = this.parent.getSelectedModelId();
        if (this.parent.smartPanelGlobalModelList.getPersistedStatus() === false) {
            enableStatus = false;
            return enableStatus;
        }
        if (selectedModelId == undefined) {
            // no Item in model list is selected - deactivate delete model button
            enableStatus = false;
            return enableStatus;
        }
        return enableStatus;
    };

    p.determineEnableStatusOfVisionComponentSelector = function () {
        var status = true;

        if (this.parent.applicationContext.includes("edit_")) {
            status = false;
        }else{
            status = !this.parent.getStatusErrorModel();
        }
        return status;
    };

    p.determineEnableStatusOfVisionFunctionSelector = function () {
        var status = true;

        if (this.parent.applicationContext.includes("edit_")) {
            status = false;
        }else{
            status = !this.parent.getStatusErrorModel();
        }
        return status;
    };

    p.determineEnableStatusOfLogoutButton = function () {
        var status = true;

        if (this.parent.applicationContext.includes("edit_")) {
            status = false;
        }else{
            status = !this.parent.getStatusErrorModel();
        }
        return status;
    };

    p.determineEnableStatusOfButtonAddMeasurement = function () {
        var status = false;

        if ((this.parent.vfCapabilities.has("GlobalModel") === true) && (this.parent.smartPanelModelList.getMinAndMaxValueIdOfPersistedModels().max > 0)) {
            if ((this.parent.smartPanelGlobalModelList.getModelData().length >= this.parent.settings.numResultsMax) || ((this.parent.smartPanelGlobalModelList.getPersistedStatus() === false))) {
                status = false;
            } else {
                if (this.isEditingModels() === false) {
                    status = true;
                }
            }
        }
        return status;


    };

    p.determineEnableStatusOfButtonRemoveMeasurement = function () {
        var status = false;

        if (this.parent.vfCapabilities.has("GlobalModel") === true) {
            if (this.parent.getSelectedGlobalModelId() && (this.isEditingModels() === false)) {
                status = true;
            }
        }
        return status;
    };

    p.determineEnableStatusOfButtonSaveGlobalModel = function () {
        var status = false;

        if (this.parent.vfCapabilities.has("GlobalModel") === true) {
            if (this.parent.getSelectedGlobalModelId() && (this.isEditingModels() === false)) {
                status = true;
            }
        }
        return status;
    };

    p.determineEnableStatusOfButtonApplicationLoad = function () {
        var status = true;
        if (this.parent.applicationContext.includes("edit_")) {
            status = false;
        }
        if (this.parent.vfCapabilities.has("GlobalModel")) {
            if (this.parent.smartPanelGlobalModelList.getPersistedStatus() === false) {
                status = false;
            }
        }
        return status;
    };

    p.determineEnableStatusOfButtonApplicationSave = function () {
        var status = true;
        if (this.parent.applicationContext.includes("edit_")) {
            status = false;
        }
        if (this.parent.vfCapabilities.has("GlobalModel")) {
            if (this.parent.smartPanelGlobalModelList.getPersistedStatus() === false) {
                status = false;
            }
        }
        return status;
    };

    return WidgetsHandling;
});