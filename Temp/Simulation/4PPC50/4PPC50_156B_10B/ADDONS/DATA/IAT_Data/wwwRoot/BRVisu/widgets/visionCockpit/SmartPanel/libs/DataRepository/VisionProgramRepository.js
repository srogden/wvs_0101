﻿/*global define */
define([], function () {
    'use strict';

    function VisionProgramRepository(parent) {
        this._parent = parent;
        this.visionProgramState = {};
        this.visionApplicationConfiguration = {};
        this.executionResult = {};
        this.visionFunctionParameter = {userDefinedParameterMode:0};
        this.defaultValueOfOutputProcessVariables = undefined;
        return this;
    }

    var p = VisionProgramRepository.prototype;

    p.setVisionProgramState = function (state) {
        this.visionProgramState = state;
    };

    p.getVisionProgramState = function () {
        return this.visionProgramState;
    };

    p.setDefaultValueOfOutputProcessVariables = function (value) {
        this.defaultValueOfOutputProcessVariables = value;
    };

    p.getDefaultValueOfOutputProcessVariables = function () {
        return this.defaultValueOfOutputProcessVariables; 
    };

    p.getVisionFunctionParameter = function () {
        return this.visionFunctionParameter;
    };  

    p.setVisionApplicationConfiguration = function (configuration) {
        this.visionApplicationConfiguration = configuration;
    };

    p.getVisionApplicationConfiguration = function () {
        return this.visionApplicationConfiguration;
    };

    p.setExecutionResult = function (executionResultMessage) {
        this.executionResult = executionResultMessage;
    };

    p.getExecutionResult = function () {
        return this.executionResult;
    };

    p.dispose = function () {};

    return VisionProgramRepository;
});