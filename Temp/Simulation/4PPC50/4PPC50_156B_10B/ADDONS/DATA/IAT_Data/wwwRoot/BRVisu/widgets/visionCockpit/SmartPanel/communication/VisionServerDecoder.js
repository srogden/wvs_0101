/*global define*/
define([
        'widgets/visionCockpit/SmartPanel/libs/pixelcloud/PixelCloud',
        'widgets/visionCockpit/SmartPanel/libs/xldcloud/XldCloud'
    ],
    function () {
        'use strict';

        function VisionServerDecoder(parentContext) {
            this._parentContext = parentContext;
            this.settings = this._parentContext.settings;
        }

        var p = VisionServerDecoder.prototype;

        p.dispose = function () {};

        p.parseMessageIfNecessary = function (context, data) {
            var message;

            if (typeof data === "string") {
                try {
                    message = JSON.parse(data);
                } catch (parseEvent) {
                    context._consoleEventsSocketInput("Parse error, incoming message was not valid JSON");
                    return {};
                }
            } else {
                message = data;
            }
            return message;
        };


        p.handleMessage = function (e) {
            var message, paramDataFlat;
            this._parentContext.setStatusResponseReciv(true);
            if (e.data instanceof Blob) {
                this._parentContext.updateButtonStates();
                this._handleImageData(e.data);
                return;
            }

            message = this.parseMessageIfNecessary(this._parentContext, e.data);

            if (message != undefined && message.param !== undefined) {
                if (Array.isArray(message.param)) {
                    // There might be a selection wich one to show in future, currently this array contains only 1 entry so we provide this data direclty to the parameter forms
                    paramDataFlat = message.param[0];
                } else {
                    paramDataFlat = message.param;
                }
            }

            if (message != undefined && message.meta) {

                if ((message.meta.errorString !== undefined || message.meta.error !== undefined)) {
                    this._parentContext._consoleEventsSocketInput("Error number= " + message.meta.error + ", Error ID= " + message.meta.errorId + ", Message: " + message.meta.errorString);
                }

                if (message.meta.active === 1) {
                    if (message.meta.command === 'open_image') {
                        this._handleCommandoAckForOpenImage(message);
                    }
                } else {
                    switch (message.meta.command) {

                        case 'get_state':
                            if (this._handleGetState(e, message)) {
                                this._parentContext.onWebSocketCommandReceived();
                                return;
                            }
                            break;

                        case 'init_vp': //offline case
                            this._handleInitVisionProgram(message);
                            break;

                        case 'teach_model':
                            this._handleTeachModel(e, message);
                            break;

                        case 'modify_model':
                            this._handleModifyModel(e, message);
                            break;

                        case 'remove_model':
                            this._handleRemoveModel(e, message);
                            break;

                        case 'get_models':
                            this._handleGetModels(e, message);
                            break;

                        case 'get_global_models':
                            this._handleGetGlobalModels(e, message);
                            this._parentContext.setInitialComplete(true);
                            break;

                        case 'set_global_model':
                            this._handleSetGlobalModel(e, message);
                            break;

                        case 'ROI':
                            this._handleRoi(e, message);
                            if (!this._parentContext.paramHandler.getModelParametersInitialized() && (!this._parentContext.vfCapabilities.has("Models"))) {
                                this._parentContext.setInitialComplete(true);
                            }
                            break;

                        case 'execute':
                            this._handleExecute(e, message);
                            break;

                        case 'open_image':
                            this.handleOpenImage(e, message);
                            break;

                        case 'get_pixel_values':
                            this._handleGetPixelValues(message);
                            break;
                        default:
                            this._parentContext._consoleEventsSocketInput("Unknown message received " + JSON.stringify(message));
                            break;
                    }

                    this._parentContext.onWebSocketCommandReceived();
                }
            } else {
                this._parentContext._consoleEventsSocketInput("Unknown message received " + JSON.stringify(message));
            }

            // Different responses might provide param updates for the parameter forms, so this is independend of any of the commands
            if (paramDataFlat !== undefined) {
                if ((message.param[1] !== undefined) && (message.param[1].outputs !== undefined)) {
                    var outputs0 = message.param[0].outputs;
                    var outputs1 = message.param[1].outputs;
                    paramDataFlat.outputs = outputs0.concat(outputs1);
                }
                this._parentContext.paramHandler.handleMessage(paramDataFlat);
                this._handleSpecificParameter(paramDataFlat);
            }
        };

        p._handleSpecificParameter = function (paramIn) {
            if (this._parentContext.getCapabilityOfExecutionRoi("OrientationTool") && paramIn.params !== undefined) {
                // search the array for the property 'SearchAngle'
                var index;
                if (paramIn.params) {
                    for (index = 0; index < paramIn.params.length; index++) {
                        if (paramIn.params[index].SearchAngle !== undefined) {
                            var angleDeg = paramIn.params[index].SearchAngle / this._parentContext.defaultSettings.orientationToolSearchAngleToDegRatio;
                            this._parentContext._setOrientationToolAngle(angleDeg);
                        }
                    }
                }
            }
        };

        p._handleRemoveModel = function (e, message) {
            var modelNumber,
                errorOutOfSync = 48137;
            if (message.meta) {
                if (message.meta.errorId === errorOutOfSync) {
                    this._parentContext.onModelRemoved(modelNumber);
                }
            }

            if (message.param) {
                modelNumber = message.param.model_number;

                if (modelNumber > 0) {
                    this._parentContext.onModelRemoved(modelNumber);
                }
            }

            if (this._parentContext.vfCapabilities.has("GlobalModel")) {
                this._sendCommand("getGlobalModels");
                this._parentContext.globalModelHandling.updateValueRangesAndVisibilityofGlobalModel();
                this._parentContext.smartPanelModelList.setSelectedModelLock(false);    
            }
        };

        p._handleGetModels = function (e, message) {
            this._parentContext._consoleEventsSocketInput(e.data);
            if (message.param && message.param.length > 0) {
                this._handleGetModelsMessage(message.param);
            }
            this._sendCommand("getGlobalModels");
        };

        p._handleGetPixelValues = function (message) {
            if (!message.meta.error) {
                this._parentContext.pipette.color = message.param.v[0].toString();
                this._parentContext.pipette.click();
            }
        };

        p._handleGetGlobalModels = function (e, message) {
            this._parentContext._consoleEventsSocketInput(e.data);
            if (message.param && message.param.length > 0) {
                this._parentContext.onGetGlobalModelsSucceeded(message);
            }
        };

        p._handleSetGlobalModel = function (e, message) {
            this._parentContext._consoleEventsSocketInput(e.data);

            if ((message.meta) && (message.meta.error === 1)) {
                this._parentContext.onSetGlobalModelListError(message);
                this._parentContext.smartPanelGlobalModelList.setSelectedModelLock(true);
            } else if ((message.param && message.param.length > 0)) {
                this._parentContext.globalModelHandling.updateGlobalModelList(message.param);
                this._parentContext.globalModelHandling.onGlobalModelListSelectionChanged(undefined);
                this._parentContext.smartPanelGlobalModelList.setSelectedModelLock(false);
                this._parentContext.globalModelHandling.setVisibleOfGlobalModelInputs();
                this._parentContext.updateButtonStates();
            }
        };

        p._handleRoi = function (e, message) {
            this._parentContext._consoleEventsSocketInput(e.data);
            this._handleRoiCommand(message);

            if (!this._parentContext.paramHandler.getModelParametersInitialized()) {
                if (this._parentContext.vfCapabilities.has("Models")) {
                    this._sendCommand("getModels");
                }
            }
        };

        p.handleOpenImage = function (e, message) {
            this._parentContext._consoleEventsSocketInput(e.data);

            if (message.meta) {
                if (message.meta.error === 1) {
                    this._parentContext.onOpenImageError(message);
                }
                if (message.meta.active === 0) {
                    this._parentContext.onOpenImageSucceeded(message);
                    this._parentContext.vsEncoder.getState();
                }
            }
        };

        p._handleExecute = function (e, message) {
            this._parentContext._consoleEventsSocketInput(e.data);
            if (message && message.param) {
                if ((message.param.meta) && (message.param.meta.error === 1)) {
                    this._parentContext.onExecuteError(message);
                }
                if (message.param.outputs) {
                    this._parentContext.onExecuteSucceeded(message);
                }
            }
            this._parentContext.vsEncoder.getState();
        };

        p._handleTeachModel = function (e, message) {
            if (message.meta) {
                if (message.meta.error === 1) {
                    this._handleCommandoTeachModelError(message);
                }
            }

            if (message.param && message.param[0].model_number && message.param.length > 0) {
                this._handleCommandoTeachModelResponse(message);
            } else {
                this._parentContext._consoleEventsSocketInput(e.data);
            }
            if (this._parentContext.vfCapabilities.has("GlobalModel")) {
                this._parentContext.globalModelHandling.updateValueRangesAndVisibilityofGlobalModel();
                this._parentContext.smartPanelGlobalModelList.deSelectAll();  
                this._parentContext.smartPanelGlobalModelList.setSelectedModelLock(false); 
            }
        };

        p._handleModifyModel = function (e, message) {
            if (message.meta) {
                if (message.meta.error === 1) {
                    this._handleCommandoModifyModelError(message);
                }
            }

            if (message.param && message.param[0].model_number && message.param.length > 0) {
                this._handleCommandoModifyModelResponse(message);
            } else {
                this._parentContext._consoleEventsSocketInput(e.data);
            }
        };

        p._handleInitVisionProgram = function (message) {
            this._parentContext.onInitVisionProgramMessage(message);
        };

        p._handleGetState = function (e, message) {
            this._parentContext._consoleEventsSocketInput(e.data);
            return this._parentContext.onGetStateMessage(message);
        };

        p._handleRoiCommand = function (message) {
            var errMsg = false;
            if (message.meta) {
                if (message.meta.error === 1) {
                    this._parentContext.onROIError();
                    errMsg = true;
                }
            }            
            if(errMsg === false){
                this._parentContext.onROISucceeded(message);
            }
        };

        p._handleGetModelsMessage = function (models) {
            var modelList = [],
                context = this,
                contextSettings = this.settings,
                modelType,
                metaData;

            models.forEach(function (model) {
                if (model.model_number === 0) {
                    modelType = model.model_type;
                    context._parentContext.selectedModelType = modelType;
                    contextSettings.defaultModelParameters.set(modelType, model.model_params);
                } else if (model.model_number > 0) {
                    metaData = model.model_type;
                    modelList.push({
                        "model_number": model.model_number,
                        "model_meta": metaData,
                    });
                    context._parentContext.addNewModelToVisionFunction(model);
                }
            });
            this._parentContext.setModelRois();
            this._parentContext.smartPanelModelList.updateSmartPanelModelList(modelList);
            this._parentContext.updateModelView();
        };

        p._handleGetGlobalModelsMessage = function (models) {
            this._parentContext.updateGlobalModelList(models);
        };

        p._handleImageData = function (data) {
            if (!this.settings.offlineMode) {
                if (!this._parentContext.initialImageLoaded) {
                    this._parentContext.initialImageLoaded = true;
                    this._sendCommand("getState");
                }
            }
            this._parentContext._consoleEventsSocketInput('Image received');
            this._parentContext.smartControl.dispImage(data, 'AnyID');
            this._parentContext._dispatchImageReceivedEvent();
        };


        p._handleCommandoAckForOpenImage = function (message) {
            if (message.meta.command === 'open_image') {
                this._parentContext.acquireImage();
            }
        };

        p._handleCommandoTeachModelResponse = function (message) {
            var modelNumber,
                teachedModels = [],
                modelParams,
                modelType,
                model,
                that = this;

            this._parentContext.updateButtonStates();

            // add model to model list - select model in model list
            message.param.forEach(function (param) {
                modelNumber = param.model_number;
                modelType = param.model_type;

                teachedModels.push({
                    modelNumber: modelNumber,
                    modelType: modelType
                });

                modelParams = param.model_params;
                model = that.settings.vfModels.get(modelNumber);

                if (model) {
                    model.parameters = modelParams;
                } else {
                    that.settings.vfModels.set(modelNumber, {
                        parameters: modelParams,
                        modelType: modelType,
                        modelNumber: modelNumber
                    });
                }

                that.decodeModelRegionIconics(that._parentContext, modelNumber, param.model_region);
                that._parentContext.updateModelRoi(modelNumber);
                that._parentContext.showModelRoi(modelNumber);
            });

            that._parentContext.onTeachModel(teachedModels);
        };

        p._handleCommandoTeachModelError = function (message) {
            this._parentContext.onTeachModelError(message);
        };

        p._handleCommandoModifyModelError = function (message) {
            this._parentContext.onSubmitModelError(message);
            this._sendCommand("getState");
        };

        p._handleCommandoModifyModelResponse = function (message) {
            var modelNumber,
                modelParams,
                modelType,
                model,
                that = this;

            // add model to model list - select model in model list
            message.param.forEach(function (param) {
                modelNumber = param.model_number;
                modelParams = param.model_params;
                modelType = param.model_type;
                model = that.settings.vfModels.get(modelNumber);

                if (model && (model.modelNumber === modelNumber)) { // Model already has to exist
                    model.parameters = modelParams;
                    that.decodeModelRegionIconics(that._parentContext, modelNumber, param.model_region);
                    that._parentContext.updateModelRoi(modelNumber);
                    that._parentContext.showModelRoi(modelNumber);
                    that._parentContext.onSubmitModelResponse(modelNumber, modelType);
                }
            });
        };

        p.decodeModelRegionIconics = function (context, modelNumber, modelRegions) {
            if (modelRegions) {
                // paint/store result iconics of param.model_region
                modelRegions.forEach(function (region) {
                    region.forEach(function (iconic) {
                        context.paintTeachResults(iconic, modelNumber);
                    });
                });
            }
        };

        p._evaluateOnlineOrOfflineMode = function (message) {
            this.settings.offlineMode = message.meta.VSM.offline;
            if (this.settings.offlineMode) {
                this._parentContext._consoleEvents('Offline Mode');
            } else {
                this._parentContext._consoleEvents('Online Mode');
            }
        };

        p._sendCommand = function (command) {
            switch (command) {
                case "getState":
                    this._parentContext.vsEncoder.getState(this.settings.visionFunctionName);
                    break;
                case "getModels":
                    this._parentContext.vsEncoder.getModels(this.settings.visionFunctionName);
                    break;
                case "getGlobalModels":
                    this._parentContext.vsEncoder.getGlobalModels(this.settings.visionFunctionName);
                    break;
                case "getRoi":
                    this._parentContext.vsEncoder.getRoi(this.settings.visionFunctionName);
                    break;
            }
        };

        p.getErrorStringFromMessage = function (message) {
            var errorIdTag = "ErrorId",
                errorString = "Unknown error",
                startIndex = message.meta.errorString.search(errorIdTag);
            if (startIndex >= 0) {
                errorString = message.meta.errorString.substr(startIndex);
            } else {
                if (message.meta.errorString) {
                    errorString = message.meta.errorString;
                }
            }
            return errorString;
        };
        return VisionServerDecoder;
    });