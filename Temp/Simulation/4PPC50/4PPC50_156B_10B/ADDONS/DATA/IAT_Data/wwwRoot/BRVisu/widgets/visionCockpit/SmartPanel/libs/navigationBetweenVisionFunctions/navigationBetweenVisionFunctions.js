
/*global define*/
define([], function () {
    'use strict';

    function VisionAplicationNavigation(context) {
        this.parent = context;
        this.defineWidgetReference(); 
    }

    var p = VisionAplicationNavigation.prototype;

    p.defineWidgetReference = function () {
        this.dropDownBox = {
            visionAplication: {
                navigation: this.parent.callWidgetOnContent(this.parent.settings.refIdDropDownBoxVisionApplicationNavigation, this.parent.settings.headerContentId), 
            }
        };
    };

    p.setDataProviderNavigationVisionApplication = function () {
        this.parent.settings.visionAplicationNavigation = [{
            "value": this.parent.settings.imageAcquisitionName,
            "text": this.parent.settings.imageAcquisitionName
        }, {
            "value": this.parent.settings.visionFunctionName,
            "text": this.parent.settings.visionFunctionName
        }];
        this.dropDownBox.visionAplication.navigation.setDataProvider(this.parent.settings.visionAplicationNavigation);
    };

    p.onSelectedIndexOfVisionApplicationNavigationChanged = function (name) {
        this.parent.setSelectedVisionFunction(name);
    };

    return VisionAplicationNavigation;
});