
PROGRAM _INIT
	(* Insert code here *)
	 
END_PROGRAM

PROGRAM _CYCLIC
	(* Insert code here *)
	

	
	CASE Step OF
				
		STEP_IDLE:
			
			FileCopyCompleted := FALSE;
			WaitingToSaveFile	:= TRUE;
			IF (SaveFileToUSB_PB) THEN
				WaitingToSaveFile	:= FALSE;
			//	SaveFileToUSB_PB := 0;
				NextStep := STEP_COPY_FILE1;
				Step := STEP_USB_BROWSE0;
			END_IF
			
			
			(********************** COPY FILE TO USB**************************)
			
			//TODO: HOW TO CALL THE CORRECT FILE, SOURCE AND DESTINATION

		
		STEP_COPY_FILE1: (**** Copy file 1 ****)
			
			brsstrcpy(ADR(strSourceDev),ADR('Results'));
			brsstrcpy(ADR(strSource),ADR(gDataFileName[1]));
			brsstrcpy(ADR(strDestDev),ADR(device_name));
			brsstrcpy(ADR(strDest),ADR(gDataFileName[1]));
			
			//TODO: THIS IS COOMENTED UNTIL DEFINING SOURCE,DESTINATION AND FILE
			FileCopy_0.enable    := TRUE;
			FileCopy_0.pSrcDev   := ADR(strSourceDev);
			FileCopy_0.pSrc      := ADR(strSource);
			FileCopy_0.pDestDev  := ADR(strDestDev);
			FileCopy_0.pDest     := ADR(strDest);
			FileCopy_0.option    := fiOVERWRITE;
			(* Call FBK *)
			FileCopy_0();
			(* Get FBK output information *)
			FileCopyStatus := FileCopy_0.status;
			(* Verify status *)
			IF (FileCopyStatus = 0) THEN
				FileCopyCompleted     := TRUE;
				Step := STEP_COPY_FILE2;
			ELSE
				IF (FileCopyStatus <> 65535) THEN
					FileCopyErrorLevel := 1;
					Step := STEP_COPY_FILE_ERROR;
					IF (FileCopyStatus = 20799) THEN
						FileCopyError := FileIoGetSysError();
					END_IF
				END_IF
			END_IF
			
		STEP_COPY_FILE2: (**** Copy file 2 ****)
			
			brsstrcpy(ADR(strSourceDev),ADR('Results'));
			brsstrcpy(ADR(strSource),ADR(gDataFileName[2]));
			brsstrcpy(ADR(strDestDev),ADR(device_name));
			brsstrcpy(ADR(strDest),ADR(gDataFileName[2]));
			
			//TODO: THIS IS COOMENTED UNTIL DEFINING SOURCE,DESTINATION AND FILE
			FileCopy_0.enable    := TRUE;
			FileCopy_0.pSrcDev   := ADR(strSourceDev);
			FileCopy_0.pSrc      := ADR(strSource);
			FileCopy_0.pDestDev  := ADR(strDestDev);
			FileCopy_0.pDest     := ADR(strDest);
			FileCopy_0.option    := fiOVERWRITE;
			(* Call FBK *)
			FileCopy_0();
			(* Get FBK output information *)
			FileCopyStatus := FileCopy_0.status;
			(* Verify status *)
			IF (FileCopyStatus = 0) THEN
				FileCopyCompleted     := TRUE;
				Step := STEP_IDLE;
			ELSE
				IF (FileCopyStatus <> 65535) THEN
					FileCopyErrorLevel := 1;
					Step := STEP_COPY_FILE_ERROR;
					IF (FileCopyStatus = 20799) THEN
						FileCopyError := FileIoGetSysError();
					END_IF
				END_IF
			END_IF
			
		STEP_COPY_FILE_ERROR: (**** Error step ****)
			FileCopyCompleted := FALSE;
			Step := STEP_IDLE;	

			
			(******************USB BROWSE FUNCTIONS*************************)
		STEP_USB_BROWSE0:
			
			//Forced USB Browse required for ARemb
			USBBrowseRequired := 1;
			IF (NOT USBBrowseRequired) THEN
				Step := NextStep; // skip if not required (ARsim/ARwin)
			ELSE
				Step := STEP_USB_BROWSE1;
			END_IF
					
					
		STEP_USB_BROWSE1: // call the UsbNodeListGet function to get a list of all usb mass storage devices attached to the system
		
			UsbNodeListGet_0.enable := 1;
			UsbNodeListGet_0.pBuffer := ADR(node_id_buffer);  //pointer to list of devices buffer
			UsbNodeListGet_0.bufferSize := SIZEOF(node_id_buffer);
			UsbNodeListGet_0.filterInterfaceClass := asusb_CLASS_MASS_STORAGE;  //filter on mass storage devices is set
			UsbNodeListGet_0.filterInterfaceSubClass := 0;
					
			IF (UsbNodeListGet_0.status = ERR_OK) THEN
				UsbNodeListGet_0.enable := 0;
				node := 0;
				brsmemset(ADR(usb_data_buffer), 0, SIZEOF(usb_data_buffer));
				Step := STEP_USB_BROWSE2;	
						
			ELSIF ( (UsbNodeListGet_0.status <> ERR_FUB_BUSY) AND (UsbNodeListGet_0.status <> ERR_FUB_ENABLE_FALSE) ) THEN
				UsbNodeListGet_0.enable := 0;
				//Error accessing USB display message
	//			HMI.OEMSysSettingsStatus := 6;
	//			HMI.RecipeStatus := 21;
	//			SHOW(ADR(HMI.RecipeStatusOKSDP));
				Step := STEP_IDLE;
			END_IF
				
				
		STEP_USB_BROWSE2: // go through the list element by element to get additional device data
			UsbNodeGet_0.enable := 1;
			UsbNodeGet_0.nodeId := node_id_buffer[node];  //specific node is read out of node_id_buffer
			UsbNodeGet_0.pBuffer := ADR(usb_data_buffer[node]);  //data of specific node get stored in usb_data_buffer
			UsbNodeGet_0.bufferSize := SIZEOF(usb_data_buffer[node]); 
					
			IF (UsbNodeGet_0.status = ERR_OK) THEN  (*FUB worked correctly*)	
				node := node + 1;  (*next node to be read out of buffer*)
				IF (node = UsbNodeListGet_0.listNodes) THEN
					UsbNodeGet_0.enable := 0;
					node := 0;
					brsmemset(ADR(device_descriptor), 0, SIZEOF(device_descriptor));
					Step := STEP_USB_BROWSE3;  (*all nodes are read out of buffer*)
				ELSE
					// update node and pointers pointers
					UsbNodeGet_0.nodeId := node_id_buffer[node];  (*specific node is read out of node_id_buffer*)
					UsbNodeGet_0.pBuffer := ADR(usb_data_buffer[node]);  (*data of specific node get stored in usb_data_buffer*)
				END_IF;
		
			ELSIF ( (UsbNodeGet_0.status <> ERR_FUB_BUSY) AND (UsbNodeGet_0.status <> ERR_FUB_ENABLE_FALSE) ) THEN
				UsbNodeGet_0.enable := 0;
				//Error accessing USB display message
	//			HMI.OEMSysSettingsStatus := 6;
	//			HMI.RecipeStatus := 21;
	//			SHOW(ADR(HMI.RecipeStatusOKSDP));
				Step := STEP_IDLE;  (*error occured*)
			END_IF;
			
																	
		STEP_USB_BROWSE3:
			UsbDescriptorGet_0.enable := 1;
			UsbDescriptorGet_0.requestType := 0;  (*Request for device*)
			UsbDescriptorGet_0.descriptorType := 1;  (*Determines the device descriptor*)
			UsbDescriptorGet_0.languageId := 0;  (*for device and configuration descriptors*)
			UsbDescriptorGet_0.nodeId := node_id_buffer[node];  (*specific node is read out of node_id_buffer*) 
			UsbDescriptorGet_0.pBuffer := ADR(device_descriptor[node]);  (*descriptor-data of specific node get stored in device_descriptor-buffer*) 
			UsbDescriptorGet_0.bufferSize := SIZEOF(device_descriptor[node]);  (*size of specific node is read out device_descriptor-buffer*)
					
			IF (UsbDescriptorGet_0.status = ERR_OK) THEN  (*FUB worked correctly*)	
				node := node + 1;  (*next node to be read out of buffer*)
				IF (node = UsbNodeListGet_0.listNodes) THEN  (*last existing node is reached*)
					UsbDescriptorGet_0.enable := 0;
				
					// check for previous file device
					IF (DevLinkHandle <> 0) THEN
						Step := STEP_USB_BROWSE4; 
					ELSE
						Step := STEP_USB_BROWSE5;
					END_IF
				ELSE
					UsbDescriptorGet_0.nodeId := node_id_buffer[node];  (*specific node is read out of node_id_buffer*) 
					UsbDescriptorGet_0.pBuffer := ADR(device_descriptor[node]);  (*descriptor-data of specific node get stored in device_descriptor-buffer*) 
				END_IF;
	
			ELSIF ( (UsbDescriptorGet_0.status <> ERR_FUB_BUSY) AND (UsbDescriptorGet_0.status <> ERR_FUB_ENABLE_FALSE) ) THEN
				UsbDescriptorGet_0.enable := 0;
				//Error accessing USB display message
	//			HMI.OEMSysSettingsStatus := 6;
	//			HMI.RecipeStatus := 21;
	//			SHOW(ADR(HMI.RecipeStatusOKSDP));
				Step := STEP_IDLE;  (*error occured*)
			END_IF;
		
		
		STEP_USB_BROWSE4: // wait for unlink of old file device
					
			DevUnlink_0.enable := 1;
			DevUnlink_0.handle := DevLinkHandle;
					
			IF (DevUnlink_0.status = ERR_OK) THEN
				DevUnlink_0.enable := 0;
				DevLink_0.enable := 1;
				DevLink_0.pDevice := ADR('usbdrive');
				DevLink_0.pParam := ADR(FileDeviceParam);
				Step := STEP_USB_BROWSE5;
			ELSIF ( (DevUnlink_0.status <> ERR_FUB_BUSY) AND (DevUnlink_0.status <> ERR_FUB_ENABLE_FALSE) ) THEN
				DevUnlink_0.enable := 0;
				//Error accessing USB display message
	//			HMI.OEMSysSettingsStatus := 6;
	//			HMI.RecipeStatus := 21;
	//			SHOW(ADR(HMI.RecipeStatusOKSDP));
				Step := STEP_IDLE;  (*error occured*)
			END_IF			
				
				
		STEP_USB_BROWSE5: // link to file device
			// generate string for devlink
			// NOTE: this only links to the 1st discoved USB device!!!!!
			brsmemset(ADR(FileDeviceParam),0,SIZEOF(FileDeviceParam));		
			strcpy(ADR(device_name), ADR('usbdrive'));  (*fixed Device-Name get copied to device_name-Variable*)															
			strcpy(ADR(FileDeviceParam), ADR('/DEVICE='));  (*first part of parameter get copied to device_param-Variable*)		
			strcat(ADR(FileDeviceParam), ADR(usb_data_buffer[0].ifName));  (*second part get added to device_param-Variable*) 				
			
			DevLink_0.enable := 1;
			DevLink_0.pDevice := ADR('usbdrive');
			DevLink_0.pParam := ADR(FileDeviceParam);
					
			IF (DevLink_0.status = ERR_OK) THEN
				DevLinkHandle := DevLink_0.handle;
				DevLink_0.enable := 0;
				Step := NextStep;
						
			ELSIF ( (DevLink_0.status <> ERR_FUB_BUSY) AND (DevLink_0.status <> ERR_FUB_ENABLE_FALSE) ) THEN
				DevLink_0.enable := 0;
				DevLinkHandle := 0;
				//Error accessing USB display message
	//			HMI.OEMSysSettingsStatus := 6;
	//			HMI.RecipeStatus := 21;
	//			SHOW(ADR(HMI.RecipeStatusOKSDP));
				Step := STEP_IDLE;  (*error occured*)
			END_IF
				
			// END OF USB BROWSE FUNCTIONS
	
	END_CASE
	
	UsbNodeListGet_0();
	UsbNodeGet_0();
	UsbDescriptorGet_0();
	DevLink_0();
	DevUnlink_0();	
END_PROGRAM

PROGRAM _EXIT
	(* Insert code here *)
	 
END_PROGRAM

