// -----------------------------------------------------------------------------------------------------------
// Use this task to add your own code
// -----------------------------------------------------------------------------------------------------------	

// -----------------------------------------------------------------------------------------------------------
// Initialization
// -----------------------------------------------------------------------------------------------------------	
PROGRAM _INIT
	// -----------------------------------------------------------------------------------------------------------
	// Vision sensor configuration
	// -----------------------------------------------------------------------------------------------------------
	gVisionImage.CFG.EthDevice				:= 'IF3';			// Ethernet interface for the PLC, this is the IF where you go online
	// Configuration for Measure 1
	gVisionSensor[1].CFG.VisionFunction		:= enumMeasurement;		// Used to run specific code for this vision function
	gVisionSensor[1].CFG.PowerlinkNode		:= 1;				// Powerlink node number
	gVisionSensor[1].CFG.DataStructure		:= ADR(gMeasure1);		// Global structures that holds all data related to this function
	gVisionSensor[1].CFG.ComponentLink		:= gCameraMeasure1;		// Vision component name defined under mappVision in the configuration view
	gVisionSensor[1].CFG.ResolutionWidth_X	:= 1280;
	gVisionSensor[1].CFG.ResolutionHeight_Y	:= 1024;
	// Configuration for Measure 2
	gVisionSensor[2].CFG.VisionFunction		:= enumBlob;
	gVisionSensor[2].CFG.PowerlinkNode		:= 2;
	gVisionSensor[2].CFG.DataStructure		:= ADR(gBlob);
	gVisionSensor[2].CFG.ComponentLink		:= gCameraBlob;
	gVisionSensor[2].CFG.ResolutionWidth_X	:= 1280;
	gVisionSensor[2].CFG.ResolutionHeight_Y	:= 1024;
	//	// Configuration for match
	//	gVisionSensor[3].CFG.VisionFunction		:= enumMatch;
	//	gVisionSensor[3].CFG.PowerlinkNode		:= 3;
	//	gVisionSensor[3].CFG.DataStructure		:= ADR(gMatch);
	//	gVisionSensor[3].CFG.ComponentLink		:= gCameraMatch;
	//	gVisionSensor[3].CFG.ResolutionWidth_X	:= 1280;
	//	gVisionSensor[3].CFG.ResolutionHeight_Y	:= 1024;
	//	// Configuration for meaurement
	//	gVisionSensor[4].CFG.VisionFunction		:= enumMeasurement;
	//	gVisionSensor[4].CFG.PowerlinkNode		:= 4;
	//	gVisionSensor[4].CFG.DataStructure		:= ADR(gMT);
	//	gVisionSensor[4].CFG.ComponentLink		:= gCameraMeasurement;
	//	gVisionSensor[4].CFG.ResolutionWidth_X	:= 1280;
	//	gVisionSensor[4].CFG.ResolutionHeight_Y	:= 1024;
	//	// Configuration for OCR
	//	gVisionSensor[5].CFG.VisionFunction		:= enumOCR;
	//	gVisionSensor[5].CFG.PowerlinkNode		:= 5;
	//	gVisionSensor[5].CFG.DataStructure		:= ADR(gOCR);
	//	gVisionSensor[5].CFG.ComponentLink		:= gCameraOCR;
	//	gVisionSensor[5].CFG.ResolutionWidth_X	:= 1280;
	//	gVisionSensor[5].CFG.ResolutionHeight_Y	:= 1024;
	//	// Configuration for PixelCounter
	//	gVisionSensor[6].CFG.VisionFunction		:= enumPixelCounter;
	//	gVisionSensor[6].CFG.PowerlinkNode		:= 6;
	//	gVisionSensor[6].CFG.DataStructure		:= ADR(gPixel);
	//	gVisionSensor[6].CFG.ComponentLink		:= gCameraPxl;
	//	gVisionSensor[6].CFG.ResolutionWidth_X	:= 1280;
	//	gVisionSensor[6].CFG.ResolutionHeight_Y	:= 1024;

	// -----------------------------------------------------------------------------------------------------------
	// Lights configuration
	// -----------------------------------------------------------------------------------------------------------	
	// Configuration for backlights
	gVisionLight[1].CFG.LightType		:= enumBacklight;
	gVisionLight[1].CFG.PowerlinkNode	:= 10;
	gVisionLight[1].DAT.FlashColor		:= 1;
	gVisionLight[2].CFG.LightType		:= enumBacklight;
	gVisionLight[2].CFG.PowerlinkNode	:= 11;
	gVisionLight[2].DAT.FlashColor		:= 1;
	// Configuration for lightbars
	gVisionLight[3].CFG.LightType		:= enumLightbar;
	gVisionLight[3].CFG.PowerlinkNode	:= 20;
	gVisionLight[3].DAT.FlashColor		:= 1;
	
	cmdLoadRecipeAfterBoot	:= TRUE;
	
	ReferenceCalc1Set	:= FALSE;
	ReferenceCalc2Set	:= FALSE;
	
END_PROGRAM


// -----------------------------------------------------------------------------------------------------------
// Cyclic part
// -----------------------------------------------------------------------------------------------------------	
PROGRAM _CYCLIC
	
	//Enabling Optics and Cameras when ready
	IF gVisionSensor[1].HW.Ready AND gVisionSensor[1].HW.Connected AND gVisionSensor[2].HW.Ready AND gVisionSensor[2].HW.Connected THEN
		AutoLoad := TRUE;
		gVisionSensor[SENSOR_1_IND].DAT.Enable	:= gVisionSensor[SENSOR_2_IND].DAT.Enable	:= TRUE;
	END_IF
	IF EDGEPOS(AutoLoad) THEN
		gVisionSensor[SENSOR_1_IND].CMD.ReadCameraInfo := TRUE;
	END_IF
	IF EDGENEG(gVisionSensor[SENSOR_1_IND].CMD.ReadCameraInfo) THEN
		gVisionSensor[SENSOR_2_IND].CMD.ReadCameraInfo := TRUE;
	END_IF
	
		
	// -----------------------------------------------------------------------------------------------------------	
	// Trigger selected backlight when selected sensor is triggered
	IF(EDGEPOS(gVisionSensor[visSelectedSensor].CMD.ImageTrigger)) THEN
		gVisionLight[visSelectedLight].DAT.Exposure		:= gVisionSensor[visSelectedSensor].DAT.Exposure;
		gVisionLight[visSelectedLight].DAT.NettimeDelay	:= gVisionSensor[visSelectedSensor].DAT.NettimeDelay;
		gVisionLight[visSelectedLight].CMD.FlashTrigger	:= TRUE;
	END_IF
	
	// -----------------------------------------------------------------------------------------------------------	
	// Transfer data when loading a recipe
	IF EDGENEG(Recipe_01.CMD.Load) OR cmdLoadRecipeAfterBoot THEN
		cmdLoadRecipeAfterBoot := FALSE;
		FOR i := 1 TO MAX_NUM_CAMS DO
			gVisionSensor[i].FCT.ApplicationName	:= RecipeData.Cam[i].ApplicationName;
			gVisionSensor[i].DAT.Exposure			:= RecipeData.Cam[i].Exposure;
			gVisionSensor[i].DAT.FlashColor			:= RecipeData.Cam[i].FlashColor;
			gVisionSensor[i].DAT.FlashSegment		:= RecipeData.Cam[i].FlashSegment;
			gVisionSensor[i].DAT.Focus				:= RecipeData.Cam[i].Focus;
			gVisionSensor[i].DAT.Gain				:= RecipeData.Cam[i].Gain;
			gVisionSensor[i].DAT.MaxItemCnt			:= RecipeData.Cam[i].MaxItemCnt;
			gVisionSensor[i].DAT.Timeout			:= RecipeData.Cam[i].Timeout;
		END_FOR
		FOR i := 1 TO MAX_NUM_LIGHTS DO
			gVisionLight[i].DAT.Exposure			:= RecipeData.Light[i].Exposure;
			gVisionLight[i].DAT.FlashColor			:= RecipeData.Light[i].FlashColor;
			gVisionLight[i].DAT.FlashSegment		:= RecipeData.Light[i].FlashSegment;
			gVisionLight[i].DAT.SetAngle			:= RecipeData.Light[i].SetAngle;
			gVisionLight[i].DAT.Timeout				:= RecipeData.Light[i].Timeout;
		END_FOR
		// Switch vision application when recipe for blob was loaded
		gVisionSensor[1].CMD.VaSwitchApplication := TRUE;
	END_IF
	
	// -----------------------------------------------------------------------------------------------------------	
	// Transfer data cyclic to the remanent variable
	IF NOT Recipe_01.CMD.Load THEN
		FOR i := 1 TO MAX_NUM_CAMS DO
			RecipeData.Cam[i].ApplicationName	:= gVisionSensor[i].FCT.ApplicationName;
			RecipeData.Cam[i].Exposure			:= gVisionSensor[i].DAT.Exposure;
			RecipeData.Cam[i].FlashColor		:= gVisionSensor[i].DAT.FlashColor;
			RecipeData.Cam[i].FlashSegment		:= gVisionSensor[i].DAT.FlashSegment;
			RecipeData.Cam[i].Focus				:= gVisionSensor[i].DAT.Focus;
			RecipeData.Cam[i].Gain				:= gVisionSensor[i].DAT.Gain;
			RecipeData.Cam[i].MaxItemCnt		:= gVisionSensor[i].DAT.MaxItemCnt;
			RecipeData.Cam[i].Timeout			:= gVisionSensor[i].DAT.Timeout;
		END_FOR
		FOR i := 1 TO MAX_NUM_LIGHTS DO
			RecipeData.Light[i].Exposure		:= gVisionLight[i].DAT.Exposure;
			RecipeData.Light[i].FlashColor		:= gVisionLight[i].DAT.FlashColor;
			RecipeData.Light[i].FlashSegment	:= gVisionLight[i].DAT.FlashSegment;
			RecipeData.Light[i].SetAngle		:= gVisionLight[i].DAT.SetAngle;
			RecipeData.Light[i].Timeout			:= gVisionLight[i].DAT.Timeout;
		END_FOR
	END_IF
	IF visSaveRecipe OR visNewRecipe THEN
		IF(visNewRecipe) THEN
			Recipe_01.CMD.New := TRUE;
		ELSE
			Recipe_01.CMD.Save := TRUE;
		END_IF
		visNewRecipe := FALSE;
		visSaveRecipe := FALSE;
	END_IF
	
	// Calculate the distance measured based on resolution for each camera
	IF gVisionSensor[1].HW.Ready AND gVisionSensor[1].HW.Connected THEN
		ResultData[1].Measure	:= gVisionSensor[1].OPT.Resolution_mmPerPx * gMeasure1.Result[1]/1000;
	END_IF
	
	IF gVisionSensor[2].HW.Ready AND gVisionSensor[2].HW.Connected THEN
//		ResultData[2].Measure	:= gVisionSensor[2].OPT.Resolution_mmPerPx * (gBlob.Xmax[1] - gBlob.Xmin[1]);
		ResultData[2].Measure	:= (gVisionSensor[2].OPT.Resolution_mmPerPx * (gBlob.Ymax[1] - gBlob.Ymin[1]))/100;
	END_IF

	(*******************USING THESE VALUES TO CALCULATE LIN MOT OFFSET*********************)
	
	IF ReferenceValueSetPB[1] THEN
		ReferenceCalc1Set	:= TRUE;
		ReferenceData[1]:= ResultData[1].Measure;
	END_IF
	
	IF ReferenceValueSetPB[2] THEN
		ReferenceCalc2Set	:= TRUE;
		ReferenceData[2]:= ResultData[2].Measure;
	END_IF
	
	IF ReferenceCalc1Set AND ReferenceCalc2Set THEN
		HeigthAdjust[1]	:= (ResultData[1].Measure - ReferenceData[1]);
		HeigthAdjust[2]	:= (ResultData[2].Measure - ReferenceData[2]);
	END_IF
	
	HeigthAdjustTot	:= (HeigthAdjust[1] - HeigthAdjust[2]);
	
	(***********************Recording Data to CSV files using MpDataRecorder********************)
	
	//Recording Data for Puck Measurement
	MpDataRecorder_01[1].MpLink := ADR(gDataRecorder_Puck);
	MpDataRecorder_01[1].Enable := TRUE;
	MpDataRecorder_01[1].DeviceName := ADR('Results');
	MpDataRecorder_01[1].RecordMode :=  mpDATA_RECORD_MODE_TRIGGER;

	IF MpDataRecorder_01[1].Active THEN
		MpDataRecorder_01[1].Record := TRUE;
	END_IF
		//Entering PV
	MpDataRegPar_01[1].MpLink	:= ADR(gDataRecorder_Puck);
	MpDataRegPar_01[1].Enable := TRUE;
	MpDataRegPar_01[1].PVName :=  ADR('YourTask:ResultData[1].Measure');
	MpDataRegPar_01[1].Unit := ADR('mm');
	MpDataRegPar_01[1].Description := ADR('Puck Measure');
	
	MpDataRecorder_01[1].Trigger :=  EDGENEG(gVisionSensor[1].DAT.ImageProcessingActive);
	brsstrcpy(ADR(gDataFileName[1]),ADR(MpDataRecorder_01[1].Info.CurrentFileName));
	//Call FB's
	MpDataRecorder_01[1]();
	MpDataRegPar_01[1]();
	
	//Recording Data for Label  Measurement
	MpDataRecorder_01[2].MpLink := ADR(gDataRecorder_Label);
	MpDataRecorder_01[2].Enable := TRUE;
	MpDataRecorder_01[2].DeviceName := ADR('Results');
	MpDataRecorder_01[2].RecordMode := mpDATA_RECORD_MODE_TRIGGER;
	IF MpDataRecorder_01[2].Active THEN
		MpDataRecorder_01[2].Record := TRUE;
	END_IF
	
	//Entering PV
	MpDataRegPar_01[2].MpLink	:= ADR(gDataRecorder_Label);
	MpDataRegPar_01[2].Enable := TRUE;
	MpDataRegPar_01[2].PVName :=  ADR('YourTask:ResultData[2].Measure');
	MpDataRegPar_01[2].Unit := ADR('mm');
	MpDataRegPar_01[2].Description := ADR('Label Measure');
		
	MpDataRecorder_01[2].Trigger :=  EDGENEG(gVisionSensor[2].DAT.ImageProcessingActive);
	brsstrcpy(ADR(gDataFileName[2]),ADR(MpDataRecorder_01[2].Info.CurrentFileName));
	
	//Call FB's
	MpDataRecorder_01[2]();
	MpDataRegPar_01[2]();
	
	(***************************END OF DATA RECORDING********************************)
		
END_PROGRAM